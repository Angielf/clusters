<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKppToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('kpp')->nullable();
            $table->integer('ogrn')->nullable();
            $table->integer('bik')->nullable();
            $table->bigInteger('single_treasury_account')->nullable();
            $table->bigInteger('treasury_account')->nullable();
            $table->string('bank')->nullable();

            $table->string('director')->nullable();
            $table->string('fullname_full')->nullable();
            $table->string('number_license')->nullable();
            $table->string('date_license')->nullable();
            $table->string('who_license')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
