function org() {
    var input, filter, tables, tr, td, i, j;
    input = document.getElementById("org");
    filter = input.value.toUpperCase();
    tables = document.querySelectorAll('.table.table-striped.table-bordered');

    tables.forEach(function(table) {
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    });
}
