function search1() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search1");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL1");
    ul2 = document.getElementById("myUL2");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
            ul.style.display = "";
            ul2.style.display = "none";
        } else {
            li[i].style.display = "none";
            ul.style.display = "";
            ul2.style.display = "none";
        }
    }
}

function search2() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("search2");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL2");
    ul1 = document.getElementById("myUL1");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
            ul.style.display = "";
            ul1.style.display = "none";
        } else {
            li[i].style.display = "none";
            ul.style.display = "";
            ul1.style.display = "none";
        }
    }
}