// let crypto = new CryptoHelper();
let c = new CryptoHelper();
let pluginReady = false;
let certs = new Set();
let selectedCert = null;

const p_element = document.getElementById('plugin-work');
const file_id = document.getElementById('agreement-id').textContent;
const file_name = document.getElementById('agreement-filename').textContent;
const user_id = document.getElementById('user-id').textContent;
const a = document.getElementById('a').textContent;
const fileUrl = './files/agreements_pupils/' + file_name;

const grade_number = document.getElementById('grade-number').textContent;
const grade_letter = document.getElementById('grade-letter').textContent;

p_element.textContent = 'Плагин не готов';

c.init()
.then(() => {
    pluginReady = true;
    if(pluginReady) {
        p_element.textContent = 'Плагин готов';
    }else{
        p_element.textContent = 'Плагин не готов';
    }
    
    c.getCertificates()
    .then(certificates => {
        certs = certificates;

        const selectElement = document.getElementById('cert-select'+file_id);

        certs.forEach(cert => {
            const option = document.createElement('option');
            option.value = cert.subject.name;
            option.text = cert.subject.name;
            option.dataset.object = JSON.stringify(cert);

            selectElement.appendChild(option);
        });

        // let selectedIndex = selectElement.selectedIndex;
        // let selectedOption = selectElement.options[selectedIndex];
        // let selectedValue = selectedOption.value;
        // selectedCert = JSON.parse(selectedOption.dataset.object);

        // let cert_info_post = null;
        // cert_info_post = document.querySelector('#cert-info-post'+file_id);
        // let existing_text_cert_info_post = cert_info_post.textContent;
        // cert_info_post.textContent = existing_text_cert_info_post + selectedCert.subject.post;

        // let cert_info_company = document.querySelector('#cert-info-company'+file_id);
        // let existing_text_cert_info_company = cert_info_company.textContent;
        // cert_info_company.textContent = existing_text_cert_info_company + selectedCert.subject.company;

        // let cert_info_email = document.querySelector('#cert-info-email'+file_id);
        // let existing_text_cert_info_email = cert_info_email.textContent;
        // cert_info_email.textContent = existing_text_cert_info_email + selectedCert.subject.email;

        // let cert_info_validFrom = document.querySelector('#cert-info-validFrom'+file_id);
        // let existing_text_cert_info_validFrom = cert_info_validFrom.textContent;
        // let validFromDate = new Date(selectedCert.validFrom);
        // let formattedFromDate = validFromDate.toLocaleDateString('ru-RU', { day: '2-digit', month: '2-digit', year: 'numeric' });
        // cert_info_validFrom.textContent = existing_text_cert_info_validFrom + formattedFromDate;

        // let cert_info_validTo = document.querySelector('#cert-info-validTo'+file_id);
        // let existing_text_cert_info_validTo = cert_info_validTo.textContent;
        // let validToDate = new Date(selectedCert.validTo);
        // let formattedToDate = validToDate.toLocaleDateString('ru-RU', { day: '2-digit', month: '2-digit', year: 'numeric' });
        // cert_info_validTo.textContent = existing_text_cert_info_validTo + formattedToDate;

        selectElement.addEventListener('change', function() {
            let selectedIndex = selectElement.selectedIndex;
            let selectedOption = selectElement.options[selectedIndex];
            let selectedValue = selectedOption.value;
            selectedCert = JSON.parse(selectedOption.dataset.object);
        
            let cert_info_post = document.querySelector('#cert-info-post'+file_id);
            cert_info_post.textContent = 'Должность: ' + selectedCert.subject.post;
        
            let cert_info_company = document.querySelector('#cert-info-company'+file_id);
            cert_info_company.textContent = 'Организация: ' + selectedCert.subject.company;
        
            let cert_info_email = document.querySelector('#cert-info-email'+file_id);
            cert_info_email.textContent = 'email, ИНН, СНИЛС: ' + selectedCert.subject.email;
        
            let cert_info_validFrom = document.querySelector('#cert-info-validFrom'+file_id);
            let validFromDate = new Date(selectedCert.validFrom);
            let formattedFromDate = validFromDate.toLocaleDateString('ru-RU', { day: '2-digit', month: '2-digit', year: 'numeric' });
            cert_info_validFrom.textContent = 'С: ' + formattedFromDate;
        
            let cert_info_validTo = document.querySelector('#cert-info-validTo'+file_id);
            let validToDate = new Date(selectedCert.validTo);
            let formattedToDate = validToDate.toLocaleDateString('ru-RU', { day: '2-digit', month: '2-digit', year: 'numeric' });
            cert_info_validTo.textContent = 'По: ' + formattedToDate;
        });

        const sign_button = document.querySelector('#sign-button');
        let general_path = document.getElementById('a').textContent;
        
        const filePath = '/../../files/' + general_path + '/' + file_name;
        fetch(filePath)
        .then(response => response.blob())
        .then(blob => {
            const file = new File([blob], file_name);
            // Теперь у вас есть объект File, который представляет файл
            sign_button.addEventListener('click', () => {
                doSign(file, selectedCert, file_name);
            })
        })
        .catch(error => {
            console.error('Ошибка при загрузке файла:', error);
        });

    })
    .catch(error => {
        console.error(error);
    });
    

    // return c.signFile(fileUrl, false);
})
.catch(() => {
    // пользователь отклонил запрос
});




async function getCertificates(c) {
    await c.getCertificates().then((certs) => {
        return certs
    });
}

function doSign(data, selectedCert, file_name) {
    c.sign(selectedCert.$original, data)
        .then((signMessage) => {
            sign = signMessage;

            var signedFile = new Blob([sign], { type: 'text/plain' });

            var formData = new FormData();
            formData.append('signed_file', signedFile);
            formData.append('file_name', file_name);

            $.ajax({
                url: '/save-signed-file/' + a + '/' + user_id + '/' + file_id,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    console.log('Файл подписан');
                    // Handle the success response
                    window.location.href = '/pupils/' + user_id + '/' + grade_number.toLowerCase() + '/' + grade_letter + '/list-of-pupils';
                },
                error: function(xhr, status, error) {
                    console.error(error);
                    // Handle the error response
                }
            });
        });
};

