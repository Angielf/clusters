<?php
namespace App\Pupils;

use App\SelectedProgram;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SelectedProgramsNotArchivePupilsExport implements FromView
{
    public function view(): View
    {
        return view('pupils.selected_programs_pupils_download', [
            'selected_programs' => SelectedProgram::whereHas('proposed_program', function($query) {
                $query->where('archive', NULL);
            })->get()
        ]);
    }
}