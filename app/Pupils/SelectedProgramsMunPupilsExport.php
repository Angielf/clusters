<?php
namespace App\Pupils;

use App\SelectedProgram;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SelectedProgramsMunPupilsExport implements FromView
{
    public function view(): View
    {
        return view('pupils.selected_programs_mun_pupils_download', [
            'selected_programs' => SelectedProgram::all()
        ]);
    }
}