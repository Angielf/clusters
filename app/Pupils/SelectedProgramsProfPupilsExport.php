<?php
namespace App\Pupils;

use App\SelectedProgram;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SelectedProgramsProfPupilsExport implements FromView
{
    public function view(): View
    {
        return view('pupils.selected_programs_prof_pupils_download', [
            'selected_programs' => SelectedProgram::all()
        ]);
    }
}