<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposed extends Model
{
    protected $table = 'proposed_programs';

    protected $fillable = ['subject', 'class', 'content', 'user_id', 'modul', 'hours', 'educational_program',
        'educational_activity', 'form_of_education', 'form_education_implementation', 'date_begin', 'date_end',
         'filename', 'archive', 'cancel_registration'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getStatus()
    {
        switch ($this->status) {
            case 0: return '<div class="alert alert-warning" role="alert">Рассмотрение</div>';
            case 1: return '<div class="alert alert-success" role="alert">Одобрена</div>';
            case 2: return '<div class="alert alert-danger" role="alert">Отклонена</div>';
            case 3: return '<div class="alert alert-info" role="alert">Своя программа</div>';

            case 9: return '<div class="alert alert-info" role="alert">Поступило предложение</div>';
        }
    }

    public function getClasses()
    {
        $classes = unserialize($this->class);
        $str = "";
        foreach ($classes as $class) {
            $str .= $class . ", ";
        }

        return substr($str, 0, -2);
    }


    public function getDataBegin()
    {
        $date = $this->date_begin;
        if($date !== NULL) {
            $year = substr($date, 0, -6);
            $month = substr($date, 5, -3);
            $day = substr($date, -2);
            $date_begin = $day . "-" . $month . "-" . $year;
            return $date_begin;
        }
        else return '';
    }

    public function getDataEnd()
    {
        $date = $this->date_end;
        if($date !== NULL) {
            $year = substr($date, 0, -6);
            $month = substr($date, 5, -3);
            $day = substr($date, -2);
            $date_end = $day . "-" . $month . "-" . $year;
            return $date_end;
        }
        else return '';
    }

    public function selected_programs()
    {
        if ($selected_programs = SelectedProgram::where('proposed_program_id', $this->id)->get()) {
            return $selected_programs;
        } else return false;

    }

    public function selected_programs_main_1()
    {
        if ($selected_programs = SelectedProgram::where('proposed_program_id', $this->id)
            ->whereHas('proposed_program', function ($query) {
                $query->where('educational_program', 'основная');
            })
            ->where('status', 1)->where('not_student', NULL)->get()) {
            return $selected_programs;
        } else return false;
    }

    public function selected_program()
    {
        return $this->hasOne(SelectedProgram::class, 'proposed_program_id');
    }

    public function selected_ids(){
        if ($selected_programs = SelectedProgram::where('proposed_program_id', $this->id)->get()) {
            $subset = $selected_programs->map(function ($selected_programs) {
                return collect($selected_programs->toArray())
                    ->only(['id'])
                    ->all();
            });
            return $subset;
        } else return false;
    }


    public function getDataBeginDots()
    {
        $date = $this->date_begin;
        if($date !== NULL) {
            $year = substr($date, 0, -6);
            $month = substr($date, 5, -3);
            $day = substr($date, -2);
            $date_begin = $day . "." . $month . "." . $year;
            return $date_begin;
        }
        else return '';
    }

    public function getDataEndDots()
    {
        $date = $this->date_end;
        if($date !== NULL) {
            $year = substr($date, 0, -6);
            $month = substr($date, 5, -3);
            $day = substr($date, -2);
            $date_end = $day . "." . $month . "." . $year;
            return $date_end;
        }
        else return '';
    }

    function getAcademicYear($startDate) {
        $month = date('m', strtotime($startDate));
        $year = date('Y', strtotime($startDate));
    
        if ($month >= 9) {
            return $year . '-' . ($year + 1);
        } else {
            return ($year - 1) . '-' . $year;
        }
    }

    function getAmountOfStudentsMain() {
        if ($selected_programs = SelectedProgram::where('proposed_program_id', $this->id)
            ->where('status', 1)
            ->where('not_student', NULL)
            ->whereHas('proposed_program', function ($query) {
                $query->where('educational_program', 'основная');
            })
            ->get()) {
            return $selected_programs->count();
        } else return 0;
    }

    function getAmountOfStudentsMainSchool($school_id) {
        if ($selected_programs = SelectedProgram::where('proposed_program_id', $this->id)
            ->where('status', 1)
            ->where('not_student', NULL)
            ->whereHas('proposed_program', function ($query) {
                $query->where('educational_program', 'основная');
            })
            ->whereHas('user', function ($query) use ($school_id) {
                $query->where('org', $school_id);
            })
            ->get()) {
            return $selected_programs->count();
        } else return 0;
    }


    function getSelectedSchools() {
        $selected_programs = $this->selected_programs_main_1();

        $schools = $selected_programs->map(function ($selected_program) {
            return $selected_program->user->getSchool;
        })->unique();

        return $schools;
    }
}
