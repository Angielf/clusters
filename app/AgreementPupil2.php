<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementPupil2 extends Model
{
    protected $table = 'agreements_pupils2';

    protected $fillable = ['selected_schedule_id', 'filename', 'sign', 'filename_sign', 'agree'];

}