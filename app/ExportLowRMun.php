<?php
namespace App;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;

class ExportLowRMun implements FromView
{
    public function view(): View
    {
        return view('clusters.export_low_r_f', [
            $district = Auth::user()->getDistrict->id,
            'users' => User::where('low', 'r')->where('district', $district)->get()
        ]);
    }
}
