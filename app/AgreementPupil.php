<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementPupil extends Model
{
    protected $table = 'agreements_pupils';

    protected $fillable = ['selected_schedule_id', 'filename', 'sign', 'filename_sign', 'agree'];

}