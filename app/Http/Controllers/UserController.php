<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Proposed;
use App\District;
use App\SelectedProgram;
use App\Bid;
use App\Program;

class UserController extends Controller
{
    private const BASE_SCHOOL = 3;
    private const RECIPIENT_SCHOOL = 0;

    public function approve($id)
    {
        $user = User::where('id', $id)->first();
        $user->status = self::BASE_SCHOOL;

        $user->save();

        return redirect('/');
    }

    public function reject($id)
    {
        $user = User::where('id', $id)->first();
        $user->status = self::RECIPIENT_SCHOOL;

        $user->save();

        return redirect('/');
    }

    public function show_users($orgtype)
    {
        if($orgtype == 'all'){
            $users = User::all()
            ->where('name', '!=', 'admin')
            ->where('name', '!=', 'admin2')
            ->where('fullname', '!=', 'Муниципальный координатор')
            ->where('status_exist', '!=', 'LIQUIDATED')
            ->where('status', '!=', '7');
        }else{
            $users = User::all()
            ->where('name', '!=', 'admin')
            ->where('name', '!=', 'admin2')
            ->where('fullname', '!=', 'Муниципальный координатор')
            ->where('status_exist', '!=', 'LIQUIDATED')
            ->where('status', '!=', '7')
            ->where('org_type', $orgtype);
        }
        return view('clusters.organizations_list_reg', compact('users'));
    }

    public function show_user(User $user_org)
    {
        return view('clusters.org_inf', compact('user_org'));
    }

    public function update_inn(Request $request, User $user_org)
    {
        $inn = $request->input('inn');
        $user_org->update(['inn' => $inn]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_add(Request $request, User $user_org)
    {
        $add = $request->input('add');
        $user_org->update(['address' => $add]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_tel(Request $request, User $user_org)
    {
        $tel = $request->input('tel');
        $user_org->update(['tel' => $tel]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_email_real(Request $request, User $user_org)
    {
        $email_real = $request->input('email_real');
        $user_org->update(['email_real' => $email_real]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_web(Request $request, User $user_org)
    {
        $web = $request->input('website');
        $user_org->update(['website' => $web]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_shnor(Request $request, User $user_org)
    {
        $shnor = $request->input('shnor');
        $user_org->update(['low' => $shnor]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_type(Request $request, User $user_org)
    {
        $type = $request->input('org_type');
        $user_org->update(['org_type' => $type]);

        return view('clusters.org_inf', compact('user_org'));
    }

    public function update_zone(Request $request, User $user_org)
    {
        $zone= $request->input('zone');
        $user_org->update(['zone' => $zone]);

        return view('clusters.org_inf', compact('user_org'));
    }

    public function update_kpp(Request $request, User $user_org)
    {
        $kpp = $request->input('kpp');
        $user_org->update(['kpp' => $kpp]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_ogrn(Request $request, User $user_org)
    {
        $ogrn = $request->input('ogrn');
        $user_org->update(['ogrn' => $ogrn]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_bik(Request $request, User $user_org)
    {
        $bik = $request->input('bik');
        $user_org->update(['bik' => $bik]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_single_treasury_account(Request $request, User $user_org)
    {
        $single_treasury_account = $request->input('single_treasury_account');
        $user_org->update(['single_treasury_account' => $single_treasury_account]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_treasury_account(Request $request, User $user_org)
    {
        $treasury_account = $request->input('treasury_account');
        $user_org->update(['treasury_account' => $treasury_account]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_single_treasury_account_type(Request $request, User $user_org)
    {
        $single_treasury_account_type = $request->input('single_treasury_account_type');
        $user_org->update(['single_treasury_account_type' => $single_treasury_account_type]);

        return view('clusters.org_inf', compact('user_org'));
    }

    public function update_treasury_account_type(Request $request, User $user_org)
    {
        $treasury_account_type = $request->input('treasury_account_type');
        $user_org->update(['treasury_account_type' => $treasury_account_type]);

        return view('clusters.org_inf', compact('user_org'));
    }

    public function update_bank(Request $request, User $user_org)
    {
        $bank = $request->input('bank');
        $user_org->update(['bank' => $bank]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_fullname_full(Request $request, User $user_org)
    {
        $fullname_full = $request->input('fullname_full');
        $user_org->update(['fullname_full' => $fullname_full]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_fullname(Request $request, User $user_org)
    {
        $fullname_full = $request->input('fullname');
        $user_org->update(['fullname' => $fullname_full]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_date_license(Request $request, User $user_org)
    {
        $date_license = $request->input('date_license');
        $user_org->update(['date_license' => $date_license]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_number_license(Request $request, User $user_org)
    {
        $number_license = $request->input('number_license');
        $user_org->update(['number_license' => $number_license]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_who_license(Request $request, User $user_org)
    {
        $who_license = $request->input('who_license');
        $user_org->update(['who_license' => $who_license]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_director_post(Request $request, User $user_org)
    {
        $director_post = $request->input('director_post');
        $user_org->update(['director_post' => $director_post]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_director_post_rod(Request $request, User $user_org)
    {
        $director_post_rod = $request->input('director_post_rod');
        $user_org->update(['director_post_rod' => $director_post_rod]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_director(Request $request, User $user_org)
    {
        $director = $request->input('director');
        $user_org->update(['director' => $director]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_director_rod(Request $request, User $user_org)
    {
        $director_rod = $request->input('director_rod');
        $user_org->update(['director_rod' => $director_rod]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_opf_short(Request $request, User $user_org)
    {
        $opf_short = $request->input('opf_short');
        $user_org->update(['opf_short' => $opf_short]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_okpo(Request $request, User $user_org)
    {
        $okpo = $request->input('okpo');
        $user_org->update(['okpo' => $okpo]);

        return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
    }

    public function update_kbk(Request $request, User $user_org)
    {
        $kbk = $request->input('kbk');
        $user_org->update(['kbk' => $kbk]);

        return view('clusters.org_inf', compact('user_org'));
    }

    public function update_ikz(Request $request, User $user_org)
    {
        $ikz = $request->input('ikz');
        $user_org->update(['ikz' => $ikz]);

        return view('clusters.org_inf', compact('user_org'));
    }

    public function update_punkt(Request $request, User $user_org)
    {
        $punkt = $request->input('punkt');
        $user_org->update(['punkt' => $punkt]);

        return view('clusters.org_inf', compact('user_org'));
    }

    public function update_oktmo(Request $request, User $user_org)
    {
        $oktmo = $request->input('oktmo');
        $user_org->update(['oktmo' => $oktmo]);

        // return view('clusters.org_inf', compact('user_org'));
        // return redirect()->back();
        return redirect()->route('home');
    }

    public function update_grade_letter(Request $request, User $user_org)
    {
        $grade_letter = $request->input('grade_letter');
        $user_org->update(['grade_letter' => $grade_letter]);

        // return view('pupils.pupil_inf', compact('user_org'));
        // return redirect()->back();
        return redirect()->route('home');
    }

    public function update_grade_number(Request $request, User $user_org)
    {
        $grade_number = $request->input('grade_number');
        $user_org->update(['grade_number' => $grade_number]);

        // return view('pupils.pupil_inf', compact('user_org'));
        // return redirect()->back();
        return redirect()->route('home');
    }

    public function update_fullname_pupil(Request $request, User $user_org)
    {
        $fullname_full = $request->input('fullname');
        $user_org->update(['fullname' => $fullname_full]);

        // return view('pupils.pupil_inf', compact('user_org'));
        // return redirect()->back();
        return redirect()->route('home');
    }

    public function update_tel_pupil(Request $request, User $user_org)
    {
        $tel = $request->input('tel');
        $user_org->update(['tel' => $tel]);

        // return view('pupils.pupil_inf', compact('user_org'));
        // return redirect()->back();
        return redirect()->route('home');
    }

    public function update_email_pupil(Request $request, User $user_org)
    {
        $email = $request->input('email');
        $user_org->update(['email' => $email]);
        $user_org->update(['email_real' => $email]);

        // return view('pupils.pupil_inf', compact('user_org'));
        // return redirect()->back();
        return redirect()->route('home');
    }

    public function update_snils_pupil(Request $request, User $user_org)
    {
        $snils = $request->input('snils');
        $user_org->update(['snils' => $snils]);

        return redirect()->route('home');
    }

    public function update_birth_pupil(Request $request, User $user_org)
    {
        $birth = $request->input('birth');
        $user_org->update(['birth' => $birth]);

        return redirect()->route('home');
    }

    public function update_district_pupil(Request $request, User $user_org)
    {
        $district = $request->input('district');
        $org = $request->input('org');
        $user_org->update(['district' => $district]);
        $user_org->update(['org' => $org]);

        // return view('pupils.pupil_inf', compact('user_org'));
        // return redirect()->back();
        return redirect()->route('home');
    }

    public function selectOrg(Request $request){
        if($request->ajax()){
            $orgs = User::
            where('district', $request->district)
            ->where('name', 'like', '%sch%')
            // ->where('id', '<=', '1845')
            ->where('inn' , '!=', '')
            ->where('status', '0')
            ->where('status_exist', '!=', 'LIQUIDATED')->get()
            ->pluck("fullname", "id");
            $data = view('auth/selectorg',['orgs' => $orgs])->render();
            return response()->json(['options'=>$data]);
        }
    }


    public function show_users_mun($id, $orgtype)
    {
        if($orgtype == 'all'){
            $users = User::where('name', '!=', 'admin')
                ->where('name', '!=', 'admin2')
                ->where('fullname', '!=', 'Муниципальный координатор')
                ->where('status_exist', '!=', 'LIQUIDATED')
                ->where('status', '!=', '7')
                ->where('district', $id)
                ->get();
        }else{
            $users = User::where('name', '!=', 'admin')
                ->where('name', '!=', 'admin2')
                ->where('fullname', '!=', 'Муниципальный координатор')
                ->where('status_exist', '!=', 'LIQUIDATED')
                ->where('status', '!=', '7')
                ->where('district', $id)
                ->where('org_type', $orgtype)
                ->get();
        }
        return view('org_list_mun', compact('users', 'id'));
    }

    public function show_org(User $org)
    {
        return view('about_org', compact('org'));
    }

    public function archive_list(User $user_org)
    {
        return view('archives.archive_list', compact('user_org'));
    }

    public function archive_bids(User $user_org, $year)
    {
        return view('archives.archive_bids', compact(['user_org', 'year']));
    }

    public function archive_list_programs(User $user_org)
    {
        return view('archives.archive_list_programs', compact('user_org'));
    }

    public function archive_list_proposed_programs(User $user_org)
    {
        return view('archives.archive_list_proposed_programs', compact('user_org'));
    }

    public function archive_list_selected_programs(User $user_org)
    {
        return view('archives.archive_list_selected_programs', compact('user_org'));
    }

    public function archive_programs(User $user_org, $year)
    {
        return view('archives.archive_programs', compact(['user_org', 'year']));
    }

    public function archive_proposed_programs(User $user_org, $year)
    {
        return view('archives.archive_proposed_programs', compact(['user_org', 'year']));
    }

    public function archive_selected_programs(User $user_org, $year)
    {
        $selected_programs_archive = $user_org->selected_programs_archive($year);
        return view('archives.archive_selected_programs', compact(['user_org', 'year', 'selected_programs_archive']));
    }

    // ---------------------------------------------------------------------------------------

    public function pupil_inf(User $user_org)
    {
        $muns = District::all();

        return view('pupils/pupil_inf', compact(['user_org', 'muns']));
    }

    public function list_of_grades(User $user)
    {
        return view('pupils/list_of_grades', compact('user'));
    }

    public function pupils_year_archive(User $user)
    {
        return view('pupils/pupils_year_archive', compact('user'));
    }

    public function list_of_pupils(User $user, $grade_number, $grade_letter)
    {
        switch($grade_letter)
        {
            case 'a':
                $grade_letter = 'А';
                break;
            case 'b':
                $grade_letter = 'Б';
                break;
            case 'v':
                $grade_letter = 'В';
                break;
            case 'g':
                $grade_letter = 'Г';
                break;
            case 'd':
                $grade_letter = 'Д';
                break;
            case 'e':
                $grade_letter = 'Е';
                break;
            case 'zh':
                $grade_letter = 'Ж';
                break;
            case 'z':
                $grade_letter = 'З';
                break;
            case 'i':
                $grade_letter = 'И';
                break;
            case 'k':
                $grade_letter = 'К';
                break;
            case 'l':
                $grade_letter = 'Л';
                break;
        }
            

        $pupils = User::all()
        ->where('status', '7')
        ->where('org', $user->id)
        ->where('grade_number', $grade_number)
        ->where('grade_letter', $grade_letter)->sortBy('fullname');

        return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
    }

    public function list_of_pupils_archive(User $user, $year)
    {
        $pupils = User::all()
        ->where('status', '7')
        ->where('org', $user->id)->sortBy('fullname');

        return view('pupils/list_of_pupils_archive', compact(['user', 'pupils', 'year']));
    }


    public function list_of_programs_vuz(User $user){
        return view('pupils/list_of_programs_vuz', compact(['user']));
    }

    public function list_of_programs_vuz_archive(User $user, $year){
        return view('pupils/list_of_programs_vuz_archive', compact(['user', 'year']));
    }

    public function list_of_programs_school(User $user){
        $pupils = User::all()
        ->where('status', '7')
        ->where('org', $user->id);

        $a = array();
        foreach ($pupils as $pupil){
            if ($pupil->selected_programs()){
                    foreach ($pupil->selected_programs() as $selected_program){
                        if ($selected_program->not_student != 1){
                            array_push($a, SelectedProgram::where('id', $selected_program->id)->first()->proposed_program);
                        }
                    }
                }
            }
        $sps = array_unique($a);
        return view('pupils/list_of_programs_school', compact(['user', 'sps']));
    }

    public function list_of_pupils_vuz(User $user, $proposed_program_id){

        $proposed_program = Proposed::where('id', $proposed_program_id)->first();

        return view('pupils/list_of_pupils_vuz', compact(['user', 'proposed_program']));

    }

    public function list_of_pupils_school(User $user, $proposed_program_id){

        $proposed_program = Proposed::where('id', $proposed_program_id)->first();

        return view('pupils/list_of_pupils_school', compact(['user', 'proposed_program']));

    }

    public function list_of_pupils_vuz_school(User $user, $proposed_program_id){

        $proposed_program = Proposed::where('id', $proposed_program_id)->first();

        return view('pupils/list_of_pupils_school', compact(['user', 'proposed_program']));

    }

    public function proposed_programs_pupils(){

        $proposed_programs = Proposed::all();

        return view('pupils/proposed_programs_pupils', compact(['proposed_programs']));

    }

    public function proposed_programs_pupils_prof(){

        $proposed_programs = Proposed::all();

        return view('pupils/proposed_programs_pupils_prof', compact(['proposed_programs']));

    }

    public function agreements_vuz_prof(User $user, $main_or_not){

        $proposed_programs = $user->proposed_programs_main_or_not($main_or_not);

        $uniquePairs = [];

        foreach ($proposed_programs as $proposed_program) {
            foreach ($proposed_program->selected_programs() as $selected_program) {
                if ($selected_program->user->status == 7 && $selected_program->status == 1 && $selected_program->not_student != 1) {
                    $pairKey = $proposed_program->subject . '-' . $selected_program->user->getSchool->fullname;
                    if (!isset($uniquePairs[$pairKey])) {
                        $uniquePairs[$pairKey] = [
                            'proposed_program' => $proposed_program,
                            'selected_program' => $selected_program,
                        ];
                    }
                }
            }
        }

        return view('pupils/download_agreements_vuz', compact(['uniquePairs', 'user', 'main_or_not']));

    }

    public function show_pupils_reg($proposed_program_id)
    {
        $proposed_program = Proposed::where('id', $proposed_program_id)->first();

        return view('pupils.show_pupils_reg', compact('proposed_program'));
    }

    public function show_pupils_mun($proposed_program_id)
    {
        $proposed_program = Proposed::where('id', $proposed_program_id)->first();

        return view('pupils.show_pupils_mun', compact('proposed_program'));
    }

    // ------------------------------------------------------------------------------

    public function index2_year($year)
    {
        $yearRange = explode('-', $year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        $bids = Bid::whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get()->sortBy('date_begin')->sortBy('date_end');
        $proposed_programs_all = Proposed::whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get()->sortBy('date_begin')->sortBy('date_end');
        
        $selected_programs = SelectedProgram::whereHas('proposed_program', function ($query) use ($startDate, $endDate) {
            $query->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()])
                  ->whereBetween('date_end', [$startDate->toDateString(), $endDate->toDateString()]);
        })->get();

        return view('clusters.admin.index2-year', compact(['year', 'bids', 'proposed_programs_all', 'selected_programs']));
    }

    public function index_prof_year($year)
    {
        return view('clusters.admin.index-prof-year', compact(['year']));
    }

    public function index_vuz_year($year)
    {
        return view('clusters.admin.index-vuz-year', compact(['year']));
    }

    public function spo_programs_year($year, $orgtype)
    {
        $proposedProgramsQuery = function ($educationalProgram, $archive, $year, $orgtype) {
            $yearRange = explode('-', $year);
            $startDate = now()->setDate($yearRange[0], 9, 1);
            $endDate = now()->setDate($yearRange[1], 8, 31);

            return Proposed::where('educational_program', $educationalProgram)
                ->where('archive', $archive === 'not_archive' ? NULL : '!=', NULL)
                ->whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
                ->whereHas('user', function ($query) use ($orgtype) {
                    $query->where('org_type', $orgtype);
                });
        };
        
        $proposed_programs_main_not_archive = $proposedProgramsQuery('основная', 'not_archive', $year, $orgtype)->get();
        $proposed_programs_main_archive = $proposedProgramsQuery('основная', 'archive', $year, $orgtype)->get();
        
        $proposed_programs_dop_not_archive = $proposedProgramsQuery('дополнительная', 'not_archive', $year, $orgtype)->get();
        $proposed_programs_dop_archive = $proposedProgramsQuery('дополнительная', 'archive', $year, $orgtype)->get();

        return view(
            'pupils.admin.spo_programs_year', 
            compact([
                'proposed_programs_main_not_archive', 'proposed_programs_main_archive',
                'proposed_programs_dop_not_archive', 'proposed_programs_dop_archive',
                'year'
            ])
        );
    }


    public function index_mun_year($district_id, $year)
    {
        $yearRange = explode('-', $year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        $bids = Bid::whereHas('user', function ($query) use ($district_id) {
                $query->where('district', $district_id);
            })
            ->whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get()
            ->sortBy('date_begin')->sortBy('date_end');
        $proposed_programs_all = Proposed::whereHas('user', function ($query) use ($district_id) {
                $query->where('district', $district_id);
            })
            ->whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get()
            ->sortBy('date_begin')->sortBy('date_end');

        
        $selected_programs = SelectedProgram::whereHas('user', function ($query) use ($district_id) {
                $query->where('district', $district_id);
            })
            ->whereHas('proposed_program', function ($query) use ($startDate, $endDate) {
                $query->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()])
                    ->whereBetween('date_end', [$startDate->toDateString(), $endDate->toDateString()]);
            })
            ->orWhereHas('proposed_program', function ($query) use ($startDate, $endDate, $district_id) {
                $query->whereHas('user', function ($query) use ($district_id) {
                        $query->where('district', $district_id);
                    })
                    ->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()])
                    ->whereBetween('date_end', [$startDate->toDateString(), $endDate->toDateString()]);
            })
            ->get();
        

        return view('clusters.admin.mun.index-mun-year', compact(['year', 'bids', 'proposed_programs_all', 'selected_programs']));
    }

    public function spo_programs_mun_year($district_id, $year, $orgtype)
    {
        $proposedProgramsQuery = function ($educationalProgram, $archive, $year, $orgtype) {
            $yearRange = explode('-', $year);
            $startDate = now()->setDate($yearRange[0], 9, 1);
            $endDate = now()->setDate($yearRange[1], 8, 31);

            return Proposed::where('educational_program', $educationalProgram)
                ->where('archive', $archive === 'not_archive' ? NULL : '!=', NULL)
                ->whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
                ->whereHas('user', function ($query) use ($orgtype) {
                    $query->where('org_type', $orgtype);
                });
        };
        
        $proposed_programs_main_not_archive = $proposedProgramsQuery('основная', 'not_archive', $year, $orgtype)->get();
        $proposed_programs_main_archive = $proposedProgramsQuery('основная', 'archive', $year, $orgtype)->get();
        
        $proposed_programs_dop_not_archive = $proposedProgramsQuery('дополнительная', 'not_archive', $year, $orgtype)->get();
        $proposed_programs_dop_archive = $proposedProgramsQuery('дополнительная', 'archive', $year, $orgtype)->get();

        return view(
            'pupils.admin.spo_programs_year', 
            compact([
                'proposed_programs_main_not_archive', 'proposed_programs_main_archive',
                'proposed_programs_dop_not_archive', 'proposed_programs_dop_archive',
                'district_id', 'year'
            ])
        );
    }
}
