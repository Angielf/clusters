<?php

namespace App\Http\Controllers;

use App\Bid;

use Illuminate\Support\Facades\Auth;


class ArchiveController extends Controller
{
    public function list()
    {
        return view('archives_reg.archive_list');
    }

    public function all_exports()
    {
        return view('clusters.all');
    }


    public function bids_archives_reg($year)
    {
        $bids = Bid::where('archive', $year)->get();
        return view('archives_reg.archive_bids', ['year' => $year, 'bids' => $bids,]);
    }

    public function bids_archives_mun($year)
    {
        $bids = Bid::where('archive', $year)->get();
        $user = Auth::user();
        return view('archives_mun.archive_bids_mun', ['year' => $year, 'bids' => $bids, 'user' => $user]);
    }

}
