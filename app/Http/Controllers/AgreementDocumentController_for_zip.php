<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Bid;
use App\Program;
use App\Proposed;
use App\SelectedProgram;
use App\User;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\PhpWord;

use \NumberFormatter;

class AgreementDocumentController_for_zip extends Controller
{

    public function create_document_agreement_selected4($user, $program)
    {
        // $user = Auth::user();
        $user = User::where('id', $user->id)->get()->first();
        $selected_program = SelectedProgram::where('id', $program)->first();

        $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-prof2.docx');
        $templateProcessor->setValue('fullname_full_other', $user->fullname_full);
        $templateProcessor->setValue('date_license_other', $user->date_license);
        $templateProcessor->setValue('number_license_other', $user->number_license);
        $templateProcessor->setValue('who_license_other', $user->who_license);
        $templateProcessor->setValue('director_post_rod_other', $user->director_post_rod);
        $templateProcessor->setValue('director_rod_other', $user->director_rod);
        $templateProcessor->setValue('director_post_other', $user->director_post);

        $director_other = $user->director;
        $m = explode(' ', (string) $director_other);
        $director_post_with_initials_other = $m[0] . ' ' . substr($m[1],0,2) . '.' . substr($m[2],0,2) . '.' ;
        $templateProcessor->setValue('director_post_with_initials_other', $director_post_with_initials_other);

        $templateProcessor->setValue('punkt', $user->punkt);
        $templateProcessor->setValue('kbk', $user->kbk);
        $templateProcessor->setValue('ikz', $user->ikz);

        if ($selected_program->proposed_program->educational_program == 'дополнительная') {
            $templateProcessor->setValue('edu_program', 'дополнительной');
        }elseif($selected_program->proposed_program->educational_program == 'основная'){
            $templateProcessor->setValue('edu_program', 'основной');
        }

        $templateProcessor->setValue('date_start', $selected_program->proposed_program->date_begin);
        $templateProcessor->setValue('date_finish', $selected_program->proposed_program->date_end);

        // $templateProcessor->setValue('amount_students', $selected_program->selected_schedule->selected_student->students_amount);

        $templateProcessor->setValue('inn_other', $user->inn);
        $templateProcessor->setValue('kpp_other', $user->kpp);
        $templateProcessor->setValue('address_other', $user->address);
        $templateProcessor->setValue('bik_other', $user->bik);
        $templateProcessor->setValue('ogrn_other', $user->ogrn);
        $templateProcessor->setValue('okpo_other', $user->okpo);
        $templateProcessor->setValue('oktmo_other', $user->oktmo);

        $templateProcessor->setValue('bank_other', $user->bank);
        $templateProcessor->setValue('single_treasury_account_other', $user->single_treasury_account);
        $templateProcessor->setValue('treasury_account_other', $user->treasury_account);

        // if ($user->opf_short == 'МКУ' or $user->opf_short == 'ГКУ' or $user->opf_short == 'ФГКУ') {
        //     $templateProcessor->setValue('s_t_a_other', 'Единый казначейский счёт');
        //     $templateProcessor->setValue('t_a_other', 'Казначейский счёт');
        // }else{
        //     $templateProcessor->setValue('s_t_a_other', 'р/сч');
        //     $templateProcessor->setValue('t_a_other', 'кор/сч');
        // }

        $templateProcessor->setValue('s_t_a_other', $user->single_treasury_account_type);
        $templateProcessor->setValue('t_a_other', $user->treasury_account_type);

        $templateProcessor->setValue('date_begin', $selected_program->proposed_program->getDataBeginDots());
        $templateProcessor->setValue('date_end', $selected_program->proposed_program->getDataEndDots());

        $templateProcessor->setValue('fullname_full', $selected_program->proposed_program->user->fullname_full);
        $templateProcessor->setValue('date_license', $selected_program->proposed_program->user->date_license);
        $templateProcessor->setValue('number_license', $selected_program->proposed_program->user->number_license);
        $templateProcessor->setValue('who_license', $selected_program->proposed_program->user->who_license);
        $templateProcessor->setValue('director_post_rod', $selected_program->proposed_program->user->director_post_rod);
        $templateProcessor->setValue('director_rod', $selected_program->proposed_program->user->director_rod);
        $templateProcessor->setValue('director_post', $selected_program->proposed_program->user->director_post);

        $director = $selected_program->proposed_program->user->director;
        $m2 = explode(' ', $director);
        $director_post_with_initials = $m2[0] . ' ' . substr($m2[1],0,2) . '.' . substr($m2[2],0,2) . '.' ;
        $templateProcessor->setValue('director_post_with_initials', $director_post_with_initials);

        $templateProcessor->setValue('inn', $selected_program->proposed_program->user->inn);
        $templateProcessor->setValue('kpp', $selected_program->proposed_program->user->kpp);
        $templateProcessor->setValue('address', $selected_program->proposed_program->user->address);
        $templateProcessor->setValue('bik', $selected_program->proposed_program->user->bik);
        $templateProcessor->setValue('ogrn', $selected_program->proposed_program->user->ogrn);
        $templateProcessor->setValue('okpo', $selected_program->proposed_program->user->okpo);
        $templateProcessor->setValue('oktmo', $selected_program->proposed_program->user->oktmo);

        $templateProcessor->setValue('bank', $selected_program->proposed_program->user->bank);
        $templateProcessor->setValue('single_treasury_account', $selected_program->proposed_program->user->single_treasury_account);
        $templateProcessor->setValue('treasury_account', $selected_program->proposed_program->user->treasury_account);

        // if ($selected_program->proposed_program->user->opf_short == 'МКУ' or $selected_program->proposed_program->user->opf_short == 'ГКУ' or $selected_program->proposed_program->user->opf_short == 'ФГКУ') {
        //     $templateProcessor->setValue('s_t_a', 'Единый казначейский счёт');
        //     $templateProcessor->setValue('t_a', 'Казначейский счёт');
        // }else{
        //     $templateProcessor->setValue('s_t_a', 'р/сч');
        //     $templateProcessor->setValue('t_a', 'кор/сч');
        // }

        $templateProcessor->setValue('s_t_a', $selected_program->proposed_program->user->single_treasury_account_type);
        $templateProcessor->setValue('t_a', $selected_program->proposed_program->user->treasury_account_type);

        $pupils_all_school = User::all()
        ->where('status', '7')
        ->where('org', $user->id);

        $pupils = "";
        $amount_students = 0;
        foreach ($pupils_all_school as $p => $pupil){
            if ($pupil->selected_programs()){
                foreach($pupil->selected_programs() as $selected){
                    if($selected->proposed_program_id == $selected_program->proposed_program->id
                        and $selected->status == 1 
                        and $selected->selected_schedule 
                        and $selected->selected_schedule->status == 1
                    ){
                        $pupils .= $pupil->fullname . " \r\n" ;
                        $amount_students += 1;
                    }
                }
            }
        }

        $phpWord = new PhpWord();
        $section = $phpWord->addSection();
        $styleCell =
        [
            'borderColor' =>'black',
            'borderSize' => 4,
        ];
        $table = $section->addTable('table');
        $table->addRow();
        $table->addCell(1000, $styleCell)->addText(htmlspecialchars("№ п/п"));
        $table->addCell(1750, $styleCell)->addText(htmlspecialchars("ФИО обучающегося"));
        $table->addCell(1750, $styleCell)->addText(htmlspecialchars("Дата рождения"));
        $table->addCell(1000, $styleCell)->addText(htmlspecialchars("Класс"));
        $table->addCell(1750, $styleCell)->addText(htmlspecialchars("Наименование образовательной программы"));
        $x = 0;
        $subject = $selected_program->proposed_program->subject;
        $vuz_id = $selected_program->proposed_program->user_id;
        $school_id = $user->id;
        $subjects = array();
        $subjects_ids = array();
        $count_9 = 0;
        $count_10 = 0;
        $count_11 = 0;
        $program_count = 0;
        $proposed_programs = Proposed::where('user_id', $vuz_id)->where('archive', NULL)->get();
        foreach ($proposed_programs as $program=>$p) { 
            if ($p->selected_programs()){
                foreach($p->selected_programs() as $selected_program=>$s){
                    if($s->not_student != 1){
                        $sub = $s->proposed_program->subject;
                        $sub_id = $s->proposed_program->id;
                        $pupil_id = $s->school_id;
                        $pupil = User::where('id', $pupil_id)->get()->first();
                        $pupil_school_id = $pupil->org;
                        if($pupil_school_id == $school_id){
                            $subjects[] = $sub;
                            $subjects_ids[] = $sub_id;
                            $x += 1;
                            $c = $pupil->grade_number . ' ' . $pupil->grade_letter;
                            if($pupil->grade_number == '11'){
                                $count_11 += 1;
                            }
                            if($pupil->grade_number == '10'){
                                $count_10 += 1;
                            }
                            if($pupil->grade_number == '9'){
                                $count_9 += 1;
                            }
                            $table->addRow();
                            $table->addCell(1000, $styleCell)->addText(htmlspecialchars("{$x}"));
                            $table->addCell(1750, $styleCell)->addText(htmlspecialchars("{$pupil->fullname}"));
                            $table->addCell(1750, $styleCell)->addText(htmlspecialchars("{$pupil->birth}"));
                            $table->addCell(1000, $styleCell)->addText(htmlspecialchars("{$c}"));
                            $table->addCell(1750, $styleCell)->addText(htmlspecialchars("{$sub}"));
                        }
                    }
                }
            }
        }

        $count_programs = array_count_values($subjects_ids);
        $section2 = $phpWord->addSection();
        $section3 = $phpWord->addSection();
        $styleCell2 =
        [
            'borderColor' =>'black',
            'borderSize' => 4,
        ];
        $y = 0;
        $table2 = $section2->addTable('table2');
        $table2->addRow();
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("№ п/п"));
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("Количество обучающихся"));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("Наименование образовательной программы"));
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("Количество часов"));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("Цена за единицу услуги (руб/час)"));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("Стоимость услуги, руб"));

        $table3 = $section3->addTable('table3');
        $table3->addRow();
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("№ п/п"));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("Количество обучающихся"));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("Наименование образовательной программы"));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("Количество часов"));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("Период обучения"));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("Цена за единицу услуги (руб/час)"));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("Стоимость услуги, руб"));


        $money_all = 0;
        foreach ($count_programs as $key => $value) {
            $proposed_program = Proposed::where('id', $key)->get()->first();
            $proposed_program_subject = $proposed_program->subject;
            // $proposed_program_hours = $proposed_program->hours;
            $proposed_program_hours = 39;

            $proposed_program_begin = $proposed_program->getDataBeginDots();
            $proposed_program_end = $proposed_program->getDataEndDots();

            $money_one = 45;
            $money_program = intval($value) * $money_one * $proposed_program_hours;
            $money_all = $money_all + $money_program;
            $y += 1;
            $table2->addRow();
            $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("{$y}"));
            $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("{$value}"));
            $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("{$proposed_program_subject}"));
            $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("{$proposed_program_hours}"));
            $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_one}"));
            $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_program}"));

            $table3->addRow();
            $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("{$y}"));
            $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("{$value}"));
            $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("{$proposed_program_subject}"));
            $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("{$proposed_program_hours}"));
            $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("{$proposed_program_begin}-{$proposed_program_end}"));
            $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_one}"));
            $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_program}"));
        }
        $table2->addRow();
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars(""));
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("Итого"));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_all}"));
        $templateProcessor->setValue('money_all', $money_all);

        // Сумма прописью
        $intPart = sprintf('%d', $money_all);
        $money_all_str = (new NumberFormatter('ru-RU', NumberFormatter::SPELLOUT))->format($intPart);
        $templateProcessor->setValue('money_all_str', $money_all_str);

        $table3->addRow();
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("Итого"));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_all}"));

        $subjects = array_unique($subjects);

        $subjects_all = implode(", ", $subjects);
        $templateProcessor->setValue('subject', $subjects_all);
        $templateProcessor->setValue('count_9', $count_9);
        $templateProcessor->setValue('count_10', $count_10);
        $templateProcessor->setValue('count_11', $count_11);
        // $templateProcessor->setValue('table', $table);
        $templateProcessor->setComplexBlock('{table}', $table);
        $templateProcessor->setComplexBlock('{table2}', $table2);
        $templateProcessor->setComplexBlock('{table3}', $table3);


        $templateProcessor->setValue('pupils', $pupils);
        $templateProcessor->setValue('amount_students', $amount_students);
    
        // Создаем временный файл для сохранения документа
        $tempFilePath = tempnam(sys_get_temp_dir(), 'agreement');
        $templateProcessor->saveAs($tempFilePath);

        // Читаем содержимое временного файла в строку
        $documentContent = file_get_contents($tempFilePath);

        // Удаляем временный файл
        unlink($tempFilePath);

        // Возвращаем содержимое документа
        return $documentContent;
    }


    public function create_document_agreement_selected_vsu($user, $program)
    {
        // $user = Auth::user();
        $user = User::where('id', $user->id)->get()->first();
        $selected_program = SelectedProgram::where('id', $program)->first();

        if($selected_program->proposed_program->user->id == 1857){
            $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-vsu.docx');
        }elseif ($selected_program->proposed_program->user->id == 802) {
            $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-vspu.docx');
        }elseif ($selected_program->proposed_program->user->id == 1839) {
            $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-vsuet.docx');
        }elseif ($selected_program->proposed_program->user->id == 1858) {
            $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-vsau.docx');
        }elseif ($selected_program->proposed_program->user->id == 4300) {
            $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-mifi.docx');
        }elseif ($selected_program->proposed_program->user->id == 1845) {
            $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-vstu.docx');
        }elseif ($selected_program->proposed_program->user->id == 15400) {
            $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-vgltu.docx');
        }else{
            $templateProcessor = new TemplateProcessor('../public/word-template/agreement-doc-pupil-other.docx');
        }

        if($selected_program->proposed_program->user->id == 4300){
            $templateProcessor->setValue('city', 'г. Нововоронеж');
        }else{
            $templateProcessor->setValue('city', 'г. Воронеж');
        }

        $templateProcessor->setValue('fullname_full_other', $user->fullname_full);
        $templateProcessor->setValue('date_license_other', $user->date_license);
        $templateProcessor->setValue('number_license_other', $user->number_license);
        $templateProcessor->setValue('who_license_other', $user->who_license);
        $templateProcessor->setValue('director_post_rod_other', $user->director_post_rod);
        $templateProcessor->setValue('director_rod_other', $user->director_rod);
        $templateProcessor->setValue('director_post_other', $user->director_post);

        $director_other = $user->director;
        $m = explode(' ', (string) $director_other);
        $director_post_with_initials_other = $m[0] . ' ' . substr($m[1],0,2) . '.' . substr($m[2],0,2) . '.' ;
        $templateProcessor->setValue('director_post_with_initials_other', $director_post_with_initials_other);

        $templateProcessor->setValue('punkt', $user->punkt);
        $templateProcessor->setValue('kbk', $user->kbk);
        $templateProcessor->setValue('ikz', $user->ikz);

        if ($selected_program->proposed_program->educational_program == 'дополнительная') {
            $templateProcessor->setValue('edu_program', 'дополнительной');
        }elseif($selected_program->proposed_program->educational_program == 'основная'){
            $templateProcessor->setValue('edu_program', 'основной');
        }

        $templateProcessor->setValue('date_start', $selected_program->proposed_program->date_begin);
        $templateProcessor->setValue('date_finish', $selected_program->proposed_program->date_end);

        $templateProcessor->setValue('inn_other', $user->inn);
        $templateProcessor->setValue('kpp_other', $user->kpp);
        $templateProcessor->setValue('address_other', $user->address);
        $templateProcessor->setValue('bik_other', $user->bik);
        $templateProcessor->setValue('ogrn_other', $user->ogrn);
        $templateProcessor->setValue('okpo_other', $user->okpo);
        $templateProcessor->setValue('oktmo_other', $user->oktmo);

        $templateProcessor->setValue('bank_other', $user->bank);
        $templateProcessor->setValue('single_treasury_account_other', $user->single_treasury_account);
        $templateProcessor->setValue('treasury_account_other', $user->treasury_account);

        $templateProcessor->setValue('tel_other', $user->tel);
        $templateProcessor->setValue('email_real_other', $user->email_real);

        // if ($user->opf_short == 'МКУ' or $user->opf_short == 'ГКУ' or $user->opf_short == 'ФГКУ') {
        //     $templateProcessor->setValue('s_t_a_other', 'Единый казначейский счёт');
        //     $templateProcessor->setValue('t_a_other', 'Казначейский счёт');
        // }else{
        //     $templateProcessor->setValue('s_t_a_other', 'р/сч');
        //     $templateProcessor->setValue('t_a_other', 'кор/сч');
        // }

        $templateProcessor->setValue('s_t_a_other', $user->single_treasury_account_type);
        $templateProcessor->setValue('t_a_other', $user->treasury_account_type);

        $templateProcessor->setValue('date_begin', $selected_program->proposed_program->getDataBeginDots());
        $templateProcessor->setValue('date_end', $selected_program->proposed_program->getDataEndDots());

        $templateProcessor->setValue('fullname_full', $selected_program->proposed_program->user->fullname_full);
        $templateProcessor->setValue('date_license', $selected_program->proposed_program->user->date_license);
        $templateProcessor->setValue('number_license', $selected_program->proposed_program->user->number_license);
        $templateProcessor->setValue('who_license', $selected_program->proposed_program->user->who_license);
        $templateProcessor->setValue('director_post_rod', $selected_program->proposed_program->user->director_post_rod);
        $templateProcessor->setValue('director_rod', $selected_program->proposed_program->user->director_rod);
        $templateProcessor->setValue('director_post', $selected_program->proposed_program->user->director_post);
        $templateProcessor->setValue('director_post_big', strtoupper($selected_program->proposed_program->user->director_post));

        $templateProcessor->setValue('inn', $selected_program->proposed_program->user->inn);
        $templateProcessor->setValue('kpp', $selected_program->proposed_program->user->kpp);
        $templateProcessor->setValue('address', $selected_program->proposed_program->user->address);
        $templateProcessor->setValue('bik', $selected_program->proposed_program->user->bik);
        $templateProcessor->setValue('ogrn', $selected_program->proposed_program->user->ogrn);
        $templateProcessor->setValue('okpo', $selected_program->proposed_program->user->okpo);
        $templateProcessor->setValue('oktmo', $selected_program->proposed_program->user->oktmo);

        $templateProcessor->setValue('bank', $selected_program->proposed_program->user->bank);
        $templateProcessor->setValue('single_treasury_account', $selected_program->proposed_program->user->single_treasury_account);
        $templateProcessor->setValue('treasury_account', $selected_program->proposed_program->user->treasury_account);

        $templateProcessor->setValue('tel', $selected_program->proposed_program->user->tel);
        $templateProcessor->setValue('email_real', $selected_program->proposed_program->user->email_real);

        // if ($selected_program->proposed_program->user->opf_short == 'МКУ' or $selected_program->proposed_program->user->opf_short == 'ГКУ' or $selected_program->proposed_program->user->opf_short == 'ФГКУ') {
        //     $templateProcessor->setValue('s_t_a', 'Единый казначейский счёт');
        //     $templateProcessor->setValue('t_a', 'Казначейский счёт');
        // }else{
        //     $templateProcessor->setValue('s_t_a', 'р/сч');
        //     $templateProcessor->setValue('t_a', 'кор/сч');
        // }

        $templateProcessor->setValue('s_t_a', $selected_program->proposed_program->user->single_treasury_account_type);
        $templateProcessor->setValue('t_a', $selected_program->proposed_program->user->treasury_account_type);

        $pupils_all_school = User::all()
        ->where('status', '7')
        ->where('org', $user->id);

        $pupils = "";
        $amount_students = 0;
        foreach ($pupils_all_school as $p => $pupil){
            if ($pupil->selected_programs()){
                foreach($pupil->selected_programs() as $selected){
                    if($selected->proposed_program_id == $selected_program->proposed_program->id
                        and $selected->status == 1 
                        and $selected->selected_schedule 
                        and $selected->selected_schedule->status == 1
                    ){
                        $pupils .= $pupil->fullname . " \r\n" ;
                        $amount_students += 1;
                    }
                }
            }
        }

        $phpWord = new PhpWord();
        $section = $phpWord->addSection();
        $styleCell =
        [
            'borderColor' =>'black',
            'borderSize' => 4,
        ];
        $table = $section->addTable('table');
        $table->addRow();
        $table->addCell(1000, $styleCell)->addText(htmlspecialchars("№ п/п"));
        $table->addCell(1750, $styleCell)->addText(htmlspecialchars("ФИО обучающегося"));
        $table->addCell(1750, $styleCell)->addText(htmlspecialchars("Дата рождения"));
        $table->addCell(1000, $styleCell)->addText(htmlspecialchars("Класс"));
        $table->addCell(1750, $styleCell)->addText(htmlspecialchars("Наименование образовательной программы"));
        $x = 0;

        $subject = $selected_program->proposed_program->subject;
        $vuz_id = $selected_program->proposed_program->user_id;
        $school_id = $user->id;
        $subjects = array();
        $subjects_ids = array();
        $count_10 = 0;
        $count_11 = 0;
        $program_count = 0;
        $proposed_programs = Proposed::where('user_id', $vuz_id)->where('archive', NULL)->get();
        foreach ($proposed_programs as $program=>$p) { 
            if ($p->selected_programs()){
                foreach($p->selected_programs() as $selected_program=>$s){
                    if($s->not_student != 1){
                        $sub = $s->proposed_program->subject;
                        $sub_id = $s->proposed_program->id;
                        $pupil_id = $s->school_id;
                        $pupil = User::where('id', $pupil_id)->get()->first();
                        $pupil_school_id = $pupil->org;
                        if($pupil_school_id == $school_id){
                            $subjects[] = $sub;
                            $subjects_ids[] = $sub_id;
                            $x += 1;
                            $c = $pupil->grade_number . ' ' . $pupil->grade_letter;
                            if($pupil->grade_number == '11'){
                                $count_11 += 1;
                            }
                            if($pupil->grade_number == '10'){
                                $count_10 += 1;
                            }
                            $table->addRow();
                            $table->addCell(1000, $styleCell)->addText(htmlspecialchars("{$x}"));
                            $table->addCell(1750, $styleCell)->addText(htmlspecialchars("{$pupil->fullname}"));
                            $table->addCell(1750, $styleCell)->addText(htmlspecialchars("{$pupil->birth}"));
                            $table->addCell(1000, $styleCell)->addText(htmlspecialchars("{$c}"));
                            $table->addCell(1750, $styleCell)->addText(htmlspecialchars("{$sub}"));
                        }
                    }
                }
            }
        }

        $count_programs = array_count_values($subjects_ids);
        $section2 = $phpWord->addSection();
        $section3 = $phpWord->addSection();
        $styleCell2 =
        [
            'borderColor' =>'black',
            'borderSize' => 4,
        ];
        $y = 0;
        $table2 = $section2->addTable('table2');
        $table2->addRow();
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("№ п/п"));
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("Количество обучающихся"));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("Наименование образовательной программы"));
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("Количество часов"));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("Цена за единицу услуги (руб/час.)"));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("Стоимость услуги, руб"));

        $table3 = $section3->addTable('table3');
        $table3->addRow();
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("№ п/п"));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("Количество обучающихся"));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("Наименование образовательной программы"));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("Количество часов"));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("Период обучения"));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("Цена за единицу услуги (руб/час.)"));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("Стоимость услуги, руб"));


        $money_all = 0;
        foreach ($count_programs as $key => $value) {
            $proposed_program = Proposed::where('id', $key)->get()->first();
            $proposed_program_subject = $proposed_program->subject;
            $proposed_program_hours = $proposed_program->hours;

            $proposed_program_begin = $proposed_program->getDataBeginDots();
            $proposed_program_end = $proposed_program->getDataEndDots();

            $money_one = 45;
            $money_program = intval($value) * $money_one * $proposed_program_hours;
            $money_all = $money_all + $money_program;
            $y += 1;
            $table2->addRow();
            $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("{$y}"));
            $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("{$value}"));
            $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("{$proposed_program_subject}"));
            $table2->addCell(1000, $styleCell)->addText(htmlspecialchars("{$proposed_program_hours}"));
            $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_one}"));
            $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_program}"));

            $table3->addRow();
            $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("{$y}"));
            $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("{$value}"));
            $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("{$proposed_program_subject}"));
            $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("{$proposed_program_hours}"));
            $table3->addCell(1000, $styleCell)->addText(htmlspecialchars("{$proposed_program_begin}-{$proposed_program_end}"));
            $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_one}"));
            $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_program}"));
        }
        $table2->addRow();
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars(""));
        $table2->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("Итого"));
        $table2->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_all}"));
        $templateProcessor->setValue('money_all', $money_all);

        // Сумма прописью
        $intPart = sprintf('%d', $money_all);
        $money_all_str = (new NumberFormatter('ru-RU', NumberFormatter::SPELLOUT))->format($intPart);
        $templateProcessor->setValue('money_all_str', $money_all_str);

        $table3->addRow();
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1000, $styleCell)->addText(htmlspecialchars(""));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("Итого"));
        $table3->addCell(1750, $styleCell)->addText(htmlspecialchars("{$money_all}"));

        $subjects = array_unique($subjects);

        $subjects_all = implode(", ", $subjects);
        $templateProcessor->setValue('subject', $subjects_all);
        $templateProcessor->setValue('count_10', $count_10);
        $templateProcessor->setValue('count_11', $count_11);

        $templateProcessor->setComplexBlock('{table}', $table);
        $templateProcessor->setComplexBlock('{table2}', $table2);
        $templateProcessor->setComplexBlock('{table3}', $table3);


        $templateProcessor->setValue('pupils', $pupils);
        $templateProcessor->setValue('amount_students', $amount_students);

        // Создаем временный файл для сохранения документа
        $tempFilePath = tempnam(sys_get_temp_dir(), 'agreement');
        $templateProcessor->saveAs($tempFilePath);

        // Читаем содержимое временного файла в строку
        $documentContent = file_get_contents($tempFilePath);

        // Удаляем временный файл
        unlink($tempFilePath);

        // Возвращаем содержимое документа
        return $documentContent;
    }
}