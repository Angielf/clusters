<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\District;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'district' => ['required', 'string', 'max:255'],
            // 'fullname' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'patronymic' => ['string', 'max:255'],
            // 'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'district' => $data['district'],
            'org' => $data['org'],
            'grade_number' => $data['grade_number'],
            'grade_letter' => $data['grade_letter'],
            'fullname' => $data['firstname'] . ' ' . $data['lastname'] . ' ' . $data['patronymic'],
            // 'name' => $data['name'],
            'name' => $data['email'],
            'email' => $data['email'],
            'email_real' => $data['email'],
            'password' => Hash::make($data['password']),
            'status' => '7',
            'birth' => $data['birth'],
            'snils' => $data['snils'],
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $muns = District::all();

        return view('auth.register', compact('muns'));
    }


    // User::where('meta->colors', 'like', '%"red"%')
    public function selectOrg(Request $request){
        if($request->ajax()){
            $orgs = User::
            where('district', $request->district)
            // ->where('type_org',$request->typeOrg)
            ->where('name', 'like', '%sch%')
            // ->where('id', '<=', '1845')
            ->where('status', '0')
            ->where('status_exist', '=', 'ACTIVE')->get()
            ->pluck("fullname", "id");
            $data = view('auth/selectorg',['orgs' => $orgs])->render();
            return response()->json(['options'=>$data]);
        }
    }
}
