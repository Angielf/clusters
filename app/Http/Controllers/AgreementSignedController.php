<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\AgreementPupil;
use App\AgreementPupil2;
use App\Agreement;
use App\SelectedAgreement;
use App\User;

class AgreementSignedController extends Controller
{
    public function agreementPage($a, User $user, $agreement_id, $grade_number, $grade_letter){
        switch($a){
            case 'agreements_pupils':
                $file_name = AgreementPupil::select('filename')->where('id', $agreement_id)->first();
                break;
            case 'agreements_pupils2':
                $file_name = AgreementPupil2::select('filename')->where('id', $agreement_id)->first();
                break;
            default:
                $file_name = AgreementPupil::select('filename')->where('id', $agreement_id)->first();
                break;
        }

        return view('sign/signed_agreements', compact(['a', 'user', 'agreement_id', 'file_name', 'grade_number', 'grade_letter']));
    }

    public function agreementPage2($a, User $user, $agreement_id, $proposed_program_id){
        switch($a){
            case 'agreements_pupils':
                $file_name = AgreementPupil::select('filename')->where('id', $agreement_id)->first();
                break;
            case 'agreements_pupils2':
                $file_name = AgreementPupil2::select('filename')->where('id', $agreement_id)->first();
                break;
            default:
                $file_name = AgreementPupil::select('filename')->where('id', $agreement_id)->first();
                break;
        }

        return view('sign/signed_agreements2', compact(['a', 'user', 'agreement_id', 'file_name', 'proposed_program_id']));
    }

    public function agreementPageOrdinary($a, User $user, $agreement_id){
        switch($a){
            case 'agreements':
                $file_name = Agreement::select('filename')->where('id', $agreement_id)->first();
                break;
            case 'selected_agreements':
                $file_name = SelectedAgreement::select('filename')->where('id', $agreement_id)->first();
                break;
            default:
                $file_name = Agreement::select('filename')->where('id', $agreement_id)->first();
                break;
        }

        return view('sign/signed_agreements_ordinary', compact(['a', 'user', 'agreement_id', 'file_name']));
    }

    public function agreementPageOrdinary2($a, User $user, $agreement_id){
        switch($a){
            case 'agreements':
                $file_name = Agreement::select('filename')->where('id', $agreement_id)->first();
                break;
            case 'selected_agreements':
                $file_name = SelectedAgreement::select('filename')->where('id', $agreement_id)->first();
                break;
            default:
                $file_name = Agreement::select('filename')->where('id', $agreement_id)->first();
                break;
        }

        return view('sign/signed_agreements_ordinary2', compact(['a', 'user', 'agreement_id', 'file_name']));
    }


    public function saveSignedFile(Request $request, $a, User $user, $agreement_id)
    {
        $signedFile = $request->file('signed_file');

        $file_name = $request->input('file_name');

        $fileName = $request->input('file_name').'.sig';
        
        // Сохраняем файл в указанной папке
        switch ($a) {
            case 'agreements_pupils':
                $signedFile->move(public_path() .'/files/agreements_pupils/signed/', $fileName);
                break;
            case 'agreements_pupils2':
                $signedFile->move(public_path() .'/files/agreements_pupils2/signed/', $fileName);
                break;
            default:
                $signedFile->move(public_path() .'/files/agreements_pupils/signed/', $fileName);
                break;
        }
        
        if ($signedFile) {
            switch ($a) {
                case 'agreements_pupils':
                    $agreements = AgreementPupil::where('filename', $file_name)->get();
                    foreach ($agreements as $agreement) {
                        $agreement->filename_sign = $fileName;
                        $agreement->sign = 1;
                        $agreement->save();
                    }
                    break;
                case 'agreements_pupils2':
                    $agreements = AgreementPupil2::where('filename', $file_name)->get();
                    foreach ($agreements as $agreement) {
                        $agreement->filename_sign = $fileName;
                        $agreement->sign = 1;
                        $agreement->save();
                    }
                    break;
            }

            return redirect('/pupils/'.$user->id.'/list-of-grades');
        } else {
            return response()->json(['message' => 'Ошибка сохранения файла'], 500);
        }
    }

    public function saveSignedFile2(Request $request, $a, User $user, $agreement_id, $proposed_program_id)
    {
        $signedFile = $request->file('signed_file');

        $file_name = $request->input('file_name');

        $fileName = $request->input('file_name').'_2'.'.sig';
        
        // Сохраняем файл в указанной папке
        switch ($a) {
            case 'agreements_pupils':
                $signedFile->move(public_path() .'/files/agreements_pupils/signed2/', $fileName);
                break;
            case 'agreements_pupils2':
                $signedFile->move(public_path() .'/files/agreements_pupils2/signed2/', $fileName);
                break;
            default:
                $signedFile->move(public_path() .'/files/agreements_pupils/signed2/', $fileName);
                break;
        }
        
        if ($signedFile) {
            switch ($a) {
                case 'agreements_pupils':
                    $agreements = AgreementPupil::where('filename', $file_name)->get();
                    foreach ($agreements as $agreement) {
                        $agreement->filename_sign2 = $fileName;
                        $agreement->sign2 = 1;
                        $agreement->save();
                    }
                    break;
                case 'agreements_pupils2':
                    $agreements = AgreementPupil2::where('filename', $file_name)->get();
                    foreach ($agreements as $agreement) {
                        $agreement->filename_sign2 = $fileName;
                        $agreement->sign2 = 1;
                        $agreement->save();
                    }
                    break;
            }

            return redirect('/pupils-vuz/'.$user->id.'/'.$proposed_program_id.'/list-of-pupils');
        } else {
            return response()->json(['message' => 'Ошибка сохранения файла'], 500);
        }
    }

    public function saveSignedFileOrdinary(Request $request, $a, User $user, $agreement_id)
    {
        $signedFile = $request->file('signed_file');

        $file_name = $request->input('file_name');

        $fileName = $request->input('file_name').'.sig';
        
        // Сохраняем файл в указанной папке
        switch ($a) {
            case 'agreements':
                $signedFile->move(public_path() .'/files/agreements/signed/', $fileName);
                break;
            case 'selected_agreements':
                $signedFile->move(public_path() .'/files/selected_agreements/signed/', $fileName);
                break;
            default:
                $signedFile->move(public_path() .'/files/agreements/signed/', $fileName);
                break;
        }
        
        if ($signedFile) {
            switch ($a) {
                case 'agreements':
                    $agreements = Agreement::where('filename', $file_name)->get();
                    foreach ($agreements as $agreement) {
                        $agreement->filename_sign = $fileName;
                        $agreement->sign = 1;
                        $agreement->save();
                    }
                    break;
                case 'selected_agreements':
                    $agreements = SelectedAgreement::where('filename', $file_name)->get();
                    foreach ($agreements as $agreement) {
                        $agreement->filename_sign = $fileName;
                        $agreement->sign = 1;
                        $agreement->save();
                    }
                    break;
            }

            return redirect('/');
        } else {
            return response()->json(['message' => 'Ошибка сохранения файла'], 500);
        }
    }


    public function saveSignedFileOrdinary2(Request $request, $a, User $user, $agreement_id)
    {
        $signedFile = $request->file('signed_file');

        $file_name = $request->input('file_name');

        $fileName = $request->input('file_name').'_2'.'.sig';
        
        // Сохраняем файл в указанной папке
        switch ($a) {
            case 'agreements':
                $signedFile->move(public_path() .'/files/agreements/signed2/', $fileName);
                break;
            case 'selected_agreements':
                $signedFile->move(public_path() .'/files/selected_agreements/signed2/', $fileName);
                break;
            default:
                $signedFile->move(public_path() .'/files/agreements/signed2/', $fileName);
                break;
        }
        
        if ($signedFile) {
            switch ($a) {
                case 'agreements':
                    $agreements = Agreement::where('filename', $file_name)->get();
                    foreach ($agreements as $agreement) {
                        $agreement->filename_sign2 = $fileName;
                        $agreement->sign2 = 1;
                        $agreement->save();
                    }
                    break;
                case 'selected_agreements':
                    $agreements = SelectedAgreement::where('filename', $file_name)->get();
                    foreach ($agreements as $agreement) {
                        $agreement->filename_sign2 = $fileName;
                        $agreement->sign2 = 1;
                        $agreement->save();
                    }
                    break;
            }

            return redirect('/');
        } else {
            return response()->json(['message' => 'Ошибка сохранения файла'], 500);
        }
    }
}

