<?php

namespace App\Http\Controllers;

use App\Agreement;
use App\Student;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class AgreementController extends Controller
{
    public function index($id)
    {
        $student = Student::where('id', $id)->first();

        return view('agreement', compact('student'));
    }

    public function add(Request $request, $id)
    {
        if($request->hasFile('agreement')) {
            $file = $request->file('agreement');
            $file_name = $id . time() . '.' . $request->agreement->extension();
            $file->move(public_path() . '/files/agreements/', $file_name);

            $agreement = new Agreement([
                'filename' => $file_name,
                'student_id' => $id,
            ]);

            $agreement->status = 1;

            $agreement->save();

            return redirect('/')->with('success', 'Договор добавлен!');
        }
    }

    public function delete(Agreement $agreement)
    {
        $agreement->delete();

        return redirect('/');
    }

    // public function approve(Agreement $agreement)
    // {
    //     $agreement->status = 1;
    //     $agreement->save();

    //     return redirect('/');
    // }

    public function downloadAllAgreements($orgId, $year, $main_or_not, $one_or_two, $archive_or_not)
    {
        $yearRange = explode('-', $year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        if ($archive_or_not == 'not') {
            $archive = null;
        } else {
            $archive = 'not_null';
        }

        if($one_or_two == '1'){
            $one_or_two = '';
        }

        $agreements = DB::table('agreements_pupils'.$one_or_two)
            ->join('selected_schedules', 'agreements_pupils'.$one_or_two.'.selected_schedule_id', '=', 'selected_schedules.id')
            ->join('selected_programs', 'selected_schedules.selected_program_id', '=', 'selected_programs.id')
            ->join('proposed_programs', 'selected_programs.proposed_program_id', '=', 'proposed_programs.id')
            ->join('users', 'selected_programs.school_id', '=', 'users.id')
            ->where('proposed_programs.user_id', $orgId)
            ->when($archive === null, function ($query) {
                return $query->whereNull('proposed_programs.archive');
            }, function ($query) {
                return $query->whereNotNull('proposed_programs.archive');
            })
            ->where('proposed_programs.educational_program', $main_or_not)
            ->whereBetween('proposed_programs.date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->where('users.status', 7)
            // ->where('users.not_student', '!=', 1)
            ->distinct('agreements_pupils'.$one_or_two.'.filename')
            ->get('agreements_pupils'.$one_or_two.'.filename');

        $zipname = 'agreements_' . now()->format('Ymd_His_u') . '.zip';
        $zipPath = tempnam(sys_get_temp_dir(), 'zip');

        $zip = new \ZipArchive();
        if ($zip->open($zipPath, \ZipArchive::CREATE) !== TRUE) {
            return response()->json(['error' => 'Не удалось создать архив'], 500);
        }

        foreach ($agreements as $agreement) {
            $filePath = public_path('files/agreements_pupils' . $one_or_two . '/' . $agreement->filename);
            $filePath2 = public_path('files/agreements_pupils' . $one_or_two . '/signed/' . $agreement->filename . '.sig');
            $filePath3 = public_path('files/agreements_pupils' . $one_or_two . '/signed2/' . $agreement->filename . '_.sig');
            if (file_exists($filePath)) {
                $zip->addFile($filePath, basename($filePath));
                $zip->addFile($filePath2, basename($filePath2));
                $zip->addFile($filePath3, basename($filePath3));
            } else {
                return response()->json(['error' => 'Файл ' . $filePath . ' не найден'], 404);
            }
        }

        $zip->close();

        try {
            return response()->download($zipPath, $zipname)->deleteFileAfterSend(true);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Ошибка при скачивании файла: ' . $e->getMessage()], 500);
        }
    }


    public function downloadAgreementsZip(User $user, $main_or_not, $org_type)
    {
        $proposed_programs = $user->proposed_programs_main_or_not($main_or_not);
        $uniquePairs = [];

        foreach ($proposed_programs as $proposed_program) {
            foreach ($proposed_program->selected_programs() as $selected_program) {
                if ($selected_program->user->status == 7 && $selected_program->status == 1 && $selected_program->not_student != 1) {
                    $pairKey = $proposed_program->subject . '-' . $selected_program->user->getSchool->fullname;
                    if (!isset($uniquePairs[$pairKey])) {
                        $uniquePairs[$pairKey] = [
                            'proposed_program' => $proposed_program,
                            'selected_program' => $selected_program,
                        ];
                    }
                }
            }
        }

        // Создаем временный файл для ZIP-архива
        $zipname = $user->id. '.zip';
        $zipPath = tempnam(sys_get_temp_dir(), 'zip');

        $zip = new \ZipArchive();

        if ($zip->open($zipPath, \ZipArchive::CREATE) !== TRUE) {
            return response()->json(['error' => 'Не удалось создать архив'], 500);
        }

        $agreementController = new AgreementDocumentController_for_zip();

        foreach ($uniquePairs as $pair) {
            $schoolId = $pair['selected_program']->user->getSchool->id;
            $selectedProgramId = $pair['selected_program']->id;

            // Генерируем договор
            if($org_type == 'prof'){
                $agreementContent = $agreementController->create_document_agreement_selected4($pair['selected_program']->user->getSchool, $pair['selected_program']->id);
            }elseif($org_type == 'vuz'){
                // var_dump($pair['selected_program']->user->getSchool->id);
                $agreementContent = $agreementController->create_document_agreement_selected_vsu($pair['selected_program']->user->getSchool, $pair['selected_program']->id);
            }

            // Создаем временный файл для договора
            $tempFilePath = tempnam(sys_get_temp_dir(), 'agreement');
            file_put_contents($tempFilePath, $agreementContent);

            // Добавляем файл в ZIP-архив
            $zip->addFile($tempFilePath, $pair['selected_program']->user->getSchool->fullname . '_' . $pair['proposed_program']->subject . '.docx');

            // Удаляем временный файл
            // unlink($tempFilePath);

            // Сохраняем путь к временному файлу для последующего удаления
            $tempFilePaths[] = $tempFilePath;
        }

        $zip->close();

        foreach ($tempFilePaths as $tempFilePath) {
            unlink($tempFilePath);
        }

        // Возвращаем ZIP-архив для скачивания
        try {
            return response()->download($zipPath, $zipname)->deleteFileAfterSend(true);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Ошибка при скачивании файла: ' . $e->getMessage()], 500);
        }
    }
}
