<?php

namespace App\Http\Controllers;

use App\AgreementPupil;
use App\AgreementPupil2;
use App\Proposed;
use App\SelectedSchedule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgreementPupilController extends Controller
{
    public function index($id, $grade_number, $grade_letter, User $user)
    {
        $selected_schedule = SelectedSchedule::where('id', $id)->first();

        return view('pupils/agreement_pupil', compact(['selected_schedule', 'grade_number', 'grade_letter', 'user']));
    }

    public function add(Request $request, $id, $grade_number, $grade_letter, User $user)
    {
        $user = Auth::user();

        if($request->hasFile('agreement_pupil')) {
            $file = $request->file('agreement_pupil');
            $file_name = $id . time() . '.' . $request->agreement_pupil->extension();
            $file->move(public_path() . '/files/agreements_pupils/', $file_name);

            $agreement_pupil = new AgreementPupil([
                'filename' => $file_name,
                'selected_schedule_id' => $id,
            ]);

            $agreement_pupil->status = 1;

            $agreement_pupil->save();

            $pupils = User::all()
            ->where('status', '7')
            ->where('org', $user->id)
            ->where('grade_number', $grade_number)
            ->where('grade_letter', $grade_letter);

            // return redirect('/pupils/'.$user->id.'/list-of-grades');
            return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
        }
    }

    public function add_many(Request $request, $id, $grade_number, $grade_letter, User $user)
    {
        $user_vuz = Auth::user();

        if($request->hasFile('agreement_pupil')) {
            $file = $request->file('agreement_pupil');
            $file_name = $id . time() . '.' . $request->agreement_pupil->extension();
            $file->move(public_path() . '/files/agreements_pupils/', $file_name);

            $selected_schedule_filename = SelectedSchedule::where('id', $id)->first()->filename;
            $selected_schedules = SelectedSchedule::where('filename', $selected_schedule_filename)->get();
            $proposed_program_id = SelectedSchedule::where('id', $id)->first()->selected_program->proposed_program->id;
            $proposed_program = Proposed::where('id', $proposed_program_id)->first();
            
            foreach ($selected_schedules as $selected_schedule) {
                if ($selected_schedule->selected_program->user->org == $user->id) {
                    $selected_schedule_id = $selected_schedule->id;
                    $agreement_pupil = new AgreementPupil([
                        'filename' => $file_name,
                        'selected_schedule_id' => $selected_schedule_id,
                    ]);

                    $agreement_pupil->status = 1;

                    $agreement_pupil->save();
                }
            }

            // $pupils = User::all()
            // ->where('status', '7')
            // ->where('org', $user->id)
            // ->where('grade_number', $grade_number)
            // ->where('grade_letter', $grade_letter);

            // return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
            // return redirect('/pupils-vuz/'.$user_vuz->id.'/'.$proposed_program_id.'/list-of-pupils');
            // return redirect('/pupils/' . $user_vuz->id . '/' . $grade_number . '/' . $grade_letter . '/list-of-pupils');
            $user = Auth::user();
            if($user->org_type == 'prof' or $user->org_type == 'vuz'){
                return view('pupils/list_of_pupils_vuz', compact(['user', 'proposed_program']));
            }else{
                $pupils = User::all()
                ->where('status', '7')
                ->where('org', $user->id)
                ->where('grade_number', $grade_number)
                ->where('grade_letter', $grade_letter);
                return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
            }
        }
    }

    public function delete(AgreementPupil $agreement_pupil, $user_id, $grade_number, $grade_letter)
    {
        $agreement_pupil->delete();

        // return redirect()->back();
        return redirect('/pupils/' . $user_id . '/' . $grade_number . '/' . $grade_letter . '/list-of-pupils');
    }

    public function delete_many(Request $request, $filename, $user_id, $grade_number, $grade_letter)
    {
        $selected_schedules = AgreementPupil::where('filename', $filename)->delete();

        return redirect('/pupils/' . $user_id . '/' . $grade_number . '/' . $grade_letter . '/list-of-pupils');
    }

    // -------------------------------------------------------------------------------------------------------------

    public function index2($id, $grade_number, $grade_letter, User $user)
    {
        $selected_schedule = SelectedSchedule::where('id', $id)->first();

        return view('pupils/agreement_pupil2', compact(['selected_schedule', 'grade_number', 'grade_letter', 'user']));
    }

    public function add2(Request $request, $id, $grade_number, $grade_letter, User $user)
    {
        $user = Auth::user();

        if($request->hasFile('agreement_pupil')) {
            $file = $request->file('agreement_pupil');
            $file_name = $id . time() . '.' . $request->agreement_pupil->extension();
            $file->move(public_path() . '/files/agreements_pupils2/', $file_name);

            $agreement_pupil = new AgreementPupil2([
                'filename' => $file_name,
                'selected_schedule_id' => $id,
            ]);

            $agreement_pupil->status = 1;

            $agreement_pupil->save();

            $pupils = User::all()
            ->where('status', '7')
            ->where('org', $user->id)
            ->where('grade_number', $grade_number)
            ->where('grade_letter', $grade_letter);

            return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
        }
    }

    public function add_many2(Request $request, $id, $grade_number, $grade_letter, User $user)
    {
        $user_vuz = Auth::user();

        if($request->hasFile('agreement_pupil')) {
            $file = $request->file('agreement_pupil');
            $file_name = $id . time() . '.' . $request->agreement_pupil->extension();
            $file->move(public_path() . '/files/agreements_pupils2/', $file_name);

            $selected_schedule_filename = SelectedSchedule::where('id', $id)->first()->filename;
            $selected_schedules = SelectedSchedule::where('filename', $selected_schedule_filename)->get();
            $proposed_program_id = SelectedSchedule::where('id', $id)->first()->selected_program->proposed_program->id;
            $proposed_program = Proposed::where('id', $proposed_program_id)->first();

            foreach ($selected_schedules as $selected_schedule) {
                if ($selected_schedule->selected_program->user->org == $user->id) {
                    $selected_schedule_id = $selected_schedule->id;
                    $agreement_pupil = new AgreementPupil2([
                        'filename' => $file_name,
                        'selected_schedule_id' => $selected_schedule_id,
                    ]);

                    $agreement_pupil->status = 1;

                    $agreement_pupil->save();
                }
            }

            // $pupils = User::all()
            // ->where('status', '7')
            // ->where('org', $user->id)
            // ->where('grade_number', $grade_number)
            // ->where('grade_letter', $grade_letter);
            
            // return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
            // return redirect('/pupils-vuz/'.$user_vuz->id.'/'.$proposed_program_id.'/list-of-pupils');
            // return redirect('/pupils/' . $user_vuz->id . '/' . $grade_number . '/' . $grade_letter . '/list-of-pupils');
            $user = Auth::user();
            if($user->org_type == 'prof' or $user->org_type == 'vuz'){
                return view('pupils/list_of_pupils_vuz', compact(['user', 'proposed_program']));
            }else{
                $pupils = User::all()
                ->where('status', '7')
                ->where('org', $user->id)
                ->where('grade_number', $grade_number)
                ->where('grade_letter', $grade_letter);
                return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
            }
        }
    }

    public function delete2(AgreementPupil2 $agreement_pupil, $user_id, $grade_number, $grade_letter)
    {
        $agreement_pupil->delete();

        // return redirect()->back();
        return redirect('/pupils/' . $user_id . '/' . $grade_number . '/' . $grade_letter . '/list-of-pupils');
    }

    public function delete_many2(Request $request, $filename, $user_id, $grade_number, $grade_letter)
    {
        $selected_schedules = AgreementPupil2::where('filename', $filename)->delete();

        return redirect('/pupils/' . $user_id . '/' . $grade_number . '/' . $grade_letter . '/list-of-pupils');
    }

    // -------------------------------------------------------------------------------------------------------------
    public function approve($filename, $grade_number, $grade_letter)
    {
        $agreements = AgreementPupil::where('filename', $filename)->get();
        foreach($agreements as $agreement) {
            $agreement->agree = 1;
            $agreement->save();
        }
        
        $user = Auth::user();
        $pupils = User::all()
            ->where('status', '7')
            ->where('org', $user->id)
            ->where('grade_number', $grade_number)
            ->where('grade_letter', $grade_letter);

        return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
    }

    public function approve2($filename, $grade_number, $grade_letter)
    {
        $agreements = AgreementPupil2::where('filename', $filename)->get();
        foreach($agreements as $agreement) {
            $agreement->agree = 1;
            $agreement->save();
        }
        
        $user = Auth::user();
        $pupils = User::all()
            ->where('status', '7')
            ->where('org', $user->id)
            ->where('grade_number', $grade_number)
            ->where('grade_letter', $grade_letter);

        return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
    }

}