<?php

namespace App\Http\Controllers;

use App\SelectedProgram;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class SelectedProgramController extends Controller
{
    public function take(Request $request, $id)
    {
        $id_of_user = Auth::user()->id;
        $user_of_program = User::where('id', $id_of_user)->first();
        $school_id = $user_of_program->id;

        $selected_program = new SelectedProgram([
            'proposed_program_id' => $id,
            'school_id' => $school_id,
        ]);

        $selected_program->save();

        return redirect('/')->with('success', 'Программа взята!');
    }


    public function show(SelectedProgram $selected_program)
    {
        return view('proposed.selected_show', compact('selected_program'));
    }

    public function delete(SelectedProgram $selected_program)
    {
        $selected_program->delete();

        return redirect('/');
    }


    public function plus($id)
    {
        $selected_program = SelectedProgram::where('id', $id)->first();

        $selected_program->status = 1;

        $selected_program->save();
        
        // return Redirect::back();
        // return redirect()->route('list_of_grades');
        return redirect('/');
    }

    public function minus($id)
    {
        $selected_program = SelectedProgram::where('id', $id)->first();

        $selected_program->status = 2;

        $selected_program->save();
        
        // return Redirect::back();
        return redirect('/');
    }

    public function show_pupil(SelectedProgram $selected_program)
    {
        return view('pupils.selected_show', compact('selected_program'));
    }

    public function show_pupil_school(SelectedProgram $selected_program)
    {
        return view('pupils.selected_school_show', compact('selected_program'));
    }

    public function student_not(SelectedProgram $selected_program, $grade_number, $grade_letter, User $user)
    {
        $selected_program->update(['not_student' => 1]);

        $pupils = User::all()
        ->where('status', '7')
        ->where('org', $user->id)
        ->where('grade_number', $grade_number)
        ->where('grade_letter', $grade_letter);

        return view('pupils/list_of_pupils', compact(['user', 'grade_letter', 'grade_number', 'pupils']));
    }

}
