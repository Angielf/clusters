<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Export;
use App\Exports\Export2;
use App\ExportLowR;
use App\Exports\ExportLowR2;
use App\ExportLowF;
use App\Exports\ExportLowF2;
use App\MonthsHoursExport;
use App\Exports\MonthsHoursExport2;
use App\SelectedMonthsHoursExport;
use App\MHExport;
use App\SelectedMHExport;
use App\ProposedExport;
use App\SelectedProgramsExport;
use App\Pupils\SelectedProgramsPupilsExport;
use App\Pupils\SelectedProgramsNotArchivePupilsExport;
use App\Pupils\SelectedProgramsProfPupilsExport;
use App\Pupils\SelectedProgramsProfNotArchivePupilsExport;
use Maatwebsite\Excel\Facades\Excel;

use App\Exports\admin\ExportYear;
use App\Exports\admin\ExportLowRYear;
use App\Exports\admin\ExportLowFYear;
use App\Exports\admin\ExportProposedYear;
use App\Exports\admin\ExportSelectedYear;
use App\Exports\admin\ExportAllYear;
use App\Exports\admin\ExportAllYearSchool;
use App\Exports\admin\ExportProfPupilsNotArchiveYear;
use App\Exports\admin\ExportProfPupilsArchiveYear;
use App\Exports\admin\ExportProfPupilsNotArchiveYear2;
use App\Exports\admin\ExportProfPupilsArchiveYear2;
use App\Exports\admin\ExportProfPupilsProposedYear;


use App\Exports\admin\ExportAmountOfStudents;
use App\Exports\admin\ExportAmountOfStudents2;

class ExcelRegionController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {
        return Excel::download(new Export, 'bids.xlsx');
    }

    public function export2(Request $request)
    {
        return Excel::download(new Export2($request->archive), 'bids_'.$request->archive.'.xlsx');
    }

    public function export_not_archive()
    {
        return Excel::download(new Export2(NULL), 'bids_not_archives.xlsx');
    }

    public function months_hours_export()
    {
        return Excel::download(new MonthsHoursExport, 'months_hours.xlsx');
    }

    public function months_hours_export_archive(Request $request)
    {
        return Excel::download(new MonthsHoursExport2($request->archive), 'months_hours_'.$request->archive.'.xlsx');
    }

    public function months_hours_export_not_archive()
    {
        return Excel::download(new MonthsHoursExport2(NULL), 'months_hours_not_archive.xlsx');
    }

    public function m_h_export()
    {
        return Excel::download(new MHExport, 'hours_per_months.xlsx');
    }

    public function proposed_programs_export()
    {
        return Excel::download(new ProposedExport, 'proposed_programs_export.xlsx');
    }

    public function selected_programs_export()
    {
        return Excel::download(new SelectedProgramsExport, 'selected_programs_export.xlsx');
    }

    public function selected_months_hours_export()
    {
        return Excel::download(new SelectedMonthsHoursExport, 'seleted_months_hours.xlsx');
    }

    public function selected_m_h_export()
    {
        return Excel::download(new SelectedMHExport, 'selected_hours_per_months.xlsx');
    }


    public function export_low_r()
    {
        return Excel::download(new ExportLowR, 'low_r.xlsx');
    }

    public function export_low_r_archive(Request $request)
    {
        return Excel::download(new ExportLowR2($request->archive), 'low_r_'.$request->archive.'.xlsx');
    }

    public function export_low_r_not_archive()
    {
        return Excel::download(new ExportLowR2(NULL), 'low_r_not_archive.xlsx');
    }

    public function export_low_f()
    {
        return Excel::download(new ExportLowF, 'low_f.xlsx');
    }

    public function export_low_f_archive(Request $request)
    {
        return Excel::download(new ExportLowF2($request->archive), 'low_f_'.$request->archive.'.xlsx');
    }

    public function export_low_f_not_archive()
    {
        return Excel::download(new ExportLowF2(NULL), 'low_f_not_archive.xlsx');
    }


    public function selected_programs_pupils_export()
    {
        return Excel::download(new SelectedProgramsPupilsExport, 'selected_programs_pupils_export.xlsx');
    }

    public function selected_programs_not_archive_pupils_export()
    {
        return Excel::download(new SelectedProgramsNotArchivePupilsExport, 'selected_programs_not_archive_pupils_export.xlsx');
    }

    public function selected_programs_pupils_export_prof()
    {
        return Excel::download(new SelectedProgramsProfPupilsExport, 'selected_programs_pupils_export_prof.xlsx');
    }

    public function selected_programs_not_archive_pupils_export_prof()
    {
        return Excel::download(new SelectedProgramsProfNotArchivePupilsExport, 'selected_programs_not_archive_pupils_export_prof.xlsx');
    }


    // ---------------------------------------------------------------------------
    public function export_year(Request $request)
    {
        return Excel::download(new ExportYear($request->year), 'bids_'.$request->year.'.xlsx');
    }

    public function export_low_r_year(Request $request)
    {
        return Excel::download(new ExportLowRYear($request->year), 'bids_low_r_'.$request->year.'.xlsx');
    }

    public function export_low_f_year(Request $request)
    {
        return Excel::download(new ExportLowFYear($request->year), 'bids_low_f_'.$request->year.'.xlsx');
    }

    public function export_proposed_programs_year(Request $request)
    {
        return Excel::download(new ExportProposedYear($request->year), 'proposed_'.$request->year.'.xlsx');
    }

    public function export_selected_programs_year(Request $request)
    {
        return Excel::download(new ExportSelectedYear($request->year), 'selected_'.$request->year.'.xlsx');
    }

    public function export_all_year(Request $request)
    {
        return Excel::download(new ExportAllYear($request->year), 'bids_programs_'.$request->year.'.xlsx');
    }

    public function export_all_year_school(Request $request)
    {
        return Excel::download(new ExportAllYearSchool($request->year), 'bids_programs_school_'.$request->year.'.xlsx');
    }

    public function export_pupils_not_archive_year(Request $request)
    {
        return Excel::download(new ExportProfPupilsNotArchiveYear($request->year, $request->orgtype), 'export_pupils_not_archive_main_'.($request->year).'.xlsx');
    }

    public function showPupilsNotArchive($year, $orgtype)
    {
        $initialData = (new ExportProfPupilsNotArchiveYear($year, $orgtype))->collection()->slice(0, 100)->toArray();
        return view('clusters.admin.pupils_not_archive', compact(['initialData', 'year', 'orgtype']));
    }

    public function loadMorePupils(Request $request, $year, $orgtype)
    {
        $offset = $request->input('offset', 0);
        $limit = 100; // Количество записей для загрузки

        $data = (new ExportProfPupilsNotArchiveYear($year, $orgtype))->collection()->slice($offset, $limit);

        return response()->json($data);
    }

    public function export_pupils_archive_year(Request $request)
    {
        return Excel::download(new ExportProfPupilsArchiveYear($request->year, $request->orgtype), 'export_pupils_archive_main_'.($request->year).'.xlsx');
    }

    public function export_pupils_not_archive_year2(Request $request)
    {
        return Excel::download(new ExportProfPupilsNotArchiveYear2($request->year, $request->orgtype), 'export_pupils_not_archive_add_'.($request->year).'.xlsx');
    }

    public function export_pupils_archive_year2(Request $request)
    {
        return Excel::download(new ExportProfPupilsArchiveYear2($request->year, $request->orgtype), 'export_pupils_archive_add_'.($request->year).'.xlsx');
    }

    public function export_pupils_proposed_year(Request $request)
    {
        return Excel::download(new ExportProfPupilsProposedYear($request->year, $request->orgtype), 'export_pupils_proposed_'.($request->year).'.xlsx');
    }

    // --------------------------------------------------------------------------------------------

    public function export_amount_of_students_year(Request $request)
    {
        return Excel::download(new ExportAmountOfStudents($request->year, $request->orgtype), 'amount_of_students_'.$request->year.'.xlsx');
    }

    public function export_amount_of_students_year2(Request $request)
    {
        return Excel::download(new ExportAmountOfStudents2($request->year, $request->orgtype), 'amount_of_students_school'.$request->year.'.xlsx');
    }
}
