<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Export_mun;
use App\Exports\Export_mun2;
use App\MonthsHoursExport_mun;
use App\Exports\MonthsHoursExport_mun2;
use App\MHExport_mun;
use App\SelectedMonthsHoursExport_mun;
use App\SelectedMHExport_mun;
use App\ProposedExport_mun;
use App\SelectedProgramsExport_mun;
use App\ExportLowRMun;
use App\Exports\ExportLowRMun2;
use App\ExportLowFMun;
use App\Exports\ExportLowFMun2;
use App\Pupils\SelectedProgramsMunPupilsExport;
use App\Pupils\SelectedProgramsMunNotArchivePupilsExport;
use App\Pupils\SelectedProgramsProfMunPupilsExport;
use App\Pupils\SelectedProgramsProfMunNotArchivePupilsExport;
use Maatwebsite\Excel\Facades\Excel;

use App\Exports\admin\mun\ExportMunYear;
use App\Exports\admin\mun\ExportLowRMunYear;
use App\Exports\admin\mun\ExportLowFMunYear;
use App\Exports\admin\mun\ExportProposedMunYear;
use App\Exports\admin\mun\ExportSelectedMunYear;
use App\Exports\admin\mun\ExportAllMunYear;
use App\Exports\admin\mun\ExportProfPupilsNotArchiveMunYear;
use App\Exports\admin\mun\ExportProfPupilsArchiveMunYear;
use App\Exports\admin\mun\ExportProfPupilsNotArchiveMunYear2;
use App\Exports\admin\mun\ExportProfPupilsArchiveMunYear2;
use App\Exports\admin\mun\ExportProfPupilsProposedMunYear;

class ExcelMunController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export()
    {

        return Excel::download(new Export_mun, 'bids_mun.xlsx');

    }

    public function export2(Request $request)
    {
        return Excel::download(new Export_mun2($request->archive), 'bids_mun_'.$request->archive.'.xlsx');
    }

    public function export_not_archive()
    {
        return Excel::download(new Export_mun2(NULL), 'bids_mun_not_archives.xlsx');
    }

    public function months_hours_export()
    {
        return Excel::download(new MonthsHoursExport_mun, 'months_hours_mun.xlsx');
    }

    public function months_hours_export_archive(Request $request)
    {
        return Excel::download(new MonthsHoursExport_mun2($request->archive), 'months_hours_mun_'.$request->archive.'.xlsx');
    }

    public function months_hours_export_not_archive()
    {
        return Excel::download(new MonthsHoursExport_mun2(NULL), 'months_hours_mun_not_archive.xlsx');
    }

    public function m_h_export()
    {
        return Excel::download(new MHExport_mun, 'hours_per_months_mun.xlsx');
    }

    public function proposed_programs_export()
    {
        return Excel::download(new ProposedExport_mun, 'proposed_programs_export_mun.xlsx');
    }

    public function selected_programs_export()
    {
        return Excel::download(new SelectedProgramsExport_mun, 'selected_programs_export_mun.xlsx');
    }

    public function selected_months_hours_export()
    {
        return Excel::download(new SelectedMonthsHoursExport_mun, 'selected_months_hours_mun.xlsx');
    }

    public function selected_m_h_export()
    {
        return Excel::download(new SelectedMHExport_mun, 'selected_hours_per_months_mun.xlsx');
    }

    public function export_low_r()
    {
        return Excel::download(new ExportLowRMun, 'low_r_mun.xlsx');
    }

    public function export_low_r_archive(Request $request)
    {
        return Excel::download(new ExportLowRMun2($request->archive), 'low_r_mun_'.$request->archive.'.xlsx');
    }

    public function export_low_r_not_archive()
    {
        return Excel::download(new ExportLowRMun2(NULL), 'low_r_mun_not_archive.xlsx');
    }

    public function export_low_f()
    {
        return Excel::download(new ExportLowFMun, 'low_f_mun.xlsx');
    }

    public function export_low_f_archive(Request $request)
    {
        return Excel::download(new ExportLowFMun2($request->archive), 'low_f_mun_'.$request->archive.'.xlsx');
    }

    public function export_low_f_not_archive()
    {
        return Excel::download(new ExportLowFMun2(NULL), 'low_f_mun_not_archive.xlsx');
    }


    public function selected_programs_pupils_export()
    {
        return Excel::download(new SelectedProgramsMunPupilsExport, 'selected_programs_pupils_export.xlsx');
    }

    public function selected_programs_not_archive_pupils_export()
    {
        return Excel::download(new SelectedProgramsMunNotArchivePupilsExport, 'selected_programs_not_archive_pupils_export.xlsx');
    }

    public function selected_programs_pupils_export_prof()
    {
        return Excel::download(new SelectedProgramsProfMunPupilsExport, 'selected_programs_pupils_export_prof.xlsx');
    }

    public function selected_programs_not_archive_pupils_export_prof()
    {
        return Excel::download(new SelectedProgramsProfMunNotArchivePupilsExport, 'selected_programs_not_archive_pupils_export_prof.xlsx');
    }


    // ---------------------------------------------------------------------------
    public function export_mun_year(Request $request)
    {
        return Excel::download(new ExportMunYear($request->year, $request->district_id), 'bids_'.$request->year.'.xlsx');
    }

    public function export_low_r_mun_year(Request $request)
    {
        return Excel::download(new ExportLowRMunYear($request->year, $request->district_id), 'bids_low_r_'.$request->year.'.xlsx');
    }

    public function export_low_f_mun_year(Request $request)
    {
        return Excel::download(new ExportLowFMunYear($request->year, $request->district_id), 'bids_low_f_'.$request->year.'.xlsx');
    }

    public function export_proposed_programs_mun_year(Request $request)
    {
        return Excel::download(new ExportProposedMunYear($request->year, $request->district_id), 'proposed_'.$request->year.'.xlsx');
    }

    public function export_selected_programs_mun_year(Request $request)
    {
        return Excel::download(new ExportSelectedMunYear($request->year, $request->district_id), 'selected_'.$request->year.'.xlsx');
    }

    public function export_all_mun_year(Request $request)
    {
        return Excel::download(new ExportAllMunYear($request->year, $request->district_id), 'bids_programs_'.$request->year.'.xlsx');
    }

    public function export_pupils_not_archive_mun_year(Request $request)
    {
        return Excel::download(new ExportProfPupilsNotArchiveMunYear($request->year, $request->orgtype, $request->district_id), 'export_pupils_not_archive_main_'.($request->year).'.xlsx');
    }

    public function export_pupils_archive_mun_year(Request $request)
    {
        return Excel::download(new ExportProfPupilsArchiveMunYear($request->year, $request->orgtype, $request->district_id), 'export_pupils_archive_main_'.($request->year).'.xlsx');
    }

    public function export_pupils_not_archive_mun_year2(Request $request)
    {
        return Excel::download(new ExportProfPupilsNotArchiveMunYear2($request->year, $request->orgtype, $request->district_id), 'export_pupils_not_archive_add_'.($request->year).'.xlsx');
    }

    public function export_pupils_archive_mun_year2(Request $request)
    {
        return Excel::download(new ExportProfPupilsArchiveMunYear2($request->year, $request->orgtype, $request->district_id), 'export_pupils_archive_add_'.($request->year).'.xlsx');
    }

    public function export_pupils_proposed_mun_year(Request $request)
    {
        return Excel::download(new ExportProfPupilsProposedMunYear($request->year, $request->orgtype, $request->district_id), 'export_pupils_proposed_'.($request->year).'.xlsx');
    }

}
