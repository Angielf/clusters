<?php

namespace App\Http\Controllers;

use App\SelectedProgram;
use App\SelectedSchedule;
use Illuminate\Http\Request;

class SelectedScheduleController extends Controller
{
    public function index($id)
    {
        $selected_program = SelectedProgram::where('id', $id)->first();

        return view('proposed.selected_schedule', compact('selected_program'));
    }

    public function add(Request $request, $id)
    {
        if($request->hasFile('selected_schedule')) {
            $file = $request->file('selected_schedule');
            $file_name = $id . time() . '.' . $request->selected_schedule->extension();
            $file->move(public_path() . '/files/selected_schedules/', $file_name);

            $selected_schedule = new SelectedSchedule([
                'status' => 0,
                'filename' => $file_name,
                'selected_program_id' => $id,
            ]);

            $selected_schedule->save();

            return redirect('/')->with('success', 'Расписание добавлено!');
        }
    }

    public function add_many(Request $request, $id, $proposed_program_id, $vuz_id)
    {
        if ($request->isMethod('post')) {
            if($request->hasFile('selected_schedule')) {
                $file = $request->file('selected_schedule');
                $file_name = $id . time() . '.' . $request->selected_schedule->extension();
                $file->move(public_path() . '/files/selected_schedules/', $file_name);

                $selected_program = SelectedProgram::where('id', $id)->first();
                $selected_ids = $selected_program->proposed_program->selected_ids();

                foreach ($selected_ids as $v) {
                    $selected_schedule = new SelectedSchedule([
                        'status' => 1,
                        'filename' => $file_name,
                        'selected_program_id' => $v['id'],
                    ]);
        
                    $selected_schedule->save();
                }


                return redirect('/pupils-vuz/' . $vuz_id . '/' . $proposed_program_id . '/list-of-pupils');
            }
        }

        $selected_program = SelectedProgram::where('id', $id)->first();

        return view('proposed.selected_schedule_many', compact(['selected_program', 'proposed_program_id', 'vuz_id']));
    }

    public function delete_many(Request $request, $filename, $vuz_id, $program_id)
    {
        $selected_schedules = SelectedSchedule::where('filename', $filename)->delete();

        return redirect('/pupils-vuz/' . $vuz_id . '/' . $program_id . '/list-of-pupils');
    }

    public function delete(SelectedSchedule $selected_schedule)
    {
        $selected_schedule->delete();

        return redirect('/');
    }

    public function approve(SelectedSchedule $selected_schedule)
    {
        $selected_schedule->status = 1;
        $selected_schedule->save();

        return redirect('/');
    }
}
