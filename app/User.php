<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'inn', 'address', 'tel', 'email_real', 'website', 'low',
        'kpp', 'ogrn', 'bik', 'single_treasury_account', 'treasury_account', 'bank',
        'director_post', 'director_post_rod', 'director', 'director_rod',
        'fullname_full', 'number_license', 'date_license', 'who_license', 'opf_short',
        'okpo', 'oktmo', 'status_exist', 
        'fullname', 'status', 'org', 'grade_number', 'grade_letter', 'district',
        'org_type', 'zone', 'snils', 'birth', 'kbk', 'ikz', 'punkt',
        'treasury_account_type', 'single_treasury_account_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getDistrict()
    {
        return $this->belongsTo(District::class, 'district' );
    }

    public function bids()
    {
        if ($bids = Bid::where('user_id', $this->id)->WhereNull('rc_cluster_id')->get()) {
            return $bids;
        } else return false;

    }

    public function regionBids()
    {
        if ($bids = Bid::where('user_id', $this->id)->WhereNotNull('rc_cluster_id')->get()) {
            return $bids;
        } else return false;

    }

    public function cluster()
    {
        return $this->hasOne(Cluster::class);
    }

    public function getClusters()
    {
        $clusters = Cluster::all();
        foreach ($clusters as $cluster) {
            $schools = json_decode($cluster->schools, true);
            foreach ($schools as $value) {
                if ($value['school_id'] == $this->id) {
                    return $cluster->User->fullname;
                }
            }
        }
        return "Вы не состоите в муниципальных кластерах";
    }


    // public function sender() {
    //     return $this->belongsTo('Program', 'school_program_id');
    // }

    public function programs_send()
    {
        if ($programs = Program::where('school_program_id', $this->id)->get()) {
            return $programs;
        } else return false;

    }

    public function programs_1_send()
    {
        if ($programs = Program::where('school_program_id', $this->id)->where('status', 1)->get()) {
            return $programs;
        } else return false;

    }

    public function months_hours_send()
    {
        if ($months_hours = MonthsHour::where('school_schedule_id', $this->id)->get()) {
            return $months_hours;
        } else return false;

    }

    public function selected_months_hours_send()
    {
        if ($selected_months_hours = SelectedMonthsHour::where('school_selected_schedule_id', $this->id)->get()) {
            return $selected_months_hours;
        } else return false;

    }

    public function amount_of_bids()
    {
        $i = 0;
        if($this->bids() !== false){
            $i = $this->bids()->count();
        }
        return $i;
    }

    public function amount_of_programs_1()
    {
        $i = 0;
        if($this->programs_1_send() !== false){
            $i = $this->programs_1_send()->count();
        }
        return $i;
    }

    public function amount_of_months_hours()
    {
        $i = 0;
        if($this->months_hours_send() !== false){
            $i = $this->months_hours_send()->count();
        }
        return $i;
    }


    public function amount_estimated_per($j)
    {
        $i = 0;
        if($this->months_hours_send() !== false){
            foreach($this->months_hours_send() as $mh){
                if($mh->month_1 == $j){
                    $i += intval($mh->estimated_1);
                }
                elseif($mh->month_2 == $j){
                    $i += intval($mh->estimated_2);
                }
                elseif($mh->month_3 == $j){
                    $i += intval($mh->estimated_3);
                }
                elseif($mh->month_4 == $j){
                    $i += intval($mh->estimated_4);
                }
                elseif($mh->month_5 == $j){
                    $i += intval($mh->estimated_5);
                }
                elseif($mh->month_6 == $j){
                    $i += intval($mh->estimated_6);
                }
                elseif($mh->month_7 == $j){
                    $i += intval($mh->estimated_7);
                }
                elseif($mh->month_8 == $j){
                    $i += intval($mh->estimated_8);
                }
                elseif($mh->month_9 == $j){
                    $i += intval($mh->estimated_9);
                }
                elseif($mh->month_10 == $j){
                    $i += intval($mh->estimated_10);
                }
                elseif($mh->month_11 == $j){
                    $i += intval($mh->estimated_11);
                }
                elseif($mh->month_12 == $j){
                    $i += intval($mh->estimated_12);
                }
            }
        }
        return $i;
    }


    public function amount_real_per($j)
    {
        $i = 0;
        if($this->months_hours_send() !== false){
            foreach($this->months_hours_send() as $mh){
                if(($mh->month_1 == $j) and ($mh->status_1 == 1)){
                    $i += intval($mh->real_1);
                }
                elseif(($mh->month_2 == $j) and ($mh->status_2 == 1)){
                    $i += intval($mh->real_2);
                }
                elseif(($mh->month_3 == $j) and ($mh->status_3 == 1)){
                    $i += intval($mh->real_3);
                }
                elseif(($mh->month_4 == $j) and ($mh->status_4 == 1)){
                    $i += intval($mh->real_4);
                }
                elseif(($mh->month_5 == $j) and ($mh->status_5 == 1)){
                    $i += intval($mh->real_5);
                }
                elseif(($mh->month_6 == $j) and ($mh->status_6 == 1)){
                    $i += intval($mh->real_6);
                }
                elseif(($mh->month_7 == $j) and ($mh->status_7 == 1)){
                    $i += intval($mh->real_7);
                }
                elseif(($mh->month_8 == $j) and ($mh->status_8 == 1)){
                    $i += intval($mh->real_8);
                }
                elseif(($mh->month_9 == $j) and ($mh->status_9 == 1)){
                    $i += intval($mh->real_9);
                }
                elseif(($mh->month_10 == $j) and ($mh->status_10 == 1)){
                    $i += intval($mh->real_10);
                }
                elseif(($mh->month_11 == $j) and ($mh->status_11 == 1)){
                    $i += intval($mh->real_11);
                }
                elseif(($mh->month_12 == $j) and ($mh->status_12 == 1)){
                    $i += intval($mh->real_12);
                }
            }
        }
        return $i;
    }


    public function proposed_programs()
    {
        if ($proposed_programs = Proposed::where('user_id', $this->id)->where('archive', NULL)->get()) {
            return $proposed_programs;
        } else return false;

    }

    public function proposed_programs_main_or_not($main_or_not)
    {
        if($main_or_not == 'main'){
            $main_or_not = 'основная';
        }else{
            $main_or_not = 'дополнительная';
        }

        if ($proposed_programs = Proposed::where('user_id', $this->id)->where('educational_program', $main_or_not)
            ->where('archive', NULL)->get()) {
            return $proposed_programs;
        } else return false;

    }

    public function proposed_programs_main_year($year)
    {
        $yearRange = explode('-', $year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        if ($proposed_programs = Proposed::where('user_id', $this->id)->where('educational_program', 'основная')
            ->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()])->get()) {
            return $proposed_programs;
        } else return false;

    }

    public function proposed_programs_archive($year)
    {
        if ($proposed_programs = Proposed::where('user_id', $this->id)->where('archive', $year)->get()) {
            return $proposed_programs;
        } else return false;

    }

    public function selected_programs()
    {
        if ($selected_programs = SelectedProgram::where('school_id', $this->id)->get()) {
            return $selected_programs;
        } else return false;

    }

    public function selected_programs_not_archive()
    {
        if ($selected_programs = SelectedProgram::where('school_id', $this->id)->get()) {
            $notArchivedPrograms = array();
            foreach ($selected_programs as $selected_program) {
                if ($selected_program->proposed_program->archive == NULL) {
                    array_push($notArchivedPrograms, $selected_program);
                }
            }
            return $notArchivedPrograms;
        } else return false;
    }

    public function selected_programs_archive($year)
    {
        if ($selected_programs = SelectedProgram::where('school_id', $this->id)->get()) {
            $notArchivedPrograms = array();
            foreach ($selected_programs as $selected_program) {
                if ($selected_program->proposed_program->archive == $year) {
                    array_push($notArchivedPrograms, $selected_program);
                }
            }
            return $notArchivedPrograms;
        } else return false;
    }

    public function selected_programs_this_year()
    {
        $this_year = date('Y');
        $this_august = date('Y-m-d', strtotime($this_year . '-08-01'));
        $previous_year = date('Y', strtotime('-1 year'));
        $previous_august = date('Y-m-d', strtotime($previous_year . '-08-01'));
        if ($selected_programs = SelectedProgram::where('school_id', $this->id)->where('created_at', '<=', $this_august)->where('created_at', '>=', $previous_august)->orderBy('created_at')->get()) {
            return $selected_programs;
        } else return false;

    }


    public function amount_of_selected_programs()
    {
        $i = 0;
        if($this->selected_programs() !== false){
            $i = $this->selected_programs()->count();
        }
        return $i;
    }

    public function amount_of_proposed_programs()
    {
        $i = 0;
        if($this->proposed_programs() !== false){
            $i = $this->proposed_programs()->count();
        }
        return $i;
    }



    public function amount_of_selected_months_hours()
    {
        $i = 0;
        if($this->selected_months_hours_send() !== false){
            $i = $this->selected_months_hours_send()->count();
        }
        return $i;
    }


    public function amount_selected_estimated_per($j)
    {
        $i = 0;
        if($this->selected_months_hours_send() !== false){
            foreach($this->selected_months_hours_send() as $mh){
                if($mh->month_1 == $j){
                    $i += intval($mh->estimated_1);
                }
                elseif($mh->month_2 == $j){
                    $i += intval($mh->estimated_2);
                }
                elseif($mh->month_3 == $j){
                    $i += intval($mh->estimated_3);
                }
                elseif($mh->month_4 == $j){
                    $i += intval($mh->estimated_4);
                }
                elseif($mh->month_5 == $j){
                    $i += intval($mh->estimated_5);
                }
                elseif($mh->month_6 == $j){
                    $i += intval($mh->estimated_6);
                }
                elseif($mh->month_7 == $j){
                    $i += intval($mh->estimated_7);
                }
                elseif($mh->month_8 == $j){
                    $i += intval($mh->estimated_8);
                }
                elseif($mh->month_9 == $j){
                    $i += intval($mh->estimated_9);
                }
                elseif($mh->month_10 == $j){
                    $i += intval($mh->estimated_10);
                }
                elseif($mh->month_11 == $j){
                    $i += intval($mh->estimated_11);
                }
                elseif($mh->month_12 == $j){
                    $i += intval($mh->estimated_12);
                }
            }
        }
        return $i;
    }


    public function amount_selected_real_per($j)
    {
        $i = 0;
        if($this->selected_months_hours_send() !== false){
            foreach($this->selected_months_hours_send() as $mh){
                if(($mh->month_1 == $j) and ($mh->status_1 == 1)){
                    $i += intval($mh->real_1);
                }
                elseif(($mh->month_2 == $j) and ($mh->status_2 == 1)){
                    $i += intval($mh->real_2);
                }
                elseif(($mh->month_3 == $j) and ($mh->status_3 == 1)){
                    $i += intval($mh->real_3);
                }
                elseif(($mh->month_4 == $j) and ($mh->status_4 == 1)){
                    $i += intval($mh->real_4);
                }
                elseif(($mh->month_5 == $j) and ($mh->status_5 == 1)){
                    $i += intval($mh->real_5);
                }
                elseif(($mh->month_6 == $j) and ($mh->status_6 == 1)){
                    $i += intval($mh->real_6);
                }
                elseif(($mh->month_7 == $j) and ($mh->status_7 == 1)){
                    $i += intval($mh->real_7);
                }
                elseif(($mh->month_8 == $j) and ($mh->status_8 == 1)){
                    $i += intval($mh->real_8);
                }
                elseif(($mh->month_9 == $j) and ($mh->status_9 == 1)){
                    $i += intval($mh->real_9);
                }
                elseif(($mh->month_10 == $j) and ($mh->status_10 == 1)){
                    $i += intval($mh->real_10);
                }
                elseif(($mh->month_11 == $j) and ($mh->status_11 == 1)){
                    $i += intval($mh->real_11);
                }
                elseif(($mh->month_12 == $j) and ($mh->status_12 == 1)){
                    $i += intval($mh->real_12);
                }
            }
        }
        return $i;
    }

    public function bids_archives($year)
    {
        if ($bids = Bid::where('user_id', $this->id)->where('archive', $year)->get()) {
            return $bids;
        } else return false;

    }

    public function programs_archives($year)
    {
        if ($programs = Program::where('school_program_id', $this->id)->where('status', 1)->get()) {
            $archive_programs = array();
            foreach($programs as $program){
                if($program->bid->archive == $year){
                    array_push($archive_programs, $program);
                }
            }
            return $archive_programs;
        } else return false;

    }

    public function bids_not_archives()
    {
        if ($bids = Bid::where('user_id', $this->id)->WhereNull('archive')->get()) {
            return $bids;
        } else return false;

    }

    public function programs_not_archives()
    {
        if ($programs = Program::where('school_program_id', $this->id)->where('status', 1)->get()) {
            $archive_programs = array();
            foreach($programs as $program){
                if($program->bid->archive == NULL){
                    array_push($archive_programs, $program);
                }
            }
            return $archive_programs;
        } else return false;

    }

    // -----------------------------------------------------------------------------------------
    public function getSchool()
    {
        return $this->belongsTo(User::class, 'org' );
    }

    // -----------------------------------------------------------------------------------------

    public function proposed_programs_archives($year)
    {
        if ($bids = Proposed::where('user_id', $this->id)->where('archive', $year)->get()) {
            return $bids;
        } else return false;

    }

    public function get_short_fullname()
    {
        return mb_substr($this->fullname, 0, 15).'...';
    }

    public function get_org_type(){

        switch ($this->org_type) {
            case 'school': return 'общеобразовательная организация';
            case 'prof': return 'профессиональная образовательная организация';
            case 'vuz': return 'ВУЗ';
            case 'doo': return 'дошкольная образовательная организация';
            case 'inter': return 'интернат';
            case 'dop': return 'организация дополнительного образования';
        }
    }


    function getAmountOfStudentsMainProf($year) {
        $prof_id = $this->id;

        $proposed_programs = $this->proposed_programs_main_year($year);

        $selected_programs = SelectedProgram::where('status', 1)
            ->where('not_student', NULL)
            ->whereHas('proposed_program', function ($query) use ($prof_id) {
                $query->where('educational_program', 'основная')
                    ->where('user_id', $prof_id);
            })
            ->whereIn('proposed_program_id', $proposed_programs->pluck('id'))
            ->get();


        if ($selected_programs) {
            return $selected_programs->count();
        } else return 0;
    }

    function getAmountOfStudentsInSchoolPerProgram($pp_id, $year) {
        $school_id = $this->id;

        $yearRange = explode('-', $year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        if ($selected_programs = SelectedProgram::where('proposed_program_id', $pp_id)
            ->where('status', 1)
            ->where('not_student', NULL)
            ->whereHas('proposed_program', function ($query) use ($startDate, $endDate) {
                $query->where('educational_program', 'основная')
                    ->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()]);
            })
            ->whereHas('user', function ($query) use ($school_id) {
                $query->where('org', $school_id);
            })
            ->get()) {
            return $selected_programs->count();
        } else return 0;
    }

    function getAmountOfStudentsInSchool9($year) {
        $school_id = $this->id;

        $yearRange = explode('-', $year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        if ($selected_programs = SelectedProgram::where('status', 1)
            ->where('not_student', NULL)
            ->whereHas('proposed_program', function ($query) use ($startDate, $endDate) {
                $query->where('educational_program', 'основная')
                    ->where('class', 'like', '%s:1:"9"%')
                    ->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()]);
            })
            ->whereHas('user', function ($query) use ($school_id) {
                $query->where('org', $school_id);
            })
            ->get()) {
            return $selected_programs->count();
        } else return 0;
    }

    function getAmountOfStudentsInSchool10_11($year) {
        $school_id = $this->id;

        $yearRange = explode('-', $year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        if ($selected_programs = SelectedProgram::where('status', 1)
            ->where('not_student', NULL)
            ->whereHas('proposed_program', function ($query) use ($startDate, $endDate) {
                $query->where('educational_program', 'основная')
                        ->where(function ($query) {
                            $query->where('class', 'like', '%s:2:"10"%')
                                ->orWhere('class', 'like', '%s:2:"11"%');
                        })
                    ->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()]);
            })
            ->whereHas('user', function ($query) use ($school_id) {
                $query->where('org', $school_id);
            })
            ->get()) {
            return $selected_programs->count();
        } else return 0;
    }

}
