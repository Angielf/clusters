<?php
namespace App;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;

class ExportLowFMun implements FromView
{
    public function view(): View
    {
        return view('clusters.export_low_r_f', [
            $district = Auth::user()->getDistrict->id,
            'users' => User::where('low', 'f')->where('district', $district)->get()
        ]);
    }
}
