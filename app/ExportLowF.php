<?php
namespace App;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportLowF implements FromView
{
    public function view(): View
    {
        return view('clusters.export_low_r_f', [
            'users' => User::where('low', 'f')->get()
        ]);
    }
}
