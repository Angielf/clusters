<?php
namespace App\Exports;

use App\MonthsHour;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MonthsHoursExport_mun2 implements FromView
{
    protected $archive;

    function __construct($archive)
    {
        $this->archive = $archive;
    }

    public function view(): View
    {
        return view('archives_mun.months_hours_list_mun', [
            'months_hours' => MonthsHour::get(),
            'year' => $this->archive,
        ]);
    }
}
