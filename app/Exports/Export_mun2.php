<?php
namespace App\Exports;

use App\Bid;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class Export_mun2 implements FromView
{
    protected $archive;

    function __construct($archive)
    {
        $this->archive = $archive;
    }

    public function view(): View
    {
        return view('export_mun', [
            'bids' => Bid::where('archive', $this->archive)->get(),
        ]);
    }
}
