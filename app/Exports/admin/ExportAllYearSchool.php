<?php

namespace App\Exports\admin;

ini_set('memory_limit', '1024M');

use App\SelectedProgram;
use App\Bid;
use App\Proposed;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportAllYearSchool implements FromCollection, WithHeadings
{
    protected $year;

    function __construct($year)
    {
        $this->year = $year;
    }

    public function collection()
    {
        $yearRange = explode('-', $this->year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        $proposed_programs_all = Proposed::whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->whereHas('user', function ($query) {
                $query->where('org_type', 'school');
            })
            ->get()->sortBy('date_begin')->sortBy('date_end');

        $selected_programs = SelectedProgram::whereHas('proposed_program', function ($query) use ($startDate, $endDate) {
            $query->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()])
                ->whereHas('user', function ($query) {
                    $query->where('org_type', 'school');
                });
        })->get();

        $bids = Bid::whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->whereHas('user', function ($query) {
                $query->where('org_type', 'school');
            })
            ->get()->sortBy('date_begin')->sortBy('date_end');

        $array = $proposed_programs_all->map(function ($b, $key) {

            return [
                "type" => 'Программа',

                "district_b" => $b->user->getDistrict->fullname,
                "fullname_b" => $b->user->fullname,
                "inn_b" => $b->user->inn,
                "type_b" => $b->user->get_org_type(),

                "district_r" => '',
                "fullname_r" => '',
                "inn_r" => '',
                "type_r" => '',

                "class" => $b->getClasses(),
                "amount_of_classes" => '',
                "subject" => $b->subject,
                "modul" => $b->modul,
                "hours" => $b->hours,
                "form_of_education" => $b->form_of_education,
                "form_education_implementation" => $b->form_education_implementation,
                "EducationalPrograms" => $b->educational_program,
                "EducationalActivities" => $b->educational_activity,
                "content" => $b->content,

                "date_begin" => $b->getDataBegin(),
                "date_end" => $b->getDataEnd(),
                "archive" => $b->archive,

                "program" => $b->filename,

                "status" => ($b->cancel_registration == 1) ? 'Регистрация завершена' : '',

                "schedule" => '',
                "student_amount" => '',
                "student_filename" => '',
                "agreement_filename" => '',
                "agreement_filename_sign1" => '',
                "agreement_filename_sign2" => '',
            ];
        });

        $array1 = $selected_programs->map(function ($b, $key) {

             return [
                "type" => ' Выбрвнная программа', 

                "district_b" => $b->proposed_program->user->getDistrict->fullname,
                "fullname_b" => $b->proposed_program->user->org == '' ? $b->proposed_program->user->fullname : '',
                "inn_b" => $b->proposed_program->user->inn,
                "type_b" => $b->proposed_program->user->get_org_type(),

                "district_r" => $b->user->getDistrict->fullname,
                "fullname_r" => $b->user->org == '' ? $b->user->fullname : '',
                "inn_r" => $b->user->inn,
                "type_r" => $b->user->get_org_type(),

                "class" => $b->proposed_program->getClasses(),
                "amount_of_classes" => '',
                "subject" => $b->proposed_program->subject,
                "modul" => $b->proposed_program->modul,
                "hours" => $b->proposed_program->hours,
                "form_of_education" => $b->proposed_program->form_of_education,
                "form_education_implementation" => $b->proposed_program->form_education_implementation,
                "EducationalPrograms" => $b->proposed_program->educational_program,
                "EducationalActivities" => $b->proposed_program->educational_activity,
                "content" => $b->proposed_program->content,

                "date_begin" => $b->proposed_program->getDataBegin(),
                "date_end" => $b->proposed_program->getDataEnd(),
                "archive" => $b->archive,

                "program" => $b->proposed_program->filename,

                "status" => ($b->proposed_program->cancel_registration == 1) ? 'Регистрация завершена' : '',

                "schedule" => ($b->selected_schedule && $b->selected_schedule->status === 1) ?
                    $b->selected_schedule->filename : '',
                
                "student_amount" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student) ?
                    $b->selected_schedule->selected_student->students_amount : '',

                "student_filename" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student) ?
                    $b->selected_schedule->selected_student->filename : '',

                "agreement_filename" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student && $b->selected_schedule->selected_student->selected_agreement) ?
                    $b->selected_schedule->selected_student->selected_agreement->filename : '',

                "agreement_filename_sign1" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student && $b->selected_schedule->selected_student->selected_agreement && $b->selected_schedule->selected_student->selected_agreement->sign == 1) ?
                    $b->selected_schedule->selected_student->selected_agreement->filename_sign : '',

                "agreement_filename_sign2" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student && $b->selected_schedule->selected_student->selected_agreement && $b->selected_schedule->selected_student->selected_agreement->sign2 == 1) ?
                    $b->selected_schedule->selected_student->selected_agreement->filename_sign2 : '',
             ];
        });


        $array2 = $bids->map(function ($b, $key) {

            return [
                "type" => 'Заявка',

                "district_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->getDistrict->fullname : '',
                "fullname_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->fullname : '',
                "inn_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->inn : '',
                "type_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->get_org_type() : '',

                "district_r" => $b->user->getDistrict->fullname,
                "fullname_r" => $b->user->fullname,
                "inn_r" => $b->user->inn,
                "type_r" => $b->user->get_org_type(),

                "class" => $b->getClasses(),
                "amount_of_classes" => $b->amount_of_classes,
                "subject" => $b->subject,
                "modul" => $b->modul,
                "hours" => $b->hours,
                "form_of_education" => $b->form_of_education,
                "form_education_implementation" => $b->form_education_implementation,
                "EducationalPrograms" => $b->getEducationalPrograms(),
                "EducationalActivities" => $b->getEducationalActivities(),
                "content" => $b->content,

                "date_begin" => $b->getDataBegin(),
                "date_end" => $b->getDataEnd(),
                "archive" => $b->archive,

                "program" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->filename : '',

                "status" => $b->getStatus2(),

                "schedule" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->filename : '',

                "students_amount" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->students_amount : '',

                "student_filename" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->filename : '',


                "agreement_filename" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename : '',
                
                "agreement_filename_sign1" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign : '',
                
                "agreement_filename_sign2" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 : '',

               ];
        });

        $mergedArray = $array->merge($array1)->merge($array2);

        return $mergedArray;
    }

    public function headings(): array
    {
        return [
            'Тип',

            'Муниципалитет организации-участника',
            'Организация-участник', 
            'ИНН базовой организации',
            'Тип базовой организации',

            'Муниципалитет базовой организации',
            'Базовая организация', 
            'ИНН базовой организации',
            'Тип базовой организации',

            'Класс',
            'Количество классов(групп)',
            'Предмет(курс)',
            'Раздел(модуль)',
            'Кол-во часов',
            'Форма реализации обучения',
            'Условия реализации обучения',
            'Образовательная программа',
            'Образовательная деятельность',
            'Комментарий',
            'Начало',
            'Конец',
            'Архив',
            'Программа',

            'Статус',

            'Расписание',
            'Кол-во учеников',
            'Список учеников',
            'Договор',
            'Подпись организации-участника',
            'Подпись базовой организации',
        ];
    }
}