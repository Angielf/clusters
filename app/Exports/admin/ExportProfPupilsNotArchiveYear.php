<?php

namespace App\Exports\admin;

use App\SelectedProgram;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportProfPupilsNotArchiveYear implements FromCollection, WithHeadings
{
    protected $year;
    protected $orgtype;

    function __construct($year, $orgtype)
    {
        $this->year = $year;
        $this->orgtype = $orgtype;
    }

    public function collection()
    {
        $yearRange = explode('-', $this->year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        $data = collect(); // Создаем коллекцию для хранения результатов

        SelectedProgram::with(['proposed_program.user.getDistrict', 'user.getSchool.getDistrict']) 
            ->whereHas('proposed_program', function ($query) use ($startDate, $endDate) { 
                $query->where('educational_program', 'основная') 
                    ->whereNull('archive') 
                    ->whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')]) 
                    ->whereHas('user', function ($query) { 
                        $query->where('org_type', $this->orgtype); 
                    }); 
            }) 
            ->whereHas('user', function ($query) {
                $query->where('org', '!=', '');
            })
            ->whereNull('not_student')
            ->chunk(100, function ($programs) use ($data) { 
                foreach ($programs as $b) { 
                    $user = $b->proposed_program->user;
                    $school = $b->user->getSchool;
                    $proposedProgram = $b->proposed_program;
                    $districtB = optional($user->getDistrict)->fullname ?? '';
                    $districtS = optional($school->getDistrict)->fullname ?? '';

                    $data->push([ 
                        "district_b" => $districtB, 
                        "fullname_b" => $user->org == '' ? $user->fullname : '', 
                        "inn_b" => optional($user)->inn ?? '', 
                        "district_s" => $districtS, 
                        "fullname_s" => optional($b->user)->fullname ?? '', 
                        "org_s" => optional($school)->fullname ?? '', 
                        "inn_s" => optional($school)->inn ?? '', 
                        "class_s" => ($b->user->grade_number ?? '') . ' ' . ($b->user->grade_letter ?? ''), 
                        "date_s" => $b->created_at, 
                        "birth_s" => optional($b->user)->birth ?? '', 
                        "snils_s" => optional($b->user)->snils ?? '', 
                        "class" => $proposedProgram->getClasses(), // Используем переменную proposedProgram
                        "subject" => $proposedProgram->subject, 
                        "modul" => $proposedProgram->modul, 
                        "hours" => $proposedProgram->hours, 
                        "form_of_education" => $proposedProgram->form_of_education, 
                        "form_education_implementation" => $proposedProgram->form_education_implementation, 
                        "EducationalPrograms" => $proposedProgram->educational_program, 
                        "EducationalActivities" => $proposedProgram->educational_activity, 
                        "content" => $proposedProgram->content, 
                        "date_begin" => $proposedProgram->getDataBegin(), 
                        "date_end" => $proposedProgram->getDataEnd(), 
                        "archive" => $b->archive, 
                        "program" => $proposedProgram->filename, 
                        "registr" => $proposedProgram->cancel_registration == 1 ? 'Регистрация завершена' : '', 
                        "status" => $this->getStatus($b->status), 
                        "schedule" => $b->selected_schedule && $b->selected_schedule->status === 1 ? $b->selected_schedule->filename : '', 
                        "agreement_filename" => $this->getAgreementFilename($b->selected_schedule, 'agreement_pupil'), 
                        "agreement_filename_sign" => $this->getAgreementFilenameSign($b->selected_schedule, 'agreement_pupil'), 
                        "agreement_filename_sign2" => $this->getAgreementFilenameSign2($b->selected_schedule, 'agreement_pupil'), 
                        "agreement_filename_2" => $this->getAgreementFilename($b->selected_schedule, 'agreement_pupil2'), 
                        "agreement_filename_sign_2" => $this->getAgreementFilenameSign($b->selected_schedule, 'agreement_pupil2'), 
                        "agreement_filename_sign2_2" => $this->getAgreementFilenameSign2($b->selected_schedule, 'agreement_pupil2'), 
                    ]); 
                } 
            }); 

        return $data; // Возвращаем собранные данные
    }

    private function getStatus($status)
    {
        switch ($status) {
            case 0:
                return 'Рассмотрение';
            case 1:
                return 'Одобрена';
            case 2:
                return 'Отклонена';
            default:
                return '';
        }
    }

    private function getAgreementFilename($schedule, $agreementType)
    {
        return $schedule && $schedule->status === 1 && $schedule->$agreementType ? $schedule->$agreementType->filename : '';
    }

    private function getAgreementFilenameSign($schedule, $agreementType)
    {
        return $schedule && $schedule->status === 1 && $schedule->$agreementType && $schedule->$agreementType->sign == 1 ? $schedule->$agreementType->filename_sign : '';
    }

    private function getAgreementFilenameSign2($schedule, $agreementType)
    {
        return $schedule && $schedule->status === 1 && $schedule->$agreementType && $schedule->$agreementType->sign2 == 1 ? $schedule->$agreementType->filename_sign2 : '';
    }
    

    public function headings(): array
    {
        return [
            'Муниципалитет организации-участника',
            'Организация-участник', 
            'ИНН организации-участника',

            'Муниципалитет базовой организации',
            'Ученик',
            'Базовая организация',
            'ИНН базовой организации',
            'Класс ученика',
            'Дата выбора программы',
            'Дата рождения',
            'СНИЛС',

            'Класс',
            'Предмет(курс)',
            'Раздел(модуль)',
            'Кол-во часов',
            'Форма реализации обучения',
            'Условия реализации обучения',
            'Образовательная программа',
            'Образовательная деятельность',
            'Комментарий',
            'Начало',
            'Конец',
            'Архив',
            'Программа',

            'Регистрация',
            'Статус',

            'Расписание',

            'Договор',
            'Подпись базовой организации',
            'Подпись организации-участника',

            'Договор2',
            'Подпись базовой организации',
            'Подпись организации-участника',
        ];
    }
}