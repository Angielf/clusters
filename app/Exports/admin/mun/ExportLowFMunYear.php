<?php

namespace App\Exports\admin\mun;

use App\Bid;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportLowFMunYear implements FromCollection, WithHeadings
{
    protected $year;
    protected $district_id;

    function __construct($year, $district_id)
    {
        $this->year = $year;
        $this->district_id = $district_id;
    }

    public function collection()
    {
        $yearRange = explode('-', $this->year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        $bids = Bid::whereHas('user', function ($query) {
            $query->where('district', $this->district_id)->where('low', 'f');
        })
        ->whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get()
        ->sortBy('date_begin')->sortBy('date_end');

        $array = $bids->map(function ($b, $key) {

             return [
                "district_r" => $b->user->getDistrict->fullname,
                "fullname_r" => $b->user->fullname,
                "inn_r" => $b->user->inn,
                "type_r" => $b->user->get_org_type(),

                "district_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->getDistrict->fullname : '',
                "fullname_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->fullname : '',
                "inn_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->inn : '',
                "type_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->get_org_type() : '',

                "class" => $b->getClasses(),
                "amount_of_classes" => $b->amount_of_classes,
                "subject" => $b->subject,
                "modul" => $b->modul,
                "hours" => $b->hours,
                "form_of_education" => $b->form_of_education,
                "form_education_implementation" => $b->form_education_implementation,
                "EducationalPrograms" => $b->getEducationalPrograms(),
                "EducationalActivities" => $b->getEducationalActivities(),

                "date_begin" => $b->getDataBegin(),
                "date_end" => $b->getDataEnd(),
                "archive" => $b->archive,

                "content" => $b->content,

                "program" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->filename : '',

                "schedule" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->filename : '',

                "students_amount" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->students_amount : '',

                "students" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->filename : '',


                "agreement" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename : '',
                
                "sign1" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign : '',
                
                "sign2" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 : '',

                ];
            });

        return $array;


    }

    public function headings(): array
    {
        return [
            'Муниципалитет базовой организации',
            'Базовая организация', 
            'ИНН базовой организации',
            'Тип базовой организации',
            
            'Муниципалитет организации-участника',
            'Организация-участник', 
            'ИНН базовой организации',
            'Тип базовой организации',

            'Класс',
            'Количество классов(групп)',
            'Предмет(курс)',
            'Раздел(модуль)',
            'Кол-во часов',
            'Форма',
            'Условия реализации обучения',
            'Образовательная программа',
            'Образовательная деятельность',
            'Начало',
            'Конец',
            'Архив',
            'Комментарий',
            'Программа',

            'Расписание',
            'Количество учеников',
            'Список учеников',
            'Договор',
            'Подпись базовой организации',
            'Подпись организации-участника',
        ];
    }
}