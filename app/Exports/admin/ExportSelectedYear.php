<?php

namespace App\Exports\admin;

ini_set('memory_limit', '1024M');

use App\SelectedProgram;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportSelectedYear implements FromCollection, WithHeadings
{
    protected $year;

    function __construct($year)
    {
        $this->year = $year;
    }

    public function collection()
    {
        $yearRange = explode('-', $this->year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        $selected_programs = SelectedProgram::whereHas('proposed_program', function ($query) use ($startDate, $endDate) {
            $query->whereBetween('date_begin', [$startDate->toDateString(), $endDate->toDateString()]);
        })->get();

        $array = $selected_programs->map(function ($b, $key) {

             return [
                "district_b" => $b->proposed_program->user->getDistrict->fullname,
                "fullname_b" => $b->proposed_program->user->org == '' ? $b->proposed_program->user->fullname : '',
                "inn_b" => $b->proposed_program->user->inn,
                "type_b" => $b->proposed_program->user->get_org_type(),

                "district" => $b->user->getDistrict->fullname,
                "fullname_r" => $b->user->org == '' ? $b->user->fullname : '',
                "inn_r" => $b->user->inn,
                "type_r" => $b->user->get_org_type(),

                "fullname_s" => ($b->user->org != '' and $b->not_student == '') ? $b->user->fullname : '',
                "org_s" =>($b->user->org != '' and $b->not_student == '') ? $b->user->getSchool->fullname  : '',
                "class_s" =>($b->user->org != '' and $b->not_student == '') ? $b->user->grade_number . ' ' . $b->user->grade_letter : '',

                "class" => $b->proposed_program->getClasses(),
                "subject" => $b->proposed_program->subject,
                "modul" => $b->proposed_program->modul,
                "hours" => $b->proposed_program->hours,
                "form_of_education" => $b->proposed_program->form_of_education,
                "form_education_implementation" => $b->proposed_program->form_education_implementation,
                "EducationalPrograms" => $b->proposed_program->educational_program,
                "EducationalActivities" => $b->proposed_program->educational_activity,
                "content" => $b->proposed_program->content,

                "date_begin" => $b->proposed_program->getDataBegin(),
                "date_end" => $b->proposed_program->getDataEnd(),
                "archive" => $b->archive,

                "program" => $b->proposed_program->filename,

                "status" => ($b->proposed_program->cancel_registration == 1) ? 'Регистрация завершена' : '',

                "schedule" => ($b->selected_schedule && $b->selected_schedule->status === 1) ?
                    $b->selected_schedule->filename : '',
                
                "student_amount" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student) ?
                    $b->selected_schedule->selected_student->students_amount : '',

                "student_filename" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student) ?
                    $b->selected_schedule->selected_student->filename : '',

                "agreement_filename" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student && $b->selected_schedule->selected_student->selected_agreement) ?
                    $b->selected_schedule->selected_student->selected_agreement->filename : '',

                "agreement_filename_sign" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student && $b->selected_schedule->selected_student->selected_agreement && $b->selected_schedule->selected_student->selected_agreement->sign == 1) ?
                    $b->selected_schedule->selected_student->selected_agreement->filename_sign : '',

                "agreement_filename_sign2" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->selected_student && $b->selected_schedule->selected_student->selected_agreement && $b->selected_schedule->selected_student->selected_agreement->sign2 == 1) ?
                    $b->selected_schedule->selected_student->selected_agreement->filename_sign2 : '',
             ];
            });

        return $array;


    }

    public function headings(): array
    {
        return [
            'Муниципалитет организации-участника',
            'Организация-участник', 
            'ИНН организации-участника',
            'Тип организации-участника',

            'Муниципалитет базовой организации',
            'Базовая организация', 
            'ИНН базовой организации',
            'Тип базовой организации',

            'Ученик',
            'Организация ученика',
            'Класс ученика',

            'Класс',
            'Предмет(курс)',
            'Раздел(модуль)',
            'Кол-во часов',
            'Форма реализации обучения',
            'Условия реализации обучения',
            'Образовательная программа',
            'Образовательная деятельность',
            'Комментарий',
            'Начало',
            'Конец',
            'Архив',
            'Программа',

            'Статус',

            'Расписание',
            'Кол-во учеников',
            'Список учеников',
            'Договор',
            'Подпись организации-участника',
            'Подпись базовой организации',
        ];
    }
}