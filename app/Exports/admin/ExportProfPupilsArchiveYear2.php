<?php

namespace App\Exports\admin;

use App\SelectedProgram;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportProfPupilsArchiveYear2 implements FromCollection, WithHeadings
{
    protected $year;
    protected $orgtype;

    function __construct($year, $orgtype)
    {
        $this->year = $year;
        $this->orgtype = $orgtype;
    }

    public function collection()
    {
        $yearRange = explode('-', $this->year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        $selected_programs_main_archive = SelectedProgram::whereHas('proposed_program', function ($query) use ($startDate, $endDate) {
            $query->where('educational_program', 'дополнительная')
                ->where('archive', '!=', NULL)
                ->whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
                ->whereHas('user', function ($query) {
                    $query->where('org_type', $this->orgtype);
                });
        })->get();

        $array = $selected_programs_main_archive->map(function ($b, $key) {
            if ($b->user->org != '' and $b->not_student == '') {

                return [
                    "district_b" => $b->proposed_program->user->getDistrict->fullname,
                    "fullname_b" => $b->proposed_program->user->org == '' ? $b->proposed_program->user->fullname : '',
                    "inn_b" => $b->proposed_program->user->inn,

                    "district_s" => ($b->user->org != '' and $b->not_student == '') ? $b->user->getSchool->getDistrict->fullname : '',
                    "fullname_s" => ($b->user->org != '' and $b->not_student == '') ? $b->user->fullname : '',
                    "org_s" =>($b->user->org != '' and $b->not_student == '') ? $b->user->getSchool->fullname  : '',
                    "inn_s" =>($b->user->org != '' and $b->not_student == '') ? $b->user->getSchool->inn  : '',
                    "class_s" =>($b->user->org != '' and $b->not_student == '') ? $b->user->grade_number . ' ' . $b->user->grade_letter : '',

                    "class" => $b->proposed_program->getClasses(),
                    "subject" => $b->proposed_program->subject,
                    "modul" => $b->proposed_program->modul,
                    "hours" => $b->proposed_program->hours,
                    "form_of_education" => $b->proposed_program->form_of_education,
                    "form_education_implementation" => $b->proposed_program->form_education_implementation,
                    "EducationalPrograms" => $b->proposed_program->educational_program,
                    "EducationalActivities" => $b->proposed_program->educational_activity,
                    "content" => $b->proposed_program->content,

                    "date_begin" => $b->proposed_program->getDataBegin(),
                    "date_end" => $b->proposed_program->getDataEnd(),
                    "archive" => $b->archive,

                    "program" => $b->proposed_program->filename,

                    "registr" => ($b->proposed_program->cancel_registration == 1) ? 'Регистрация завершена' : '',
                    "status" => ($b->status == 0) ? 'Рассмотрение' : (($b->status == 1) ? 'Одобрена' : (($b->status == 2) ? 'Отклонена' : '')),

                    "schedule" => ($b->selected_schedule && $b->selected_schedule->status === 1) ?
                        $b->selected_schedule->filename : '',
                    
                    "agreement_filename" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->agreement_pupil) ?
                        $b->selected_schedule->agreement_pupil->filename : '',

                    "agreement_filename_sign" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->agreement_pupil && $b->selected_schedule->agreement_pupil->sign == 1) ?
                        $b->selected_schedule->agreement_pupil->filename_sign : '',

                    "agreement_filename_sign2" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->agreement_pupil && $b->selected_schedule->agreement_pupil->sign2 == 1) ?
                        $b->selected_schedule->agreement_pupil->filename_sign2 : '',


                    "agreement_filename_2" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->agreement_pupil2) ?
                        $b->selected_schedule->agreement_pupil2->filename : '',

                    "agreement_filename_sign_2" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->agreement_pupil2 && $b->selected_schedule->agreement_pupil2->sign == 1) ?
                        $b->selected_schedule->agreement_pupil2->filename_sign : '',

                    "agreement_filename_sign2_2" => ($b->selected_schedule && $b->selected_schedule->status === 1 && $b->selected_schedule->agreement_pupil2 && $b->selected_schedule->agreement_pupil2->sign2 == 1) ?
                        $b->selected_schedule->agreement_pupil2->filename_sign2 : '',
                ];
            }
            return null;
        })->filter();

        return $array;


    }

    public function headings(): array
    {
        return [
            'Муниципалитет организации-участника',
            'Организация-участник', 
            'ИНН организации-участника',

            'Муниципалитет базовой организации',
            'Ученик',
            'Базовая организация',
            'ИНН базовой организации',
            'Класс ученика',

            'Класс',
            'Предмет(курс)',
            'Раздел(модуль)',
            'Кол-во часов',
            'Форма реализации обучения',
            'Условия реализации обучения',
            'Образовательная программа',
            'Образовательная деятельность',
            'Комментарий',
            'Начало',
            'Конец',
            'Архив',
            'Программа',

            'Регистрация',
            'Статус',

            'Расписание',

            'Договор',
            'Подпись базовой организации',
            'Подпись организации-участника',

            'Договор2',
            'Подпись базовой организации',
            'Подпись организации-участника',
        ];
    }
}