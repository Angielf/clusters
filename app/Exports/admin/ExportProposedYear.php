<?php

namespace App\Exports\admin;

use App\Proposed;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportProposedYear implements FromCollection, WithHeadings
{
    protected $year;

    function __construct($year)
    {
        $this->year = $year;
    }

    public function collection()
    {
        $yearRange = explode('-', $this->year);
        $startDate = now()->setDate($yearRange[0], 9, 1);
        $endDate = now()->setDate($yearRange[1], 8, 31);

        $proposed_programs_all = Proposed::whereBetween('date_begin', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get()->sortBy('date_begin')->sortBy('date_end');

        $array = $proposed_programs_all->map(function ($b, $key) {

             return [
                "district_b" => $b->user->getDistrict->fullname,
                "fullname_b" => $b->user->fullname,
                "inn_b" => $b->user->inn,
                "type_b" => $b->user->get_org_type(),

                "class" => $b->getClasses(),
                "subject" => $b->subject,
                "modul" => $b->modul,
                "hours" => $b->hours,
                "form_of_education" => $b->form_of_education,
                "form_education_implementation" => $b->form_education_implementation,
                "EducationalPrograms" => $b->educational_program,
                "EducationalActivities" => $b->educational_activity,
                "content" => $b->content,

                "date_begin" => $b->getDataBegin(),
                "date_end" => $b->getDataEnd(),
                "archive" => $b->archive,

                "program" => $b->filename,

                "status" => ($b->cancel_registration == 1) ? 'Регистрация завершена' : '',
             ];
        });

        return $array;


    }

    public function headings(): array
    {
        return [
            'Муниципалитет',
            'Организация', 
            'ИНН',
            'Тип организации',
            'Класс',
            'Предмет(курс)',
            'Раздел(модуль)',
            'Кол-во часов',
            'Форма реализации обучения',
            'Условия реализации обучения',
            'Образовательная программа',
            'Образовательная деятельность',
            'Комментарий',
            'Начало',
            'Конец',
            'Архив',
            'Программа',
            'Статус',
        ];
    }


}