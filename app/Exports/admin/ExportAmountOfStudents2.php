<?php
namespace App\Exports\admin;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;
use App\User;

class ExportAmountOfStudents2 implements FromView
{
    private $year;
    private $orgtype;

    public function __construct($year, $orgtype)
    {
        $this->year = $year;
        $this->orgtype = $orgtype;
    }
    public function view(): View
    {
        return view('exports.amount_of_students_school', [
            'year' => $this->year,
            'profs' => User::where('org_type', $this->orgtype)->get()->sortBy('district')->sortBy('fullname'),
            'orgtype' => $this->orgtype
        ]);
    }
}