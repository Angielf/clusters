<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\User;

class ExportLowF2 implements FromView
{
    protected $archive;

    function __construct($archive)
    {
        $this->archive = $archive;
    }

    public function view(): View
    {
        return view('archives_reg.export_low_r_f', [
            'users' => User::where('low', 'f')->get(),
            'year' => $this->archive,
        ]);
    }
}
