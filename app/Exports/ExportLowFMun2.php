<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;

use App\User;

class ExportLowFMun2 implements FromView
{
    protected $archive;

    function __construct($archive)
    {
        $this->archive = $archive;
    }

    public function view(): View
    {
        return view('archives_reg.export_low_r_f', [
            $district = Auth::user()->getDistrict->id,
            'users' => User::where('low', 'f')->where('district', $district)->get(),
            'year' => $this->archive,
        ]);
    }
}
