<?php

namespace App\Exports;

use App\Bid;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Export2 implements FromCollection, WithHeadings
{
    protected $archive;

    function __construct($archive)
    {
        $this->archive = $archive;
    }

    public function collection()
    {
        $bids = Bid::where('archive', $this->archive)->get();

        $array = $bids->map(function ($b, $key) {

             return [
                "district" => $b->user->getDistrict->fullname,
                "fullname_r" => $b->user->fullname,

                "inn_r" => $b->user->inn,

                "fullname_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->fullname : '',

                "inn_b" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->
                sender()->first()->inn : '',

                "class" => $b->getClasses(),
                "amount_of_classes" => $b->amount_of_classes,
                "subject" => $b->subject,
                "modul" => $b->modul,
                "hours" => $b->hours,
                "form_of_education" => $b->form_of_education,
                "form_education_implementation" => $b->form_education_implementation,
                "EducationalPrograms" => $b->getEducationalPrograms(),
                "EducationalActivities" => $b->getEducationalActivities(),

                "date_begin" => $b->getDataBegin(),
                "date_end" => $b->getDataEnd(),

                "content" => $b->content,

                "program" => (($b->status === 1)) ? $b->programs()->sortByDesc('status')->first()->filename : '',

                "schedule" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->filename : '',

                "students_amount" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->students_amount : '',

                "students" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->filename : '',


                "agreement" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename : '',
                
                "sign1" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign : '',
                
                "sign2" => (($b->status === 1) and ($b->programs()->sortByDesc('status')->first()->schedule) and ($b->programs()->sortByDesc('status')->first()->schedule->status === 1)
                    and ($b->programs()->sortByDesc('status')->first()->schedule->student) and ($b->programs()->sortByDesc('status')->first()->schedule->student->agreement)) ?
                    $b->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 : '',

                ];
            });

        return $array;


    }

    public function headings(): array
    {
        return [
            'Муниципалитет',
            'Организация реципиент',
            'ИНН реципиента',
            'Базовая организация',
            'ИНН базовой организации',
            'Класс',
            'Колисество кдассов(групп)',
            'Предмет(курс)',
            'Раздел(модуль)',
            'Кол-во часов',
            'Форма',
            'Условия реализации обучения',
            'Образовательная программа',
            'Образовательная деятельность',
            'Начало',
            'Конец',
            'Комментарий',
            'Программа',
            'Расписание',
            'Количество учеников',
            'Список учеников',
            'Договор',
            'Подпись организации реципиента',
            'Подпись базовой организации',
        ];
    }


}
