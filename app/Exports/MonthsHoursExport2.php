<?php
namespace App\Exports;

use App\MonthsHour;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MonthsHoursExport2 implements FromView
{
    protected $archive;

    function __construct($archive)
    {
        $this->archive = $archive;
    }

    public function view(): View
    {
        return view('archives_reg.months_hours_list_reg', [
            'months_hours' => MonthsHour::all(),
            'year' => $this->archive,
        ]);
    }
}
