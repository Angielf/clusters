<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::post('selectorg', 'Auth\RegisterController@selectOrg')->name('selectorg');

Route::get('/home', 'HomeController@index');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/clusters/add/{cluster}', 'ClusterController@add');

Route::get('/clusters/requestbaseschool/{user}', 'ClusterController@requestbaseschool');

Route::get('/clusters/addcontract/{school_id}/{cluster_id}', 'ClusterController@addcontract');

Route::post('/clusters/addingprogram/{school_id}/{cluster_id}', 'ClusterController@addingprogram');

Route::get('/clusters/addagreement/{cluster_id}', 'ClusterController@addagreement');

Route::post('/clusters/addingagreement/{cluster_id}', 'ClusterController@addingagreement');

Route::get('/bids/add', 'BidController@add')->name('bids.add');

Route::post('/bids/adding', 'BidController@adding')->name('bids.adding');

Route::get('/bids/createrc/{id}', 'BidController@createrc');

Route::post('/bids/storerc/{id}', 'BidController@storerc');

Route::resource('bids', 'BidController');

Route::resource('clusters', 'ClusterController');

Route::get('/clusters/addoneschool/{baseSchoolId}', 'ClusterController@addoneschool');

Route::delete('/bid/{bid}', 'BidController@delete');

Route::delete('/bid/{bid}/2', 'BidController@delete2');

Route::get('/bids/{bid}/update', 'BidController@show_update');

Route::put('/bids/{bid}/subject-update', 'BidController@update_subject');

Route::put('/bids/{bid}/modul-update', 'BidController@update_modul');

Route::put('/bids/{bid}/hours-update', 'BidController@update_hours');

Route::put('/bids/{bid}/class-update', 'BidController@update_class');

Route::put('/bids/{bid}/amount_of_classes-update', 'BidController@update_amount_of_classes');

Route::put('/bids/{bid}/form_of_education-update', 'BidController@update_form_of_education');

Route::put('/bids/{bid}/form_education_implementation-update', 'BidController@update_form_education_implementation');

Route::put('/bids/{bid}/educational_program-update', 'BidController@update_educational_program');

Route::put('/bids/{bid}/educational_activity-update', 'BidController@update_educational_activity');

Route::put('/bids/{bid}/date_begin-update', 'BidController@update_date_begin');

Route::put('/bids/{bid}/date_end-update', 'BidController@update_date_end');

Route::put('/bids/{bid}/date_end-content', 'BidController@update_content');

Route::delete('/bids/{bid}/back-programs', 'BidController@back_programs');


Route::get('/bids/{bid}/to-archive', 'BidController@show_to_archive');

Route::put('/bids/{bid}/to-archive-send', 'BidController@send_to_archive');

Route::put('/bids/{bid}/from-archive', 'BidController@from_archive');



Route::get('/program/{id}', 'ProgramController@index');

Route::get('/program/add/{program}', 'ProgramController@approve');

Route::post('/program/{id}', 'ProgramController@add');

Route::delete('/program/{program}', 'ProgramController@delete');



Route::get('/schedule/{id}', 'ScheduleController@index');

Route::get('/schedule/add/{schedule}', 'ScheduleController@approve');

Route::post('/schedule/{id}', 'ScheduleController@add');

Route::delete('/schedule/{schedule}', 'ScheduleController@delete');


Route::get('/student/{id}', 'StudentController@index');

// Route::get('/student/add/{student}', 'StudentController@approve');

Route::post('/student/{id}', 'StudentController@add');

Route::delete('/student/{student}', 'StudentController@delete');


Route::get('/agreement/{id}', 'AgreementController@index');

// Route::get('/student/add/{student}', 'StudentController@approve');

Route::post('/agreement/{id}', 'AgreementController@add');

Route::delete('/agreement/{agreement}', 'AgreementController@delete');


Route::get('/agreement-document/create-document/{user_org}/{id}/{program_id}','AgreementDocumentController@create_document_agreement')->name('create_document_agreement');

Route::get('/agreement-document-selected/create-document/{user_org}/{program_id}','AgreementDocumentController@create_document_agreement_selected')->name('create_document_agreement_selected');

Route::get('/agreement-document-selected/create-document2/{user_org}/{program_id}','AgreementDocumentController@create_document_agreement_selected2')->name('create_document_agreement2');

Route::get('/agreement-document-selected/create-document3/{user_org}/{program_id}','AgreementDocumentController@create_document_agreement_selected3')->name('create_document_agreement3');

Route::get('/agreement-document-selected/create-document4/{user_org}/{program_id}','AgreementDocumentController@create_document_agreement_selected4')->name('create_document_agreement4');

Route::get('/agreement-document-selected/create-document-vsu/{user_org}/{program_id}','AgreementDocumentController@create_document_agreement_selected_vsu')->name('create_document_agreement_vsu');

Route::get('/agreement-document-selected/show-document/{user_org}/{program_id}','AgreementDocumentController@show_document_agreement_selected')->name('show_document_agreement_selected');

Route::get('/agreement-document-selected/create-document-vuz/{user_org}/{program_id}','AgreementDocumentController@create_document_agreement_selected_vuz');


Route::post('/instruction/add', 'FileController@add');

Route::post('/public/add2', 'FileController@add2');


Route::get('export', 'ExcelRegionController@export')->name('export');

Route::get('export_mun', 'ExcelMunController@export')->name('export_mun');

Route::get('export_hours', 'ExcelRegionController@months_hours_export')->name('export_hours');

Route::get('export_hours_mun', 'ExcelMunController@months_hours_export')->name('export_hours_mun');

Route::get('export_h', 'ExcelRegionController@m_h_export')->name('export_h');

Route::get('export_h_mun', 'ExcelMunController@m_h_export')->name('export_h_mun');

Route::get('proposed_programs_export', 'ExcelRegionController@proposed_programs_export')->name('proposed_programs_export');

Route::get('selected_programs_export', 'ExcelRegionController@selected_programs_export')->name('selected_programs_export');

Route::get('proposed_programs_export_mun', 'ExcelMunController@proposed_programs_export')->name('proposed_programs_export_mun');

Route::get('selected_programs_export_mun', 'ExcelMunController@selected_programs_export')->name('selected_programs_export_mun');

Route::get('selected_export_hours', 'ExcelRegionController@selected_months_hours_export')->name('selected_export_hours');

Route::get('selected_export_hours_mun', 'ExcelMunController@selected_months_hours_export')->name('selected_export_hours_mun');

Route::get('selected_export_h', 'ExcelRegionController@selected_m_h_export')->name('selected_export_h');

Route::get('selected_export_h_mun', 'ExcelMunController@selected_m_h_export')->name('selected_export_h_mun');

Route::get('export_low_r', 'ExcelRegionController@export_low_r')->name('export_low_r');

Route::get('export_low_f', 'ExcelRegionController@export_low_f')->name('export_low_f');

Route::get('export_low_r_mun', 'ExcelMunController@export_low_r')->name('export_low_r_mun');

Route::get('export_low_f_mun', 'ExcelMunController@export_low_f')->name('export_low_f_mun');


Route::post('/region-clusters', 'RegionClusterController@store');

Route::get('/region-clusters/create', 'RegionClusterController@create');

Route::post('/region-clusters/addingprogram/{id}', 'RegionClusterController@addingprogram');

Route::get('/region-clusters/addcontract/{id}', 'RegionClusterController@addcontract');

Route::get('/user/approve/{user_id}', 'UserController@approve');

Route::get('/user/reject/{user_id}', 'UserController@reject');


Route::get('/users/{orgtype}/org-list', 'UserController@show_users')->name('org_list');

Route::get('/users/{user_org}/show-org', 'UserController@show_user');

Route::put('/users/{user_org}/update-inn', 'UserController@update_inn');

Route::put('/users/{user_org}/update-add', 'UserController@update_add');

Route::put('/users/{user_org}/update-tel', 'UserController@update_tel');

Route::put('/users/{user_org}/update-email_real', 'UserController@update_email_real');

Route::put('/users/{user_org}/update-web', 'UserController@update_web');

Route::put('/users/{user_org}/update-shnor', 'UserController@update_shnor');

Route::put('/users/{user_org}/update-type', 'UserController@update_type');

Route::put('/users/{user_org}/update-zone', 'UserController@update_zone');

Route::put('/users/{user_org}/update-kpp', 'UserController@update_kpp');

Route::put('/users/{user_org}/update-ogrn', 'UserController@update_ogrn');

Route::put('/users/{user_org}/update-kbk', 'UserController@update_kbk');

Route::put('/users/{user_org}/update-ikz', 'UserController@update_ikz');

Route::put('/users/{user_org}/update-punkt', 'UserController@update_punkt');

Route::put('/users/{user_org}/update-bik', 'UserController@update_bik');

Route::put('/users/{user_org}/update-okpo', 'UserController@update_okpo');

Route::put('/users/{user_org}/update-oktmo', 'UserController@update_oktmo');

Route::put('/users/{user_org}/update-single_treasury_account', 'UserController@update_single_treasury_account');

Route::put('/users/{user_org}/update-treasury_account', 'UserController@update_treasury_account');

Route::put('/users/{user_org}/update-single_treasury_account_type', 'UserController@update_single_treasury_account_type');

Route::put('/users/{user_org}/update-treasury_account_type', 'UserController@update_treasury_account_type');

Route::put('/users/{user_org}/update-bank', 'UserController@update_bank');

Route::put('/users/{user_org}/update-fullname_full', 'UserController@update_fullname_full');

Route::put('/users/{user_org}/update-fullname', 'UserController@update_fullname');

Route::put('/users/{user_org}/update-date_license', 'UserController@update_date_license');

Route::put('/users/{user_org}/update-number_license', 'UserController@update_number_license');

Route::put('/users/{user_org}/update-who_license', 'UserController@update_who_license');

Route::put('/users/{user_org}/update-director_post', 'UserController@update_director_post');

Route::put('/users/{user_org}/update-director_post_rod', 'UserController@update_director_post_rod');

Route::put('/users/{user_org}/director', 'UserController@update_director');

Route::put('/users/{user_org}/director_rod', 'UserController@update_director_rod');

Route::put('/users/{user_org}/opf_short', 'UserController@update_opf_short');


Route::get('/users/{id}/{orgtype}/org-list-mun', 'UserController@show_users_mun')->name('org_list_mun');


Route::get('/users/{org}/about-org', 'UserController@show_org');


Route::get('/users/{user_org}/archive-list', 'UserController@archive_list');

Route::get('/users/{user_org}/{year}/archive-bids', 'UserController@archive_bids');

Route::get('/users/{user_org}/archive-list-programs', 'UserController@archive_list_programs');

Route::get('/users/{user_org}/{year}/archive-programs', 'UserController@archive_programs');

Route::get('/users/{user_org}/archive-list-proposed-programs', 'UserController@archive_list_proposed_programs');

Route::get('/users/{user_org}/archive-list-selected-programs', 'UserController@archive_list_selected_programs');

Route::get('/users/{user_org}/{year}/archive-proposed-programs', 'UserController@archive_proposed_programs');

Route::get('/users/{user_org}/{year}/archive-selected-programs', 'UserController@archive_selected_programs');

Route::put('/pupil/not/{selected_program}/{grade_number}/{grade_letter}/{user}', 'SelectedProgramController@student_not');


Route::get('/months_hours/{id}', 'MonthsHourController@index');

Route::post('/months_hours/{id}', 'MonthsHourController@add');

Route::delete('/months_hours/{months_hour}', 'MonthsHourController@delete');

Route::get('/months_hours/{months_hour}/update', 'MonthsHourController@show_update');

Route::put('/months_hours/{months_hour}/real_1-update', 'MonthsHourController@update_real_1');

Route::put('/months_hours/{months_hour}/real_2-update', 'MonthsHourController@update_real_2');

Route::put('/months_hours/{months_hour}/real_3-update', 'MonthsHourController@update_real_3');

Route::put('/months_hours/{months_hour}/real_4-update', 'MonthsHourController@update_real_4');

Route::put('/months_hours/{months_hour}/real_5-update', 'MonthsHourController@update_real_5');

Route::put('/months_hours/{months_hour}/real_6-update', 'MonthsHourController@update_real_6');

Route::put('/months_hours/{months_hour}/real_7-update', 'MonthsHourController@update_real_7');

Route::put('/months_hours/{months_hour}/real_8-update', 'MonthsHourController@update_real_8');

Route::put('/months_hours/{months_hour}/real_9-update', 'MonthsHourController@update_real_9');

Route::put('/months_hours/{months_hour}/real_10-update', 'MonthsHourController@update_real_10');

Route::put('/months_hours/{months_hour}/real_11-update', 'MonthsHourController@update_real_11');

Route::put('/months_hours/{months_hour}/real_12-update', 'MonthsHourController@update_real_12');

Route::get('/months_hours/{months_hour}/update-rez', 'MonthsHourController@show_update_rez');

Route::get('/users/months-hours-list', 'MonthsHourController@show_hours')->name('hours_list');

Route::get('/months_hour/true1/{months_hour}', 'MonthsHourController@approve_1');

Route::get('/months_hour/true2/{months_hour}', 'MonthsHourController@approve_2');

Route::get('/months_hour/true3/{months_hour}', 'MonthsHourController@approve_3');

Route::get('/months_hour/true4/{months_hour}', 'MonthsHourController@approve_4');

Route::get('/months_hour/true5/{months_hour}', 'MonthsHourController@approve_5');

Route::get('/months_hour/true6/{months_hour}', 'MonthsHourController@approve_6');

Route::get('/months_hour/true7/{months_hour}', 'MonthsHourController@approve_7');

Route::get('/months_hour/true8/{months_hour}', 'MonthsHourController@approve_8');

Route::get('/months_hour/true9/{months_hour}', 'MonthsHourController@approve_9');

Route::get('/months_hour/true10/{months_hour}', 'MonthsHourController@approve_10');

Route::get('/months_hour/true11/{months_hour}', 'MonthsHourController@approve_11');

Route::get('/months_hour/true12/{months_hour}', 'MonthsHourController@approve_12');

Route::get('/months_hour/not1/{months_hour}', 'MonthsHourController@not_1');

Route::get('/months_hour/not2/{months_hour}', 'MonthsHourController@not_2');

Route::get('/months_hour/not3/{months_hour}', 'MonthsHourController@not_3');

Route::get('/months_hour/not4/{months_hour}', 'MonthsHourController@not_4');

Route::get('/months_hour/not5/{months_hour}', 'MonthsHourController@not_5');

Route::get('/months_hour/not6/{months_hour}', 'MonthsHourController@not_6');

Route::get('/months_hour/not7/{months_hour}', 'MonthsHourController@not_7');

Route::get('/months_hour/not8/{months_hour}', 'MonthsHourController@not_8');

Route::get('/months_hour/not9/{months_hour}', 'MonthsHourController@not_9');

Route::get('/months_hour/not10/{months_hour}', 'MonthsHourController@not_10');

Route::get('/months_hour/not11/{months_hour}', 'MonthsHourController@not_11');

Route::get('/months_hour/not12/{months_hour}', 'MonthsHourController@not_12');

Route::get('/months_hours/{months_hour}/inf', 'MonthsHourController@show_inf');


Route::get('/proposed/add', 'ProposedController@add')->name('proposed.add');

Route::post('/proposed/adding', 'ProposedController@adding')->name('proposed.adding');

Route::get('/proposed_programs/{proposed_program}/update', 'ProposedController@show_update');

Route::delete('/proposed_programs/{proposed_program}/delete', 'ProposedController@delete_proposed');

Route::put('/proposed_programs/{proposed_program}/subject-update', 'ProposedController@update_subject');

Route::put('/proposed_programs/{proposed_program}/modul-update', 'ProposedController@update_modul');

Route::put('/proposed_programs/{proposed_program}/hours-update', 'ProposedController@update_hours');

Route::put('/proposed_programs/{proposed_program}/class-update', 'ProposedController@update_class');

Route::put('/proposed_programs/{proposed_program}/form_of_education-update', 'ProposedController@update_form_of_education');

Route::put('/proposed_programs/{proposed_program}/form_education_implementation-update', 'ProposedController@update_form_education_implementation');

Route::put('/proposed_programs/{proposed_program}/educational_program-update', 'ProposedController@update_educational_program');

Route::put('/proposed_programs/{proposed_program}/educational_activity-update', 'ProposedController@update_educational_activity');

Route::put('/proposed_programs/{proposed_program}/date_begin-update', 'ProposedController@update_date_begin');

Route::put('/proposed_programs/{proposed_program}/date_end-update', 'ProposedController@update_date_end');

Route::put('/proposed_programs/{proposed_program}/content-update', 'ProposedController@update_content');

Route::put('/proposed_programs/{proposed_program}/file-update', 'ProposedController@update_file');


Route::get('/proposed_programs/{proposed_program}/to-archive', 'ProposedController@show_to_archive');

Route::put('/proposed_programs/{proposed_program}/to-archive-send', 'ProposedController@send_to_archive');

Route::put('/proposed_programs/{proposed_program}/from-archive', 'ProposedController@from_archive');

Route::put('/proposed_programs/{proposed_program}/cancel-registration', 'ProposedController@cancel_registration');

Route::put('/proposed_programs/{proposed_program}/return-registration', 'ProposedController@return_registration');


Route::post('/selected_programs/{id}', 'SelectedProgramController@take');

Route::get('/selected_programs/{selected_program}/show', 'SelectedProgramController@show');

Route::delete('/selected_programs/{selected_program}/delete', 'SelectedProgramController@delete');


Route::get('/selected_schedule/{id}', 'SelectedScheduleController@index');

Route::get('/selected_schedule/add/{selected_schedule}', 'SelectedScheduleController@approve');

Route::post('/selected_schedule/{id}', 'SelectedScheduleController@add');

Route::get('/selected_schedule_add_many/{id}/{proposed_program_id}/{vuz_id}', 'SelectedScheduleController@add_many');

Route::post('/selected_schedule_add_many/{id}/{proposed_program_id}/{vuz_id}', 'SelectedScheduleController@add_many');

Route::delete('/selected_schedule_delete_many/{filename}/{vuz_id}/{program_id}', 'SelectedScheduleController@delete_many');

Route::delete('/selected_schedule/{selected_schedule}', 'SelectedScheduleController@delete');


Route::get('/selected_student/{id}', 'SelectedStudentController@index');

Route::post('/selected_student/{id}', 'SelectedStudentController@add');

Route::delete('/selected_student/{selected_student}', 'SelectedStudentController@delete');


Route::get('/selected_agreement/{id}', 'SelectedAgreementController@index');

Route::post('/selected_agreement/{id}', 'SelectedAgreementController@add');

Route::delete('/selected_agreement/{selected_agreement}', 'SelectedAgreementController@delete');


Route::get('/selected_months_hours/{id}', 'SelectedMonthsHourController@index');

Route::post('/selected_months_hours/{id}', 'SelectedMonthsHourController@add');

Route::delete('/selected_months_hours/{selected_months_hour}', 'SelectedMonthsHourController@delete');

Route::get('/selected_months_hours/{selected_months_hour}/update', 'SelectedMonthsHourController@show_update');

Route::put('/selected_months_hours/{selected_months_hour}/real_1-update', 'SelectedMonthsHourController@update_real_1');

Route::put('/selected_months_hours/{selected_months_hour}/real_2-update', 'SelectedMonthsHourController@update_real_2');

Route::put('/selected_months_hours/{selected_months_hour}/real_3-update', 'SelectedMonthsHourController@update_real_3');

Route::put('/selected_months_hours/{selected_months_hour}/real_4-update', 'SelectedMonthsHourController@update_real_4');

Route::put('/selected_months_hours/{selected_months_hour}/real_5-update', 'SelectedMonthsHourController@update_real_5');

Route::put('/selected_months_hours/{selected_months_hour}/real_6-update', 'SelectedMonthsHourController@update_real_6');

Route::put('/selected_months_hours/{selected_months_hour}/real_7-update', 'SelectedMonthsHourController@update_real_7');

Route::put('/selected_months_hours/{selected_months_hour}/real_8-update', 'SelectedMonthsHourController@update_real_8');

Route::put('/selected_months_hours/{selected_months_hour}/real_9-update', 'SelectedMonthsHourController@update_real_9');

Route::put('/selected_months_hours/{selected_months_hour}/real_10-update', 'SelectedMonthsHourController@update_real_10');

Route::put('/selected_months_hours/{selected_months_hour}/real_11-update', 'SelectedMonthsHourController@update_real_11');

Route::put('/selected_months_hours/{selected_months_hour}/real_12-update', 'SelectedMonthsHourController@update_real_12');

Route::get('/selected_months_hours/{selected_months_hour}/update-rez', 'SelectedMonthsHourController@show_update_rez');

Route::get('/users/selected-months-hours-list', 'MonthsHourController@show_hours')->name('selected_hours_list');

Route::get('/selected_months_hour/true1/{selected_months_hour}', 'SelectedMonthsHourController@approve_1');

Route::get('/selected_months_hour/true2/{selected_months_hour}', 'SelectedMonthsHourController@approve_2');

Route::get('/selected_months_hour/true3/{selected_months_hour}', 'SelectedMonthsHourController@approve_3');

Route::get('/selected_months_hour/true4/{selected_months_hour}', 'SelectedMonthsHourController@approve_4');

Route::get('/selected_months_hour/true5/{selected_months_hour}', 'SelectedMonthsHourController@approve_5');

Route::get('/selected_months_hour/true6/{selected_months_hour}', 'SelectedMonthsHourController@approve_6');

Route::get('/selected_months_hour/true7/{selected_months_hour}', 'SelectedMonthsHourController@approve_7');

Route::get('/selected_months_hour/true8/{selected_months_hour}', 'SelectedMonthsHourController@approve_8');

Route::get('/selected_months_hour/true9/{selected_months_hour}', 'SelectedMonthsHourController@approve_9');

Route::get('/selected_months_hour/true10/{selected_months_hour}', 'SelectedMonthsHourController@approve_10');

Route::get('/selected_months_hour/true11/{selected_months_hour}', 'SelectedMonthsHourController@approve_11');

Route::get('/selected_months_hour/true12/{selected_months_hour}', 'SelectedMonthsHourController@approve_12');

Route::get('/selected_months_hour/not1/{selected_months_hour}', 'SelectedMonthsHourController@not_1');

Route::get('/selected_months_hour/not2/{selected_months_hour}', 'SelectedMonthsHourController@not_2');

Route::get('/selected_months_hour/not3/{selected_months_hour}', 'SelectedMonthsHourController@not_3');

Route::get('/selected_months_hour/not4/{selected_months_hour}', 'SelectedMonthsHourController@not_4');

Route::get('/selected_months_hour/not5/{selected_months_hour}', 'SelectedMonthsHourController@not_5');

Route::get('/selected_months_hour/not6/{selected_months_hour}', 'SelectedMonthsHourController@not_6');

Route::get('/selected_months_hour/not7/{selected_months_hour}', 'SelectedMonthsHourController@not_7');

Route::get('/selected_months_hour/not8/{selected_months_hour}', 'SelectedMonthsHourController@not_8');

Route::get('/selected_months_hour/not9/{selected_months_hour}', 'SelectedMonthsHourController@not_9');

Route::get('/selected_months_hour/not10/{selected_months_hour}', 'SelectedMonthsHourController@not_10');

Route::get('/selected_months_hour/not11/{selected_months_hour}', 'SelectedMonthsHourController@not_11');

Route::get('/selected_months_hour/not12/{selected_months_hour}', 'SelectedMonthsHourController@not_12');

Route::get('/selected_months_hours/{selected_months_hour}/inf', 'SelectedMonthsHourController@show_inf');


Route::get('/archive-list', 'ArchiveController@list');

Route::get('/all-exports', 'ArchiveController@all_exports');

Route::get('/archive/{year}', 'ArchiveController@bids_archives_reg');

Route::get('/mun/archive/{year}', 'ArchiveController@bids_archives_mun');


Route::get('/reg/{archive}/export2', 'ExcelRegionController@export2');

Route::get('/reg/export-not-archive', 'ExcelRegionController@export_not_archive');

Route::get('/reg/export_low_r/{archive}', 'ExcelRegionController@export_low_r_archive');

Route::get('/reg/export_low_f/{archive}', 'ExcelRegionController@export_low_f_archive');

Route::get('/reg/export-low-r-not-archive', 'ExcelRegionController@export_low_r_not_archive');

Route::get('/reg/export-low-f-not-archive', 'ExcelRegionController@export_low_f_not_archive');

Route::get('/reg/export_hours/{archive}', 'ExcelRegionController@months_hours_export_archive');

Route::get('/reg/export_hours-not-archive', 'ExcelRegionController@months_hours_export_not_archive');


Route::get('/mun/{archive}/export2', 'ExcelMunController@export2');

Route::get('/mun/export-not-archive', 'ExcelMunController@export_not_archive');

Route::get('/mun/export_low_r/{archive}', 'ExcelMunController@export_low_r_archive');

Route::get('/mun/export_low_f/{archive}', 'ExcelMunController@export_low_f_archive');

Route::get('/mun/export-low-r-not-archive', 'ExcelMunController@export_low_r_not_archive');

Route::get('/mun/export-low-f-not-archive', 'ExcelMunController@export_low_f_not_archive');

Route::get('/mun/export_hours/{archive}', 'ExcelMunController@months_hours_export_archive');

Route::get('/mun/export_hours-not-archive', 'ExcelMunController@months_hours_export_not_archive');


// ---------------------------------------------------------------------

Route::get('/pupil/{user_org}/pupil-inf', 'UserController@pupil_inf');

Route::get('/pupils/{user}/list-of-grades', 'UserController@list_of_grades')->name('list_of_grades');

Route::get('/pupils/{user}/{grade_number}/{grade_letter}/list-of-pupils', 'UserController@list_of_pupils');

Route::get('/pupils-year-archive/{user}', 'UserController@pupils_year_archive');

Route::get('/pupils/{user}/list-of-pupils-archive/{year}', 'UserController@list_of_pupils_archive');

Route::get('/selected-program/plus/{selected_program}', 'SelectedProgramController@plus');

Route::get('/selected-program/minus/{selected_program}', 'SelectedProgramController@minus');

Route::get('/pupils/{user}/vuz/list-of-programs', 'UserController@list_of_programs_vuz');

Route::get('/pupils/{user}/vuz/list-of-programs/{year}', 'UserController@list_of_programs_vuz_archive');

Route::get('/pupils/{user}/school/programs', 'UserController@list_of_programs_school');

Route::get('/pupils-vuz/{user}/{proposed_program_id}/list-of-pupils', 'UserController@list_of_pupils_vuz');

Route::get('/pupils-school/{user}/{proposed_program_id}/list-of-pupils', 'UserController@list_of_pupils_school');

Route::get('/pupils-vuz-school/{user}/{proposed_program_id}/list-of-pupils', 'UserController@list_of_pupils_vuz_school');

Route::get('/agreement_pupil/{selected_schedule_id}/{grade_number}/{grade_letter}/{user}', 'AgreementPupilController@index');

Route::post('/agreement_pupil/{id}/{grade_number}/{grade_letter}/{user}', 'AgreementPupilController@add_many');

Route::delete('/agreement_pupil/delete/{agreement_pupil}/{user_id}/{grade_number}/{grade_letter}', 'AgreementPupilController@delete');

Route::delete('/agreement_pupil/delete-many/{agreement_pupil}/{user_id}/{grade_number}/{grade_letter}', 'AgreementPupilController@delete_many');

Route::put('/agreement_pupil/approve/{filename}/{grade_number}/{grade_letter}', 'AgreementPupilController@approve');


Route::get('/agreement_pupil2/{selected_schedule_id}/{grade_number}/{grade_letter}/{user}', 'AgreementPupilController@index2');

Route::post('/agreement_pupil2/{id}/{grade_number}/{grade_letter}/{user}', 'AgreementPupilController@add_many2');

Route::delete('/agreement_pupil2/delete/{agreement_pupil}/{user_id}/{grade_number}/{grade_letter}', 'AgreementPupilController@delete2');

Route::delete('/agreement_pupil2/delete_many/{agreement_pupil}/{user_id}/{grade_number}/{grade_letter}', 'AgreementPupilController@delete_many2');

Route::put('/agreement_pupil2/approve/{filename}/{grade_number}/{grade_letter}', 'AgreementPupilController@approve2');


Route::get('/pupil/selected_programs/{selected_program}/show', 'SelectedProgramController@show_pupil');

Route::get('/pupil/school/selected_programs/{selected_program}/show', 'SelectedProgramController@show_pupil_school');

Route::put('/users/{user_org}/update-fullname-pupil', 'UserController@update_fullname_pupil');

Route::put('/users/{user_org}/update-grade_letter', 'UserController@update_grade_letter');

Route::put('/users/{user_org}/update-grade_number', 'UserController@update_grade_number');

Route::put('/users/{user_org}/update-tel-pupil', 'UserController@update_tel_pupil');

Route::put('/users/{user_org}/update-email-pupil', 'UserController@update_email_pupil');

Route::put('/users/{user_org}/update-snils-pupil', 'UserController@update_snils_pupil');

Route::put('/users/{user_org}/update-birth-pupil', 'UserController@update_birth_pupil');

Route::put('/users/{user_org}/update-district-pupil', 'UserController@update_district_pupil');

Route::post('selectorg2', 'UserController@selectOrg')->name('selectorg2');

Route::get('/proposed-programs-pupils', 'UserController@proposed_programs_pupils');

Route::get('/proposed-programs-pupils-prof', 'UserController@proposed_programs_pupils_prof');

Route::get('/{user}/not-archive/{main_or_not}/agreements-vuz-prof', 'UserController@agreements_vuz_prof');


Route::get('/{proposed_program}/show-pupils-reg', 'UserController@show_pupils_reg');

Route::get('/{proposed_program}/show-pupils-mun', 'UserController@show_pupils_mun');


Route::get('/pupils/export_programs', 'ExcelRegionController@selected_programs_pupils_export');

Route::get('/pupils/export_programs_mun', 'ExcelMunController@selected_programs_pupils_export');

Route::get('/pupils/export_programs_not_archive', 'ExcelRegionController@selected_programs_not_archive_pupils_export');

Route::get('/pupils/export_programs_mun_not_archive', 'ExcelMunController@selected_programs_not_archive_pupils_export');

Route::get('/pupils/export_programs_prof', 'ExcelRegionController@selected_programs_pupils_export_prof');

Route::get('/pupils/export_programs_mun_prof', 'ExcelMunController@selected_programs_pupils_export_prof');

Route::get('/pupils/export_programs_not_archive_prof', 'ExcelRegionController@selected_programs_not_archive_pupils_export_prof');

Route::get('/pupils/export_programs_mun_not_archive_prof', 'ExcelMunController@selected_programs_not_archive_pupils_export_prof');


Route::post('/save-signed-file/{a}/{user}/{agreement_id}', 'AgreementSignedController@saveSignedFile');

Route::post('/save-signed-file2/{a}/{user}/{agreement_id}/{proposed_program_id}', 'AgreementSignedController@saveSignedFile2');

Route::post('/save-signed-file-ordinary/{a}/{user}/{agreement_id}', 'AgreementSignedController@saveSignedFileOrdinary');

Route::post('/save-signed-file-ordinary2/{a}/{user}/{agreement_id}', 'AgreementSignedController@saveSignedFileOrdinary2');

Route::get('/agreement-sign/{a}/{user}/{agreement_id}/{grade_number}/{grade_letter}', 'AgreementSignedController@agreementPage');

Route::get('/agreement-sign2/{a}/{user}/{agreement_id}/{proposed_program_id}', 'AgreementSignedController@agreementPage2');

Route::get('/agreement-sign-ordinary/{a}/{user}/{agreement_id}', 'AgreementSignedController@agreementPageOrdinary');

Route::get('/agreement-sign-ordinary2/{a}/{user}/{agreement_id}', 'AgreementSignedController@agreementPageOrdinary2');


// ------------------------

Route::get('/admin/{year}', 'UserController@index2_year');

Route::get('/admin/prof/{year}', 'UserController@index_prof_year');

Route::get('/admin/vuz/{year}', 'UserController@index_vuz_year');

Route::get('/reg/{year}/export-year', 'ExcelRegionController@export_year');

Route::get('/reg/{year}/export-low-r-year', 'ExcelRegionController@export_low_r_year');

Route::get('/reg/{year}/export-low-f-year', 'ExcelRegionController@export_low_f_year');

Route::get('/reg/{year}/export-proposed-programs-year', 'ExcelRegionController@export_proposed_programs_year');

Route::get('/reg/{year}/export-selected-programs-year', 'ExcelRegionController@export_selected_programs_year');

Route::get('/reg/{year}/export-all-year', 'ExcelRegionController@export_all_year');

Route::get('/reg/{year}/export-all-year-school', 'ExcelRegionController@export_all_year_school');

Route::get('/reg/{year}/{orgtype}/spo-programs-year', 'UserController@spo_programs_year');

Route::get('/reg/{year}/{orgtype}/export-spo-programs-not-archive-year', 'ExcelRegionController@export_pupils_not_archive_year');

Route::get('/reg/{year}/{orgtype}/page/export-spo-programs-not-archive-year', 'ExcelRegionController@showPupilsNotArchive');

Route::get('/load-more-pupils/{year}/{orgtype}', 'ExcelRegionController@loadMorePupils');

Route::get('/reg/{year}/{orgtype}/export-spo-vuz-programs-archive-year', 'ExcelRegionController@export_pupils_archive_year');

Route::get('/reg/{year}/{orgtype}/export-spo-programs-not-archive-year2', 'ExcelRegionController@export_pupils_not_archive_year2');

Route::get('/reg/{year}/{orgtype}/export-spo-vuz-programs-archive-year2', 'ExcelRegionController@export_pupils_archive_year2');

Route::get('/reg/{year}/{orgtype}/export-pupils-proposed-year', 'ExcelRegionController@export_pupils_proposed_year');

Route::get('/reg/{year}/{orgtype}/export-amount-of-students-year', 'ExcelRegionController@export_amount_of_students_year');

Route::get('/reg/{year}/{orgtype}/export-amount-of-students-year2', 'ExcelRegionController@export_amount_of_students_year2');


Route::get('/admin/mun/{district_id}/{year}', 'UserController@index_mun_year');

Route::get('/mun/{district_id}/{year}/export-year', 'ExcelMunController@export_mun_year');

Route::get('/mun/{district_id}/{year}/export-low-r-year', 'ExcelMunController@export_low_r_mun_year');

Route::get('/mun/{district_id}/{year}/export-low-f-year', 'ExcelMunController@export_low_f_mun_year');

Route::get('/mun/{district_id}/{year}/export-proposed-programs-year', 'ExcelMunController@export_proposed_programs_mun_year');

Route::get('/mun/{district_id}/{year}/export-selected-programs-year', 'ExcelMunController@export_selected_programs_mun_year');

Route::get('/mun/{district_id}/{year}/export-all-year', 'ExcelMunController@export_all_mun_year');

Route::get('/mun/{district_id}/{year}/{orgtype}/spo-programs-year', 'UserController@spo_programs_mun_year');

Route::get('/mun/{district_id}/{year}/{orgtype}/export-spo-programs-not-archive-year', 'ExcelMunController@export_pupils_not_archive_mun_year');

Route::get('/mun/{district_id}/{year}/{orgtype}/export-spo-vuz-programs-archive-year', 'ExcelMunController@export_pupils_archive_mun_year');

Route::get('/mun/{district_id}/{year}/{orgtype}/export-spo-programs-not-archive-year2', 'ExcelMunController@export_pupils_not_archive_mun_year2');

Route::get('/mun/{district_id}/{year}/{orgtype}/export-spo-vuz-programs-archive-year2', 'ExcelMunController@export_pupils_archive_mun_year2');

Route::get('/mun/{district_id}/{year}/{orgtype}/export-pupils-proposed-year', 'ExcelMunController@export_pupils_proposed_mun_year');


// -----------------------------------------------------------------------------------------

Route::get('/download/all-agreements/{orgId}/{year}/{main_or_not}/{one_or_two}/{archive_or_not}', 'AgreementController@downloadAllAgreements')->name('download.all.agreements');

Route::get('/agreements/vuz/prof/{user}/{main_or_not}/{org_type}/download-zip', 'AgreementController@downloadAgreementsZip')->name('agreements.vuz.prof.download.zip');