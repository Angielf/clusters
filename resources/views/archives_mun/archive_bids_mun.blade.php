@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 10))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">
                    <h5>Период {{ $year }}</h5>

                    <ul class="list-group">
                        <ul class="list-group list-group-horizontal">
                            <li class="list-group-item flex-fill">
                                Выгрузка заявок:
                                <a class="btn btn-outline-dark" href="/mun/{{$year}}/export2">Export</a>
                            </li>
                            <li class="list-group-item flex-fill">
                                Выгрузка заявок организаций с низкими результатами (региональный уровень):
                                <a class="btn btn-outline-dark" href="/mun/export_low_r/{{$year}}">Export</a>
                            </li>
                            <li class="list-group-item flex-fill">
                                Выгрузка заявок организаций с низкими результатами (федеральный уровень):
                                <a class="btn btn-outline-dark" href="/mun/export_low_f/{{$year}}">Export</a>
                            </li>
                        </ul>
                        <ul class="list-group list-group-horizontal">
                            <li class="list-group-item flex-fill">
                                Кол-во часов по месяцам:
                                <a class="btn btn-outline-dark" href="/mun/export_hours/{{$year}}">Export</a>
                            </li>

                        </ul>
                    </ul>
                </div>

                <div class="card-body">

                    <ul class="nav nav-tabs">
                        <!-- Первая вкладка (активная) -->
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#bids">
                                Заявки
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="bids">

                            <ul class="list-group">
                                <li class="list-group-item">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                              Организация реципиент
                                          </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="rez" onkeyup="rez()">
                                    </div>

                                </li>

                                <li class="list-group-item">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                              Базовая организация
                                          </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="bas" onkeyup="bas()">
                                    </div>

                                </li>

                                <li class="list-group-item">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            Класс
                                          </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="classs" onkeyup="classs()">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              Предмет(курс)
                                            </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="subject" onkeyup="subject()">
                                    </div>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            Раздел(модуль)
                                          </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="modul" onkeyup="modul()">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                Кол-во часов
                                            </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="hour" onkeyup="hour()">
                                    </div>

                                </li>

                                <li class="list-group-item">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            Форма обучения
                                          </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="forma" onkeyup="forma()" list="Forma">
                                        <datalist id="Forma">
                                            <option value="очная">очная</option>
                                            <option value="очно-заочная">очно-заочная</option>
                                            <option value="заочная">заочная</option>
                                        </datalist>



                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                Условия реализации обучения
                                            </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="imp" onkeyup="imp()" list="Imp">
                                        <datalist id="Imp">
                                            <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                с использованием дистанционных образовательных технологий, электронного обучения
                                            </option>
                                            <option value="трансфер детей до организации">трансфер детей до организации</option>
                                        </datalist>
                                    </div>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            Образовательная программа
                                          </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="pro" onkeyup="pro()" list="Pro">
                                        <datalist id="Pro">
                                            <option value="основная">основная</option>
                                            <option value="дополнительная">дополнительная</option>
                                        </datalist>


                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                Образовательная деятельность
                                            </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="act" onkeyup="act()" list="Act">
                                        <datalist id="Act">
                                            <option value="урочная">урочная</option>
                                            <option value="внеурочная">внеурочная</option>
                                        </datalist>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                Начало
                                            </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="begin" onkeyup="begin()">

                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                Конец
                                            </span>
                                        </div>
                                        <input type="text" class="form-control"
                                        id="end" onkeyup="end()">
                                    </div>
                                </li>
                            </ul>

                            <table class="table table-striped" id="myTable">
                                <thead>
                                    <tr>
                                        <th scope="col">Организация реципиент</th>
                                        <th scope="col">Базовая организация</th>

                                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                        <th scope="col">
                                            Форма/условия реализации обучения/ Образовательная программа/деятельность/ Комментарий
                                        </th>

                                        <th scope="col">
                                            Даты
                                        </th>

                                        <th scope="col">Программа/ Расписание</th>
                                        <th scope="col">Кол-во учеников/Список</th>
                                        <th scope="col">Договор</th>
                                    </tr>
                                </thead>
                                <tbody id="table1">
                                    @foreach($bids as $bid)
                                    @if(($bid->user->district == $user->getDistrict->id))
                                        <tr>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $bid->user->fullname }}</li>
                                                    <li class="list-group-item">
                                                        <a class="btn btn-outline-dark" href="/users/{{ $bid->user->id }}/show-org">Информация</a>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->fullname}}</li>
                                                    <li class="list-group-item"><a class="btn btn-outline-dark" href="/users/{{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->id }}/show-org">Информация</a></li>
                                                </ul>
                                            </td>

                                            <td>
                                                <ul class="list-group" id="ul1">
                                                    <li class="list-group-item">{{ $bid->getClasses() }}</li>
                                                    <li class="list-group-item">{{ $bid->subject }}</li>
                                                    <li class="list-group-item">{{ $bid->modul }}</li>
                                                    <li class="list-group-item">{{ $bid->hours }}</li>
                                                </ul>
                                            </td>

                                            <td>
                                                <ul class="list-group" id="ul2">
                                                    <li class="list-group-item">{{ $bid->form_of_education }} </li>
                                                    <li class="list-group-item">{{ $bid->form_education_implementation }}</li>
                                                    <li class="list-group-item">{{ $bid->getEducationalPrograms() }}</li>
                                                    <li class="list-group-item">{{ $bid->getEducationalActivities() }}</li>
                                                    <li class="list-group-item">{{ $bid->content }}</li>
                                                </ul>
                                            </td>

                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $bid->getDataBegin() }} </li>
                                                    <li class="list-group-item">{{ $bid->getDataEnd() }}</li>
                                                </ul>
                                            </td>


                                            <td>
                                                <li class="list-group-item">
                                                    <a href="/files/programs/{{ $bid->programs()->sortByDesc('status')->first()->filename }}"
                                                        class="btn btn-outline-success">
                                                        Программа
                                                    </a>
                                                </li>
                                                <li class="list-group-item">
                                                    @if(($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1))
                                                        <a href="/files/schedules/{{ $bid->programs()->sortByDesc('status')->first()->schedule->filename }}"
                                                            class="btn btn-outline-success">
                                                            Расписание
                                                        </a>
                                                    @endif
                                                </li>
                                                <li class="list-group-item">
                                                    @if(($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule->months_hour))
                                                        <a href="/months_hours/{{ $bid->programs()->sortByDesc('status')->first()->schedule->months_hour->id }}/inf"
                                                            class="btn btn-outline-success">
                                                                Кол-во часов по месяцам
                                                        </a>
                                                    @endif
                                                </li>

                                            </td>

                                            <td>
                                                @if (($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                                and ($bid->programs()->sortByDesc('status')->first()->schedule->student))
                                                    <li class="list-group-item">
                                                        {{$bid->programs()->sortByDesc('status')->first()->schedule->student->students_amount}}
                                                    </li>
                                                    <li class="list-group-item">
                                                        <a href="/files/students/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->filename }}"
                                                            class="btn btn-outline-success">
                                                            Список учеников
                                                        </a>
                                                    </li>
                                                @endif
                                            </td>

                                            <td>
                                                @if (($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                                and ($bid->programs()->sortByDesc('status')->first()->schedule->student) and ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement))
                                                    <li class="list-group-item">
                                                        <a href="/files/agreements/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                            class="btn btn-outline-success">
                                                                Договор
                                                        </a>
                                                        <br>
                                                        @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign == 1)
                                                        <a href="/files/agreements/signed/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign }}"
                                                            class="btn btn-outline-success">
                                                            Подпись организации реципиента
                                                        </a>
                                                        @endif
                                                        <br>
                                                        @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign2 == 1)
                                                            <a href="/files/agreements/signed2/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 }}"
                                                                class="btn btn-outline-success">
                                                                Подпись базовой организации
                                                            </a>
                                                        @endif
                                                    </li>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/poisk_mun.js?version=2') }}"></script>
@endif
@endsection
