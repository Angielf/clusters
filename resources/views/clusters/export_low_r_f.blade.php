<table>
    <thead>
        <tr>
            <th>Муниципалитет организации реципиента</th>
            <th>Организация реципиент</th>
            <th>ИНН реципиента</th>

            <th>Базовая организация</th>
            <th>ИНН базовой организации</th>

            <th>Класс</th>
            <th>Количество классов(групп)</th>
            <th>Предмет(курс)</th>
            <th>Раздел(модуль)</th>
            <th>Кол-во часов</th>

            <th>Формареализации обучения</th>
            <th>Условия реализации обучения</th>
            <th>Образовательная программа</th>
            <th>Образовательная деятельность</th>
            <th>Комментарий</th>

            <th>Начало</th>
            <th>Конец</th>

            <th>Программа</th>
            <th>Расписание</th>
            <th>Кол-во учеников</th>
            <th>Список</th>
            <th>Договор</th>
            <th>Подпись организации реципиента</th>
            <th>Подпись базовой организации</th>
        </tr>
    </thead>

    <tbody>
        @foreach($users as $user)
            <tr>
                <td>
                    {{ $user->getDistrict->fullname }}
                </td>
                <td>
                    {{ $user->fullname }}
                </td>
                <td>
                    {{ $user->inn }}
                </td>
            </tr>
            @if($user->bids())
                @foreach($user->bids() as $bid)
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            @if($bid->status === 1)
                                {{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->fullname}}
                            @endif
                        </td>
                        <td>
                            @if($bid->status === 1)
                                {{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->inn}}
                            @endif
                        </td>
                        <td>{{ $bid->getClasses() }}</td>
                        <td>{{ $bid->amount_of_classes }}</td>
                        <td>{{ $bid->subject }}</td>
                        <td>{{ $bid->modul }}</td>
                        <td>{{ $bid->hours }}</td>

                        <td>{{ $bid->form_of_education }}</td>
                        <td>{{ $bid->form_education_implementation }}</td>
                        <td>{{ $bid->getEducationalPrograms() }}</td>
                        <td>{{ $bid->getEducationalActivities()}}</td>
                        <td>{{ $bid->content }}</td>

                        <td>{{ $bid->getDataBegin() }}</td>
                        <td>{{ $bid->getDataEnd() }}</td>

                        <td>
                            @if($bid->status === 1)
                                {{ $bid->programs()->sortByDesc('status')->first()->filename }}
                            @endif
                        </td>
                        <td>
                            @if(($bid->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1))
                                {{ $bid->programs()->sortByDesc('status')->first()->schedule->filename }}
                            @endif
                        </td>

                        <td>
                            @if (($bid->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                            and ($bid->programs()->sortByDesc('status')->first()->schedule->student))
                                {{$bid->programs()->sortByDesc('status')->first()->schedule->student->students_amount}}
                            @endif
                        </td>
                        <td>
                            @if (($bid->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                            and ($bid->programs()->sortByDesc('status')->first()->schedule->student))
                                {{ $bid->programs()->sortByDesc('status')->first()->schedule->student->filename }}
                            @endif
                        </td>

                        <td>
                            @if (($bid->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                            and ($bid->programs()->sortByDesc('status')->first()->schedule->student) and ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement))
                                {{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}
                            @endif
                        </td>

                        <td>
                            @if (($bid->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                            and ($bid->programs()->sortByDesc('status')->first()->schedule->student) and ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement))
                                {{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign }}
                            @endif
                        </td>

                        <td>
                            @if (($bid->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                            and ($bid->programs()->sortByDesc('status')->first()->schedule->student) and ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement))
                                {{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 }}
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>
