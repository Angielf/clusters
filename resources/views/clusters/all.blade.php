@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10))
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        @if(Auth::user()->status == 1)
                        <h2>Региональный координатор</h2>
                        @elseif(Auth::user()->status == 10)
                        <h2>Муниципальный координатор</h2>
                        @endif

                        @if(Auth::user()->status == 1)
                        <ul class="list-group">
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item flex-fill">
                                    Выгрузка всех заявок:
                                    <a class="btn btn-outline-dark" href="{{ route('export') }}">Export</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Выгрузка предложенных программ:
                                    <a class="btn btn-outline-dark" href="{{ route('proposed_programs_export') }}">Export</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item flex-fill">
                                    Список действующих организаций Воронежской области:
                                    <a class="btn btn-outline-dark" href="{{ route('org_list') }}">Организации</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Выгрузка взятых предложеннных программ:
                                    <a class="btn btn-outline-dark" href="{{ route('selected_programs_export') }}">Export</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item flex-fill">
                                    Кол-во часов по месяцам:
                                    <a class="btn btn-outline-dark" href="{{ route('export_hours') }}">Export</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Кол-во часов по месяцам (по предложенным программам):
                                    <a class="btn btn-outline-dark" href="{{ route('selected_export_hours') }}">Export</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item flex-fill">
                                    Кол-во часов по месяцам у базовых организаций (2022-2023):
                                    <a class="btn btn-outline-dark" href="{{ route('export_h') }}">Export</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Кол-во часов по месяцам у базовых организаций (2022-2023) (по предложенным программам):
                                    <a class="btn btn-outline-dark" href="{{ route('selected_export_h') }}">Export</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item flex-fill">
                                    Выгрузка заявок организаций с низкими результатами (региональный уровень):
                                    <a class="btn btn-outline-dark" href="{{ route('export_low_r') }}">Export</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Выгрузка заявок организаций с низкими результатами (федеральный уровень):
                                    <a class="btn btn-outline-dark" href="{{ route('export_low_f') }}">Export</a>
                                </li>
                            </ul>

                        </ul>

                        @elseif(Auth::user()->status == 10)
                        <ul class="list-group">
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item">
                                    Выгрузка заявок муниципалитета:
                                    <a class="btn btn-outline-dark" href="{{ route('export_mun') }}">Export</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Выгрузка предложенных программ:
                                    <a class="btn btn-outline-dark" href="{{ route('proposed_programs_export_mun') }}">Export</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item">
                                    Список действующих организаций муниципалитета:
                                    <a class="btn btn-outline-dark" href="/users/{{ Auth::user()->getDistrict->id }}/all/org-list-mun">Организации</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Выгрузка взятых предложеннных программ:
                                    <a class="btn btn-outline-dark" href="{{ route('selected_programs_export_mun') }}">Export</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item">
                                    Кол-во часов по месяцам:
                                    <a class="btn btn-outline-dark" href="{{ route('export_hours_mun') }}">Export</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Кол-во часов по месяцам (по предложенным программам):
                                    <a class="btn btn-outline-dark" href="{{ route('selected_export_hours_mun') }}">Export</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item">
                                    Кол-во часов по месяцам у базовых организаций (2022-2023):
                                    <a class="btn btn-outline-dark" href="{{ route('export_h_mun') }}">Export</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Кол-во часов по месяцам у базовых организаций (2022-2023) (по предложенным программам):
                                    <a class="btn btn-outline-dark" href="{{ route('selected_export_h_mun') }}">Export</a>
                                </li>
                            </ul>
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item flex-fill">
                                    Выгрузка заявок организаций с низкими результатами (региональный уровень):
                                    <a class="btn btn-outline-dark" href="{{ route('export_low_r_mun') }}">Export</a>
                                </li>
                                <li class="list-group-item flex-fill">
                                    Выгрузка заявок организаций с низкими результатами (федеральный уровень):
                                    <a class="btn btn-outline-dark" href="{{ route('export_low_f_mun') }}">Export</a>
                                </li>
                            </ul>
                        </ul>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
