@extends('layouts.app')

@section('content')

<button type="button" id="export_button" class="btn btn-outline-dark">Выгрузить в Excel (xlsx)</button>

<table class="table table-bordered" id="myTable">
    <thead>
        <tr>
            <th>Муниципалитет организации-участника</th>
            <th>Организация-участник</th>
            <th>ИНН организации-участника</th>
            <th>Муниципалитет базовой организации</th>
            <th>Ученик</th>
            <th>Базовая организация</th>
            <th>ИНН базовой организации</th>
            <th>Класс ученика</th>
            <th>Дата выбора программы</th>
            <th>Дата рождения</th>
            <th>СНИЛС</th>
            <th>Класс</th>
            <th>Предмет(курс)</th>
            <th>Раздел(модуль)</th>
            <th>Кол-во часов</th>
            <th>Форма реализации обучения</th>
            <th>Условия реализации обучения</th>
            <th>Образовательная программа</th>
            <th>Образовательная деятельность</th>
            <th>Комментарий</th>
            <th>Начало</th>
            <th>Конец</th>
            <th>Архив</th>
            <th>Программа</th>
            <th>Регистрация</th>
            <th>Статус</th>
            <th>Расписание</th>
            <th>Договор</th>
            <th>Подпись базовой организации</th>
            <th>Подпись организации-участника</th>
            <th>Договор2</th>
            <th>Подпись базовой организации</th>
            <th>Подпись организации-участника</th>
        </tr>
    </thead>
    <tbody>
        @foreach($initialData as $row)
            <tr>
                <td>{{ $row['district_b'] }}</td>
                <td>{{ $row['fullname_b'] }}</td>
                <td>{{ $row['inn_b'] }}</td>
                <td>{{ $row['district_s'] }}</td>
                <td>{{ $row['fullname_s'] }}</td>
                <td>{{ $row['org_s'] }}</td>
                <td>{{ $row['inn_s'] }}</td>
                <td>{{ $row['class_s'] }}</td>
                <td>{{ $row['date_s'] }}</td>
                <td>{{ $row['birth_s'] }}</td>
                <td>{{ $row['snils_s'] }}</td>
                <td>{{ $row['class'] }}</td>
                <td>{{ $row['subject'] }}</td>
                <td>{{ $row['modul'] }}</td>
                <td>{{ $row['hours'] }}</td>
                <td>{{ $row['form_of_education'] }}</td>
                <td>{{ $row['form_education_implementation'] }}</td>
                <td>{{ $row['EducationalPrograms'] }}</td>
                <td>{{ $row['EducationalActivities'] }}</td>
                <td>{{ $row['content'] }}</td>
                <td>{{ $row['date_begin'] }}</td>
                <td>{{ $row['date_end'] }}</td>
                <td>{{ $row['archive'] }}</td>
                <td>{{ $row['program'] }}</td>
                <td>{{ $row['registr'] }}</td>
                <td>{{ $row['status'] }}</td>
                <td>{{ $row['schedule'] }}</td>
                <td>{{ $row['agreement_filename'] }}</td>
                <td>{{ $row['agreement_filename_sign'] }}</td>
                <td>{{ $row['agreement_filename_sign2'] }}</td>
                <td>{{ $row['agreement_filename_2'] }}</td>
                <td>{{ $row['agreement_filename_sign_2'] }}</td>
                <td>{{ $row['agreement_filename_sign2_2'] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<script>
    let offset = 100;
    const limit = 100; // Количество загружаемых записей

    // $(document).ready(function() {
    //     loadMoreData(); // Загружаем первые данные при загрузке страницы
    // });

    function loadMoreData() {
        $.ajax({
            url: '/load-more-pupils/{{ $year }}/{{ $orgtype }}',
            method: 'GET',
            data: { offset: offset },
            success: function(data) {
                if (data.length > 0) {
                    data.forEach(row => {
                        $('tbody').append(`
                            <tr>
                                <td>${row.district_b}</td>
                                <td>${row.fullname_b}</td>
                                <td>${row.inn_b}</td>
                                <td>${row.district_s}</td>
                                <td>${row.fullname_s}</td>
                                <td>${row.org_s}</td>
                                <td>${row.inn_s}</td>
                                <td>${row.class_s}</td>
                                <td>${row.date_s}</td>
                                <td>${row.birth_s}</td>
                                <td>${row.snils_s}</td>
                                <td>${row.class}</td>
                                <td>${row.subject}</td>
                                <td>${row.modul}</td>
                                <td>${row.hours}</td>
                                <td>${row.form_of_education}</td>
                                <td>${row.form_education_implementation}</td>
                                <td>${row.EducationalPrograms}</td>
                                <td>${row.EducationalActivities}</td>
                                <td>${row.content}</td>
                                <td>${row.date_begin}</td>
                                <td>${row.date_end}</td>
                                <td>${row.archive}</td>
                                <td>${row.program}</td>
                                <td>${row.registr}</td>
                                <td>${row.status}</td>
                                <td>${row.schedule}</td>
                                <td>${row.agreement_filename}</td>
                                <td>${row.agreement_filename_sign}</td>
                                <td>${row.agreement_filename_sign2}</td>
                                <td>${row.agreement_filename_2}</td>
                                <td>${row.agreement_filename_sign_2}</td>
                                <td>${row.agreement_filename_sign2_2}</td>
                            </tr>
                        `);
                    });
                    offset += limit; // Увеличиваем смещение
                } else {
                    // Если больше нет данных, скрываем кнопку "Загрузить еще"
                    $('#load-more-button').hide();
                }
            }
        });
    }

    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
            loadMoreData(); // Загружаем больше данных при достижении конца страницы
        }
    });
</script>

<!-- Кнопка для загрузки данных -->
<button id="load-more-button" onclick="loadMoreData()">Загрузить еще</button>


<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
<script type='text/javascript'>
    function html_table_to_excel(type)
    {
        var data1 = document.getElementById('myTable');

        var ws = XLSX.utils.table_to_sheet(data1);

        var wb = XLSX.utils.book_new();

        XLSX.utils.book_append_sheet(wb, ws, "Таблица 1");

        XLSX.write(wb, {bookType: 'xlsx', bookSST: true, type: 'base64'});

        XLSX.writeFile(wb, 'Экспорт.' + type);
    }

    const export_button = document.getElementById('export_button');
    export_button.addEventListener('click', () =>  {
        html_table_to_excel('xlsx');
    });
</script>

@endsection