@extends('layouts.app')

@section('content')
@if (Auth::user() && Auth::user()->status == 10)
<div class="row justify-content-center">
    <div style="width:400px;max-width:100%;">
        <h2>Муниципальный координатор ({{Auth::user()->getDistrict->fullname}})</h2><br/>
        <ul class="list-group">
            @for($i = date('Y'); $i >= 2020; $i--)
                <li class="list-group-item">
                    <a class="btn btn-outline-dark" href="/admin/mun/{{ Auth::user()->getDistrict->id }}/{{ $i }}-{{ $i+1 }}" target="_blank">
                        {{ $i }}-{{ $i+1 }}
                    </a>
                </li>
            @endfor
        </ul>
    </div>
</div>
@endif
@endsection