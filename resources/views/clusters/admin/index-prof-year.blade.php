@extends('layouts.app')

@section('content')
@if (Auth::user() && Auth::user()->status == 27)

<h2>Координатор СПО</h2>
<h2>{{ $year }}</h2>

<ul class="list-group list-group-horizontal">
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'all']) }}">Список действующих организаций Воронежской области</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'school']) }}">Школы</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'doo']) }}">Сады</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'dop']) }}">Доп. образование</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'prof']) }}">СПО</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'vuz']) }}">ВУЗы</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'inter']) }}">Интернаты</a>
    </li>
</ul>

<ul class="list-group list-group-horizontal">
    <li class="list-group-item flex-fill" style="background-color:lightsteelblue">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/prof/spo-programs-year">
            Программы СПО
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:lightsteelblue">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/prof/export-pupils-proposed-year">
            Скачать список программ СПО
        </a>
    </li>
</ul>
<ul class="list-group list-group-horizontal">
    <li class="list-group-item flex-fill" style="background-color:#d8e1ee">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/prof/export-spo-programs-not-archive-year">
            Скачать не архив (основные)
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:#d8e1ee">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/prof/export-spo-vuz-programs-archive-year">
            Скачать архив (основные)
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:#d8e1ee">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/prof/export-amount-of-students-year">
            Скачать количество учеников (основные)
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:#d8e1ee">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/prof/export-amount-of-students-year2">
            Скачать количество учеников по школам (основные)
        </a>
    </li>
</ul>
<ul class="list-group list-group-horizontal">
    <li class="list-group-item flex-fill" style="background-color:#f2f5f9">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/prof/export-spo-programs-not-archive-year2">
            Скачать не архив (дополнительные)
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:#f2f5f9">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/prof/export-spo-vuz-programs-archive-year2">
            Скачать архив (дополнительные)
        </a>
    </li>
</ul>
@endif
@endsection