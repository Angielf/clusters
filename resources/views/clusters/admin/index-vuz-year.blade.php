@extends('layouts.app')

@section('content')
@if (Auth::user() && Auth::user()->status == 17)

<h2>Координатор ВУЗов</h2>
<h2>{{ $year }}</h2>

<ul class="list-group list-group-horizontal">
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'all']) }}">Список действующих организаций Воронежской области</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'school']) }}">Школы</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'doo']) }}">Сады</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'dop']) }}">Доп. образование</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'prof']) }}">СПО</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'vuz']) }}">ВУЗы</a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:antiquewhite">
        <a class="btn btn-outline-dark" href="{{ route('org_list', ['orgtype' => 'inter']) }}">Интернаты</a>
    </li>
</ul>

<ul class="list-group list-group-horizontal">
    <li class="list-group-item flex-fill" style="background-color:rosybrown">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/vuz/spo-programs-year">
            Программы ВУЗов
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:rosybrown">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/vuz/export-pupils-proposed-year">
            Скачать список программ ВУЗов
        </a>
    </li>
</ul>
<ul class="list-group list-group-horizontal">
    <li class="list-group-item flex-fill" style="background-color:#dec6c5">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/vuz/export-spo-programs-not-archive-year">
            Скачать не архив (основные)
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:#dec6c5">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/vuz/export-spo-vuz-programs-archive-year">
            Скачать архив (основные)
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:#dec6c5">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/vuz/export-amount-of-students-year">
            Скачать количество учеников (основные)
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:#dec6c5">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/vuz/export-amount-of-students-year2">
            Скачать количество учеников по школам (основные)
        </a>
    </li>
</ul>
<ul class="list-group list-group-horizontal">
    <li class="list-group-item flex-fill" style="background-color:#f4ecec">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/vuz/export-spo-programs-not-archive-year2">
            Скачать не архив (дополнительные)
        </a>
    </li>
    <li class="list-group-item flex-fill" style="background-color:#f4ecec">
        <a class="btn btn-outline-dark" href="/reg/{{$year}}/vuz/export-spo-vuz-programs-archive-year2">
            Скачать архив (дополнительные)
        </a>
    </li>
</ul>

@endif
@endsection