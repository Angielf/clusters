@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10 || Auth::user()->status == 17 || Auth::user()->status == 27 || Auth::user()->id == $user_org->id))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @if(Auth::user()->status == 1)
            <p><a href="{{ route('org_list', ['orgtype' => 'all'])}}"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться к списку организаций Воронежской области
            </a></p>
            @elseif(Auth::user()->status == 10)
            <p><a href="/users/{{ Auth::user()->getDistrict->id }}/all/org-list-mun"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться к списку организаций муниципалитета
            </a></p>
            @elseif(Auth::user()->id == $user_org->id)
            <p><a href="{{ route('home')}}"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться на главную страницу
            </a></p>
            @endif

            <table class="table table-striped table-bordered">
                <tbody>
                    @if(Auth::user()->id != $user_org->id)
                  <tr>
                    <th scope="row">id</th>
                    <td>{{ $user_org->id }}</td>
                    <td></td>
                  </tr>
                  @endif
                  <tr>
                    <th scope="row">Район</th>
                    <td>{{ $user_org->getDistrict->fullname }}</td>
                    <td></td>
                  </tr>
                  <tr>
                    <th scope="row">Организация</th>
                    <td>{{ $user_org->fullname }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myFullname">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Полное название организации</th>
                    <td>{{ $user_org->fullname_full }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myFullname_full">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Организационно-правовая форма (краткое название)</th>
                    <td>{{ $user_org->opf_short }}</td>
                    <td>
                        {{-- <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myOpf_short">
                            <i class="far fa-edit"></i> Изменить
                        </button> --}}
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Дата выдачи лицензии</th>
                    <td>{{ $user_org->date_license }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myDate_license">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Номер лицензии</th>
                    <td>{{ $user_org->number_license }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myNumber_license">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Кем выдана лицензия (в творительном падеже)</th>
                    <td>{{ $user_org->who_license }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myWho_license">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Должность руководителя</th>
                    <td>{{ $user_org->director_post }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myDirector_post">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Должность руководителя (в родительном падеже, с маленькой буквы)</th>
                    <td>{{ $user_org->director_post_rod }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myDirector_post_rod">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">ФИО руководителя</th>
                    <td>{{ $user_org->director }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myDirector">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">ФИО руководителя (в родительном падеже)</th>
                    <td>{{ $user_org->director_rod }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myDirector_rod">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row">ИНН</th>
                    <td>{{ $user_org->inn }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myInn">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">КПП</th>
                    <td>{{ $user_org->kpp }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myKpp">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">ОГРН</th>
                    <td>{{ $user_org->ogrn }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myOgrn">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">ОКПО</th>
                    <td>{{ $user_org->okpo }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myOkpo">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">ОКТМО</th>
                    <td>{{ $user_org->oktmo }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myOktmo">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">КБК</th>
                    <td>{{ $user_org->kbk }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myKbk">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">ИКЗ</th>
                    <td>{{ $user_org->ikz }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myIkz">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Пункт части I статьи 93</th>
                    <td>{{ $user_org->punkt }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myPunkt">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">БИК</th>
                    <td>{{ $user_org->bik }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myBik">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                        {{-- @if($user_org->opf_short == 'МКУ' or $user_org->opf_short == 'ГКУ' or $user_org->opf_short == 'ФГКУ')
                            Единый казначейский счёт
                        @else
                            Расчётный счёт
                        @endif --}}
                        <p>
                            <i>Расчётный счёт или Единый казначейский счёт?</i>
                            <br>
                            {{ $user_org->single_treasury_account_type }}
                        </p>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mySingle_treasury_account_type">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </th>
                    <td>{{ $user_org->single_treasury_account }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mySingle_treasury_account">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                        {{-- @if($user_org->opf_short == 'МКУ' or $user_org->opf_short == 'ГКУ' or $user_org->opf_short == 'ФГКУ')
                            Казначейский счёт
                        @else
                            Корреспондентский счет
                        @endif --}}
                        <p>
                            <i>Казначейский счёт или Корреспондентский счёт?</i>
                            <br>
                            {{ $user_org->treasury_account_type }}
                        </p>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myTreasury_account_type">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </th>
                    <td>{{ $user_org->treasury_account }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myTreasury_account">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Банк</th>
                    <td>{{ $user_org->bank }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myBank">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>

                  <tr>
                    <th scope="row">Адрес</th>
                    <td>{{ $user_org->address }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myAdd">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Телефон</th>
                    <td>{{ $user_org->tel }}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myTel">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Почта</th>
                    <td><a href="mailto:{{ $user_org->email_real }}">{{ $user_org->email_real }}</a></td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myEmail">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Сайт</th>
                    <td><a href="http://{{ $user_org->website }}" target="_blank">{{ $user_org->website }}</a></td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myWeb">
                            <i class="far fa-edit"></i> Изменить
                        </button>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">ШНОР</th>
                    @if($user_org->low == 'r')
                        <td>рег. ур.</td>
                    @elseif($user_org->low == 'f')
                        <td>фед. ур.</td>
                    @else
                        <td></td>
                    @endif
                    <td>
                        @if(Auth::user()->status == 1 or Auth::user()->status == 10 or Auth::user()->status == 17 or Auth::user()->status == 27)
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myShnor">
                                <i class="far fa-edit"></i> Изменить
                            </button>
                        @endif
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Тип</th>
                    @if($user_org->org_type == 'school')
                        <td>общеобразовательная организация</td>
                    @elseif($user_org->org_type == 'prof')
                        <td>профессиональная образовательная организация</td>
                    @elseif($user_org->org_type == 'vuz')
                        <td>ВУЗ</td>
                    @elseif($user_org->org_type == 'doo')
                        <td>дошкольная образовательная организация</td>
                    @elseif($user_org->org_type == 'inter')
                        <td>интернат</td>
                    @elseif($user_org->org_type == 'dop')
                        <td>организация дополнительного образования</td>
                    @else
                        <td></td>
                    @endif
                    <td>
                        @if(Auth::user()->status == 1 or Auth::user()->status == 10 or Auth::user()->status == 17 or Auth::user()->status == 27)
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myType">
                                <i class="far fa-edit"></i> Изменить
                            </button>
                        @endif
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Зона</th>
                    <td>{{ $user_org->zone }}</td>
                    <td>
                        @if(Auth::user()->status == 1 or Auth::user()->status == 10 or Auth::user()->status == 17 or Auth::user()->status == 27)
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myZone">
                                <i class="far fa-edit"></i> Изменить
                            </button>
                        @endif
                    </td>
                  </tr>
                  @if($user_org->status !== 100)
                  <tr>
                    <th scope="row">Кол-во заявок</th>
                    <td>{{ $user_org->amount_of_bids() }}</td>
                    <td></td>
                  </tr>
                  @endif
                  <tr>
                    <th scope="row">Кол-во одобренных программ</th>
                    <td>{{ $user_org->amount_of_programs_1() }}</td>
                    <td></td>
                  </tr>
                  <tr>
                    <th scope="row">Кол-во предложенных программ</th>
                    <td>{{ $user_org->amount_of_proposed_programs() }}</td>
                    <td></td>
                  </tr>
                </tbody>
            </table>
            <br/>


            <!-- Модальное окно Fullname-->
            <div class="modal fade" id="myFullname" tabindex="-1" role="dialog" aria-labelledby="myModalLabelFullname" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelFullname">Краткое название организации {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_fullname', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <textarea class="form-control" name="fullname" id="fullname" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Fullname --}}

            <!-- Модальное окно fullname_full-->
            <div class="modal fade" id="myFullname_full" tabindex="-1" role="dialog" aria-labelledby="myModalLabelFullname_full" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelFullname_full">Полное название организации {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_fullname_full', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="fullname_full" id="fullname_full" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна fullname_full --}}

            <!-- Модальное окно opf_short-->
            <div class="modal fade" id="myOpf_short" tabindex="-1" role="dialog" aria-labelledby="myModalLabelOpf_short" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelOpf_short">Организационно-правовая форма (краткое название) {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_opf_short', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="opf_short" id="opf_short" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна opf_short --}}

            <!-- Модальное окно date_license-->
            <div class="modal fade" id="myDate_license" tabindex="-1" role="dialog" aria-labelledby="myModalLabelDate_license" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelDate_license">Дата выдачи лицензии {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_date_license', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="date_license" id="date_license" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна date_license --}}

            <!-- Модальное окно number_license-->
            <div class="modal fade" id="myNumber_license" tabindex="-1" role="dialog" aria-labelledby="myModalLabelNumber_license" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelNumber_license">Номер лицензии {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_number_license', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="number_license" id="number_license" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна number_license --}}

            <!-- Модальное окно who_license-->
            <div class="modal fade" id="myWho_license" tabindex="-1" role="dialog" aria-labelledby="myModalLabelWho_license" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelWho_license">Кем выдана лицензия {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_who_license', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="who_license" id="who_license" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна who_license --}}

            <!-- Модальное окно director_post-->
            <div class="modal fade" id="myDirector_post" tabindex="-1" role="dialog" aria-labelledby="myModalLabelDirector_post" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelDirector_post">Должность руководителя {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_director_post', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="director_post" id="director_post" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна director_post --}}

            <!-- Модальное окно ddirector_post_rod-->
            <div class="modal fade" id="myDirector_post_rod" tabindex="-1" role="dialog" aria-labelledby="myModalLabelDirector_post_rod" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelDirector_post_rod">Должность руководителя (род. п.) {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_director_post_rod', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="director_post_rod" id="director_post_rod" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна director_post_rod --}}

            <!-- Модальное окно director-->
            <div class="modal fade" id="myDirector" tabindex="-1" role="dialog" aria-labelledby="myModalLabelDirector" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelDirector">ФИО руководителя {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_director', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="director" id="director" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна director --}}

            <!-- Модальное окно director_rod-->
            <div class="modal fade" id="myDirector_rod" tabindex="-1" role="dialog" aria-labelledby="myModalLabelDirector_rod" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelDirector_rod">ФИО руководителя (род. п.) {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_director_rod', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="director_rod" id="director_rod" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна director_rod --}}

            <!-- Модальное окно inn-->
            <div class="modal fade" id="myInn" tabindex="-1" role="dialog" aria-labelledby="myModalLabelInn" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelInn">ИНН {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_inn', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="inn" id="inn" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна inn --}}

            <!-- Модальное окно okpo-->
            <div class="modal fade" id="myOkpo" tabindex="-1" role="dialog" aria-labelledby="myModalLabelOkpo" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelOkpo">ОКПО {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_okpo', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="okpo" id="okpo" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна okpo --}}

            <!-- Модальное окно oktmo-->
            <div class="modal fade" id="myOktmo" tabindex="-1" role="dialog" aria-labelledby="myModalLabelOktmo" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelOktmo">ОКТМО {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_oktmo', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="oktmo" id="oktmo" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна oktmo --}}


            <!-- Модальное окно kpp-->
            <div class="modal fade" id="myKpp" tabindex="-1" role="dialog" aria-labelledby="myModalLabelKpp" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelInn">КПП {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_kpp', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="kpp" id="kpp" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна kpp --}}

            <!-- Модальное окно ogrn-->
            <div class="modal fade" id="myOgrn" tabindex="-1" role="dialog" aria-labelledby="myModalLabelOgrn" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelOgrn">ОГРН {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_ogrn', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="ogrn" id="ogrn" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна ogrn --}}

            <!-- Модальное окно kbk-->
            <div class="modal fade" id="myKbk" tabindex="-1" role="dialog" aria-labelledby="myModalLabelKbk" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelKbk">КБК {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_kbk', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="kbk" id="kbk" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна kbk --}}

            <!-- Модальное окно ikz-->
            <div class="modal fade" id="myIkz" tabindex="-1" role="dialog" aria-labelledby="myModalLabelIkz" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelIkz">ИКЗ {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_ikz', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="ikz" id="ikz" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна ikz --}}

            <!-- Модальное окно punkt-->
            <div class="modal fade" id="myPunkt" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPunkt" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelPunkt">Пункт {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_punkt', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="punkt" id="punkt" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна punkt --}}

            <!-- Модальное окно bik-->
            <div class="modal fade" id="myBik" tabindex="-1" role="dialog" aria-labelledby="myModalLabelBik" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelBik">БИК {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_bik', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="bik" id="bik" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна bik --}}

            <!-- Модальное окно single_treasury_account-->
            <div class="modal fade" id="mySingle_treasury_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSingle_treasury_account" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelSingle_treasury_account"> {{ $user_org->single_treasury_account_type }} {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_single_treasury_account', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="single_treasury_account" id="single_treasury_account" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна single_treasury_account --}}

            <!-- Модальное окно single_treasury_account_type-->
            <div class="modal fade" id="mySingle_treasury_account_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSingle_treasury_account_type" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelSingle_treasury_account_type">Расчётный счёт или Единый казначейский счёт?{{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_single_treasury_account_type', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="single_treasury_account_type" id="single_treasury_account_type" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна single_treasury_account_type --}}

            <!-- Модальное окно treasury_account-->
            <div class="modal fade" id="myTreasury_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTreasury_account" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelTreasury_account">{{ $user_org->treasury_account_type }} {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_treasury_account', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="treasury_account" id="treasury_account" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна treasury_account --}}

            <!-- Модальное окно treasury_account_type-->
            <div class="modal fade" id="myTreasury_account_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTreasury_account_type" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelTreasury_account_type">Казначейский счёт или Корреспондентский счёт? {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_treasury_account_type', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="treasury_account_type" id="treasury_account_type" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна treasury_account_type --}}

            <!-- Модальное окно bank-->
            <div class="modal fade" id="myBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabelBank" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelBank">Банк {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_bank', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="bank" id="bank" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна treasury_account --}}

            <!-- Модальное окно Add-->
            <div class="modal fade" id="myAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAdd" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelAdd">Адрес {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_add', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <textarea class="form-control" name="add" id="add" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Add --}}

            <!-- Модальное окно Tel-->
            <div class="modal fade" id="myTel" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelTel">Телефон {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_tel', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <textarea class="form-control" name="tel" id="tel" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Tel --}}

            <!-- Модальное окно Email-->
            <div class="modal fade" id="myEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEmail" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelEmail">Почта {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_email_real', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="email_real" id="email_real" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Email --}}

            <!-- Модальное окно Web-->
            <div class="modal fade" id="myWeb" tabindex="-1" role="dialog" aria-labelledby="myModalLabelWeb" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelWeb">Сайт {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_web', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="website" id="website" class="form-control" />
                                    <small class="form-text text-muted">Без http:// или https://</small>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Web --}}

            <!-- Модальное окно Shnor-->
            <div class="modal fade" id="myShnor" tabindex="-1" role="dialog" aria-labelledby="myModalLabelShnor" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelShnor">ШНОР {{ $user_org->fullname}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_shnor', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    {{-- <input type="text" name="shnor" id="shnor" class="form-control" /> --}}
                                    {{-- <input type="text" class="form-control" name="shnor" id="shnor" list="Shnor">
                                    <datalist id="Shnor">
                                        <option value=""></option>
                                        <option value="r">рег. ур.</option>
                                        <option value="f">фед. ур.</option>
                                    </datalist> --}}
                                    <select class="form-control" name="shnor" id="shnor">
                                        <option></option>
                                        <option value="r">
                                            рег. ур.
                                        </option>
                                        <option value="f">
                                            фед. ур.
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Shnor --}}

            <!-- Модальное окно Type-->
            <div class="modal fade" id="myType" tabindex="-1" role="dialog" aria-labelledby="myModalLabelType" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelType">Тип {{ $user_org->fullname}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_type', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <select class="form-control" name="org_type" id=org_type">
                                        <option></option>
                                        <option value="school">
                                            общеобразовательная организация
                                        </option>
                                        <option value="prof">
                                            профессиональная образовательная организация
                                        </option>
                                        <option value="vuz">
                                            ВУЗ
                                        </option>
                                        <option value="doo">
                                            дошкольная образовательная организация
                                        </option>
                                        <option value="inter">
                                            интернат
                                        </option>
                                        <option value="dop">
                                            организация дополнительного образования
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Type --}}

            <!-- Модальное окно Zone-->
            <div class="modal fade" id="myZone" tabindex="-1" role="dialog" aria-labelledby="myModalLabelZone" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelZone">Зона {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_zone', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="zone" id="zone" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Zone --}}


            @if($user_org->status !== 100)
            <h5 align="center">Заявки на дефицит</h5>
            <table class="table table-striped" id="myTable">
                <thead>
                    <tr>
                        <th scope="col">Организация-участник</th>

                        <th scope="col">Класс/ Кол-во классов(групп)/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                        <th scope="col">
                            Форма/условия реализации обучения/ Образовательная программа/деятельность/ Комментарий
                        </th>

                        <th scope="col">
                            Даты
                        </th>

                        <th scope="col">Программа/ Расписание</th>
                        <th scope="col">Кол-во учеников/Список</th>
                        <th scope="col">Договор</th>
                    </tr>
                    </thead>

                    <tbody id="table1">
                        @foreach($user_org->bids_not_archives() as $bid)
                            @if(($bid->status === 1))
                                <tr>
                                    <td>
                                        {{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->fullname}}
                                        @if (Auth::user()->status == 1 || Auth::user()->status == 10)
                                        <p><a class="btn btn-outline-dark" href="/users/{{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->id }}/show-org">Информация</a></p>
                                        @elseif(Auth::user()->id == $user_org->id)
                                        <p><a class="btn btn-outline-dark" href="/users/{{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->id }}/about-org">Сведения</a></p>
                                        @endif
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul1">
                                            <li class="list-group-item">{{ $bid->getClasses() }}</li>
                                            <li class="list-group-item">{{ $bid->amount_of_classes }}</li>
                                            <li class="list-group-item">{{ $bid->subject }}</li>
                                            <li class="list-group-item">{{ $bid->modul }}</li>
                                            <li class="list-group-item">{{ $bid->hours }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul2">
                                            <li class="list-group-item">{{ $bid->form_of_education }} </li>
                                            <li class="list-group-item">{{ $bid->form_education_implementation }}</li>
                                            <li class="list-group-item">{{ $bid->getEducationalPrograms() }}</li>
                                            <li class="list-group-item">{{ $bid->getEducationalActivities() }}</li>
                                            <li class="list-group-item">{{ $bid->content }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $bid->getDataBegin() }} </li>
                                            <li class="list-group-item">{{ $bid->getDataEnd() }}</li>
                                        </ul>
                                    </td>


                                    <td>
                                        <li class="list-group-item">
                                            <a href="/files/programs/{{ $bid->programs()->sortByDesc('status')->first()->filename }}"
                                                class="btn btn-outline-success">
                                                Программа
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            @if(($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1))
                                                <a href="/files/schedules/{{ $bid->programs()->sortByDesc('status')->first()->schedule->filename }}"
                                                    class="btn btn-outline-success">
                                                    Расписание
                                                </a>
                                            @endif
                                        </li>
                                        <li class="list-group-item">
                                            @if(($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule->months_hour))
                                                <a href="/months_hours/{{ $bid->programs()->sortByDesc('status')->first()->schedule->months_hour->id }}/inf"
                                                    class="btn btn-outline-success">
                                                        Кол-во часов по месяцам
                                                </a>
                                            @endif
                                        </li>

                                    </td>

                                    <td>
                                        @if (($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                        and ($bid->programs()->sortByDesc('status')->first()->schedule->student))
                                            <li class="list-group-item">
                                                {{$bid->programs()->sortByDesc('status')->first()->schedule->student->students_amount}}
                                            </li>
                                            <li class="list-group-item">
                                                <a href="/files/students/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->filename }}"
                                                    class="btn btn-outline-success">
                                                    Список учеников
                                                </a>
                                            </li>
                                        @endif
                                    </td>

                                    <td>
                                        @if (($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                        and ($bid->programs()->sortByDesc('status')->first()->schedule->student) and ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement))
                                            <li class="list-group-item">
                                                <a href="/files/agreements/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                    class="btn btn-outline-success">
                                                        Договор
                                                </a>
                                                <br>
                                                @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign == 1)
                                                <a href="/files/agreements/signed/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign }}"
                                                    class="btn btn-outline-success">
                                                    Подпись базовой организации
                                                </a>
                                                @endif
                                                <br>
                                                @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign2 == 1)
                                                    <a href="/files/agreements/signed2/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 }}"
                                                        class="btn btn-outline-success">
                                                        Подпись организации-участника
                                                    </a>
                                                @endif
                                            </li>
                                        @endif
                                    </td>
                                <tr>
                            @endif

                            @if(($bid->status === 0) or ($bid->status === 9))
                                <tr>
                                    <td></td>
                                    <td>
                                        <ul class="list-group" id="ul3">
                                            <li class="list-group-item">{{ $bid->getClasses() }}</li>
                                            <li class="list-group-item">{{ $bid->amount_of_classes }}</li>
                                            <li class="list-group-item">{{ $bid->subject }}</li>
                                            <li class="list-group-item">{{ $bid->modul }}</li>
                                            <li class="list-group-item">{{ $bid->hours }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul4">
                                            <li class="list-group-item">{{ $bid->form_of_education }}</li>
                                            <li class="list-group-item">{{ $bid->form_education_implementation }}</li>
                                            <li class="list-group-item">{{ $bid->getEducationalPrograms() }}</li>
                                            <li class="list-group-item">{{ $bid->getEducationalActivities() }}</li>
                                            <li class="list-group-item">{{ $bid->content }}</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $bid->getDataBegin() }} </li>
                                            <li class="list-group-item">{{ $bid->getDataEnd() }}</li>
                                        </ul>
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                    </tbody>
            </table>
            <br/>
            <br/>
            @endif




            <h5 align="center">Одобренные программы</h5>
            <table class="table table-striped" id="myTable2">
                <thead>
                    <tr>
                        <th scope="col">Базовая организация</th>

                        <th scope="col">Класс/ Кол-во классов(групп)/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                        <th scope="col">
                            Форма/условия реализации обучения/ Образовательная программа/деятельность/ Комментарий
                        </th>

                        <th scope="col">
                            Даты
                        </th>

                        <th scope="col">Программа/ Расписание</th>
                        <th scope="col">Кол-во учеников/Список</th>
                        <th scope="col">Договор</th>
                    </tr>
                    </thead>

                    <tbody id="table2">
                        @foreach($user_org->programs_not_archives() as $program)
                            @if(($program->status === 1))
                                <tr>
                                    <td>
                                        {{ $program->bid->user->fullname }}
                                        @if (Auth::user()->status == 1 || Auth::user()->status == 10)
                                        <p><a class="btn btn-outline-dark" href="/users/{{ $program->bid->user->id }}/show-org">Информация</a></p>
                                        @elseif(Auth::user()->id == $user_org->id)
                                        <p><a class="btn btn-outline-dark" href="/users/{{ $program->bid->user->id }}/about-org">Сведения</a></p>
                                        @endif
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul1">
                                            <li class="list-group-item">{{ $program->bid->getClasses() }}</li>
                                            <li class="list-group-item">{{ $program->bid->amount_of_classes }}</li>
                                            <li class="list-group-item">{{ $program->bid->subject }}</li>
                                            <li class="list-group-item">{{ $program->bid->modul }}</li>
                                            <li class="list-group-item">{{ $program->bid->hours }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul2">
                                            <li class="list-group-item">{{ $program->bid->form_of_education }} </li>
                                            <li class="list-group-item">{{ $program->bid->form_education_implementation }}</li>
                                            <li class="list-group-item">{{ $program->bid->getEducationalPrograms() }}</li>
                                            <li class="list-group-item">{{ $program->bid->getEducationalActivities() }}</li>
                                            <li class="list-group-item">{{ $program->bid->content }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $program->bid->getDataBegin() }} </li>
                                            <li class="list-group-item">{{ $program->bid->getDataEnd() }}</li>
                                        </ul>
                                    </td>


                                    <td>
                                        <li class="list-group-item">
                                            <a href="/files/programs/{{ $program->filename }}"
                                                class="btn btn-outline-success">
                                                Программа
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            @if(($program->schedule) and ($program->schedule->status === 1))
                                                <a href="/files/schedules/{{ $program->schedule->filename }}"
                                                    class="btn btn-outline-success">
                                                    Расписание
                                                </a>
                                            @endif
                                        </li>
                                        <li class="list-group-item">
                                            @if(($program->schedule) and ($program->schedule->status === 1) and ($program->schedule->months_hour))
                                                <a href="/months_hours/{{ $program->schedule->months_hour->id }}/inf"
                                                    class="btn btn-outline-success">
                                                        Кол-во часов по месяцам
                                                </a>
                                            @endif
                                        </li>

                                    </td>

                                    <td>
                                        @if (($program->schedule) and ($program->schedule->status === 1)
                                        and ($program->schedule->student))
                                            <li class="list-group-item">
                                                {{$program->schedule->student->students_amount}}
                                            </li>
                                            <li class="list-group-item">
                                                <a href="/files/students/{{ $program->schedule->student->filename }}"
                                                    class="btn btn-outline-success">
                                                    Список учеников
                                                </a>
                                            </li>
                                        @endif
                                    </td>

                                    <td>
                                        @if (($program->schedule) and ($program->schedule->status === 1)
                                        and ($program->schedule->student) and ($program->schedule->student->agreement))
                                            <li class="list-group-item">
                                                <a href="/files/agreements/{{ $program->schedule->student->agreement->filename }}"
                                                    class="btn btn-outline-success">
                                                        Договор
                                                </a>
                                                <br>
                                                @if($program->schedule->student->agreement->sign == 1)
                                                <a href="/files/agreements/signed/{{ $program->schedule->student->agreement->filename_sign }}"
                                                    class="btn btn-outline-success">
                                                    Подпись базовой организации
                                                </a>
                                                @endif
                                                <br>
                                                @if($program->schedule->student->agreement->sign2 == 1)
                                                <a href="/files/agreements/signed2/{{ $program->schedule->student->agreement->filename_sign2 }}"
                                                    class="btn btn-outline-success">
                                                    Подпись организации-участника
                                                </a>
                                                @endif
                                            </li>
                                        @endif
                                    </td>
                                <tr>
                            @endif
                        @endforeach

                    </tbody>
              </table>
            </br>
            <br/>


            <h5 align="center">Ваши образовательные программы</h5>
            <table class="table table-striped" id="myTable3">
                <thead>
                    <tr>

                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                        <th scope="col">
                            Форма/условия реализации обучения/ Образовательная программа/деятельность/ Комментарий
                        </th>

                        <th scope="col">
                            Даты
                        </th>

                    </tr>
                    </thead>

                    <tbody id="table3">
                        @foreach($user_org->proposed_programs() as $proposed_program)
                                <tr>
                                    <td>
                                        <ul class="list-group" id="ul3">
                                            <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                            <li class="list-group-item">{{ $proposed_program->subject }}</li>
                                            <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                            <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul4">
                                            <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                            <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                            <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                            <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                            <li class="list-group-item">{{ $proposed_program->content }}</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $proposed_program->getDataBegin() }} </li>
                                            <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                        </ul>
                                    </td>
                                </tr>
                        @endforeach

                    </tbody>
            </table>
            <br/>
            <br/>


            <h5 align="center">Организации, которые взяли ваши программы</h5>
            <table class="table table-striped" id="myTable4">
                <thead>
                    <tr>
                        <th scope="col">Базовая организация</th>

                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                        <th scope="col">
                            Форма/условия реализации обучения/ Образовательная программа/деятельность/ Комментарий
                        </th>

                        <th scope="col">
                            Даты
                        </th>

                        <th scope="col">Программа/ Расписание</th>
                        <th scope="col">Кол-во учеников/Список</th>
                        <th scope="col">Договор</th>
                    </tr>
                    </thead>

                    <tbody id="table4">
                        @if ($user_org->proposed_programs())
                        @foreach( $user_org->proposed_programs() as $proposed_program)
                            @if ($proposed_program->selected_programs())
                                @foreach($proposed_program->selected_programs() as $selected_program)
                                <tr>
                                    <td>
                                        {{$selected_program->user->fullname }}
                                        @if (Auth::user()->status == 1 || Auth::user()->status == 10)
                                        <p><a class="btn btn-outline-dark" href="/users/{{ $selected_program->user->id }}/show-org">Информация</a></p>
                                        @elseif(Auth::user()->id == $user_org->id)
                                        <p><a class="btn btn-outline-dark" href="/users/{{ $selected_program->user->id }}/about-org">Сведения</a></p>
                                        @endif
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul1">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getClasses() }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->subject }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->modul }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->hours }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul2">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->form_of_education }} </li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->form_education_implementation }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->educational_program }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->educational_activitiy }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->content }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getDataBegin() }} </li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getDataEnd() }}</li>
                                        </ul>
                                    </td>


                                    <td>
                                        <li class="list-group-item">
                                            <a href="/files/proposed_programs/{{ $selected_program->proposed_program->filename }}"
                                                class="btn btn-outline-success">
                                                Программа
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            @if(($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1))
                                                <a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                                class="btn btn-outline-success">
                                                    Расписание
                                                </a>
                                            @endif
                                        </li>
                                        <li class="list-group-item">
                                            @if(($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1) and ($selected_program->selected_schedule->selected_months_hour))
                                                <a href="/selected_months_hours/{{ $selected_program->selected_schedule->selected_months_hour->id }}/update"
                                                    class="btn btn-outline-info btn">
                                                    Кол-во часов по месяцам
                                                </a>
                                            @endif
                                        </li>

                                    </td>

                                    <td>
                                        @if (($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1)
                                        and ($selected_program->selected_schedule->selected_student))
                                            <li class="list-group-item">
                                                {{$selected_program->selected_schedule->selected_student->students_amount}}
                                            </li>
                                            <li class="list-group-item">
                                                <a href="/files/students/{{ $selected_program->selected_schedule->selected_student->filename }}"
                                                    class="btn btn-outline-success">
                                                    Список учеников
                                                </a>
                                            </li>
                                        @endif
                                    </td>

                                    <td>
                                        @if (($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1)
                                        and ($selected_program->selected_schedule->selected_student) and ($selected_program->selected_schedule->selected_student->selected_agreement))
                                            <li class="list-group-item">
                                                <a href="/files/selected_agreements/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                                    class="btn btn-outline-success">
                                                    Договор
                                                </a>
                                                <br>
                                                @if($selected_program->selected_schedule->selected_student->selected_agreement->sign == 1)
                                                <a href="/files/selected_agreements/signed/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign }}"
                                                    class="btn btn-outline-success">
                                                    Подпись базовой организации
                                                </a>
                                                @endif
                                                <br>
                                                @if($selected_program->selected_schedule->selected_student->selected_agreement->sign2 == 1)
                                                <a href="/files/selected_agreements/signed2/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign2 }}"
                                                    class="btn btn-outline-success">
                                                    Подпись организации-участника
                                                </a>
                                                @endif
                                            </li>
                                        @endif
                                    </td>
                                <tr>
                            @endforeach
                            @endif
                        @endforeach
                        @endif

                    </tbody>
              </table>
            </br>
            <br/>


            @if($user_org->status !== 100)
            <h5 align="center">Взятые вами образовательные программы</h5>
            <table class="table table-striped" id="myTable5">
                <thead>
                    <tr>
                        <th scope="col">Организация-участник</th>

                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                        <th scope="col">
                            Форма/условия реализации обучения/ Образовательная программа/деятельность/ Комментарий
                        </th>

                        <th scope="col">
                            Даты
                        </th>

                        <th scope="col">Программа/ Расписание</th>
                        <th scope="col">Кол-во учеников/Список</th>
                        <th scope="col">Договор</th>
                    </tr>
                    </thead>

                    <tbody id="table5">
                        @if ($user_org->selected_programs())
                            @foreach( $user_org->selected_programs() as $selected_program)
                                <tr>
                                    <td>
                                        {{$selected_program->proposed_program->user->fullname }}
                                        @if (Auth::user()->status == 1 || Auth::user()->status == 10)
                                        <p><a class="btn btn-outline-dark" href="/users/{{ $selected_program->proposed_program->user->id }}/show-org">Информация</a></p>
                                        @elseif(Auth::user()->id == $user_org->id)
                                        <p><a class="btn btn-outline-dark" href="/users/{{ $selected_program->proposed_program->user->id }}/about-org">Сведения</a></p>
                                        @endif
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul1">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getClasses() }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->subject }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->modul }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->hours }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group" id="ul2">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->form_of_education }} </li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->form_education_implementation }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->educational_program }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->educational_activitiy }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->content }}</li>
                                        </ul>
                                    </td>

                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getDataBegin() }} </li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getDataEnd() }}</li>
                                        </ul>
                                    </td>


                                    <td>
                                        <li class="list-group-item">
                                            <a href="/files/proposed_programs/{{ $selected_program->proposed_program->filename }}"
                                                class="btn btn-outline-success">
                                                Программа
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            @if(($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1))
                                                <a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                                class="btn btn-outline-success">
                                                    Расписание
                                                </a>
                                            @endif
                                        </li>
                                        <li class="list-group-item">
                                            @if(($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1) and ($selected_program->selected_schedule->selected_months_hour))
                                                <a href="/selected_months_hours/{{ $selected_program->selected_schedule->selected_months_hour->id }}/update"
                                                    class="btn btn-outline-info btn">
                                                    Кол-во часов по месяцам
                                                </a>
                                            @endif
                                        </li>

                                    </td>

                                    <td>
                                        @if (($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1)
                                        and ($selected_program->selected_schedule->selected_student))
                                            <li class="list-group-item">
                                                {{$selected_program->selected_schedule->selected_student->students_amount}}
                                            </li>
                                            <li class="list-group-item">
                                                <a href="/files/students/{{ $selected_program->selected_schedule->selected_student->filename }}"
                                                    class="btn btn-outline-success">
                                                    Список учеников
                                                </a>
                                            </li>
                                        @endif
                                    </td>

                                    <td>
                                        @if (($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1)
                                        and ($selected_program->selected_schedule->selected_student) and ($selected_program->selected_schedule->selected_student->selected_agreement))
                                            <li class="list-group-item">
                                                <a href="/files/selected_agreements/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                                    class="btn btn-outline-success">
                                                    Договор
                                                </a>
                                                <br>
                                                @if($selected_program->selected_schedule->selected_student->selected_agreement->sign == 1)
                                                <a href="/files/selected_agreements/signed/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign }}"
                                                    class="btn btn-outline-success">
                                                    Подпись базовой организации
                                                </a>
                                                @endif
                                                <br>
                                                @if($selected_program->selected_schedule->selected_student->selected_agreement->sign2 == 1)
                                                <a href="/files/selected_agreements/signed2/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign2 }}"
                                                    class="btn btn-outline-success">
                                                    Подпись организации-участника
                                                </a>
                                                @endif
                                            </li>
                                        @endif
                                    </td>
                                <tr>
                        @endforeach
                        @endif

                    </tbody>
              </table>
            </br>
            <br/>
            @endif


              @if(Auth::user()->status == 1)
                <p><a href="{{ route('org_list', ['orgtype' => 'all'])}}"
                    class="btn btn-outline-info btn">
                    <i class="fas fa-arrow-left"></i> Вернуться к списку организаций Воронежской области
                </a></p>
                @elseif(Auth::user()->status == 10)
                <p><a href="/users/{{ Auth::user()->getDistrict->id }}/all/org-list-mun"
                    class="btn btn-outline-info btn">
                    <i class="fas fa-arrow-left"></i> Вернуться к списку организаций муниципалитета
                </a></p>
                @elseif(Auth::user()->id == $user_org->id)
                <p><a href="{{ route('home')}}"
                    class="btn btn-outline-info btn">
                    <i class="fas fa-arrow-left"></i> Вернуться на главную страницу
                </a></p>
                @endif

        </div>
    </div>
</div>
@endif
@endsection
