@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h3>Переместить в архив</h3>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif

                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                    <li class="list-group-item">{{ $proposed_program->subject }}</li>
                    <li class="list-group-item">{{ $proposed_program->modul }}</li>
                </ul>
                <br>

                <form method="post" action="/proposed_programs/{{ $proposed_program->id }}/to-archive-send" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    {{-- <div class="form-group">
                        <label for="archive">Учебный период:</label>
                        <input type="text" class="form-control" name="archive" list="Archive">
                        <datalist id="Archive">
                            @for($i = 2020; $i <= date('Y'); $i++)
                                <option value="{{ $i }}-{{ $i+1 }}">{{ $i }}-{{ $i+1 }}</option>
                            @endfor
                        </datalist>
                    </div> --}}

                    <p>Дата начала обучения: {{ $proposed_program->date_begin }}</p>
                    <p>
                        Учебный год: 
                        <input type="text" class="form-control" name="archive" 
                            value="{{ $proposed_program->getAcademicYear($proposed_program->date_begin) }}" readonly/>
                    </p>

                    <br>
                    <button type="submit" class="btn btn-primary">Отправить образовательную программу в архив</button>
                </form>
            </div>
        </div>
    </div>
@endsection
