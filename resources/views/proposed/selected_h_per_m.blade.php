<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Муниципалитет</th>
        <th scope="col">Базовая организация</th>
        <th scope="col">ИНН базовой организации</th>

        <th scope="col">Предполагаемое кол-во часов за 2022-01</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-01</th>
        <th scope="col">Предполагаемое кол-во часов за 2022-02</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-02</th>
        <th scope="col">Предполагаемое кол-во часов за 2022-03</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-03</th>
        <th scope="col">Предполагаемое кол-во часов за 2022-04</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-04</th>
        <th scope="col">Предполагаемое кол-во часов за 2022-05</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-05</th>
        <th scope="col">Предполагаемое кол-во часов за 2022-09</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-09</th>
        <th scope="col">Предполагаемое кол-во часов за 2022-10</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-10</th>
        <th scope="col">Предполагаемое кол-во часов за 2022-11</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-11</th>
        <th scope="col">Предполагаемое кол-во часов за 2022-12</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2022-12</th>

        <th scope="col">Предполагаемое кол-во часов за 2023-01</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-01</th>
        <th scope="col">Предполагаемое кол-во часов за 2023-02</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-02</th>
        <th scope="col">Предполагаемое кол-во часов за 2023-03</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-03</th>
        <th scope="col">Предполагаемое кол-во часов за 2023-04</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-04</th>
        <th scope="col">Предполагаемое кол-во часов за 2023-05</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-05</th>
        <th scope="col">Предполагаемое кол-во часов за 2023-09</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-09</th>
        <th scope="col">Предполагаемое кол-во часов за 2023-10</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-10</th>
        <th scope="col">Предполагаемое кол-во часов за 2023-11</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-11</th>
        <th scope="col">Предполагаемое кол-во часов за 2023-12</th>
        <th scope="col">Реальное подтверждённое кол-во часов за 2023-12</th>

      </tr>
    </thead>
    <tbody>
        @foreach($users as $user_org)
        @if($user_org->amount_of_selected_months_hours() > 0)
        <tr>
            <td>{{$user_org->getDistrict->fullname }}</td>
            <td>{{ $user_org->fullname}}</td>
            <td>{{ $user_org->inn }}</td>


            <td>
                @if($user_org->amount_selected_estimated_per('2022-01') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-01') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-01') > 0)
                    {{$user_org->amount_selected_real_per('2022-01') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2022-02') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-02') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-02') > 0)
                    {{$user_org->amount_selected_real_per('2022-02') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2022-03') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-03') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-03') > 0)
                    {{$user_org->amount_selected_real_per('2022-03') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2022-04') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-04') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-04')> 0)
                    {{$user_org->amount_selected_real_per('2022-04') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2022-05') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-05') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-05') > 0)
                    {{$user_org->amount_selected_real_per('2022-05') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2022-09') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-09') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-09') > 0)
                    {{$user_org->amount_selected_real_per('2022-09') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2022-10') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-10') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-10') > 0)
                    {{$user_org->amount_selected_real_per('2022-10') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2022-11') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-11') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-11') > 0)
                    {{$user_org->amount_selected_real_per('2022-11') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2022-12') > 0)
                    {{$user_org->amount_selected_estimated_per('2022-12') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2022-12') > 0)
                    {{$user_org->amount_selected_real_per('2022-12') }}
                @endif
            </td>


            <td>
                @if($user_org->amount_selected_estimated_per('2023-01') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-01') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-01') > 0)
                    {{$user_org->amount_selected_real_per('2023-01') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2023-02') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-02') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-02') > 0)
                    {{$user_org->amount_selected_real_per('2023-02') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2023-03') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-03') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-03') > 0)
                    {{$user_org->amount_selected_real_per('2023-03') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2023-04') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-04') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-04')> 0)
                    {{$user_org->amount_selected_real_per('2023-04') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2023-05') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-05') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-05') > 0)
                    {{$user_org->amount_selected_real_per('2023-05') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2023-09') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-09') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-09') > 0)
                    {{$user_org->amount_selected_real_per('2023-09') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2023-10') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-10') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-10') > 0)
                    {{$user_org->amount_selected_real_per('2023-10') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2023-11') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-11') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-11') > 0)
                    {{$user_org->amount_selected_real_per('2023-11') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_estimated_per('2023-12') > 0)
                    {{$user_org->amount_selected_estimated_per('2023-12') }}
                @endif
            </td>
            <td>
                @if($user_org->amount_selected_real_per('2023-12') > 0)
                    {{$user_org->amount_selected_real_per('2023-12') }}
                @endif
            </td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>

