@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">
                    <h2>{{$user->fullname}} <br>
                        <small>{{$user->getDistrict->fullname}}</small>
                    </h2>
                    @if($user->date_license == NULL or $user->number_license == NULL or $user->who_license == NULL or
                        $user->director_post_rod == NULL or $user->director_rod == NULL or
                        $user->bik == NULL or $user->bank == NULL or $user->single_treasury_account == NULL 
                        or $user->treasury_account == NULL)
                        <div class="alert alert-info" role="alert">
                            Заполните полностью сведения о вашей организации.
                        </div>
                    @endif

                    <ul class="list-group list-group-horizontal">
                        <li class="list-group-item list-group-item-dark">
                            <a class="btn btn-outline-dark" href="/users/{{ $user->id }}/show-org">Сведения о вашей организации</a>
                        </li>
                        <li class="list-group-item list-group-item-light">
                            <a class="btn btn-outline-dark" href="/users/{{ $user->id }}/archive-list">Архив заявок</a>
                        </li>
                        <li class="list-group-item list-group-item-light">
                            <a class="btn btn-outline-dark" href="/users/{{ $user->id }}/archive-list-programs">Архив утверждённых программ</a>
                        </li>
                        @if($user->org_type != 'prof')
                        <li class="list-group-item list-group-item-dark">
                            <a class="btn btn-outline-dark" href="/pupils/{{ $user->id }}/list-of-grades">Список учеников</a>
                        </li>
                        <li class="list-group-item list-group-item-dark">
                            <a class="btn btn-outline-dark" href="/pupils-year-archive/{{ $user->id }}">Список учеников (архивные программы)</a>
                        </li>
                        @else
                        <li class="list-group-item list-group-item-dark">
                            <a class="btn btn-outline-dark" href="/pupils/{{ $user->id }}/vuz/list-of-programs">Список учеников для СПО</a>
                        </li>
                        <li class="list-group-item list-group-item-dark">
                            <a class="btn btn-outline-dark" href="/pupils-year-archive/{{ $user->id }}">Список учеников для СПО (архивные программы)</a>
                        </li>
                        @endif
                        <li class="list-group-item list-group-item-light">
                            <a class="btn btn-outline-dark" href="/users/{{ $user->id }}/archive-list-proposed-programs">Архив образовательных программ</a>
                        </li>
                        <li class="list-group-item list-group-item-light">
                            <a class="btn btn-outline-dark" href="/users/{{ $user->id }}/archive-list-selected-programs">Архив взятых вами образовательных программ</a>
                        </li>
                    </ul>

                </div>

                <ul class="nav nav-tabs">
                    <!-- Первая вкладка (активная) -->
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#nav-bids">
                            Заявки
                        </a>
                    </li>
                    <!-- Вторая вкладка -->
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#nav-proposed">
                            Образовательные программы
                        </a>
                    </li>
                </ul>


                <div class="tab-content">
                    <div class="tab-pane fade show active" id="nav-bids">
                        <ul class="card-body">

                            <a href="bids/create" class="btn btn-outline-primary btn-block">
                                Подать заявление на дефицит
                            </a>
                            <br>

                            <h5 align="center">Ваши заявления на дефицит</h5>

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">
                                        Класс/предмет/курс
                                    </th>
                                    <th scope="col">Статус заявки</th>
                                    <th scope="col">Программа</th>
                                    <th scope="col">Расписание</th>
                                    <th scope="col">Список учеников</th>
                                    <th scope="col">Количество учеников</th>
                                    <th scope="col">Договор</th>
                                </tr>
                                </thead>

                                <tbody id="own_bids">
                                    <tr>
                                        @if ($user->bids())
                                            @foreach( $user->bids_not_archives() as $bid)
                                            <tr>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $bid->getClasses() }}</li>
                                                        <li class="list-group-item">{{ $bid->subject }} </li>
                                                        <li class="list-group-item">{{ $bid->modul }}</li>
                                                        <li class="list-group-item">
                                                            <a href="/bids/{{ $bid->id }}/update"
                                                                class="btn btn-outline-info btn">
                                                                <i class="far fa-eye"></i> Редактирование и удаление
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{!! $bid->getStatus() !!}</li>
                                                        <li class="list-group-item">
                                                            @if($bid->status === 0)
                                                                <form action="{{ action('BidController@delete',$bid->id) }}"
                                                                    method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit"
                                                                        class="btn btn-outline-danger btn">
                                                                        <i class="far fa-trash-alt"></i> Удалить дефицит
                                                                    </button>
                                                                </form>
                                                            @elseif($bid->status === 9)
                                                                <form action="{{ action('BidController@delete2',$bid->id) }}"
                                                                    method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit"
                                                                        class="btn btn-outline-danger btn">
                                                                        <i class="far fa-trash-alt"></i> Удалить дефицит и программы
                                                                    </button>
                                                                </form>
                                                            @endif


                                                        </li>
                                                    </ul>
                                                </td>
                                                {{-- @if ($bid->status !== 1) --}}
                                                    <td>
                                                        @if ($bid->status === 9)
                                                            @foreach($bid->programs() as $program)

                                                                @if ($program->status !== 2)
                                                                    <div class="card" style="width: 18rem;">
                                                                        <div class="card-body">
                                                                            <h6 class="card-title">
                                                                                {{$program->sender()->first()->fullname}}
                                                                            </h6>
                                                                            <p><a class="btn btn-outline-dark" href="/users/{{ $program->sender()->first()->id }}/about-org">Сведения</a></p>
                                                                            <p>
                                                                                <a href="/files/programs/{{ $program->filename }}"
                                                                                    class="btn btn-outline-success">
                                                                                    Программа
                                                                                </a>
                                                                                @if ($program->status !== 1)
                                                                                    <p>
                                                                                        <a href="program/add/{{ $program->id }}"
                                                                                            class="btn btn-outline-info btn">
                                                                                            <i class="far fa-check-square"></i> Согласовать
                                                                                        </a>
                                                                                    <p>

                                                                                    <form action="{{ action('ProgramController@delete',$program->id) }}"
                                                                                        method="POST">
                                                                                        @csrf
                                                                                        @method('DELETE')
                                                                                        <button type="submit"
                                                                                            class="btn btn-outline-danger btn">
                                                                                            <i class="far fa-trash-alt"></i> Отклонить
                                                                                        </button>
                                                                                    </form>
                                                                                @endif
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                @endif

                                                            @endforeach

                                                        @elseif($bid->status === 1)
                                                            @foreach($bid->programs() as $program)
                                                                @if($program->status === 1)
                                                                <div class="card" style="width: 18rem;">
                                                                    <div class="card-body">
                                                                        <h6 class="card-title">
                                                                            {{$program->sender()->first()->fullname}}
                                                                        </h6>
                                                                        <p><a class="btn btn-outline-dark" href="/users/{{ $program->sender()->first()->id }}/about-org">Сведения</a></p>
                                                                        <p>
                                                                            <a href="/files/programs/{{ $program->filename }}"
                                                                                class="btn btn-outline-success">
                                                                                Программа
                                                                            </a>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            @endforeach

                                                        @endif

                                                    </td>
                                                    {{-- @endif --}}

                                                    @if (($bid->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule))
                                                        <td>
                                                            <p><a href="/files/schedules/{{ $bid->programs()->sortByDesc('status')->first()->schedule->filename }}"
                                                            class="btn btn-outline-success">
                                                            Расписание
                                                            </a><br></p>

                                                            @if($bid->programs()->sortByDesc('status')->first()->schedule->months_hour)
                                                                <a href="/months_hours/{{ $bid->programs()->sortByDesc('status')->first()->schedule->months_hour->id }}/update-rez"
                                                                    class="btn btn-outline-info btn">
                                                                        Кол-во часов по месяцам
                                                                </a>
                                                            @endif

                                                            @if ($bid->programs()->sortByDesc('status')->first()->schedule->status !== 1)
                                                                <p><a href="schedule/add/{{ $bid->programs()->sortByDesc('status')->first()->schedule->id }}"
                                                                class="btn btn-outline-info">
                                                                <i class="far fa-check-square"></i> Согласовать
                                                                </a></p>
                                                                <form action="{{ action('ScheduleController@delete',$bid->programs()->sortByDesc('status')->first()->schedule->id) }}"
                                                                    method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit"
                                                                            class="btn btn-outline-danger">
                                                                            <i class="far fa-trash-alt"></i> Отклонить
                                                                    </button>
                                                                </form>
                                                            @endif
                                                        </td>

                                                        @if($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                                        <td>
                                                            @if ($bid->programs()->sortByDesc('status')->first()->schedule->student)
                                                                <p><a href="/files/students/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->filename }}"
                                                                    class="btn btn-outline-success">
                                                                    Список учеников
                                                                </a><br></p>

                                                                <td>
                                                                    <p>
                                                                        {{ $bid->programs()->sortByDesc('status')->first()->schedule->student->students_amount }}
                                                                    </p>
                                                                </td>

                                                                <td>
                                                                    @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement)
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item">
                                                                                <a href="/files/agreements/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                                                    class="btn btn-outline-success">
                                                                                    Договор
                                                                                </a>
                                                                                <br>
                                                                                @if($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign != 1)
                                                                                {{-- <a href='/agreement-sign-ordinary/agreements/{{ $user->id }}/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->id }}'
                                                                                    type="button" id="signButton-{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                                                    class="btn btn-outline-primary signButton">
                                                                                    Ссылка на подпись документа
                                                                                </a> --}}
                                                                                @else
                                                                                <a href="/files/agreements/signed/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign }}"
                                                                                    class="btn btn-outline-success">
                                                                                    Ваша подпись
                                                                                </a>
                                                                                @endif
                                                                                @if($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign2 == 1)
                                                                                <a href="/files/agreements/signed2/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 }}"
                                                                                    class="btn btn-outline-success">
                                                                                    Подпись другой строны
                                                                                </a>
                                                                                @endif
                                                                            </li>
                                                                            <li class="list-group-item">
                                                                                <a href="/bids/{{ $bid->id }}/to-archive"
                                                                                    class="btn btn-outline-info btn">
                                                                                    <i class="fas fa-box"></i> Отправить заявку в архив
                                                                                </a>
                                                                            </li>
                                                                        </ul>

                                                                    @else
                                                                    <ul class="list-group">
                                                                        @if($user->date_license != NULL and $user->number_license != NULL and $user->who_license != NULL and
                                                                                $user->director_post_rod != NULL and $user->director_rod != NULL and
                                                                                $user->bik != NULL and $user->bank != NULL and $user->single_treasury_account != NULL and $user->treasury_account != NULL)
                                                                            <li class="list-group-item">
                                                                                <a href="/agreement-document/create-document/{{ $user->id }}/{{ $bid->id }}/{{$program->id}}"
                                                                                    class="btn btn-outline-primary">
                                                                                    Скачать образец договора
                                                                                </a>
                                                                            </li>
                                                                        @elseif($program->sender()->first()->date_license == NULL or $program->sender()->first()->number_license == NULL or $program->sender()->first()->who_license == NULL or
                                                                                $program->sender()->first()->director_post_rod == NULL or $program->sender()->first()->director_rod == NULL or
                                                                                $program->sender()->first()->bik == NULL or $program->sender()->first()->bank == NULL or $program->sender()->first()->single_treasury_account == NULL or $program->sender()->first()->treasury_account == NULL)
                                                                            <div class="alert alert-danger" role="alert">
                                                                                Другая организация должна заполнить сведения о себе
                                                                            </div>
                                                                        @else
                                                                            <div class="alert alert-danger" role="alert">
                                                                                Заполните полностью <a href="/users/{{ $user->id }}/show-org">Сведениях о вашей организации</a>
                                                                            </div>
                                                                        @endif
                                                                        <li class="list-group-item">  
                                                                            <a href="/agreement/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->id }}"
                                                                                class="btn btn-outline-primary">
                                                                                <i class="fas fa-file-upload"></i> Добавить договор
                                                                            </a>
                                                                            </li>
                                                                        </ul>
                                                                    @endif


                                                                </td>

                                                            @else
                                                                <a href="/student/{{ $bid->programs()->sortByDesc('status')->first()->schedule->id }}"
                                                                    class="btn btn-outline-primary">
                                                                    <i class="fas fa-file-upload"></i> Добавить список учеников
                                                                </a>
                                                            @endif
                                                        </td>
                                                        @endif

                                                    @endif

                                            </tr>
                                            @endforeach
                                        @endif

                                    </tr>
                                </tbody>

                            </table>

                            <nav aria-label="Page navigation example">
                                <ul class="pagination" id="pag1">

                                </ul>
                            </nav>

                        </ul>




                        <div class="card-footer">
                            <div class="accordion" id="accordionExample">

                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" href="#collapseDeficits" aria-expanded="false"
                                        aria-controls="collapseDeficits">
                                            Дефициты других организаций вашего муниципалитета
                                        </a>
                                    </h5>
                                    </div>

                                    <div id="collapseDeficits" class="collapse"
                                        aria-labelledby="collapseDeficits" data-parent="#accordionExample">
                                    <div class="card-body">

                                        <div class="accordion" id="accordionPoisk">

                                            <div class="card">
                                                <div class="card-header" id="headingPoisk">
                                                    <h5 class="mb-0">
                                                        <a data-toggle="collapse" href="#collapsePoisk" aria-expanded="false"
                                                        aria-controls="collapsePoisk">
                                                            Поиск
                                                        </a>
                                                    </h5>
                                                </div>

                                                <div id="collapsePoisk" class="collapse"
                                                    aria-labelledby="collapsePoisk" data-parent="#accordionPoisk">
                                                    <div class="card-body">
                                                        <ul class="list-group">

                                                            <li class="list-group-item">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        Базовая организация
                                                                    </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="rez" onkeyup="rez()">
                                                                </div>
                                                            </li>
                                                            <li class="list-group-item">

                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        Класс
                                                                    </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="classs" onkeyup="classs()">

                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                        Предмет(курс)
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="subject" onkeyup="subject()">
                                                                </div>

                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        Раздел(модуль)
                                                                    </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="modul" onkeyup="modul()">

                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Кол-во часов
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="hour" onkeyup="hour()">
                                                                </div>
                                                            </li>

                                                            <li class="list-group-item">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        Форма обучения
                                                                    </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="forma" onkeyup="forma()" list="Forma">
                                                                    <datalist id="Forma">
                                                                        <option value="очная">очная</option>
                                                                        <option value="очно-заочная">очно-заочная</option>
                                                                        <option value="заочная">заочная</option>
                                                                    </datalist>

                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Условия реализации обучения
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="imp" onkeyup="imp()" list="Imp">
                                                                    <datalist id="Imp">
                                                                        <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                                            с использованием дистанционных образовательных технологий, электронного обучения
                                                                        </option>
                                                                        <option value="трансфер детей до организации">трансфер детей до организации</option>
                                                                        <option value="трансфер + дистанционные технологии">трансфер + дистанционные технологии</option>
                                                                    </datalist>
                                                                </div>

                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        Образовательная программа
                                                                    </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="pro" onkeyup="pro()" list="Pro">
                                                                    <datalist id="Pro">
                                                                        <option value="основная">основная</option>
                                                                        <option value="дополнительная">дополнительная</option>
                                                                    </datalist>

                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Образовательная деятельность
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="act" onkeyup="act()" list="Act">
                                                                    <datalist id="Act">
                                                                        <option value="урочная">урочная</option>
                                                                        <option value="внеурочная">внеурочная</option>
                                                                    </datalist>
                                                                </div>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        Начало
                                                                    </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="begin" onkeyup="begin()">

                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                        Конец
                                                                        </span>
                                                                    </div>
                                                                    <input type="text" class="form-control"
                                                                    id="end" onkeyup="end()">
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <table class="table table-stripe table-responsive" id="collapseExample3">
                                            <input type="hidden" id="rez_order" value="asc">
                                            <input type="hidden" id="pr_order" value="asc">
                                            <input type="hidden" id="ra_order" value="asc">
                                            <input type="hidden" id="st_order" value="asc">
                                            <input type="hidden" id="dog_order" value="asc">
                                            <thead>
                                            <tr>
                                                <th scope="col" onclick="sort_rez();">Организация <i class="fas fa-arrows-alt-v"></th>

                                                <th scope="col">Класс/ Кол-во классов(групп)/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                                <th scope="col">
                                                    Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                                </th>

                                                <th scope="col">Даты</th>

                                                <th scope="col">Предложить программу</th>
                                                <th scope="col">Предложить расписание</th>
                                                <th scope="col">Кол-во учеников/Список</th>
                                                <th scope="col">Договор</th>

                                            </tr>
                                            </thead>

                                            <tbody id="table2">
                                            @foreach($bids_all as $bid)
                                                @if(($bid->user_id != $user->id) and ($bid->user->district == $user->getDistrict->id))
                                                <tr class="tr_h">
                                                    <td>
                                                        {{ $bid->user->fullname }}
                                                        <p><a class="btn btn-outline-dark" href="/users/{{ $bid->user->id }}/about-org">Сведения</a></p>
                                                    </td>

                                                    <td>
                                                        <ul class="list-group" id="ul1">
                                                            <li class="list-group-item">{{ $bid->getClasses() }}</li>
                                                            <li class="list-group-item">{{ $bid->amount_of_classes }}</li>
                                                            <li class="list-group-item">{{ $bid->subject }}</li>
                                                            <li class="list-group-item">{{ $bid->modul }}</li>
                                                            <li class="list-group-item">{{ $bid->hours }}</li>
                                                        </ul>
                                                    </td>

                                                    <td>
                                                        <ul class="list-group" id="ul2">
                                                            <li class="list-group-item">{{ $bid->form_of_education }}</li>
                                                            <li class="list-group-item">{{ $bid->form_education_implementation }}</li>
                                                            <li class="list-group-item">{{ $bid->getEducationalPrograms() }}</li>
                                                            <li class="list-group-item">{{ $bid->getEducationalActivities() }}</li>
                                                            <li class="list-group-item">{{ $bid->content }}</li>
                                                        </ul>
                                                    </td>

                                                    <td>
                                                        <ul class="list-group">
                                                            <li class="list-group-item">{{ $bid->getDataBegin() }} </li>
                                                            <li class="list-group-item">{{ $bid->getDataEnd() }}</li>
                                                        </ul>
                                                    </td>

                                                    <td>
                                                        @if ($bid->status !== 1)

                                                            @foreach($bid->programs() as $program)
                                                                @if($program->school_program_id === $user->id)
                                                                    <p class="alert alert-info p1" role="alert">
                                                                        Программа отправлена
                                                                    </p>
                                                                @endif
                                                            @endforeach

                                                            <a href="/program/{{ $bid->id }}"
                                                                class="btn btn-outline-primary p1">
                                                                <i class="fas fa-file-upload"></i> Предложить программу
                                                            </a>

                                                        @else
                                                            @foreach($bid->programs() as $program)
                                                                @if($program->school_program_id === $user->id)
                                                                    @if($program->status === 1)

                                                                        <p class="alert alert-success p1" role="alert">
                                                                            Одобрена
                                                                        </p>

                                                                        <a href="/files/programs/{{ $bid->programs()->sortByDesc('status')->first()->filename }}"
                                                                            class="btn btn-outline-success">
                                                                            Программа
                                                                        </a>

                                                                        @if ($bid->programs()->sortByDesc('status')->first()->schedule)
                                                                            <td>
                                                                                <ul class="list-group">
                                                                                    <li class="list-group-item">
                                                                                    @if($bid->programs()->sortByDesc('status')->first()->schedule->status !== 2)
                                                                                        <p>
                                                                                            {!! $bid->programs()->sortByDesc('status')->first()->schedule->getStatus() !!}
                                                                                        </p>
                                                                                        <a href="/files/schedules/{{ $bid->programs()->sortByDesc('status')->first()->schedule->filename }}"
                                                                                            class="btn btn-outline-success">
                                                                                            Расписание
                                                                                        </a>
                                                                                    @endif
                                                                                    </li>
                                                                                    @if($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                                                                    <li class="list-group-item">
                                                                                        @if($bid->programs()->sortByDesc('status')->first()->schedule->months_hour)
                                                                                        <a href="/months_hours/{{ $bid->programs()->sortByDesc('status')->first()->schedule->months_hour->id }}/update"
                                                                                            class="btn btn-outline-info btn">
                                                                                            Кол-во часов по месяцам
                                                                                        </a>
                                                                                        @else
                                                                                        <a href="/months_hours/{{ $bid->programs()->sortByDesc('status')->first()->schedule->id }}"
                                                                                            class="btn btn-outline-info btn">
                                                                                            Добавить кол-во часов по месяцам
                                                                                        </a>
                                                                                        @endif
                                                                                    </li>
                                                                                    @endif
                                                                                </ul>
                                                                            </td>
                                                                            @if($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                                                                <td>
                                                                                    @if ($bid->programs()->sortByDesc('status')->first()->schedule->student)
                                                                                        <p>
                                                                                            {{ $bid->programs()->sortByDesc('status')->first()->schedule->student->students_amount }}
                                                                                        </p>
                                                                                        <a href="/files/students/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->filename }}"
                                                                                            class="btn btn-outline-success">
                                                                                            Список учеников
                                                                                        </a>

                                                                                        <td>
                                                                                            @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement)
                                                                                                <a href="/files/agreements/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                                                                    class="btn btn-outline-success">
                                                                                                    Договор
                                                                                                </a>
                                                                                                <br>
                                                                                                @if($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign == 1)
                                                                                                <a href="/files/agreements/signed/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign }}"
                                                                                                    class="btn btn-outline-success">
                                                                                                    Подпись другой строны
                                                                                                </a>
                                                                                                @endif
                                                                                                <br>
                                                                                                @if($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign2 != 1)
                                                                                                {{-- <a href='/agreement-sign-ordinary2/agreements/{{ $user->id }}/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->id }}'
                                                                                                    type="button" id="signButton2-{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                                                                    class="btn btn-outline-primary signButton2">
                                                                                                    Ссылка на подпись документа
                                                                                                </a> --}}
                                                                                                @else
                                                                                                <a href="/files/agreements/signed2/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 }}"
                                                                                                    class="btn btn-outline-success">
                                                                                                    Ваша подпись
                                                                                                </a>
                                                                                                @endif
                                                                                            @endif
                                                                                        </td>


                                                                                    @endif
                                                                                </td>
                                                                            @endif

                                                                        @else
                                                                        <td>
                                                                            <a href="/schedule/{{ $bid->programs()->where('status', '=', 1)->first()->id }}"
                                                                                class="btn btn-outline-primary">
                                                                                <i class="fas fa-file-upload"></i> Добавить расписание
                                                                            </a>
                                                                        </td>
                                                                        @endif


                                                                    @elseif($program->status === 2)
                                                                        <p>
                                                                            <div class="alert alert-danger">
                                                                                Программа отклонена
                                                                            </div>
                                                                        </p>
                                                                    @endif

                                                                @endif
                                                            @endforeach

                                                        @endif

                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                    </div>
                                </div>

                            </div>



                            <div class="accordion" id="accordionExample10">

                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" href="#collapseDeficitsOther" aria-expanded="false"
                                            aria-controls="collapseDeficitsOther">
                                                Дефициты организаций других муниципалитетов
                                            </a>
                                        </h5>
                                    </div>

                                    <div id="collapseDeficitsOther" class="collapse"
                                        aria-labelledby="collapseDeficitsOther" data-parent="#accordionExample10">
                                        <div class="card-body">

                                            <div class="accordion" id="accordionPoisk2">

                                                <div class="card">
                                                    <div class="card-header" id="headingPoisk2">
                                                        <h5 class="mb-0">
                                                            <a data-toggle="collapse" href="#collapsePoisk2" aria-expanded="false"
                                                            aria-controls="collapsePoisk2">
                                                                Поиск
                                                            </a>
                                                        </h5>
                                                    </div>

                                                    <div id="collapsePoisk2" class="collapse"
                                                        aria-labelledby="collapsePoisk2" data-parent="#accordionPoisk2">
                                                        <div class="card-body">
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Муниципалитет
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="mun2" onkeyup="mun2()" list="Mun">
                                                                        <datalist id="Mun">
                                                                            <option value="Городской округ город Воронеж">Городской округ город Воронеж</option>
                                                                            <option value="Аннинский муниципальный район">Аннинский муниципальный район</option>
                                                                            <option value="Бобровский муниципальный район">Бобровский муниципальный район</option>
                                                                            <option value="Богучарский муниципальный район">Богучарский муниципальный район</option>
                                                                            <option value="Борисоглебский городской округ">Борисоглебский городской округ</option>
                                                                            <option value="Бутурлиновский муниципальный район">Бутурлиновский муниципальный район</option>
                                                                            <option value="Верхнемамонский муниципальный район">Верхнемамонский муниципальный район</option>
                                                                            <option value="Верхнехавский муниципальный район">Верхнехавский муниципальный район</option>
                                                                            <option value="Воробьевский муниципальный район">Воробьевский муниципальный район</option>
                                                                            <option value="Городской округ город Нововоронеж">Городской округ город Нововоронеж</option>
                                                                            <option value="Калачеевский муниципальный район">Калачеевский муниципальный район</option>
                                                                            <option value="Каменский муниципальный район">Каменский муниципальный район</option>
                                                                            <option value="Кантемировский муниципальный район">Кантемировский муниципальный район</option>
                                                                            <option value="Каширский муниципальный район">Каширский муниципальный район</option>
                                                                            <option value="Лискинский муниципальный район">Лискинский муниципальный район</option>
                                                                            <option value="Нижнедевицкий муниципальный район">Нижнедевицкий муниципальный район</option>
                                                                            <option value="Новоусманский муниципальный район">Новоусманский муниципальный район</option>
                                                                            <option value="Новохоперский муниципальный район">Новохоперский муниципальный район</option>
                                                                            <option value="Ольховатский муниципальный район">Ольховатский муниципальный район</option>
                                                                            <option value="Острогожский муниципальный район">Острогожский муниципальный район</option>
                                                                            <option value="Павловский муниципальный район">Павловский муниципальный район</option>
                                                                            <option value="Панинский муниципальный район">Панинский муниципальный район</option>
                                                                            <option value="Петропавловский муниципальный район">Петропавловский муниципальный район</option>
                                                                            <option value="Поворинский муниципальный район">Поворинский муниципальный район</option>
                                                                            <option value="Подгоренский муниципальный район">Подгоренский муниципальный район</option>
                                                                            <option value="Рамонский муниципальный район">Рамонский муниципальный район</option>
                                                                            <option value="Репьевский муниципальный район">Репьевский муниципальный район</option>
                                                                            <option value="Россошанский муниципальный район">Россошанский муниципальный район</option>
                                                                            <option value="Семилукский муниципальный район">Семилукский муниципальный район</option>
                                                                            <option value="Таловский муниципальный район">Таловский муниципальный район</option>
                                                                            <option value="Терновский муниципальный район">Терновский муниципальный район</option>
                                                                            <option value="Хохольский муниципальный район">Хохольский муниципальный район</option>
                                                                            <option value="Эртильский муниципальный район">Эртильский муниципальный район</option>
                                                                        </datalist>
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Базовая организация
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="rez2" onkeyup="rez2()">
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Класс
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="classs2" onkeyup="classs2()">

                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                            Предмет(курс)
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="subject2" onkeyup="subject2()">
                                                                    </div>

                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Раздел(модуль)
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="modul2" onkeyup="modul2()">

                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Кол-во часов
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="hour2" onkeyup="hour2()">
                                                                    </div>
                                                                </li>

                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Форма обучения
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="forma2" onkeyup="forma2()" list="Forma">
                                                                        <datalist id="Forma">
                                                                            <option value="очная">очная</option>
                                                                            <option value="очно-заочная">очно-заочная</option>
                                                                            <option value="заочная">заочная</option>
                                                                        </datalist>

                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Условия реализации обучения
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="imp2" onkeyup="imp2()" list="Imp">
                                                                        <datalist id="Imp">
                                                                            <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                                                с использованием дистанционных образовательных технологий, электронного обучения
                                                                            </option>
                                                                            <option value="трансфер детей до организации">трансфер детей до организации</option>
                                                                            <option value="трансфер + дистанционные технологии">трансфер + дистанционные технологии</option>
                                                                        </datalist>
                                                                    </div>

                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Образовательная программа
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="pro2" onkeyup="pro2()" list="Pro">
                                                                        <datalist id="Pro">
                                                                            <option value="основная">основная</option>
                                                                            <option value="дополнительная">дополнительная</option>
                                                                        </datalist>


                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Образовательная деятельность
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="act2" onkeyup="act2()" list="Act">
                                                                        <datalist id="Act">
                                                                            <option value="урочная">урочная</option>
                                                                            <option value="внеурочная">внеурочная</option>
                                                                        </datalist>
                                                                    </div>
                                                                </li>

                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Начало
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="begin2" onkeyup="begin2()">

                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                            Конец
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="end2" onkeyup="end2()">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <table class="table table-stripe table-responsive" id="collapseExample11">
                                                <input type="hidden" id="rez_order2" value="asc">
                                                <input type="hidden" id="pr_order2" value="asc">
                                                <input type="hidden" id="ra_order2" value="asc">
                                                <input type="hidden" id="st_order2" value="asc">
                                                <input type="hidden" id="dog_order2" value="asc">
                                                <thead>
                                                <tr>
                                                    <th scope="col" onclick="sort_rez2();">Муниципалитет/ Организация <i class="fas fa-arrows-alt-v"></th>

                                                    <th scope="col">Класс/ Кол-во классов(групп)/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                                    <th scope="col">
                                                        Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                                    </th>

                                                    <th scope="col">Даты</th>

                                                    <th scope="col">Предложить программу</th>
                                                    <th scope="col">Предложить расписание</th>
                                                    <th scope="col">Кол-во учеников/Список</th>
                                                    <th scope="col">Договор</th>

                                                </tr>
                                                </thead>

                                                <tbody id="table11">
                                                @foreach($bids_all as $bid)
                                                    @if(($bid->user_id != $user->id) and ($bid->user->district != $user->getDistrict->id))
                                                    <tr class="tr_h">
                                                        <td>
                                                            <ul class="list-group">
                                                                <li class="list-group-item">{{ $bid->user->getDistrict->fullname }}</li>
                                                                <li class="list-group-item">{{ $bid->user->fullname }}</li>
                                                                <li class="list-group-item"><a class="btn btn-outline-dark" href="/users/{{ $bid->user->id }}/about-org">Сведения</a></li>
                                                            </ul>
                                                        </td>

                                                        <td>
                                                            <ul class="list-group" id="ul3">
                                                                <li class="list-group-item">{{ $bid->getClasses() }}</li>
                                                                <li class="list-group-item">{{ $bid->amount_of_classes }}</li>
                                                                <li class="list-group-item">{{ $bid->subject }}</li>
                                                                <li class="list-group-item">{{ $bid->modul }}</li>
                                                                <li class="list-group-item">{{ $bid->hours }}</li>
                                                            </ul>
                                                        </td>

                                                        <td>
                                                            <ul class="list-group" id="ul4">
                                                                <li class="list-group-item">{{ $bid->form_of_education }}</li>
                                                                <li class="list-group-item">{{ $bid->form_education_implementation }}</li>
                                                                <li class="list-group-item">{{ $bid->getEducationalPrograms() }}</li>
                                                                <li class="list-group-item">{{ $bid->getEducationalActivities() }}</li>
                                                                <li class="list-group-item">{{ $bid->content }}</li>
                                                            </ul>
                                                        </td>

                                                        <td>
                                                            <ul class="list-group">
                                                                <li class="list-group-item">{{ $bid->getDataBegin() }} </li>
                                                                <li class="list-group-item">{{ $bid->getDataEnd() }}</li>
                                                            </ul>
                                                        </td>

                                                        <td>
                                                            @if ($bid->status !== 1)

                                                                @foreach($bid->programs() as $program)
                                                                    @if($program->school_program_id === $user->id)
                                                                        <p class="alert alert-info p1" role="alert">
                                                                            Программа отправлена
                                                                        </p>
                                                                    @endif
                                                                @endforeach

                                                                <a href="/program/{{ $bid->id }}"
                                                                    class="btn btn-outline-primary p1">
                                                                    <i class="fas fa-file-upload"></i> Предложить программу
                                                                </a>

                                                            @else
                                                                @foreach($bid->programs() as $program)
                                                                    @if($program->school_program_id === $user->id)
                                                                        @if($program->status === 1)

                                                                            <p class="alert alert-success p1" role="alert">
                                                                                Одобрена
                                                                            </p>

                                                                            <a href="/files/programs/{{ $bid->programs()->sortByDesc('status')->first()->filename }}"
                                                                                class="btn btn-outline-success">
                                                                                Программа
                                                                            </a>

                                                                            @if ($bid->programs()->sortByDesc('status')->first()->schedule)
                                                                                <td>
                                                                                    <ul class="list-group">
                                                                                        <li class="list-group-item">
                                                                                    {{-- @if ($bid->program->schedule) --}}
                                                                                        @if($bid->programs()->sortByDesc('status')->first()->schedule->status !== 2)
                                                                                            <p>
                                                                                                {!! $bid->programs()->sortByDesc('status')->first()->schedule->getStatus() !!}
                                                                                            </p>
                                                                                            <a href="/files/schedules/{{ $bid->programs()->sortByDesc('status')->first()->schedule->filename }}"
                                                                                                class="btn btn-outline-success">
                                                                                                Расписание
                                                                                            </a>
                                                                                        @endif
                                                                                        </li>
                                                                                        @if($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                                                                        <li class="list-group-item">
                                                                                            @if($bid->programs()->sortByDesc('status')->first()->schedule->months_hour)
                                                                                            <a href="/months_hours/{{ $bid->programs()->sortByDesc('status')->first()->schedule->months_hour->id }}/update"
                                                                                                class="btn btn-outline-info btn">
                                                                                                Кол-во часов по месяцам
                                                                                            </a>
                                                                                            @else
                                                                                            <a href="/months_hours/{{ $bid->programs()->sortByDesc('status')->first()->schedule->id }}"
                                                                                                class="btn btn-outline-info btn">
                                                                                                Добавить кол-во часов по месяцам
                                                                                            </a>
                                                                                            @endif
                                                                                        </li>
                                                                                        @endif
                                                                                    </ul>
                                                                                </td>
                                                                                @if($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                                                                    <td>
                                                                                        @if ($bid->programs()->sortByDesc('status')->first()->schedule->student)
                                                                                            <p>
                                                                                                {{ $bid->programs()->sortByDesc('status')->first()->schedule->student->students_amount }}
                                                                                            </p>
                                                                                            <a href="/files/students/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->filename }}"
                                                                                                class="btn btn-outline-success">
                                                                                                Список учеников
                                                                                            </a>

                                                                                            <td>
                                                                                                @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement)
                                                                                                    <a href="/files/agreements/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                                                                        class="btn btn-outline-success">
                                                                                                        Договор
                                                                                                    </a>
                                                                                                    <br>
                                                                                                    @if($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign == 1)
                                                                                                    <a href="/files/agreements/signed/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign }}"
                                                                                                        class="btn btn-outline-success">
                                                                                                        Подпись другой строны
                                                                                                    </a>
                                                                                                    @endif
                                                                                                    <br>
                                                                                                    @if($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign2 != 1)
                                                                                                    {{-- <a href='/agreement-sign-ordinary2/agreements/{{ $user->id }}/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->id }}'
                                                                                                        type="button" id="signButton2-{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                                                                        class="btn btn-outline-primary signButton2">
                                                                                                        Ссылка на подпись документа
                                                                                                    </a> --}}
                                                                                                    @else
                                                                                                    <a href="/files/agreements/signed2/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 }}"
                                                                                                        class="btn btn-outline-success">
                                                                                                        Ваша подпись
                                                                                                    </a>
                                                                                                    @endif
                                                                                                {{-- @else
                                                                                                    <a href="/agreement/{{ $bid->program->schedule->student->id }}"
                                                                                                        class="btn btn-outline-danger">
                                                                                                            Добавить договор
                                                                                                    </a> --}}
                                                                                                @endif
                                                                                            </td>


                                                                                        @endif
                                                                                    </td>
                                                                                @endif

                                                                            @else
                                                                            <td>

                                                                                    {{-- <a href="/schedule/{{ $bid->program->id }}"
                                                                                        class="btn btn-outline-primary">
                                                                                        <i class="fas fa-file-upload"></i> Добавить расписание
                                                                                    </a> --}}
                                                                                    <a href="/schedule/{{ $bid->programs()->where('status', '=', 1)->first()->id }}"
                                                                                        class="btn btn-outline-primary">
                                                                                        <i class="fas fa-file-upload"></i> Добавить расписание
                                                                                    </a>

                                                                            </td>
                                                                            @endif


                                                                        @elseif($program->status === 2)
                                                                            <p>
                                                                                <div class="alert alert-danger">
                                                                                    Программа отклонена
                                                                                </div>
                                                                            </p>
                                                                        @endif

                                                                    @endif
                                                                @endforeach

                                                            @endif

                                                        </td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="nav-proposed">
                        <ul class="card-body">

                            <a href="proposed/add" class="btn btn-outline-primary btn-block">
                                Предложить свою образовательную программу
                            </a>
                            <br>

                            <div class="accordion" id="accordionExample3">
                                <div class="card">
                                    <div class="card-header" id="headingOwn">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" href="#collapseOwnProposed" aria-expanded="false"
                                                aria-controls="collapseOwnProposed">
                                                Ваши образовательные программы
                                            </a>
                                        </h5>
                                    </div>

                                    <div id="collapseOwnProposed" class="collapse"
                                        aria-labelledby="collapseOwnProposed" data-parent="#accordionExample3">
                                        <div class="card-body">
                                            <table class="table table-stripe">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>
                                                        <th scope="col">
                                                            Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                                        </th>
                                                        <th scope="col">Даты</th>
                                                        <th scope="col">Программа</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="own_proposed">
                                                    {{-- <tr> --}}
                                                        @if ($user->proposed_programs())
                                                            @foreach( $user->proposed_programs() as $proposed_program)
                                                            <tr>
                                                                <td>
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                                                        <li class="list-group-item">{{ $proposed_program->subject }} </li>
                                                                        <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                                                        <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                                                        <li class="list-group-item">
                                                                            <a href="/proposed_programs/{{ $proposed_program->id }}/update"
                                                                                class="btn btn-outline-info btn">
                                                                                <i class="far fa-eye"></i> Редактирование, архивирование, удаление
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                <td>
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                                                        <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                                                        <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                                                        <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                                                        <li class="list-group-item">{{ $proposed_program->content }}</li>
                                                                    </ul>
                                                                </td>
                                                                <td>
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item">{{ $proposed_program->getDataBegin() }}</li>
                                                                        <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                                                    </ul>
                                                                </td>
                                                                <td>
                                                                    <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                                        class="btn btn-outline-success">
                                                                        Программа
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        @endif
                                                    {{-- </tr> --}}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </br>
                            </br>
                            <h5 align="center">Организации, которые взяли ваши программы</h5>

                            <table class="table table-stripe border-bottom">
                                <thead>
                                    <tr>
                                        <th scope="col">Организация-участник</th>
                                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                        <th scope="col">
                                            Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                        </th>

                                        <th scope="col">Даты</th>

                                        <th scope="col">Программа</th>
                                        <th scope="col">Расписание</th>
                                        <th scope="col">Ученики</th>
                                        <th scope="col">Договор</th>
                                    </tr>
                                </thead>

                                <tbody id="own_proposed">
                                    {{-- <tr> --}}
                                        @if ($user->proposed_programs())
                                            @foreach( $user->proposed_programs() as $proposed_program)
                                            @if ($proposed_program->selected_programs())
                                            @foreach($proposed_program->selected_programs() as $selected_program)
                                            @if($selected_program->user->status != 7)
                                            <tr>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $selected_program->user->fullname }}</li>
                                                    <li class="list-group-item">
                                                        <a class="btn btn-outline-dark" href="/users/{{ $selected_program->user->id }}/about-org">Сведения</a>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->getClasses() }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->subject }} </li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->modul }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->hours }}</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->form_of_education }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->form_education_implementation }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->educational_program }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->educational_activity }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->content }}</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->getDataBegin() }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->getDataEnd() }}</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <a href="/files/proposed_programs/{{ $selected_program->proposed_program->filename }}"
                                                    class="btn btn-outline-success">
                                                    Программа
                                                </a>
                                            </td>
                                            @if ($selected_program->selected_schedule)
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            @if($selected_program->selected_schedule->status !== 2)
                                                                <p>
                                                                    {!! $selected_program->selected_schedule->getStatus() !!}
                                                                </p>
                                                                <a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                                                    class="btn btn-outline-success">
                                                                        Расписание
                                                                </a>
                                                            @endif
                                                        </li>
                                                        @if($selected_program->selected_schedule->status === 1)
                                                            <li class="list-group-item">
                                                                @if($selected_program->selected_schedule->selected_months_hour)
                                                                    <a href="/selected_months_hours/{{ $selected_program->selected_schedule->selected_months_hour->id }}/update"
                                                                        class="btn btn-outline-info btn">
                                                                        Кол-во часов по месяцам
                                                                    </a>
                                                                @else
                                                                    <a href="/selected_months_hours/{{ $selected_program->selected_schedule->id }}"
                                                                        class="btn btn-outline-info btn">
                                                                        Добавить кол-во часов по месяцам
                                                                    </a>
                                                                @endif
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </td>
                                                @if($selected_program->selected_schedule->status === 1)
                                                    <td>
                                                        @if ($selected_program->selected_schedule->selected_student)
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    {{ $selected_program->selected_schedule->selected_student->students_amount }}
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <a href="/files/students/{{ $selected_program->selected_schedule->selected_student->filename }}"
                                                                        class="btn btn-outline-success">
                                                                        Список учеников
                                                                    </a>
                                                                </li>
                                                            </ul>

                                                            <td>
                                                                @if ($selected_program->selected_schedule->selected_student->selected_agreement)
                                                                    <a href="/files/selected_agreements/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                                                        class="btn btn-outline-success">
                                                                        Договор
                                                                    </a>
                                                                    <br>
                                                                    @if($selected_program->selected_schedule->selected_student->selected_agreement->sign == 1)
                                                                    <a href="/files/selected_agreements/signed/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign }}"
                                                                        class="btn btn-outline-success">
                                                                        Подпись другой строны
                                                                    </a>
                                                                    @endif
                                                                    @if($selected_program->selected_schedule->selected_student->selected_agreement->sign2 != 1)
                                                                    {{-- <a href='/agreement-sign-ordinary2/selected_agreements/{{ $user->id }}/{{ $selected_program->selected_schedule->selected_student->selected_agreement->id }}'
                                                                        type="button" id="signButton2-{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                                                        class="btn btn-outline-primary signButton2">
                                                                        Ссылка на подпись документа
                                                                    </a> --}}
                                                                    @else
                                                                    <a href="/files/selected_agreements/signed2/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign2 }}"
                                                                        class="btn btn-outline-success">
                                                                        Ваша подпись
                                                                    </a>
                                                                    @endif

                                                                @endif
                                                            </td>


                                                        @endif
                                                    </td>
                                                @endif
                                            @else
                                                <td>
                                                    <a href="/selected_schedule/{{ $selected_program->id }}"
                                                        class="btn btn-outline-primary">
                                                        <i class="fas fa-file-upload"></i> Добавить расписание
                                                    </a>
                                                </td>
                                            @endif

                                            </tr>
                                            @endif
                                            @endforeach
                                            @endif
                                            @endforeach
                                        @endif
                                    {{-- </tr> --}}
                                </tbody>

                            </table>



                            </br>
                            </br>
                            <h5 align="center">Взятые вами образовательные программы</h5>

                            <table class="table table-stripe">
                                <thead>
                                    <tr>
                                        <th scope="col">Организация-участник</th>

                                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                        <th scope="col">
                                            Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                        </th>

                                        <th scope="col">Даты</th>

                                        <th scope="col">Программа</th>

                                        <th scope="col">Расписание</th>
                                        <th scope="col">Ученики</th>
                                        <th scope="col">Договор</th>
                                    </tr>
                                </thead>

                                <tbody id="taken_programs">
                                    {{-- <tr> --}}
                                        @if ($user->selected_programs_not_archive())
                                            @foreach( $user->selected_programs_not_archive() as $selected_program)
                                            <tr>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->user->fullname }}</li>
                                                    <li class="list-group-item">
                                                        <a class="btn btn-outline-dark" href="/users/{{ $selected_program->proposed_program->user->id }}/about-org">Сведения</a>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <a href="/selected_programs/{{ $selected_program->id }}/show"
                                                            class="btn btn-outline-info btn">
                                                            <i class="far fa-eye"></i> Удаление
                                                        </a>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->getClasses() }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->subject }} </li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->modul }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->hours }}</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->form_of_education }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->form_education_implementation }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->educational_program }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->educational_activity }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->content }}</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->getDataBegin() }}</li>
                                                    <li class="list-group-item">{{ $selected_program->proposed_program->getDataEnd() }}</li>
                                                </ul>
                                            </td>
                                            <td>
                                                <a href="/files/proposed_programs/{{ $selected_program->proposed_program->filename }}"
                                                    class="btn btn-outline-success">
                                                    Программа
                                                </a>
                                            </td>
                                            @if ($selected_program->selected_schedule)
                                                <td>
                                                    <p><a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                                        class="btn btn-outline-success">
                                                        Расписание
                                                    </a><br></p>

                                                    @if ($selected_program->selected_schedule->status !== 1)
                                                        <p><a href="selected_schedule/add/{{ $selected_program->selected_schedule->id }}"
                                                            class="btn btn-outline-info">
                                                            <i class="far fa-check-square"></i> Согласовать
                                                        </a></p>
                                                        <form action="{{ action('SelectedScheduleController@delete',$selected_program->selected_schedule->id) }}"
                                                                method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit"
                                                                    class="btn btn-outline-danger">
                                                                    <i class="far fa-trash-alt"></i> Отклонить
                                                            </button>
                                                        </form>
                                                    @endif

                                                    @if($selected_program->selected_schedule->selected_months_hour)
                                                        <a href="/selected_months_hours/{{ $selected_program->selected_schedule->selected_months_hour->id }}/update-rez"
                                                            class="btn btn-outline-info btn">
                                                                Кол-во часов по месяцам
                                                        </a>
                                                    @endif
                                                </td>

                                                @if($selected_program->selected_schedule->status === 1)
                                                    <td>
                                                        @if ($selected_program->selected_schedule->selected_student)
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <a href="/files/selected_students/{{ $selected_program->selected_schedule->selected_student->filename }}"
                                                                    class="btn btn-outline-success">
                                                                        Список учеников
                                                                    </a>
                                                                </li>


                                                                <li class="list-group-item">
                                                                    {{ $selected_program->selected_schedule->selected_student->students_amount }}
                                                                </li>
                                                            </ul>


                                                            <td>
                                                                @if ($selected_program->selected_schedule->selected_student->selected_agreement)
                                                                    <a href="/files/selected_agreements/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                                                        class="btn btn-outline-success">
                                                                        Договор
                                                                    </a>
                                                                    <br>
                                                                    @if($selected_program->selected_schedule->selected_student->selected_agreement->sign != 1)
                                                                    {{-- <a href='/agreement-sign-ordinary/selected_agreements/{{ $user->id }}/{{ $selected_program->selected_schedule->selected_student->selected_agreement->id }}'
                                                                        type="button" id="signButton-{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                                                        class="btn btn-outline-primary signButton">
                                                                        Ссылка на подпись документа
                                                                    </a> --}}
                                                                    @else
                                                                    <a href="/files/selected_agreements/signed/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign }}"
                                                                        class="btn btn-outline-success">
                                                                        Ваша подпись
                                                                    </a>
                                                                    @endif
                                                                    @if($selected_program->selected_schedule->selected_student->selected_agreement->sign2 == 1)
                                                                    <a href="/files/selected_agreements/signed2/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign2 }}"
                                                                        class="btn btn-outline-success">
                                                                        Подпись другой строны
                                                                    </a>
                                                                    @endif

                                                                @else
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item">
                                                                            @if($user->date_license != NULL and $user->number_license != NULL and $user->who_license != NULL and
                                                                                $user->director_post_rod != NULL and $user->director_rod != NULL and
                                                                                $user->bik != NULL and $user->bank != NULL and $user->single_treasury_account != NULL and $user->treasury_account != NULL)

                                                                                @if($selected_program->proposed_program->user->status != 100)
                                                                                    {{-- <a href="/agreement-document-selected/create-document/{{ $user->id }}/{{$selected_program->id}}"
                                                                                        class="btn btn-outline-primary">
                                                                                        Скачать образец договора
                                                                                    </a> --}}
                                                                                @else
                                                                                    <a href="/agreement-document-selected/show-document/{{ $user->id }}/{{$selected_program->id}}"
                                                                                        class="btn btn-outline-primary">
                                                                                        Форма для договора
                                                                                    </a>
                                                                                @endif

                                                                            @elseif($selected_program->proposed_program->user->date_license == NULL or $selected_program->proposed_program->user->number_license == NULL or $selected_program->proposed_program->user->who_license == NULL or
                                                                                    $selected_program->proposed_program->user->director_post_rod == NULL or $selected_program->proposed_program->user->director_rod == NULL or
                                                                                    $selected_program->proposed_program->user->bik == NULL or $selected_program->proposed_program->user->bank == NULL or $selected_program->proposed_program->user->single_treasury_account == NULL or $selected_program->proposed_program->user->treasury_account == NULL)
                                                                                <div class="alert alert-danger" role="alert">
                                                                                    Другая организация должна заполнить сведения о себе
                                                                                </div>
                                                                            @else
                                                                                <div class="alert alert-danger" role="alert">
                                                                                    Заполните полностью <a href="/users/{{ $user->id }}/show-org">Сведениях о вашей организации</a>
                                                                                </div>
                                                                            @endif
                                                                        </li>
                                                                        <li class="list-group-item"> 
                                                                            <a href="/selected_agreement/{{ $selected_program->selected_schedule->selected_student->id }}"
                                                                                class="btn btn-outline-primary">
                                                                                <i class="fas fa-file-upload"></i> Добавить договор
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                @endif


                                                            </td>

                                                        @else
                                                            <a href="/selected_student/{{ $selected_program->selected_schedule->id }}"
                                                                class="btn btn-outline-primary">
                                                                <i class="fas fa-file-upload"></i> Добавить список учеников
                                                            </a>
                                                        @endif
                                                    </td>
                                                @endif

                                            @endif

                                            </tr>
                                            @endforeach
                                        @endif
                                    {{-- </tr> --}}
                                </tbody>
                            </table>
                        </ul>

                        <div class="card-footer">
                            <div class="accordion" id="accordionExample2">

                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" href="#collapseProposed" aria-expanded="false"
                                        aria-controls="collapseProposed">
                                            Образовательные программы других организаций вашего муниципалитета
                                        </a>
                                    </h5>
                                    </div>

                                    <div id="collapseProposed" class="collapse"
                                        aria-labelledby="collapseProposed" data-parent="#accordionExample2">
                                        <div class="card-body">

                                            <div class="accordion" id="accordionPoisk4">

                                                <div class="card">
                                                    <div class="card-header" id="headingPoisk4">
                                                        <h5 class="mb-0">
                                                            <a data-toggle="collapse" href="#collapsePoisk4" aria-expanded="false"
                                                            aria-controls="collapsePoisk4">
                                                                Поиск
                                                            </a>
                                                        </h5>
                                                    </div>
    
                                                    <div id="collapsePoisk4" class="collapse"
                                                        aria-labelledby="collapsePoisk4" data-parent="#accordionPoisk4">
                                                        <div class="card-body">
                                                            <ul class="list-group">
    
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Базовая организация
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="rez4" onkeyup="rez4()">
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item">
    
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Класс
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="classs4" onkeyup="classs4()">
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                            Предмет(курс)
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="subject4" onkeyup="subject4()">
                                                                    </div>
    
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Раздел(модуль)
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="modul4" onkeyup="modul4()">
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Кол-во часов
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="hour4" onkeyup="hour4()">
                                                                    </div>
                                                                </li>
    
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Форма обучения
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="forma4" onkeyup="forma4()" list="Forma4">
                                                                        <datalist id="Forma4">
                                                                            <option value="очная">очная</option>
                                                                            <option value="очно-заочная">очно-заочная</option>
                                                                            <option value="заочная">заочная</option>
                                                                        </datalist>
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Условия реализации обучения
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="imp4" onkeyup="imp4()" list="Imp4">
                                                                        <datalist id="Imp4">
                                                                            <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                                                с использованием дистанционных образовательных технологий, электронного обучения
                                                                            </option>
                                                                            <option value="трансфер детей до организации">трансфер детей до организации</option>
                                                                            <option value="трансфер + дистанционные технологии">трансфер + дистанционные технологии</option>
                                                                        </datalist>
                                                                    </div>
    
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Образовательная программа
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="pro4" onkeyup="pro4()" list="Pro4">
                                                                        <datalist id="Pro4">
                                                                            <option value="основная">основная</option>
                                                                            <option value="дополнительная">дополнительная</option>
                                                                        </datalist>
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Образовательная деятельность
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="act4" onkeyup="act4()" list="Act4">
                                                                        <datalist id="Act4">
                                                                            <option value="урочная">урочная</option>
                                                                            <option value="внеурочная">внеурочная</option>
                                                                        </datalist>
                                                                    </div>

                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                            Комментарий
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="com4" onkeyup="com4()">
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Начало
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="begin4" onkeyup="begin4()">
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                            Конец
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="end4" onkeyup="end4()">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <table class="table table-stripe table-responsive" id="collapseExample4">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Организация</th>

                                                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                                        <th scope="col">
                                                            Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                                        </th>

                                                        <th scope="col">Даты</th>

                                                        <th scope="col">Программа</th>

                                                        <th scope="col">Взять программу</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="other_proposed">
                                                    @foreach($proposed_programs_all as $proposed_program)
                                                        @if(
                                                            ($proposed_program->user_id != $user->id)
                                                            and ($proposed_program->archive == NULL)
                                                            and ($proposed_program->cancel_registration == NULL)
                                                            and (($proposed_program->user->district == $user->getDistrict->id) or ($proposed_program->user->status == 100))
                                                        )
                                                        <tr>
                                                            <td>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">
                                                                        {{ $proposed_program->user->fullname }}
                                                                    </li>
                                                                    <li class="list-group-item">
                                                                        <a class="btn btn-outline-dark" href="/users/{{ $proposed_program->user->id }}/about-org">Сведения</a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->subject }} </li>
                                                                    <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->content }}</li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">{{ $proposed_program->getDataBegin() }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                                    class="btn btn-outline-success">
                                                                    Программа
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <form action="{{ action('SelectedProgramController@take', $proposed_program->id) }}" method="POST">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-success">Взять программу</button>
                                                                    </div>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="accordion" id="accordionExample20">

                                <div class="card">
                                    <div class="card-header" id="headingFive">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" href="#collapseProposedOther" aria-expanded="false"
                                        aria-controls="collapseProposedOther">
                                            Образовательные программы организаций других муниципалитетов
                                        </a>
                                    </h5>
                                    </div>

                                    <div id="collapseProposedOther" class="collapse"
                                        aria-labelledby="collapseProposedOther" data-parent="#accordionExample20">
                                        <div class="card-body">

                                            <div class="accordion" id="accordionPoisk3">

                                                <div class="card">
                                                    <div class="card-header" id="headingPoisk3">
                                                        <h5 class="mb-0">
                                                            <a data-toggle="collapse" href="#collapsePoisk3" aria-expanded="false"
                                                            aria-controls="collapsePoisk3">
                                                                Поиск
                                                            </a>
                                                        </h5>
                                                    </div>
    
                                                    <div id="collapsePoisk3" class="collapse"
                                                        aria-labelledby="collapsePoisk3" data-parent="#accordionPoisk3">
                                                        <div class="card-body">
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Муниципалитет
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="mun3" onkeyup="mun3()" list="Mun3">
                                                                        <datalist id="Mun3">
                                                                            <option value="Городской округ город Воронеж">Городской округ город Воронеж</option>
                                                                            <option value="Аннинский муниципальный район">Аннинский муниципальный район</option>
                                                                            <option value="Бобровский муниципальный район">Бобровский муниципальный район</option>
                                                                            <option value="Богучарский муниципальный район">Богучарский муниципальный район</option>
                                                                            <option value="Борисоглебский городской округ">Борисоглебский городской округ</option>
                                                                            <option value="Бутурлиновский муниципальный район">Бутурлиновский муниципальный район</option>
                                                                            <option value="Верхнемамонский муниципальный район">Верхнемамонский муниципальный район</option>
                                                                            <option value="Верхнехавский муниципальный район">Верхнехавский муниципальный район</option>
                                                                            <option value="Воробьевский муниципальный район">Воробьевский муниципальный район</option>
                                                                            <option value="Городской округ город Нововоронеж">Городской округ город Нововоронеж</option>
                                                                            <option value="Калачеевский муниципальный район">Калачеевский муниципальный район</option>
                                                                            <option value="Каменский муниципальный район">Каменский муниципальный район</option>
                                                                            <option value="Кантемировский муниципальный район">Кантемировский муниципальный район</option>
                                                                            <option value="Каширский муниципальный район">Каширский муниципальный район</option>
                                                                            <option value="Лискинский муниципальный район">Лискинский муниципальный район</option>
                                                                            <option value="Нижнедевицкий муниципальный район">Нижнедевицкий муниципальный район</option>
                                                                            <option value="Новоусманский муниципальный район">Новоусманский муниципальный район</option>
                                                                            <option value="Новохоперский муниципальный район">Новохоперский муниципальный район</option>
                                                                            <option value="Ольховатский муниципальный район">Ольховатский муниципальный район</option>
                                                                            <option value="Острогожский муниципальный район">Острогожский муниципальный район</option>
                                                                            <option value="Павловский муниципальный район">Павловский муниципальный район</option>
                                                                            <option value="Панинский муниципальный район">Панинский муниципальный район</option>
                                                                            <option value="Петропавловский муниципальный район">Петропавловский муниципальный район</option>
                                                                            <option value="Поворинский муниципальный район">Поворинский муниципальный район</option>
                                                                            <option value="Подгоренский муниципальный район">Подгоренский муниципальный район</option>
                                                                            <option value="Рамонский муниципальный район">Рамонский муниципальный район</option>
                                                                            <option value="Репьевский муниципальный район">Репьевский муниципальный район</option>
                                                                            <option value="Россошанский муниципальный район">Россошанский муниципальный район</option>
                                                                            <option value="Семилукский муниципальный район">Семилукский муниципальный район</option>
                                                                            <option value="Таловский муниципальный район">Таловский муниципальный район</option>
                                                                            <option value="Терновский муниципальный район">Терновский муниципальный район</option>
                                                                            <option value="Хохольский муниципальный район">Хохольский муниципальный район</option>
                                                                            <option value="Эртильский муниципальный район">Эртильский муниципальный район</option>
                                                                        </datalist>
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Базовая организация
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="rez3" onkeyup="rez3()">
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item">
    
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Класс
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="classs3" onkeyup="classs3()">
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                            Предмет(курс)
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="subject3" onkeyup="subject3()">
                                                                    </div>
    
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Раздел(модуль)
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="modul3" onkeyup="modul3()">
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Кол-во часов
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="hour3" onkeyup="hour3()">
                                                                    </div>
                                                                </li>
    
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Форма обучения
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="forma3" onkeyup="forma3()" list="Forma3">
                                                                        <datalist id="Forma3">
                                                                            <option value="очная">очная</option>
                                                                            <option value="очно-заочная">очно-заочная</option>
                                                                            <option value="заочная">заочная</option>
                                                                        </datalist>
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Условия реализации обучения
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="imp3" onkeyup="imp3()" list="Imp3">
                                                                        <datalist id="Imp3">
                                                                            <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                                                с использованием дистанционных образовательных технологий, электронного обучения
                                                                            </option>
                                                                            <option value="трансфер детей до организации">трансфер детей до организации</option>
                                                                            <option value="трансфер + дистанционные технологии">трансфер + дистанционные технологии</option>
                                                                        </datalist>
                                                                    </div>
    
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Образовательная программа
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="pro3" onkeyup="pro3()" list="Pro3">
                                                                        <datalist id="Pro3">
                                                                            <option value="основная">основная</option>
                                                                            <option value="дополнительная">дополнительная</option>
                                                                        </datalist>
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                                Образовательная деятельность
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="act3" onkeyup="act3()" list="Act3">
                                                                        <datalist id="Act3">
                                                                            <option value="урочная">урочная</option>
                                                                            <option value="внеурочная">внеурочная</option>
                                                                        </datalist>
                                                                    </div>

                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                            Комментарий
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="com3" onkeyup="com3()">
                                                                    </div>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            Начало
                                                                        </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="begin3" onkeyup="begin3()">
    
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">
                                                                            Конец
                                                                            </span>
                                                                        </div>
                                                                        <input type="text" class="form-control"
                                                                        id="end3" onkeyup="end3()">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <table class="table table-stripe table-responsive" id="collapseExample21">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Организация</th>

                                                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                                        <th scope="col">
                                                            Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                                        </th>

                                                        <th scope="col">Даты</th>

                                                        <th scope="col">Программа</th>

                                                        <th scope="col">Взять программу</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="other_proposed2">
                                                    @foreach($proposed_programs_all as $proposed_program)
                                                        @if(
                                                            ($proposed_program->user_id != $user->id)
                                                            and ($proposed_program->archive == NULL)
                                                            and ($proposed_program->cancel_registration == NULL)
                                                            and (($proposed_program->user->district != $user->getDistrict->id) or ($proposed_program->user->status == 100))
                                                        )
                                                        <tr>
                                                            <td>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">{{ $proposed_program->user->getDistrict->fullname }}</li>
                                                                    <li class="list-group-item">
                                                                        {{ $proposed_program->user->fullname }}
                                                                    </li>
                                                                    <li class="list-group-item">
                                                                        <a class="btn btn-outline-dark" href="/users/{{ $proposed_program->user->id }}/about-org">Сведения</a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->subject }} </li>
                                                                    <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->content }}</li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">{{ $proposed_program->getDataBegin() }}</li>
                                                                    <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                                    class="btn btn-outline-success">
                                                                    Программа
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <form action="{{ action('SelectedProgramController@take', $proposed_program->id) }}" method="POST">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-success">Взять программу</button>
                                                                    </div>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>


<script src="js/poisk_sch.js?version=5"></script>
<script src="js/sort_sch.js"></script>
<script src="js/hide_sch.js"></script>
{{-- <script src="js/pag_sch.js"></script> --}}

@endsection
