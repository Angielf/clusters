@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))

<h4>Данные</h4>
<p id="agreement-id" style="display: none;">{{ $agreement_id }}</p>
<p id="agreement-filename" style="display: none;">{{ $file_name->filename }}</p>
<p id="user-id" style="display: none;">{{ $user->id }}</p>
<p id="a" style="display: none;">{{ $a }}</p>

<p>Для подписания документа необходимо установить <a class="link-info link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover" href="https://cryptopro.ru/products/cades/plugin" target="_blank">плагин КриптоПро</a>.</p>

<p id="plugin-work"></p>

<p>Убедитесь, что у вас есть сертификат и контейнер в программе <a class="link-info link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover" href="https://www.cryptopro.ru/blog/2019/05/21/instrumenty-cryptopro-krossplatformennyi-graficheskii-interfeis" target="_blank">Инструменты КриптоПро</a>.</p>

<label for="cert-select{{ $agreement_id }}">Выберите сертификат:</label>
<select class="form-select" id="cert-select{{ $agreement_id }}">
</select>

<label for="cert-info{{ $agreement_id }}">Информация о сертификате:</label>
<ul class="list-group" id="cert-info{{ $agreement_id }}">
    <li class="list-group-item" id="cert-info-post{{ $agreement_id }}">
        Должность: 
    </li>
    <li class="list-group-item" id="cert-info-company{{ $agreement_id }}">
        Организация:
    </li>
    <li class="list-group-item" id="cert-info-email{{ $agreement_id }}">
        email, ИНН, СНИЛС:
    </li>
    <li class="list-group-item" id="cert-info-validFrom{{ $agreement_id }}">
        С:
    </li>
    <li class="list-group-item" id="cert-info-validTo{{ $agreement_id }}">
        По:
    </li>
</ul>

<p>
    Документ для подписи: 
    <a href="/files/{{ $a }}/{{ $file_name->filename }}" target="_blank">
        {{ $file_name->filename }}
    </a>
</p>

<p>
    Нажмите кнопку "Подписать", а затем "Да", чтобы подписать этот документ.
    <br>
    Дождитесь перехода на предыдущую страницу.
</p>

<button class='btn btn-success' data-toggle="modal" data-target="#exampleModalLong">
    Подписать
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Вы уверены, что хотите подписать этот документ?
            <p>
                Документ для подписи: 
                <a href="/files/{{ $a }}/{{ $file_name->filename }}" target="_blank">
                    {{ $file_name->filename }}
                </a>
            </p>
        </div>
        <div class="modal-footer">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <button type="button" class="btn btn-success" data-dismiss="modal" id='sign-button'>
                Да
            </button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        </div>
        </div>
    </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.2.8/es6-promise.min.js" integrity="sha512-JMK7ImCd/9VxQM7FWvAT3njqo5iGKkWcOax6Bwzuq48xxFd7/jekKcgN+59ZRwBoEpZvv6Jkwh3fDGrBVWX5vA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('js/lib/cadesplugin_api.js') }}"></script>
<script type="text/javascript" src="chrome-extension://iifchhfnnmpdbibifmljnfjhpififfog/nmcades_plugin_api.js"></script>
<script src="{{ asset('js/lib/cryptohelper.js') }}"></script>
<script src="{{ asset('js/signature-script-ordinary.js') }}"></script>

@endif
@endsection