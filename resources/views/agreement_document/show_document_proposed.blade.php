@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <h2>Скачать образец договора</h2><br/>
            <form method="get" action="/agreement-document-selected/create-document-vuz/{{ $user->id }}/{{$selected_program->id}}">
                @csrf
                <table class="table table-striped table-bordered">
                    <thead>
                        <th colspan='6'>
                            Предложенные программы {{$selected_program->proposed_program->user->fullname}}
                            <br><br>
                            Напишите количество учеников напротив каждой программы. Если вы не берёте эту программу, напишите 0
                        </th>
                    </thead>
                    <tbody>
                        @foreach ($selected_program->proposed_program->user->proposed_programs() as $p)
                            <tr>
                                <td><input type="text" name="subjects[]" value="{{$p->subject}}" readonly></td>
                                <td><input type="text" name="subjects[]" value="{{$p->modul}}" readonly></td>
                                <td><input type="text" name="subjects[]" value="{{$p->hours}} часов" readonly></td>
                                <td><input type="text" name="subjects[]" value="с {{$p->getDataBeginDots()}}" readonly></td>
                                <td><input type="text" name="subjects[]" value="по {{$p->getDataEndDots()}}" readonly></td>
                                <td>
                                    Количество учеников:
                                    <input type="text" class="form-control" name="subjects[]" style="width:2cm;"/>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @if($user->date_license == NULL or $user->number_license == NULL or $user->who_license == NULL or
                    $user->director_post_rod == NULL or $user->director_rod == NULL or
                    $user->bik == NULL or $user->bank == NULL or $user->single_treasury_account == NULL or $user->treasury_account == NULL)

                    <div class="alert alert-danger" role="alert">
                        Заполните полностью <a href="/users/{{ $user->id }}/show-org">Сведениях о вашей организации</a>
                    </div>

                @elseif($selected_program->proposed_program->user->date_license == NULL or $selected_program->proposed_program->user->number_license == NULL or $selected_program->proposed_program->user->who_license == NULL or
                        $selected_program->proposed_program->user->director_post_rod == NULL or $selected_program->proposed_program->user->director_rod == NULL or
                        $selected_program->proposed_program->user->bik == NULL or $selected_program->proposed_program->user->bank == NULL or $selected_program->proposed_program->user->single_treasury_account == NULL or $selected_program->proposed_program->user->treasury_account == NULL)
                    <div class="alert alert-danger" role="alert">
                        Другая организация должна заполнить сведения о себе
                    </div>
                @else
                    <button type="submit" class="btn btn-primary">Скачать договор</button>
                @endif
                {{-- <button type="submit" class="btn btn-primary">Скачать договор</button> --}}
            </form>

        </div>
    </div>
</div>

@endsection