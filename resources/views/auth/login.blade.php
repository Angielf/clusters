@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <ul class="list-group list-group-horizontal">
                        {{-- <li class="list-group-item">{{ __('Авторизация') }}</li> --}}
                        <li class="list-group-item">
                            <a href="/register"
                                class="btn btn-outline-success">
                                Регистрация для учеников
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="/password/reset"
                                class="btn btn-outline-success">
                                Если ученик забыл пароль
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ asset('/instructions/Инструкция-для-учеников.pdf') }}"
                                class="btn btn-outline-dark">
                                Инструкция для учеников
                            </a>
                        </li>
                    </ul>
                    {{-- <ul> --}}
                        {{-- <li class="list-group-item">
                            <a href="{{ asset('/instructions/Инструкция по подаче заявки для участия в проекте «Акселератор персональных профессиональных маршрутов».docx') }}"
                                class="btn btn-outline-dark">
                                Инструкция по подаче заявки для участия в проекте «Акселератор персональных профессиональных маршрутов»
                            </a>
                        </li> --}}
                    {{-- </ul> --}}
                </div>

                {{-- <div class="alert alert-danger" role="alert">
                    Сайт находится в разработке
                </div> --}}
                {{-- <div class="alert alert-warning" role="alert">
                    Регистрация учеников на программы ВГУ завершена
                </div> --}}
                <div class="alert alert-info" role="alert">
                    Для учеников: В качестве логина используйте свой email
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Логин') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Запомнить меня') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Войти') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
