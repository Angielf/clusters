@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Регистрация. Заполните все поля') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="district" class="col-md-4 col-form-label text-md-right">{{ __('Район') }}</label>

                            <div class="col-md-6">
                                {{-- <input id="fullname" type="text" class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" name="district" value="{{ old('district') }}" required autofocus> --}}
                                <select class="form-select{{ $errors->has('district') ? ' is-invalid' : '' }}" id="district" name="district" required>
                                    <option selected></option>
                                    @foreach( $muns as $mun)
                                        @if($mun->id != 100)
                                            <option value="{{$mun->id}}">{{$mun->fullname}}</option>
                                        @endif
                                    @endforeach
                                </select>

                                @if ($errors->has('district'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('district') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="org" class="col-md-4 col-form-label text-md-right">{{ __('Школа') }}</label>

                            <div class="col-md-6">
                                <select class="form-select{{ $errors->has('school') ? ' is-invalid' : '' }}" id="org" name="org" required>
                                    <option></option>
                                </select>

                                @if ($errors->has('org'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('org') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="grade_number" class="col-md-4 col-form-label text-md-right">{{ __('Класс число') }}</label>

                            <div class="col-md-6">
                                <select class="form-select{{ $errors->has('grade_number') ? ' is-invalid' : '' }}" id="grade_number" name="grade_number" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                </select>

                                @if ($errors->has('grade_number'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('grade_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="grade_letter" class="col-md-4 col-form-label text-md-right">{{ __('Класс буква') }}</label>

                            <div class="col-md-6">
                                <select class="form-select{{ $errors->has('grade_letter') ? ' is-invalid' : '' }}" id="grade_letter" name="grade_letter" required>
                                    <option value="А">А</option>
                                    <option value="Б">Б</option>
                                    <option value="В">В</option>
                                    <option value="Г">Г</option>
                                    <option value="Д">Д</option>
                                    <option value="Е">Е</option>
                                    <option value="Ж">Ж</option>
                                    <option value="З">З</option>
                                    <option value="И">И</option>
                                    <option value="К">К</option>
                                    <option value="Л">Л</option>
                                    <option value="М">М</option>
                                    <option value="Н">Н</option>
                                </select>

                                @if ($errors->has('grade_letter'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('grade_letter') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Фамилия') }}</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Имя') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="patronymic" class="col-md-4 col-form-label text-md-right">{{ __('Отчество (если есть)') }}</label>

                            <div class="col-md-6">
                                <input id="patronymic" type="text" class="form-control{{ $errors->has('patronymic') ? ' is-invalid' : '' }}" name="patronymic" value="{{ old('patronymic') }}" autofocus>

                                @if ($errors->has('patronymic'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('patronymic') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="snils" class="col-md-4 col-form-label text-md-right">{{ __('СНИЛС') }}</label>

                            <div class="col-md-6">
                                <input id="snils" type="snils" class="form-control{{ $errors->has('snils') ? ' is-invalid' : '' }}" name="snils" value="{{ old('snils') }}" required>
                                <small>Формат: 000-000-000 00</small>

                                @if ($errors->has('snils'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('snils') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth" class="col-md-4 col-form-label text-md-right">{{ __('Дата рождения') }}</label>

                            <div class="col-md-6">
                                <input id="birth" type="birth" class="form-control{{ $errors->has('birth') ? ' is-invalid' : '' }}" name="birth" value="{{ old('birth') }}" required>
                                <small>Например, 01.01.2010</small>

                                @if ($errors->has('birth'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('birth') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                <small>Пароль должен состоять минимум из 8 символов</small>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Подтвердить пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>


                        @if(config('settings.reCaptchStatus'))
                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-4">
                                    <div class="g-recaptcha" data-sitekey="{{ config('settings.reCaptchSite') }}"></div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group row mb-4">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Регистрация') }}
                                </button>
                            </div>
                        </div>

                        {{-- <div class="row">
                            <div class="col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                                <p class="text-center mb-4">
                                    Or Use Social Logins to Register
                                </p>
                                @include('partials.socials')
                            </div>
                        </div> --}}

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $("#district").change(function(){
        var district = $(this).val();

        var token = $("input[name='_token']").val();
        console.log(district);

        // $("#type").change(function(){
        //     var type = $(this).val();

            $.ajax({
                url: "{{ route('selectorg') }}",
                method: 'POST',
                data: {
                    district:district,
                    // typeOrg:type,
                    _token:token
                    },
                success: function(data) {
                    $("#org").html('');
                    $("#org").html(data.options);
                }
            });
        // });
    });

</script>
@endsection