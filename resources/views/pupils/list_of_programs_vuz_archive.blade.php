@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))
    <div class="container-fluid">
        <div class = "col">
            <div class="container d-flex align-items-center justify-content-center">
                <div class="row">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                Поиск по основным программам
                            </span>
                        </div>
                        <input type="text" class="form-control"
                        id="search1" onkeyup="search1()">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                Поиск по дополнительным программам
                            </span>
                        </div>
                        <input type="text" class="form-control"
                        id="search2" onkeyup="search2()">
                    </div>
                    <h4>Образовательные программы (основные)</h4>
                    <ul class="list-group" id="myUL1">
                        @if ($user->proposed_programs_archive($year))
                            @foreach( $user->proposed_programs_archive($year) as $proposed_program)
                                @if($proposed_program->educational_program == 'основная')
                                    <li class="list-group-item">
                                        <a href="/pupils-vuz/{{ $user->id }}/{{ $proposed_program->id }}/list-of-pupils" class='btn btn-outline-success'>
                                            {{ $proposed_program->subject }} - {{ $proposed_program->modul }} 
                                            - {{ $proposed_program->educational_program }}
                                            - {{ $proposed_program->getClasses() }} класс -
                                            с {{ $proposed_program->getDataBegin() }} -
                                            по {{ $proposed_program->getDataEnd() }}
                                        </a>
                                        <br>
                                        <a href="/pupils-vuz-school/{{ $user->id }}/{{ $proposed_program->id }}/list-of-pupils" class='btn btn-outline-dark'>
                                            Скачать список учеников программы
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    <h4>Образовательные программы (дополнительные)</h4>
                    <ul class="list-group" id="myUL2">
                        @if ($user->proposed_programs_archive($year))
                            @foreach( $user->proposed_programs_archive($year) as $proposed_program)
                                @if($proposed_program->educational_program == 'дополнительная')
                                    <li class="list-group-item">
                                        <a href="/pupils-vuz/{{ $user->id }}/{{ $proposed_program->id }}/list-of-pupils" class='btn btn-outline-success'>
                                            {{ $proposed_program->subject }} - {{ $proposed_program->modul }} 
                                            - {{ $proposed_program->educational_program }}
                                            - {{ $proposed_program->getClasses() }} класс -
                                            с {{ $proposed_program->getDataBegin() }} -
                                            по {{ $proposed_program->getDataEnd() }}
                                        </a>
                                        <br>
                                        <a href="/pupils-vuz-school/{{ $user->id }}/{{ $proposed_program->id }}/list-of-pupils" class='btn btn-outline-dark'>
                                            Скачать список учеников программы
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection

<script src="{{ asset('js/search_programs.js') }}"></script>