@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))
    <div class="container-fluid d-flex align-items-center justify-content-center">
        <div class = "col">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Поиск по основным программам
                    </span>
                </div>
                <input type="text" class="form-control"
                id="search1" onkeyup="search1()">
            </div>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Поиск по дополнительным программам
                    </span>
                </div>
                <input type="text" class="form-control"
                id="search2" onkeyup="search2()">
            </div>
            <hr>
            <h4>Образовательные программы (основные)</h4>
            <ul class="list-group" id="myUL1">
                @if ($user->proposed_programs())
                    @foreach( $user->proposed_programs() as $proposed_program)
                        @if($proposed_program->educational_program == 'основная')
                            <li class="list-group-item">
                                <a href="/pupils-vuz/{{ $user->id }}/{{ $proposed_program->id }}/list-of-pupils" class='btn btn-outline-success'>
                                    {{ $proposed_program->subject }} - {{ $proposed_program->modul }} 
                                    - {{ $proposed_program->educational_program }}
                                    - {{ $proposed_program->getClasses() }} класс -
                                    с {{ $proposed_program->getDataBegin() }} -
                                    по {{ $proposed_program->getDataEnd() }}
                                </a>
                                <br>
                                <a href="/pupils-vuz-school/{{ $user->id }}/{{ $proposed_program->id }}/list-of-pupils" class='btn btn-outline-dark'>
                                    Скачать список учеников программы
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
            <hr>
            <h4>Образовательные программы (дополнительные)</h4>
            <ul class="list-group" id="myUL2">
                @if ($user->proposed_programs())
                    @foreach( $user->proposed_programs() as $proposed_program)
                        @if($proposed_program->educational_program == 'дополнительная')
                            <li class="list-group-item">
                                <a href="/pupils-vuz/{{ $user->id }}/{{ $proposed_program->id }}/list-of-pupils" class='btn btn-outline-success'>
                                    {{ $proposed_program->subject }} - {{ $proposed_program->modul }} 
                                    - {{ $proposed_program->educational_program }}
                                    - {{ $proposed_program->getClasses() }} класс -
                                    с {{ $proposed_program->getDataBegin() }} -
                                    по {{ $proposed_program->getDataEnd() }}
                                </a>
                                <br>
                                <a href="/pupils-vuz-school/{{ $user->id }}/{{ $proposed_program->id }}/list-of-pupils" class='btn btn-outline-dark'>
                                    Скачать список учеников программы
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>

            <hr>
            <h4>
                <a href='/{{ $user->id }}/not-archive/main/agreements-vuz-prof' class="btn btn-outline-dark">
                    Страница образцов договоров (основные)
                </a>
                <a href='/{{ $user->id }}/not-archive/not/agreements-vuz-prof' class="btn btn-outline-dark">
                    Страница образцов договоров (дополнительные)
                </a>
            </h4>
        </div>
    </div>
@endif
@endsection

<script src="{{ asset('js/search_programs.js') }}"></script>