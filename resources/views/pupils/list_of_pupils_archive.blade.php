@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))
<div class="container-fluid">
    <ul class="list-group">
        <li class="list-group-item">
            Поиск
        </li>
        <li class="list-group-item">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        ФИО
                    </span>
                </div>
                <input type="text" class="form-control"
                id="one" onkeyup="one()">

                <div class="input-group-prepend">
                    <span class="input-group-text">
                        ВУЗ
                    </span>
                </div>
                <input type="text" class="form-control"
                id="two" onkeyup="two()">
            </li>
    </ul>

    <table class="table table-stripe border-bottom" id="myTable">
        <thead>
            <tr>
                <th scope="col">⇅ ФИО</th>
                <th scope="col">⇅ ВУЗ</th>
                <th scope="col">Программа</th>
                <th scope="col">⇅ Одобрить программу</th>
                <th scope="col">Расписание</th>
                <th scope="col">Договор</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pupils as $pupil)
                @if ($pupil->selected_programs_archive($year))
                    @foreach($pupil->selected_programs_archive($year) as $selected_program)
                        @if ($selected_program->not_student != 1)
                        <tr>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        {{$pupil->fullname}}
                                        <br>
                                        {{$pupil->name}}
                                    </li>
                                    <li class="list-group-item">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong{{$pupil->id}}">
                                            Убрать ученика из списка
                                        </button>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="/pupil/school/selected_programs/{{ $selected_program->id }}/show"
                                            class="btn btn-outline-info btn">
                                            <i class="far fa-eye"></i> Удаление
                                        </a>
                                    </li>
                                </ul>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalLong{{$pupil->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Вы уверены, что хотите удалить ученика {{$pupil->fullname}} из списка?
                                            <form action="{{ action('SelectedProgramController@student_not', [$selected_program->id, $pupil->grade_number, $pupil->grade_letter, $user->id] ) }}" method="POST">
                                                @method('PUT')
                                                @csrf
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success">Да</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>{{$selected_program->proposed_program->user->fullname}}</td>

                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <a href="/files/proposed_programs/{{ $selected_program->proposed_program->filename }}"
                                            class="btn btn-outline-success">
                                            Программа
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        {{ $selected_program->proposed_program->subject}}
                                    </li>
                                    <li class="list-group-item">
                                        {{ $selected_program->proposed_program->getDataBegin() }} - {{ $selected_program->proposed_program->getDataEnd() }}
                                    </li>
                                    <li class="list-group-item">
                                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myProgram{{$selected_program->id}}">
                                            Информация о программе
                                        </button>
                                        <div class="modal fade" id="myProgram{{$selected_program->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabelProgram{{$selected_program->id}}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content" style="width:600px">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="myModalLabelProgram{{$selected_program->id}}">Информация о программе</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <ul class="list-group">
                                                            <li class="list-group-item">
                                                                <ul class="list-group list-group-horizontal">
                                                                    <li class="list-group-item" style="width:35%">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</li>
                                                                    <li class="list-group-item" style="width:45%">
                                                                        Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                                                    </li>
                                                                    <li class="list-group-item" style="width:auto">Даты</li>
                                                                </ul>
                                                            </li>
                                                            <li class="list-group-item">
                                                                <ul class="list-group list-group-horizontal">
                                                                    <li class="list-group-item" style="width:35%">
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->getClasses() }}</li>
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->subject }} </li>
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->modul }}</li>
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->hours }}</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="list-group-item" style="width:45%">
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->form_of_education }}</li>
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->form_education_implementation }}</li>
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->educational_program }}</li>
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->educational_activity }}</li>
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->content }}</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="list-group-item" style="width:auto">
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->getDataBegin() }}</li>
                                                                            <li class="list-group-item">{{ $selected_program->proposed_program->getDataEnd() }}</li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                               
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </td>

                            <td>
                                @if ($selected_program->status == 0)
                                    <p>
                                        <a href="/selected-program/plus/{{ $selected_program->id }}" class="btn btn-outline-success btn">
                                            Согласовать
                                        </a>
                                    <p>
                                    <p>
                                        <a href="/selected-program/minus/{{ $selected_program->id }}" class="btn btn-outline-danger btn">
                                            Отклонить
                                        </a>
                                    <p>
                                @endif

                                @if ($selected_program->status == 1)
                                    <div class="alert alert-success" role="alert">
                                        Программа согласована
                                    </div>
                                @endif

                                @if ($selected_program->status == 2)
                                    <div class="alert alert-danger" role="alert">
                                        Программа отклонена
                                    </div>
                                @endif
                            </td>
                            @if ($selected_program->selected_schedule)
                                <td> 
                                    @if($selected_program->selected_schedule->status !== 2)
                                        {!! $selected_program->selected_schedule->getStatus() !!}
                                        <a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                            class="btn btn-outline-success">
                                                Расписание
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @if($selected_program->selected_schedule->status == 1)
                                        @if ($selected_program->selected_schedule->agreement_pupil)
                                            <a href="/files/agreements_pupils/{{ $selected_program->selected_schedule->agreement_pupil->filename }}"
                                                class="btn btn-outline-success">
                                                Договор
                                            </a>
                                            <br>
                                            @if($selected_program->selected_schedule->agreement_pupil->sign != 1)
                                            {{-- <a href='/agreement-sign/agreements_pupils/{{ $user->id }}/{{ $selected_program->selected_schedule->agreement_pupil->id }}/{{$pupil->grade_number}}/{{$pupil->grade_letter}}'
                                                type="button" id="signButton-{{ $selected_program->selected_schedule->agreement_pupil->filename }}"
                                                class="btn btn-outline-primary signButton">
                                                Ссылка на подпись документа
                                            </a> --}}
                                            @else
                                            <a href="/files/agreements_pupils/signed/{{ $selected_program->selected_schedule->agreement_pupil->filename_sign }}"
                                                class="btn btn-outline-success">
                                                Ваша подпись
                                            </a>
                                            @endif
                                            <br>
                                            @if($selected_program->selected_schedule->agreement_pupil->sign2 == 1)
                                            <a href="/files/agreements_pupils/signed/{{ $selected_program->selected_schedule->agreement_pupil->filename_sign2 }}"
                                                class="btn btn-outline-success">
                                                Подпись другой стороны
                                            </a>
                                            @endif

                                            <hr>

                                            @if($selected_program->proposed_program->user->org_type == 'prof')
                                                @if ($selected_program->selected_schedule->agreement_pupil2)
                                                    <a href="/files/agreements_pupils2/{{ $selected_program->selected_schedule->agreement_pupil2->filename }}"
                                                        class="btn btn-outline-success">
                                                        Договор 2
                                                    </a>
                                                    <br>
                                                    @if($selected_program->selected_schedule->agreement_pupil2->sign != 1)
                                                    {{-- <a href='/agreement-sign/agreements_pupils2/{{ $user->id }}/{{ $selected_program->selected_schedule->agreement_pupil2->id }}/{{$pupil->grade_number}}/{{$pupil->grade_letter}}'
                                                        type="button" id="signButton-{{ $selected_program->selected_schedule->agreement_pupil2->filename }}"
                                                        class="btn btn-outline-primary signButton">
                                                        Ссылка на подпись документа
                                                    </a> --}}
                                                    @else
                                                    <a href="/files/agreements_pupils2/signed/{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign }}"
                                                        class="btn btn-outline-success">
                                                        Ваша подпись
                                                    </a>
                                                    @endif
                                                    <br>
                                                    @if($selected_program->selected_schedule->agreement_pupil2->sign2 == 1)
                                                    <a href="/files/agreements_pupils2/signed2/{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign2 }}"
                                                        class="btn btn-outline-success">
                                                        Подпись другой стороны
                                                    </a>
                                                    @endif
                                                @else
                                                    <a href="/agreement-document-selected/create-document4/{{ $user->id }}/{{$selected_program->id}}"
                                                        class="btn btn-outline-primary">
                                                        Скачать образец договора
                                                    </a>
                                                    <a href="/agreement_pupil2/{{ $selected_program->selected_schedule->id }}/{{$pupil->grade_number}}/{{$pupil->grade_letter}}/{{$user->id}}"
                                                        class="btn btn-outline-danger">
                                                            Добавить договор
                                                    </a>
                                                @endif
                                            @endif

                                            @if($selected_program->proposed_program->user->org_type == 'vuz')
                                                @if ($selected_program->selected_schedule->agreement_pupil2)
                                                    <a href="/files/agreements_pupils2/{{ $selected_program->selected_schedule->agreement_pupil2->filename }}"
                                                        class="btn btn-outline-success">
                                                        Договор 2
                                                    </a>
                                                    <br>
                                                    @if($selected_program->selected_schedule->agreement_pupil2->sign != 1)
                                                    {{-- <a href='/agreement-sign/agreements_pupils2/{{ $user->id }}/{{ $selected_program->selected_schedule->agreement_pupil2->id }}/{{$pupil->grade_number}}/{{$pupil->grade_letter}}'
                                                        type="button" id="signButton-{{ $selected_program->selected_schedule->agreement_pupil2->filename }}"
                                                        class="btn btn-outline-primary signButton">
                                                        Ссылка на подпись документа
                                                    </a> --}}
                                                    @else
                                                    <a href="/files/agreements_pupils2/signed/{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign }}"
                                                        class="btn btn-outline-success">
                                                        Ваша подпись
                                                    </a>
                                                    @endif
                                                    <br>
                                                    @if($selected_program->selected_schedule->agreement_pupil2->sign2 == 1)
                                                    <a href="/files/agreements_pupils2/signed2/{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign2 }}"
                                                        class="btn btn-outline-success">
                                                        Подпись другой стороны
                                                    </a>
                                                    @endif
                                                @else
                                                    <a href="/agreement-document-selected/create-document-vsu/{{ $user->id }}/{{$selected_program->id}}"
                                                        class="btn btn-outline-primary">
                                                        Скачать образец договора
                                                    </a>
                                                    <a href="/agreement_pupil2/{{ $selected_program->selected_schedule->id }}/{{$pupil->grade_number}}/{{$pupil->grade_letter}}/{{$user->id}}"
                                                        class="btn btn-outline-danger">
                                                            Добавить договор
                                                    </a>
                                                @endif
                                            @endif

                                        @else
                                        @if($user->date_license != NULL and $user->number_license != NULL and $user->who_license != NULL and
                                            $user->director_post_rod != NULL and $user->director_rod != NULL and
                                            $user->bik != NULL and $user->bank != NULL and $user->single_treasury_account != NULL and $user->treasury_account != NULL)

                                            @if($selected_program->proposed_program->user->org_type == 'prof')
                                                <a href="/agreement-document-selected/create-document3/{{ $user->id }}/{{$selected_program->id}}"
                                                    class="btn btn-outline-primary">
                                                    Скачать образец договора
                                                </a>
                                            @else
                                                <a href="/agreement-document-selected/create-document2/{{ $user->id }}/{{$selected_program->id}}"
                                                    class="btn btn-outline-primary">
                                                    Скачать образец договора
                                                </a>
                                            @endif

                                        @elseif($selected_program->proposed_program->user->date_license == NULL or $selected_program->proposed_program->user->number_license == NULL or $selected_program->proposed_program->user->who_license == NULL or
                                                $selected_program->proposed_program->user->director_post_rod == NULL or $selected_program->proposed_program->user->director_rod == NULL or
                                                $selected_program->proposed_program->user->bik == NULL or $selected_program->proposed_program->user->bank == NULL or $selected_program->proposed_program->user->single_treasury_account == NULL or $selected_program->proposed_program->user->treasury_account == NULL)
                                            <div class="alert alert-danger" role="alert">
                                                Другая организация должна заполнить сведения о себе
                                            </div>
                                        @else
                                            <div class="alert alert-danger" role="alert">
                                                Заполните полностью <a href="/users/{{ $user->id }}/show-org">Сведениях о вашей организации</a>
                                            </div>
                                        @endif

                                            <a href="/agreement_pupil/{{ $selected_program->selected_schedule->id }}/{{$pupil->grade_number}}/{{$pupil->grade_letter}}/{{$user->id}}"
                                                class="btn btn-outline-danger">
                                                    Добавить договор
                                            </a>
                                        @endif
                                    @endif
                                </td>
                            @endif
                        </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endif

<script src="{{ asset('js/sort_org_list_reg.js') }}"></script>
<script src="{{ asset('js/poisk.js') }}"></script>
@endsection