@extends('layouts.app')

@section('content')
@if (Auth::user() && ((Auth::user()->status == 1) || (Auth::user()->status == 17) || (Auth::user()->status == 27)))
<div class="container-fluid">
    <p>Список учеников</p>
    <p>
        Программа: {{ $proposed_program->user->fullname }} - {{ $proposed_program->subject }} - {{ $proposed_program->modul }} 
        - {{ $proposed_program->getClasses() }} класс -
        с {{ $proposed_program->getDataBegin() }} -
        по {{ $proposed_program->getDataEnd() }}
        <br>
        <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
            class="btn btn-outline-success">
            Программа
        </a>
    </p>

    <ul class="list-group">
        <li class="list-group-item">
            Поиск
        </li>

        <li class="list-group-item">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Район
                    </span>
                </div>
                <input type="text" class="form-control"
                id="one" onkeyup="one()">

                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Школа
                    </span>
                </div>
                <input type="text" class="form-control"
                id="two" onkeyup="two()">
            </div>
        </li>

        <li class="list-group-item">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        ФИО
                    </span>
                </div>
                <input type="text" class="form-control"
                id="three" onkeyup="three()">

                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Класс
                    </span>
                </div>
                <input type="text" class="form-control"
                id="four" onkeyup="four()">
            </div>
        </li>
    </ul>

    <table class="table table-stripe border-bottom" id="myTable">
        <thead>
            <tr>
                <th scope="col">⇅ Район</th>
                <th scope="col">⇅ Школа</th>
                <th scope="col">⇅ ФИО</th>
                <th scope="col">⇅ Класс</th>
                <th scope="col">Расписание</th>
                <th scope="col">Договор</th>
            </tr>
        </thead>
        <tbody>
            @if ($proposed_program->selected_programs())
                @foreach($proposed_program->selected_programs() as $selected_program)
                    @if($selected_program->user->status == 7 and $selected_program->status == 1 and $selected_program->not_student != 1)
                        <tr>
                            <td>{{$selected_program->user->getDistrict->fullname}}</td>
                            <td>{{$selected_program->user->getSchool->fullname}}</td>
                            <td>{{$selected_program->user->fullname}}</td>
                            <td>{{$selected_program->user->grade_number}} {{$selected_program->user->grade_letter}}</td>
                            @if ($selected_program->selected_schedule)
                                <td>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            @if($selected_program->selected_schedule->status !== 2)
                                                <p>
                                                    {!! $selected_program->selected_schedule->getStatus() !!}
                                                </p>
                                                <a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                                    class="btn btn-outline-success">
                                                        Расписание
                                                </a>
                                            @endif
                                        </li>
                                    </ul>
                                </td>
                                @if($selected_program->selected_schedule->status === 1)
                                    @if ($selected_program->selected_schedule->agreement_pupil)
                                        <td>
                                            <a href="/files/agreements_pupils/{{ $selected_program->selected_schedule->agreement_pupil->filename }}"
                                                class="btn btn-outline-success">
                                                Договор
                                            </a>
                                            <br>
                                            @if($selected_program->selected_schedule->agreement_pupil->sign === 1)
                                            <a href="/files/agreements_pupils/signed/{{ $selected_program->selected_schedule->agreement_pupil->filename_sign }}"
                                                class="btn btn-outline-success">
                                                Подпись базовой организации
                                            </a>
                                            @endif
                                            <br>
                                            @if($selected_program->selected_schedule->agreement_pupil->sign2 === 1)
                                            <a href="/files/agreements_pupils/signed2/{{ $selected_program->selected_schedule->agreement_pupil->filename_sign2 }}"
                                                class="btn btn-outline-success">
                                                Подпись организации-участника
                                            </a>
                                            @endif
                                        </td>
                                    @endif
                                    @if ($selected_program->selected_schedule->agreement_pupil2)
                                        <td>
                                            <a href="/files/agreements_pupils2/{{ $selected_program->selected_schedule->agreement_pupil2->filename }}"
                                                class="btn btn-outline-success">
                                                Договор 2
                                            </a>
                                            <br>
                                            @if($selected_program->selected_schedule->agreement_pupil2->sign === 1)
                                            <a href="/files/agreements_pupils2/signed/{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign }}"
                                                class="btn btn-outline-success">
                                                Подпись базовой организации
                                            </a>
                                            @endif
                                            <br>
                                            @if($selected_program->selected_schedule->agreement_pupil2->sign2 === 1)
                                            <a href="/files/agreements_pupils2/signed2/{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign2 }}"
                                                class="btn btn-outline-success">
                                                Подпись организации-участника
                                            </a>
                                            @endif
                                        </td>
                                    @endif
                                @endif
                            @endif
                        </tr>
                    @endif
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endif

<script src="{{ asset('js/sort_org_list_reg.js') }}"></script>
<script src="{{ asset('js/poisk.js') }}"></script>
@endsection