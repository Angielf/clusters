@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))
    <div class="container-fluid">
        <div class = "col">
            <div class="container d-flex align-items-center justify-content-center">
                <div class="row">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                Поиск по основным программам
                            </span>
                        </div>
                        <input type="text" class="form-control"
                        id="search1" onkeyup="search1()">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                Поиск по дополнительным программам
                            </span>
                        </div>
                        <input type="text" class="form-control"
                        id="search2" onkeyup="search2()">
                    </div>
                    <h4>Образовательные программы (основные)</h4>
                    <ul class="list-group" id="myUL1">
                        @foreach($sps as $sp)
                            @if($sp->archive == NULL)
                                @if($sp->educational_program == 'основная')
                                    <li class="list-group-item">
                                        <a href="/pupils-school/{{ $user->id }}/{{ $sp->id }}/list-of-pupils">

                                            {{ $sp->user->fullname }} 
                                            - {{ $sp->subject }} - {{ $sp->modul }} 
                                            - {{ $sp->educational_program }}
                                            - {{ $sp->getClasses() }} класс -
                                            с {{ $sp->getDataBegin() }} -
                                            по {{ $sp->getDataEnd() }}

                                        </a>
                                    </li>
                                @endif
                            @endif
                        @endforeach
                    </ul>
                    <h4>Образовательные программы (дополнительные)</h4>
                    <ul class="list-group" id="myUL2">
                        @foreach($sps as $sp)
                            @if($sp->archive == NULL)
                                @if($sp->educational_program == 'дополнительная')
                                    <li class="list-group-item">
                                        <a href="/pupils-school/{{ $user->id }}/{{ $sp->id }}/list-of-pupils">

                                            {{ $sp->user->fullname }} 
                                            - {{ $sp->subject }} - {{ $sp->modul }} 
                                            - {{ $sp->educational_program }}
                                            - {{ $sp->getClasses() }} класс -
                                            с {{ $sp->getDataBegin() }} -
                                            по {{ $sp->getDataEnd() }}

                                        </a>
                                    </li>
                                @endif
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection

<script src="{{ asset('js/search_programs.js') }}"></script>
