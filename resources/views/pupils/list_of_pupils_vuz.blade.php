@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))
<div class="container-fluid">
    <p>Список учеников</p>
    <p>
        Программа: {{ $proposed_program->subject }} - {{ $proposed_program->modul }} 
        - {{ $proposed_program->getClasses() }} класс -
        с {{ $proposed_program->getDataBegin() }} -
        по {{ $proposed_program->getDataEnd() }}
        <br>
        <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
            class="btn btn-outline-success">
            Программа
        </a>
    </p>

    <ul class="list-group">
        <li class="list-group-item">
            Поиск
        </li>
        <li class="list-group-item">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Район
                    </span>
                </div>
                <input type="text" class="form-control"
                id="one" onkeyup="one()">

                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Школа
                    </span>
                </div>
                <input type="text" class="form-control"
                id="two" onkeyup="two()">
            </div>
        </li>

        <li class="list-group-item">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        ФИО
                    </span>
                </div>
                <input type="text" class="form-control"
                id="three" onkeyup="three()">

                <div class="input-group-prepend">
                    <span class="input-group-text">
                        Класс
                    </span>
                </div>
                <input type="text" class="form-control"
                id="four" onkeyup="four()">
            </div>
        </li>
    </ul>

    <table class="table table-stripe border-bottom" id="myTable">
        <thead>
            <tr>
                <th scope="col">⇅ Район</th>
                <th scope="col">⇅ Школа</th>
                <th scope="col">⇅ ФИО</th>
                <th scope="col">⇅ Класс</th>
                <th scope="col">Расписание</th>
                <th scope="col">Договор</th>
            </tr>
        </thead>
        <tbody>
            @if ($proposed_program->selected_programs())
                @foreach($proposed_program->selected_programs() as $selected_program)
                    @if($selected_program->user->status == 7 and $selected_program->status == 1 and $selected_program->not_student != 1)
                        <tr>
                            <td>{{$selected_program->user->getDistrict->fullname}}</td>
                            <td>{{$selected_program->user->getSchool->fullname}}</td>
                            <td>{{$selected_program->user->fullname}}</td>
                            <td>{{$selected_program->user->grade_number}} {{$selected_program->user->grade_letter}}</td>
                            @if ($selected_program->selected_schedule)
                                <td>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            @if($selected_program->selected_schedule->status !== 2)
                                                <p>
                                                    {!! $selected_program->selected_schedule->getStatus() !!}
                                                </p>
                                                <a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                                    class="btn btn-outline-success">
                                                        Расписание
                                                </a>
                                                @if(!$selected_program->selected_schedule->agreement_pupil)
                                                    <p><button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteSchedule">
                                                        <i class="far fa-trash-alt"></i> Удалить расписание
                                                    </button></p>
                                                @endif
                    
                                                <!-- Удалить расписание -->
                                                <div class="modal fade" id="deleteSchedule" tabindex="-1" aria-labelledby="deleteScheduleLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Вы уверены, что хотите удалить расписание {{ $selected_program->selected_schedule->filename }}?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
                                                                    <form action="{{ action('SelectedScheduleController@delete_many', [$selected_program->selected_schedule->filename, $user->id, $proposed_program->id]) }}"
                                                                        method="POST">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button type="submit"
                                                                            class="btn btn-outline-success btn">
                                                                            Да
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </li>
                                        {{-- @if($selected_program->selected_schedule->status === 1)
                                            <li class="list-group-item">
                                                @if($selected_program->selected_schedule->selected_months_hour)
                                                    <a href="/selected_months_hours/{{ $selected_program->selected_schedule->selected_months_hour->id }}/update"
                                                        class="btn btn-outline-info btn">
                                                        Кол-во часов по месяцам
                                                    </a>
                                                @else
                                                    <a href="/selected_months_hours/{{ $selected_program->selected_schedule->id }}"
                                                        class="btn btn-outline-info btn">
                                                        Добавить кол-во часов по месяцам
                                                    </a>
                                                @endif
                                            </li>
                                        @endif --}}
                                    </ul>
                                </td>
                                @if($selected_program->selected_schedule->status === 1)
                                    @if ($selected_program->selected_schedule->agreement_pupil)
                                        <td>
                                            <a href="/files/agreements_pupils/{{ $selected_program->selected_schedule->agreement_pupil->filename }}"
                                                class="btn btn-outline-success">
                                                Договор
                                            </a>
                                            @if($selected_program->selected_schedule->agreement_pupil->agree == 1)
                                                <div class="alert alert-success mt-2" role="alert">
                                                    Договор одобрен
                                                </div>
                                            @endif
                                            @if($selected_program->selected_schedule->agreement_pupil->sign == 1)
                                            <a href="/files/agreements_pupils/signed/{{ $selected_program->selected_schedule->agreement_pupil->filename_sign }}"
                                                class="btn btn-outline-success">
                                                Подпись другой стороны
                                            </a>
                                            @endif
                                            <br>
                                            @if($selected_program->selected_schedule->agreement_pupil->sign2 != 1)
                                            {{-- <a href='/agreement-sign2/agreements_pupils/{{ $user->id }}/{{ $selected_program->selected_schedule->agreement_pupil->id }}/{{ $proposed_program->id }}/'
                                                type="button" id="signButton-{{ $selected_program->selected_schedule->agreement_pupil->filename }}"
                                                class="btn btn-outline-primary signButton">
                                                Ссылка на подпись документа
                                            </a> --}}
                                            @else
                                            <a href="/files/agreements_pupils/signed2/{{ $selected_program->selected_schedule->agreement_pupil->filename_sign2 }}"
                                                class="btn btn-outline-success">
                                                Ваша подпись
                                            </a>
                                            @endif
                                            <hr>
                                            <p>Договор на второе полугодие</p>

                                            @if ($selected_program->selected_schedule->agreement_pupil2)
                                                <a href="/files/agreements_pupils2/{{ $selected_program->selected_schedule->agreement_pupil2->filename }}"
                                                    class="btn btn-outline-success">
                                                    Договор 2
                                                </a>
                                                @if($selected_program->selected_schedule->agreement_pupil2->agree == 1)
                                                    <div class="alert alert-success mt-2" role="alert">
                                                        Договор одобрен
                                                    </div>
                                                @endif
                                                @if($selected_program->selected_schedule->agreement_pupil2->sign == 1)
                                                <a href="/files/agreements_pupils2/signed/{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign }}"
                                                    class="btn btn-outline-success">
                                                    Подпись другой стороны
                                                </a>
                                                @endif
                                                <br>
                                                @if($selected_program->selected_schedule->agreement_pupil2->sign2 != 1)
                                                {{-- <a href='/agreement-sign2/agreements_pupils2/{{ $user->id }}/{{ $selected_program->selected_schedule->agreement_pupil2->id }}/{{ $proposed_program->id }}/'
                                                    type="button" id="signButton-{{ $selected_program->selected_schedule->agreement_pupil2->filename }}"
                                                    class="btn btn-outline-primary signButton">
                                                    Ссылка на подпись документа
                                                </a> --}}
                                                @else
                                                <a href="/files/agreements_pupils2/signed2/{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign2 }}"
                                                    class="btn btn-outline-success">
                                                    Ваша подпись
                                                </a>
                                                @endif
                                            @else
                                                @if($user->org_type == 'prof')
                                                    <a href="/agreement-document-selected/create-document4/{{ $selected_program->user->getSchool->id }}/{{$selected_program->id}}"
                                                        class="btn btn-outline-primary">
                                                        Скачать образец договора
                                                    </a>
                                                @elseif($user->org_type == 'vuz')
                                                    <a href="/agreement-document-selected/create-document-vsu/{{ $selected_program->user->getSchool->id }}/{{$selected_program->id}}"
                                                        class="btn btn-outline-primary">
                                                        Скачать образец договора
                                                    </a>
                                                @endif
                                                <br>
                                                <a href="/agreement_pupil2/{{ $selected_program->selected_schedule->id }}/{{$selected_program->user->grade_number}}/{{$selected_program->user->grade_letter}}/{{$selected_program->user->getSchool->id}}"
                                                    class="btn btn-outline-danger">
                                                        Добавить договор
                                                </a>
                                            @endif
                                        </td>
                                    @else
                                        <td>
                                            @if($user->date_license != NULL and $user->number_license != NULL and $user->who_license != NULL and
                                                $user->director_post_rod != NULL and $user->director_rod != NULL and
                                                $user->bik != NULL and $user->bank != NULL and $user->single_treasury_account != NULL and $user->treasury_account != NULL)

                                                @if($user->org_type == 'prof')
                                                    <a href="/agreement-document-selected/create-document3/{{ $selected_program->user->getSchool->id }}/{{$selected_program->id}}"
                                                        class="btn btn-outline-primary">
                                                        Скачать образец договора
                                                    </a>
                                                @else
                                                    <a href="/agreement-document-selected/create-document-vsu/{{ $selected_program->user->getSchool->id }}/{{$selected_program->id}}"
                                                        class="btn btn-outline-primary">
                                                        Скачать образец договора
                                                    </a>
                                                @endif

                                            @elseif($selected_program->user->getSchool->date_license == NULL or $selected_program->user->getSchool->number_license == NULL or $selected_program->user->getSchool->who_license == NULL or
                                                    $selected_program->user->getSchool->director_post_rod == NULL or $selected_program->user->getSchool->director_rod == NULL or
                                                    $selected_program->user->getSchool->bik == NULL or $selected_program->user->getSchool->bank == NULL or $selected_program->user->getSchool->single_treasury_account == NULL or $selected_program->user->getSchool->treasury_account == NULL)
                                                <div class="alert alert-danger" role="alert">
                                                    Другая организация должна заполнить сведения о себе
                                                </div>
                                            @else
                                                <div class="alert alert-danger" role="alert">
                                                    Заполните полностью <a href="/users/{{ $user->id }}/show-org">Сведениях о вашей организации</a>
                                                </div>
                                            @endif
                                            <br>
                                            <a href="/agreement_pupil/{{ $selected_program->selected_schedule->id }}/{{$selected_program->user->grade_number}}/{{$selected_program->user->grade_letter}}/{{$selected_program->user->getSchool->id}}"
                                                class="btn btn-outline-danger">
                                                    Добавить договор
                                            </a>
                                        </td>
                                    @endif
                                @endif
                            @else
                                <td>
                                    <form action="{{ action('SelectedScheduleController@add_many', [$selected_program->id, $proposed_program->id, $user->id]) }}" 
                                        method="GET">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-primary">
                                            <i class="fas fa-file-upload"></i> Добавить расписание
                                        </button>
                                    </form>
                                </td>
                            @endif
                        </tr>
                    @endif
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endif

<script src="{{ asset('js/sort_org_list_reg.js') }}"></script>
<script src="{{ asset('js/poisk.js') }}"></script>
@endsection