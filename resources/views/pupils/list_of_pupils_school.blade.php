@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))
<div class="container-fluid">
    <p>Список учеников</p>

    <p>
        <button type="button" id="export_button" class="btn btn-outline-dark"><i class='fa-solid fa-file-arrow-down'></i> Выгрузить в Excel (xlsx)</button>
    </p>

    <table class="table table-stripe border-bottom" id="myTable">
        <thead>
            <tr>
                <th scope="col">⇅ ФИО</th>
                <th scope="col">⇅ Класс</th>

                <th scope="col">Муниципалитет</th>
                <th scope="col">Школа ученика</th>
                <th scope="col">ИНН школы ученика</th>

                <th scope="col">Организация-участник</th>
                <th scope="col">ИНН базовой организации</th>
                <th scope="col">Класс</th>
                <th scope="col">Предмет(курс)</th>
                <th scope="col">Раздел(модуль)</th>
                <th scope="col">Кол-во часов</th>
                <th scope="col">Форма реализации обучения</th>
                <th scope="col">Условия реализации обучения</th>
                <th scope="col">Образовательная программа</th>
                <th scope="col">Образовательная деятельность</th>
                <th scope="col">Комментарий</th>
                <th scope="col">Начало</th>
                <th scope="col">Конец</th>
            </tr>
        </thead>
        <tbody>
            @if ($proposed_program->selected_programs())
                @foreach($proposed_program->selected_programs() as $selected_program)
                    @if($selected_program->user->status == 7 
                    and $selected_program->status == 1 
                    and $selected_program->not_student != 1)
                        @if($user->org_type != 'vuz' and $user->org_type != 'prof')
                            @if ($selected_program->user->org == $user->id)
                            <tr>
                                <td>{{$selected_program->user->fullname}}</td>
                                <td>{{$selected_program->user->grade_number}} {{$selected_program->user->grade_letter}}</td>
    
                                <td>{{ $selected_program->user->getDistrict->fullname }}</td>
                                <td>{{ $selected_program->user->getSchool->fullname }}</td>
                                <td>{{ $selected_program->user->getSchool->inn }}</td>
    
                                <td>{{ $selected_program->proposed_program->user->fullname}}</td>
                                <td>{{ $selected_program->proposed_program->user->inn}}</td>
                                <td>{{ $selected_program->proposed_program->getClasses() }}</td>
                                <td>{{ $selected_program->proposed_program->subject }}</td>
                                <td>{{ $selected_program->proposed_program->modul }}</td>
                                <td>{{ $selected_program->proposed_program->hours }}</td>
                                <td>{{ $selected_program->proposed_program->form_of_education }} </td>
                                <td>{{ $selected_program->proposed_program->form_education_implementation }}</td>
                                <td>{{ $selected_program->proposed_program->educational_program }}</td>
                                <td>{{ $selected_program->proposed_program->educational_activity }}</td>
                                <td>{{ $selected_program->proposed_program->content }}</td>
                                <td>{{ $selected_program->proposed_program->getDataBegin() }}</td>
                                <td>{{ $selected_program->proposed_program->getDataEnd() }}</td>
                            </tr>
                            @endif
                        @else
                            <tr>
                                <td>{{$selected_program->user->fullname}}</td>
                                <td>{{$selected_program->user->grade_number}} {{$selected_program->user->grade_letter}}</td>

                                <td>{{ $selected_program->user->getDistrict->fullname }}</td>
                                <td>{{ $selected_program->user->getSchool->fullname }}</td>
                                <td>{{ $selected_program->user->getSchool->inn }}</td>

                                <td>{{ $selected_program->proposed_program->user->fullname}}</td>
                                <td>{{ $selected_program->proposed_program->user->inn}}</td>
                                <td>{{ $selected_program->proposed_program->getClasses() }}</td>
                                <td>{{ $selected_program->proposed_program->subject }}</td>
                                <td>{{ $selected_program->proposed_program->modul }}</td>
                                <td>{{ $selected_program->proposed_program->hours }}</td>
                                <td>{{ $selected_program->proposed_program->form_of_education }} </td>
                                <td>{{ $selected_program->proposed_program->form_education_implementation }}</td>
                                <td>{{ $selected_program->proposed_program->educational_program }}</td>
                                <td>{{ $selected_program->proposed_program->educational_activity }}</td>
                                <td>{{ $selected_program->proposed_program->content }}</td>
                                <td>{{ $selected_program->proposed_program->getDataBegin() }}</td>
                                <td>{{ $selected_program->proposed_program->getDataEnd() }}</td>
                            </tr>
                        @endif
                    @endif
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endif

<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/xlsx-js-style@1.2.0/dist/xlsx.bundle.js"></script>
<script type='text/javascript'>

    function fittoColumn(arrayOfArray) {
        return arrayOfArray[0].map((a, i) => ({ wch: Math.max(...arrayOfArray.map(a2 => a2[i] ? a2[i].toString().length : 0)) }));
    }
    
    function html_table_to_excel(type)
    {
        var data1 = document.getElementById('myTable');
        
        var file = XLSX.utils.table_to_book(data1, {sheet: "Таблица 1"});
        
        var ws = file.Sheets["Таблица 1"];
        
        for (const key in ws) {
            // Первая строка
            if(key.replace(/[^0-9]/ig, '') === '1') {
                ws[key].s = {
                font: {
                    bold: true
                    },
                }
            }
        }

        var range = XLSX.utils.decode_range(ws['!ref']);
        var noCols = range.e.c;

        XLSX.write(file, { bookType: type, bookSST: true, type: 'base64' });

        XLSX.writeFile(file, 'ученики.' + type);
    }

    const export_button = document.getElementById('export_button');
    export_button.addEventListener('click', () => {
    html_table_to_excel('xlsx');
});
</script>

<script src="{{ asset('js/sort_org_list_reg.js') }}"></script>
<script src="{{ asset('js/poisk.js') }}"></script>
@endsection