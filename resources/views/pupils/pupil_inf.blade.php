@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user_org->id))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            
            <p><a href="{{ route('home')}}"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться на главную страницу
            </a></p>

            <table class="table table-striped table-bordered">
                <tbody>
                    <tr>
                        <th scope="row">Логин</th>
                        <td>{{ $user_org->name }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">Район</th>
                        <td>{{ $user_org->getDistrict->fullname }}</td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myDistrict">
                                <i class="far fa-edit"></i> Изменить район и школу
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Школа</th>
                        <td>{{ $user_org->getSchool->fullname }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">Класс</th>
                        <td>{{ $user_org->grade_number }} {{ $user_org->grade_letter }}</td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myGrade_number">
                                <i class="far fa-edit"></i> Изменить число
                            </button>
                            <br>
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myGrade_letter">
                                <i class="far fa-edit"></i> Изменить букву
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">ФИО</th>
                        <td>{{ $user_org->fullname }}</td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myFullname">
                                <i class="far fa-edit"></i> Изменить
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Телефон</th>
                        <td>{{ $user_org->tel }}</td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myTel">
                                <i class="far fa-edit"></i> Изменить
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Почта</th>
                        <td><a href="mailto:{{ $user_org->email_real }}">{{ $user_org->email_real }}</a></td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myEmail">
                                <i class="far fa-edit"></i> Изменить
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">СНИЛС</th>
                        <td>{{ $user_org->snils }}</td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mySnils">
                                <i class="far fa-edit"></i> Изменить
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Дата рождения</th>
                        <td>{{ $user_org->birth }}</td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myBirth">
                                <i class="far fa-edit"></i> Изменить
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br/>

            <!-- Модальное окно District -->
            <div class="modal fade" id="myDistrict" tabindex="-1" role="dialog" aria-labelledby="myModalLabelDistrict" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelDistrict">Район и школа</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_district_pupil', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <select class="form-control" name="district" id="district" required>
                                        <option selected></option>
                                        @foreach( $muns as $mun)
                                            @if($mun->id != 100)
                                                <option value="{{$mun->id}}">{{$mun->fullname}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <br>
                                    <select class="form-control" id="org" name="org" required>
                                        <option></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна District --}}

            <!-- Модальное окно Grade_number-->
            <div class="modal fade" id="myGrade_number" tabindex="-1" role="dialog" aria-labelledby="myModalLabelGrade_number" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelGrade_number">Число {{ $user_org->grade_number }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_grade_number', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <select class="form-control" name="grade_number" id="grade_number" required>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Grade_number --}}

            
            <!-- Модальное окно Grade_letter-->
            <div class="modal fade" id="myGrade_letter" tabindex="-1" role="dialog" aria-labelledby="myModalLabelGrade_letter" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelGrade_letter">Буква {{ $user_org->grade_letter }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_grade_letter', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <select class="form-control" name="grade_letter" id="grade_letter" required>
                                        <option value="А">А</option>
                                        <option value="Б">Б</option>
                                        <option value="В">В</option>
                                        <option value="Г">Г</option>
                                        <option value="Д">Д</option>
                                        <option value="Е">Е</option>
                                        <option value="Ж">Ж</option>
                                        <option value="З">З</option>
                                        <option value="И">И</option>
                                        <option value="К">К</option>
                                        <option value="Л">Л</option>
                                        <option value="М">М</option>
                                        <option value="Н">Н</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Grade_letter --}}

            <!-- Модальное окно Fullname-->
            <div class="modal fade" id="myFullname" tabindex="-1" role="dialog" aria-labelledby="myModalLabelFullname" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelFullname">ФИО {{ $user_org->fullname }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_fullname_pupil', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <textarea class="form-control" name="fullname" id="fullname" rows="3" required></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Fullname --}}

            <!-- Модальное окно Tel-->
            <div class="modal fade" id="myTel" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelTel">Телефон {{ $user_org->tel }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_tel_pupil', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <textarea class="form-control" name="tel" id="tel" rows="3" required></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Tel --}}

            <!-- Модальное окно Email-->
            <div class="modal fade" id="myEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEmail" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelEmail">Почта {{ $user_org->email }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_email_pupil', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="email" id="email" class="form-control" required/>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Email --}}

            <!-- Модальное окно Snils-->
            <div class="modal fade" id="mySnils" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSnils" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelSnils">СНИЛС {{ $user_org->snils }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_snils_pupil', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="snils" id="snils" class="form-control" required/>
                                    <small>Формат: 000-000-000 00</small>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Snils --}}

            <!-- Модальное окно Birth-->
            <div class="modal fade" id="myBirth" tabindex="-1" role="dialog" aria-labelledby="myModalLabelBirth" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabelBirth">Дата рождения {{ $user_org->birth }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ action('UserController@update_birth_pupil', $user_org->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="birth" id="birth" class="form-control" required/>
                                    <br>
                                    <small>Например, 01.01.2010</small>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Конец окна Birth --}}

              
            <p><a href="{{ route('home')}}"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться на главную страницу
            </a></p>
                

        </div>
    </div>
</div>

<script type="text/javascript">
    $("#district").change(function(){
        var district = $(this).val();

        var token = $("input[name='_token']").val();
        console.log(district);
            $.ajax({
                url: "{{ route('selectorg2') }}",
                method: 'POST',
                data: {
                    district:district,
                    _token:token
                    },
                success: function(data) {
                    $("#org").html('');
                    $("#org").html(data.options);
                }
            });
    });

</script>
@endif
@endsection