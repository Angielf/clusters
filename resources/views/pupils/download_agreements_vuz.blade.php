<!-- resources/views/pupils/download_agreements.blade.php -->
@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))
<div class="container-fluid">
    <h1>Скачать образцы договоров</h1>

    @if($user->org_type == 'prof')
        <a href="{{ route('agreements.vuz.prof.download.zip', ['user' => $user->id, 'main_or_not' => $main_or_not, 'org_type' => 'prof']) }}"
            class="btn btn-outline-primary mb-3">
            Скачать все образцы договоров в ZIP
        </a>
    @elseif($user->org_type == 'vuz')
        <a href="{{ route('agreements.vuz.prof.download.zip', ['user' => $user->id, 'main_or_not' => $main_or_not, 'org_type' => 'vuz']) }}"
            class="btn btn-outline-primary mb-3">
            Скачать все образцы договоров в ZIP
        </a>
    @endif

    <table class="table table-stripe border-bottom" id="myTable">
        <thead>
            <tr>
                <th scope="col">Программа</th>
                <th scope="col">Район</th>
                <th scope="col">Школа</th>
                <th scope="col">Договор</th>
            </tr>
        </thead>
        <tbody>
        @foreach($uniquePairs as $pair)
            <tr>
                <td>{{ $pair['proposed_program']->subject }}</td>
                <td>{{ $pair['selected_program']->user->getDistrict->fullname }}</td>
                <td>{{ $pair['selected_program']->user->getSchool->fullname }}</td>
                <td>
                    @if($user->org_type == 'prof')
                        <a href="/agreement-document-selected/create-document4/{{ $pair['selected_program']->user->getSchool->id }}/{{ $pair['selected_program']->id }}"
                            class="btn btn-outline-primary">
                            Скачать образец договора
                        </a>
                    @elseif($user->org_type == 'vuz')
                        <a href="/agreement-document-selected/create-document-vsu/{{ $pair['selected_program']->user->getSchool->id }}/{{ $pair['selected_program']->id }}"
                            class="btn btn-outline-primary">
                            Скачать образец договора
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
@endsection
