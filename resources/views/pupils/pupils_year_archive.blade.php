@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10 || Auth::user()->id == $user->id))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row justify-content-center">
                <div style="width:400px;max-width:100%;">
                    <ul class="list-group">
                        @for($i = date('Y'); $i >= 2022; $i--)
                            <li class="list-group-item">
                                @if($user->org_type != 'prof')
                                <a class="btn btn-outline-dark" href="/pupils/{{$user->id}}/list-of-pupils-archive/{{ $i }}-{{ $i+1 }}" target="_blank">
                                    {{ $i }}-{{ $i+1 }}
                                </a>
                                @else
                                <a class="btn btn-outline-dark" href="/pupils/{{$user->id}}/vuz/list-of-programs/{{ $i }}-{{ $i+1 }}" target="_blank">
                                    {{ $i }}-{{ $i+1 }}
                                </a>
                                @endif
                            </li>
                        @endfor
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
@endif
@endsection