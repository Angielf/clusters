<table class="table">
    <thead>
        <tr>
            <th scope="col">Муниципалитет</th>
            <th scope="col">ФИО ученика</th>
            @if(Auth::user()->status == 11)
                <th scope="col">СНИЛС ученика</th>
                <th scope="col">Дата рождения ученика</th>
                <th scope="col">Телефон ученика</th>
            @endif
            <th scope="col">Школа ученика</th>
            <th scope="col">ИНН школы ученика</th>
            <th scope="col">Класс ученика</th>

            <th scope="col">Муниципалитет базовой организации</th>
            <th scope="col">Базовая организация</th>
            <th scope="col">ИНН базовой организации</th>
            <th scope="col">Класс</th>
            <th scope="col">Предмет(курс)</th>
            <th scope="col">Раздел(модуль)</th>
            <th scope="col">Кол-во часов</th>
            <th scope="col">Форма реализации обучения</th>
            <th scope="col">Условия реализации обучения</th>
            <th scope="col">Образовательная программа</th>
            <th scope="col">Образовательная деятельность</th>
            <th scope="col">Комментарий</th>
            <th scope="col">Начало</th>
            <th scope="col">Конец</th>
            <th scope="col">Программа</th>
            <th scope="col">Статус программы</th>
            <th scope="col">Расписание</th>
            <th scope="col">Договор</th>
            <th scope="col">Подпись организации реципиента</th>
            <th scope="col">Подпись базовой организации</th>
            <th scope="col">Договор 2</th>
            <th scope="col">Подпись организации реципиента 2</th>
            <th scope="col">Подпись базовой организации 2</th>
            <th scope="col">Архив</th>
        </tr>
    </thead>

    <tbody id="table4">
        @foreach($selected_programs as $selected_program)
        @if($selected_program->user->status == 7 and $selected_program->proposed_program->user->org_type == 'prof' and $selected_program->not_student != 1)
            <tr>
                <td>{{ $selected_program->user->getDistrict->fullname }}</td>
                <td>{{ $selected_program->user->fullname }}</td>
                @if(Auth::user()->status == 11)
                    <td>{{ $selected_program->user->snils }}</td>
                    <td>{{ $selected_program->user->birth }}</td>
                    <td>{{ $selected_program->user->tel }}</td>
                @endif
                <td>{{ $selected_program->user->getSchool->fullname }}</td>
                <td>{{ $selected_program->user->getSchool->inn }}</td>
                <td>{{ $selected_program->user->grade_number }}  {{ $selected_program->user->grade_letter }}</td>

                <td>{{ $selected_program->proposed_program->user->getDistrict->fullname}}</td>
                <td>{{ $selected_program->proposed_program->user->fullname}}</td>
                <td>{{ $selected_program->proposed_program->user->inn}}</td>

                <td>{{ $selected_program->proposed_program->getClasses() }}</td>
                <td>{{ $selected_program->proposed_program->subject }}</td>
                <td>{{ $selected_program->proposed_program->modul }}</td>
                <td>{{ $selected_program->proposed_program->hours }}</td>

                <td>{{ $selected_program->proposed_program->form_of_education }} </td>
                <td>{{ $selected_program->proposed_program->form_education_implementation }}</td>
                <td>{{ $selected_program->proposed_program->educational_program }}</td>
                <td>{{ $selected_program->proposed_program->educational_activity }}</td>
                <td>{{ $selected_program->proposed_program->content }}</td>

                <td>{{ $selected_program->proposed_program->getDataBegin() }}</td>
                <td>{{ $selected_program->proposed_program->getDataEnd() }}</td>

                <td>{{ $selected_program->proposed_program->filename }}</td>

                @if($selected_program->status == 1)
                    <td>Согласована</td>
                @elseif($selected_program->status == 2)
                    <td>Отклонена</td>
                @else
                    <td>Рассматривается</td>
                @endif

                @if(($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1))
                    <td>{{ $selected_program->selected_schedule->filename }}</td>
                @else
                    <td></td>
                @endif


                @if (($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1)
                    and ($selected_program->selected_schedule->agreement_pupil))

                    <td>{{ $selected_program->selected_schedule->agreement_pupil->filename }}</td>

                    @if($selected_program->selected_schedule->agreement_pupil->sign === 1)
                    <td>{{ $selected_program->selected_schedule->agreement_pupil->filename_sign }}</td>
                    @endif
                    @if($selected_program->selected_schedule->agreement_pupil->sign2 === 1)
                    <td>{{ $selected_program->selected_schedule->agreement_pupil->filename_sign2 }}</td>
                    @endif
                @else
                    <td></td>
                @endif

                @if (($selected_program->selected_schedule) and ($selected_program->selected_schedule->status === 1)
                    and ($selected_program->selected_schedule->agreement_pupil2))

                    <td>{{ $selected_program->selected_schedule->agreement_pupil2->filename }}</td>

                    @if($selected_program->selected_schedule->agreement_pupil2->sign === 1)
                    <td>{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign }}</td>
                    @endif
                    @if($selected_program->selected_schedule->agreement_pupil2->sign2 === 1)
                    <td>{{ $selected_program->selected_schedule->agreement_pupil2->filename_sign2 }}</td>
                    @endif
                @else
                    <td></td>
                @endif

                <td>{{ $selected_program->proposed_program->archive }}</td>
            </tr>
        @endif
        @endforeach


    </tbody>
  </table>