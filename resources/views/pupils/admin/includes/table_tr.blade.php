<tr>
    <td>
        {{ $program->user->fullname }}
    </td>
    <td>
        @if($program->cancel_registration == 1)
            Регистрация завершена
        @endif
    </td>
    <td>{{ $program->getClasses() }}</td>
    <td>{{ $program->subject }}</td>
    <td>{{ $program->modul }}</td>
    <td>{{ $program->hours }}</td>
    <td>{{ $program->form_of_education}}</td>
    <td>{{ $program->form_education_implementation}}</td>
    <td>{{ $program->educational_program }}</td>
    <td>{{ $program->educational_activity }}</td>
    <td>{{ $program->content }}</td>
    <td>{{ $program->getDataBegin() }}</td>
    <td>{{ $program->getDataEnd() }}</td>
    <td>
        <a href="/files/proposed_programs/{{ $program->filename }}"
            class="btn btn-outline-success">
            Программа
        </a>
    </td>
    @if(Auth::user()->status == 27 or Auth::user()->status == 1 or Auth::user()->status == 17)
        <td><a href="/{{ $program->id }}/show-pupils-reg">Ученики</a></td>
    @elseif(Auth::user()->status == 10)
        <td><a href="/{{ $program->id }}/show-pupils-mun">Ученики</a></td>
    @endif
    <td>
        <a href="{{ route('download.all.agreements', [$program->user_id, $year, $program->educational_program, '1',  $program->archive ?? 'not']) }}"
            class="btn btn-outline-primary">
             Скачать
        </a>
    </td>
    <td>
        <a href="{{ route('download.all.agreements', [$program->user_id, $year, $program->educational_program, '2',  $program->archive ?? 'not']) }}"
            class="btn btn-outline-primary">
             Скачать
        </a>
    </td>
</tr>