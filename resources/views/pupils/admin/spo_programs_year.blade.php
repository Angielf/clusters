@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 27 or Auth::user()->status == 1 or Auth::user()->status == 17 or Auth::user()->status == 10))
    <div class="container-fluid">
        <h4>Образовательные программы ({{ $year }})</h4>

        <ul class="list-group">
            <li class="list-group-item">
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          Организация
                      </span>
                    </div>
                    <input type="text" class="form-control"
                    id="org" onkeyup="org()">
                </div>
            </li>
        </ul>

        <ul class="nav nav-tabs">
            <!-- Первая вкладка (активная) -->
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#proposed_programs_main_not_archive">
                    Основые не архив
                </a>
            </li>
            <!-- Вторая вкладка -->
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#proposed_programs_main_archive">
                    Основные архив
                </a>
            </li>
            <!-- Третья вкладка -->
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#proposed_programs_dop_not_archive">
                    Дополнительные не архив
                </a>
            </li>
            <!-- Четвертая вкладка -->
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#proposed_programs_dop_archive">
                    Дополнительные архив
                </a>
            </li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane fade show active" id="proposed_programs_main_not_archive">
                <table class="table table-striped table-bordered" id='table1'>
                    @include('pupils.admin.includes.table_th')
                    <tbody>
                        @foreach ($proposed_programs_main_not_archive as $program)
                            @include('pupils.admin.includes.table_tr', ['program' => $program])
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="proposed_programs_main_archive">
                <table class="table table-striped table-bordered" id='table2'>
                    @include('pupils.admin.includes.table_th')
                    <tbody>
                        @foreach ($proposed_programs_main_archive as $program)
                            @include('pupils.admin.includes.table_tr', ['program' => $program])
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="proposed_programs_dop_not_archive">
                <table class="table table-striped table-bordered" id='table3'>
                    @include('pupils.admin.includes.table_th')
                    <tbody>
                        @foreach ($proposed_programs_dop_not_archive as $program)
                            @include('pupils.admin.includes.table_tr', ['program' => $program])
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="proposed_programs_dop_archive">
                <table class="table table-striped table-bordered" id='table4'>
                    @include('pupils.admin.includes.table_th')
                    <tbody>
                        @foreach ($proposed_programs_dop_archive as $program)
                            @include('pupils.admin.includes.table_tr', ['program' => $program])
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </div>


    <script src="{{ asset('js/search_many_tables.js') }}"></script>
@endif
@endsection