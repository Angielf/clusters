@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class = "row">
            <div class="container d-flex align-items-center justify-content-center">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href='/pupils/{{ $user->id }}/school/programs'>
                            Скачать списки учеников по программам
                        </a>
                    </li>
                    @for ($i = 11; $i >= 1; $i--)
                        <li class="list-group-item">
                            {{$i}} класс
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/a/list-of-pupils">
                                            А
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/b/list-of-pupils">
                                            Б
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/v/list-of-pupils">
                                            В
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/g/list-of-pupils">
                                            Г
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/d/list-of-pupils">
                                            Д
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/e/list-of-pupils">
                                            Е
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/zh/list-of-pupils">
                                            Ж
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/z/list-of-pupils">
                                            З
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/i/list-of-pupils">
                                            И
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/k/list-of-pupils">
                                            К
                                        </a>
                                    </td>
                                    <td>
                                        <a href="/pupils/{{ $user->id }}/{{$i}}/l/list-of-pupils">
                                            Л
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </li>
                    @endfor
                </ul>
                
            </div>
        </div>
    </div>
@endsection