@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->id == $user->id))
<div class="row justify-content-center">
    <div class="card">
        <div class="card-header">
            <ul class="list-group">
                <li class="list-group-item list-group-item-light">
                    {{$user->fullname}}
                </li>
                <li class="list-group-item list-group-item-light">
                    {{$user->getDistrict->fullname}}
                </li>
                <li class="list-group-item list-group-item-light">
                    {{$user->getSchool->fullname}}
                </li>
                <li class="list-group-item list-group-item-light">
                    {{$user->grade_number}} {{$user->grade_letter}} класс
                </li>
                <li class="list-group-item list-group-item-light">
                    <a class="btn btn-outline-dark" href="/pupil/{{ $user->id }}/pupil-inf">Сведения о вас</a>
                    @if($user->snils == NULL)
                        <div class="alert alert-info" role="alert">
                            Заполните СНИЛС.
                        </div>
                    @endif
                    @if($user->birth == NULL)
                        <div class="alert alert-info" role="alert">
                            Заполните дату рождения.
                        </div>
                    @endif
                </li>
            </ul>
        </div>
        <div class="card-body">
            <h5 align="center">Взятые вами образовательные программы</h5>

                <table class="table table-stripe">
                    <thead>
                        <tr>
                            <th scope="col">Организация-участник</th>

                            <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                            <th scope="col">
                                Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                            </th>

                            <th scope="col">Даты</th>

                            <th scope="col">Программа</th>

                            <th scope="col">Расписание</th>
                            <th scope="col">Договор</th>
                        </tr>
                    </thead>

                    <tbody id="taken_programs">
                        @if ($user->selected_programs())
                            @foreach( $user->selected_programs() as $selected_program)
                                <tr>
                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->user->fullname }}</li>
                                            <li class="list-group-item">
                                                <a class="btn btn-outline-dark" href="/users/{{ $selected_program->proposed_program->user->id }}/about-org">Сведения</a>
                                            </li>
                                            <li class="list-group-item">
                                                <a href="/pupil/selected_programs/{{ $selected_program->id }}/show"
                                                    class="btn btn-outline-info btn">
                                                    <i class="far fa-eye"></i> Удаление
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getClasses() }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->subject }} </li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->modul }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->hours }}</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->form_of_education }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->form_education_implementation }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->educational_program }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->educational_activity }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->content }}</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getDataBegin() }}</li>
                                            <li class="list-group-item">{{ $selected_program->proposed_program->getDataEnd() }}</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <a href="/files/proposed_programs/{{ $selected_program->proposed_program->filename }}"
                                                class="btn btn-outline-success">
                                                    Программа
                                                </a>
                                            </li>
                                            <li class="list-group-item">
                                                @if($selected_program->status == 0)
                                                    <div class="alert alert-warning" role="alert">
                                                        Рассмотрение вашей школой
                                                    </div>
                                                @endif
                                                @if($selected_program->status == 2)
                                                    <div class="alert alert-danger" role="alert">
                                                        Программа отклонена вашей школой
                                                    </div>
                                                @endif
                                                @if($selected_program->status == 1)
                                                    <div class="alert alert-success" role="alert">
                                                        Программа согласована
                                                    </div>
                                                @endif
                                            </li>
                                        </ul>
                                    </td>
                                    @if ($selected_program->selected_schedule)
                                        <td>
                                            <p><a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                                class="btn btn-outline-success">
                                                Расписание
                                            </a></p>

                                            @if ($selected_program->selected_schedule->status !== 1)
                                                <p><a href="selected_schedule/add/{{ $selected_program->selected_schedule->id }}"
                                                    class="btn btn-outline-info">
                                                    <i class="far fa-check-square"></i> Согласовать
                                                </a></p>
                                                <form action="{{ action('SelectedScheduleController@delete',$selected_program->selected_schedule->id) }}"
                                                        method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit"
                                                            class="btn btn-outline-danger">
                                                            <i class="far fa-trash-alt"></i> Отклонить
                                                    </button>
                                                </form>
                                            @endif
                                        </td>

                                        @if($selected_program->selected_schedule->status == 1)
                                            @if ($selected_program->selected_schedule->agreement_pupil)
                                                <td>
                                                    <a href="/files/agreements_pupils/{{ $selected_program->selected_schedule->agreement_pupil->filename }}"
                                                        class="btn btn-outline-success">
                                                        Договор
                                                    </a>
                                                    @if ($selected_program->selected_schedule->agreement_pupil2)
                                                        <a href="/files/agreements_pupils2/{{ $selected_program->selected_schedule->agreement_pupil2->filename }}"
                                                            class="btn btn-outline-success">
                                                            Договор 2
                                                        </a>
                                                    @endif
                                                </td>
                                            @endif
                                        @endif

                                    @endif
                                </tr>
                            @endforeach
                        @endif
                </tbody>
            </table>

            <div class="card-footer">
                <div class="accordion" id="accordionExample2">

                    <div class="card">
                        <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapseProposed" aria-expanded="false"
                            aria-controls="collapseProposed">
                                Образовательные программы вузов (основные)
                            </a>
                        </h5>
                        </div>

                        <div id="collapseProposed" class="collapse"
                            aria-labelledby="collapseProposed" data-parent="#accordionExample2">
                            <div class="card-body">

                                <div class="accordion" id="accordionPoisk">

                                    <div class="card">
                                        <div class="card-header" id="headingPoisk">
                                            <h5 class="mb-0">
                                                <a data-toggle="collapse" href="#collapsePoisk" aria-expanded="false"
                                                aria-controls="collapsePoisk">
                                                    Поиск
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="collapsePoisk" class="collapse"
                                            aria-labelledby="collapsePoisk" data-parent="#accordionPoisk">
                                            <div class="card-body">
                                                <ul class="list-group">

                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Организация
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="rez" onkeyup="rez()">
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Класс
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="classs" onkeyup="classs()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                Предмет(курс)
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="subject" onkeyup="subject()">
                                                        </div>

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Раздел(модуль)
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="modul" onkeyup="modul()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Кол-во часов
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="hour" onkeyup="hour()">
                                                        </div>
                                                    </li>

                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Форма обучения
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="forma" onkeyup="forma()" list="Forma">
                                                            <datalist id="Forma">
                                                                <option value="очная">очная</option>
                                                                <option value="очно-заочная">очно-заочная</option>
                                                                <option value="заочная">заочная</option>
                                                            </datalist>

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Условия реализации обучения
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="imp" onkeyup="imp()" list="Imp">
                                                            <datalist id="Imp">
                                                                <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                                    с использованием дистанционных образовательных технологий, электронного обучения
                                                                </option>
                                                                <option value="трансфер детей до организации">трансфер детей до организации</option>
                                                                <option value="трансфер + дистанционные технологии">трансфер + дистанционные технологии</option>
                                                            </datalist>
                                                        </div>

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Образовательная программа
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="pro" onkeyup="pro()" list="Pro">
                                                            <datalist id="Pro">
                                                                <option value="основная">основная</option>
                                                                <option value="дополнительная">дополнительная</option>
                                                            </datalist>

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Образовательная деятельность
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="act" onkeyup="act()" list="Act">
                                                            <datalist id="Act">
                                                                <option value="урочная">урочная</option>
                                                                <option value="внеурочная">внеурочная</option>
                                                            </datalist>
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Начало
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="begin" onkeyup="begin()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                Конец
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="end" onkeyup="end()">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <table class="table table-stripe table-responsive" id="collapseExample3">
                                    <thead>
                                        <tr>
                                            <th scope="col">Организация</th>

                                            <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                            <th scope="col">
                                                Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                            </th>

                                            <th scope="col">Даты</th>

                                            <th scope="col">Программа</th>

                                            <th scope="col">Взять программу</th>
                                        </tr>
                                    </thead>

                                    <tbody id="other_proposed">
                                        @foreach($proposed_programs_all as $proposed_program)
                                            @if(
                                                ($proposed_program->user_id != $user->id) 
                                                and ($proposed_program->user->status == 100)
                                                and ($proposed_program->archive == NULL)
                                                and (strpos($proposed_program->getClasses(), $user->grade_number) !== False)
                                                // без ВГУ
                                                // and ($proposed_program->user->id != 1857)
                                                and ($proposed_program->cancel_registration == NULL)
                                                and $proposed_program->educational_program == 'основная'
                                            )
                                            <tr>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            {{ $proposed_program->user->fullname }}
                                                        </li>
                                                        <li class="list-group-item">
                                                            <a class="btn btn-outline-dark" href="/users/{{ $proposed_program->user->id }}/about-org">Сведения</a>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->subject }} </li>
                                                        <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->content }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->getDataBegin() }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                        class="btn btn-outline-success">
                                                        Программа
                                                    </a>
                                                </td>
                                                <td>
                                                    <form action="{{ action('SelectedProgramController@take', $proposed_program->id) }}" method="POST">
                                                        @csrf
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-success">Взять программу</button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="accordion" id="accordionExample21">

                    <div class="card">
                        <div class="card-header" id="headingThreeDop">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapseProposedDop" aria-expanded="false"
                            aria-controls="collapseProposedDop">
                                Образовательные программы вузов (дополнительные)
                            </a>
                        </h5>
                        </div>

                        <div id="collapseProposedDop" class="collapse"
                            aria-labelledby="collapseProposedDop" data-parent="#accordionExample21">
                            <div class="card-body">

                                <div class="accordion" id="accordionPoiskDop">

                                    <div class="card">
                                        <div class="card-header" id="headingPoiskDop">
                                            <h5 class="mb-0">
                                                <a data-toggle="collapse" href="#collapsePoiskDop" aria-expanded="false"
                                                aria-controls="collapsePoiskDop">
                                                    Поиск
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="collapsePoiskDop" class="collapse"
                                            aria-labelledby="collapsePoiskDop" data-parent="#accordionPoiskDop">
                                            <div class="card-body">
                                                <ul class="list-group">

                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Организация
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="rezDop" onkeyup="rezDop()">
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Класс
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="classsDop" onkeyup="classsDop()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                Предмет(курс)
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="subjectDop" onkeyup="subjectDop()">
                                                        </div>

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Раздел(модуль)
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="modulDop" onkeyup="modulDop()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Кол-во часов
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="hourDop" onkeyup="hourDop()">
                                                        </div>
                                                    </li>

                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Форма обучения
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="formaDop" onkeyup="formaDop()" list="FormaDop">
                                                            <datalist id="FormaDop">
                                                                <option value="очная">очная</option>
                                                                <option value="очно-заочная">очно-заочная</option>
                                                                <option value="заочная">заочная</option>
                                                            </datalist>

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Условия реализации обучения
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="impDop" onkeyup="impDop()" list="ImpDop">
                                                            <datalist id="ImpDop">
                                                                <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                                    с использованием дистанционных образовательных технологий, электронного обучения
                                                                </option>
                                                                <option value="трансфер детей до организации">трансфер детей до организации</option>
                                                                <option value="трансфер + дистанционные технологии">трансфер + дистанционные технологии</option>
                                                            </datalist>
                                                        </div>

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Образовательная программа
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="proDop" onkeyup="proDop()" list="ProDop">
                                                            <datalist id="ProDop">
                                                                <option value="основная">основная</option>
                                                                <option value="дополнительная">дополнительная</option>
                                                            </datalist>

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Образовательная деятельность
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="actDop" onkeyup="actDop()" list="ActDop">
                                                            <datalist id="ActDop">
                                                                <option value="урочная">урочная</option>
                                                                <option value="внеурочная">внеурочная</option>
                                                            </datalist>
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Начало
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="beginDop" onkeyup="beginDop()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                Конец
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="endDop" onkeyup="endDop()">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <table class="table table-stripe table-responsive" id="collapseExample3Dop">
                                    <thead>
                                        <tr>
                                            <th scope="col">Организация</th>

                                            <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                            <th scope="col">
                                                Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                            </th>

                                            <th scope="col">Даты</th>

                                            <th scope="col">Программа</th>

                                            <th scope="col">Взять программу</th>
                                        </tr>
                                    </thead>

                                    <tbody id="other_proposedDop">
                                        @foreach($proposed_programs_all as $proposed_program)
                                            @if(
                                                ($proposed_program->user_id != $user->id) 
                                                and ($proposed_program->user->status == 100)
                                                and ($proposed_program->archive == NULL)
                                                and (strpos($proposed_program->getClasses(), $user->grade_number) !== False)
                                                and ($proposed_program->cancel_registration == NULL)
                                                and $proposed_program->educational_program == 'дополнительная'
                                            )
                                            <tr>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            {{ $proposed_program->user->fullname }}
                                                        </li>
                                                        <li class="list-group-item">
                                                            <a class="btn btn-outline-dark" href="/users/{{ $proposed_program->user->id }}/about-org">Сведения</a>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->subject }} </li>
                                                        <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->content }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->getDataBegin() }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                        class="btn btn-outline-success">
                                                        Программа
                                                    </a>
                                                </td>
                                                <td>
                                                    <form action="{{ action('SelectedProgramController@take', $proposed_program->id) }}" method="POST">
                                                        @csrf
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-success">Взять программу</button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="accordion" id="accordionProf">

                    <div class="card">
                        <div class="card-header" id="headingProf">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapseProf" aria-expanded="false"
                            aria-controls="collapseProf">
                                Профориентационный минимум (Образовательные программы СПО) (основные)
                            </a>
                        </h5>
                        </div>

                        <div id="collapseProf" class="collapse"
                            aria-labelledby="collapseProf" data-parent="#accordionProf">
                            <div class="card-body">

                                <div class="accordion" id="accordionPoisk2">

                                    <div class="card">
                                        <div class="card-header" id="headingPoisk2">
                                            <h5 class="mb-0">
                                                <a data-toggle="collapse" href="#collapsePoisk2" aria-expanded="false"
                                                aria-controls="collapsePoisk2">
                                                    Поиск
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="collapsePoisk2" class="collapse"
                                            aria-labelledby="collapsePoisk2" data-parent="#accordionPoisk2">
                                            <div class="card-body">
                                                <ul class="list-group">

                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Организация
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="rez2" onkeyup="rez2()">
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Класс
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="classs2" onkeyup="classs2()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                Предмет(курс)
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="subject2" onkeyup="subject2()">
                                                        </div>

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Раздел(модуль)
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="modul2" onkeyup="modul2()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Кол-во часов
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="hour2" onkeyup="hour2()">
                                                        </div>
                                                    </li>

                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Форма обучения
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="forma2" onkeyup="forma2()" list="Forma2">
                                                            <datalist id="Forma2">
                                                                <option value="очная">очная</option>
                                                                <option value="очно-заочная">очно-заочная</option>
                                                                <option value="заочная">заочная</option>
                                                            </datalist>

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Условия реализации обучения
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="imp2" onkeyup="imp2()" list="Imp2">
                                                            <datalist id="Imp2">
                                                                <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                                    с использованием дистанционных образовательных технологий, электронного обучения
                                                                </option>
                                                                <option value="трансфер детей до организации">трансфер детей до организации</option>
                                                                <option value="трансфер + дистанционные технологии">трансфер + дистанционные технологии</option>
                                                            </datalist>
                                                        </div>

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Образовательная программа
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="pro2" onkeyup="pro2()" list="Pro2">
                                                            <datalist id="Pro2">
                                                                <option value="основная">основная</option>
                                                                <option value="дополнительная">дополнительная</option>
                                                            </datalist>

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Образовательная деятельность
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="act2" onkeyup="act2()" list="Act2">
                                                            <datalist id="Act2">
                                                                <option value="урочная">урочная</option>
                                                                <option value="внеурочная">внеурочная</option>
                                                            </datalist>
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Начало
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="begin2" onkeyup="begin2()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                Конец
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="end2" onkeyup="end2()">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <table class="table table-stripe table-responsive" id="collapseProf">
                                    <thead>
                                        <tr>
                                            <th scope="col">Организация</th>

                                            <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                            <th scope="col">
                                                Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                            </th>

                                            <th scope="col">Даты</th>

                                            <th scope="col">Программа</th>

                                            <th scope="col">Взять программу</th>
                                        </tr>
                                    </thead>

                                    <tbody id="other_proposed">
                                        @foreach($proposed_programs_all as $proposed_program)
                                            @if(
                                                ($proposed_program->user_id != $user->id)
                                                and ($proposed_program->user->zone != NULL)
                                                and ($proposed_program->user->org_type == 'prof')
                                                and ($user->getSchool->zone != NULL)
                                                and ($proposed_program->user->zone == $user->getSchool->zone)
                                                and ($proposed_program->archive == NULL)
                                                and (strpos($proposed_program->getClasses(), $user->grade_number) !== False)
                                                and ($proposed_program->cancel_registration == NULL)
                                                and $proposed_program->educational_program == 'основная'
                                            )
                                            <tr>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            {{ $proposed_program->user->fullname }}
                                                        </li>
                                                        <li class="list-group-item">
                                                            <a class="btn btn-outline-dark" href="/users/{{ $proposed_program->user->id }}/about-org">Сведения</a>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->subject }} </li>
                                                        <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->content }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->getDataBegin() }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                        class="btn btn-outline-success">
                                                        Программа
                                                    </a>
                                                </td>
                                                <td>
                                                    <form action="{{ action('SelectedProgramController@take', $proposed_program->id) }}" method="POST">
                                                        @csrf
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-success">Взять программу</button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="accordion" id="accordionProfDop">

                    <div class="card">
                        <div class="card-header" id="headingProfDop">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapseProfDop" aria-expanded="false"
                            aria-controls="collapseProf">
                                Профориентационный минимум (Образовательные программы СПО) (дополнительные)
                            </a>
                        </h5>
                        </div>

                        <div id="collapseProfDop" class="collapse"
                            aria-labelledby="collapseProf" data-parent="#accordionProfDop">
                            <div class="card-body">

                                <div class="accordion" id="accordionPoisk2Dop">

                                    <div class="card">
                                        <div class="card-header" id="headingPoisk2Dop">
                                            <h5 class="mb-0">
                                                <a data-toggle="collapse" href="#collapsePoisk2Dop" aria-expanded="false"
                                                aria-controls="collapsePoisk2Dop">
                                                    Поиск
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="collapsePoisk2Dop" class="collapse"
                                            aria-labelledby="collapsePoisk2Dop" data-parent="#accordionPoisk2Dop">
                                            <div class="card-body">
                                                <ul class="list-group">

                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Организация
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="rez2Dop" onkeyup="rez2Dop()">
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Класс
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="classs2Dop" onkeyup="classs2Dop()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                Предмет(курс)
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="subject2Dop" onkeyup="subject2Dop()">
                                                        </div>

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Раздел(модуль)
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="modul2Dop" onkeyup="modul2Dop()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Кол-во часов
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="hour2Dop" onkeyup="hour2Dop()">
                                                        </div>
                                                    </li>

                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Форма обучения
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="forma2Dop" onkeyup="forma2Dop()" list="Forma2Dop">
                                                            <datalist id="Forma2Dop">
                                                                <option value="очная">очная</option>
                                                                <option value="очно-заочная">очно-заочная</option>
                                                                <option value="заочная">заочная</option>
                                                            </datalist>

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Условия реализации обучения
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="imp2Dop" onkeyup="imp2Dop()" list="Imp2Dop">
                                                            <datalist id="Imp2Dop">
                                                                <option value="с использованием дистанционных образовательных технологий, электронного обучения">
                                                                    с использованием дистанционных образовательных технологий, электронного обучения
                                                                </option>
                                                                <option value="трансфер детей до организации">трансфер детей до организации</option>
                                                                <option value="трансфер + дистанционные технологии">трансфер + дистанционные технологии</option>
                                                            </datalist>
                                                        </div>

                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Образовательная программа
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="pro2Dop" onkeyup="pro2Dop()" list="Pro2Dop">
                                                            <datalist id="Pro2Dop">
                                                                <option value="основная">основная</option>
                                                                <option value="дополнительная">дополнительная</option>
                                                            </datalist>

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    Образовательная деятельность
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="act2Dop" onkeyup="act2Dop()" list="Act2Dop">
                                                            <datalist id="Act2Dop">
                                                                <option value="урочная">урочная</option>
                                                                <option value="внеурочная">внеурочная</option>
                                                            </datalist>
                                                        </div>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                Начало
                                                            </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="begin2Dop" onkeyup="begin2Dop()">

                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                Конец
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control"
                                                            id="end2Dop" onkeyup="end2Dop()">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <table class="table table-stripe table-responsive" id="collapseProfDop">
                                    <thead>
                                        <tr>
                                            <th scope="col">Организация</th>

                                            <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                                            <th scope="col">
                                                Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                                            </th>

                                            <th scope="col">Даты</th>

                                            <th scope="col">Программа</th>

                                            <th scope="col">Взять программу</th>
                                        </tr>
                                    </thead>

                                    <tbody id="other_proposedDop">
                                        @foreach($proposed_programs_all as $proposed_program)
                                            @if(
                                                ($proposed_program->user_id != $user->id)
                                                and ($proposed_program->user->zone != NULL)
                                                and ($proposed_program->user->org_type == 'prof')
                                                and ($user->getSchool->zone != NULL)
                                                and ($proposed_program->archive == NULL)
                                                and (strpos($proposed_program->getClasses(), $user->grade_number) !== False)
                                                and ($proposed_program->cancel_registration == NULL)
                                                and $proposed_program->educational_program == 'дополнительная'
                                            )
                                            <tr>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            {{ $proposed_program->user->fullname }}
                                                        </li>
                                                        <li class="list-group-item">
                                                            <a class="btn btn-outline-dark" href="/users/{{ $proposed_program->user->id }}/about-org">Сведения</a>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->subject }} </li>
                                                        <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->content }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">{{ $proposed_program->getDataBegin() }}</li>
                                                        <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                                    </ul>
                                                </td>
                                                <td>
                                                    <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                        class="btn btn-outline-success">
                                                        Программа
                                                    </a>
                                                </td>
                                                <td>
                                                    <form action="{{ action('SelectedProgramController@take', $proposed_program->id) }}" method="POST">
                                                        @csrf
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-success">Взять программу</button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        </div>
    </div>
@endif

<script src="js/search_pupil.js?version=4"></script>
@endsection