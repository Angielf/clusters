@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 27))
    <div class="container-fluid">
        <h4>Образовательные программы СПО</h4>
        <p>
            <a class="btn btn-outline-dark" href="/pupils/export_programs_prof">Скачать (все)</a>
            <a class="btn btn-outline-dark" href="/pupils/export_programs_not_archive_prof">Скачать (не архив)</a>
        </p>

        <table class="table table-striped table-bordered">
            <tbody>
                <tr>
                    <td>
                        <table>
                            <tr><td colspan="15">Основные</td></tr>
                            <tr>
                                <th scope="col">Название СПО</th>
                                <th scope="col">Регистрация</th>
                                <th scope="col">Класс</th>
                                <th scope="col">Предмет(курс)</th>
                                <th scope="col">Раздел(модуль)</th>
                                <th scope="col">Кол-во часов</th>
                                <th scope="col">Форма реализации обучения</th>
                                <th scope="col">Условия реализации обучения</th>
                                <th scope="col">Образовательная программа</th>
                                <th scope="col">Образовательная деятельность</th>
                                <th scope="col">Комментарий</th>
                                <th scope="col">Начало</th>
                                <th scope="col">Конец</th>
                                <th scope="col">Программа</th>
                                <th scope="col">Ученики</th>
                            </tr>
                            @foreach($proposed_programs as $proposed_program)
                                @if($proposed_program->user->org_type == 'prof' and $proposed_program->archive == NULL and $proposed_program->educational_program == 'основная')
                                    <tr>
                                        <td>{{ $proposed_program->user->fullname }}</td>
                                        <td>
                                            @if($proposed_program->cancel_registration == 1)
                                                Регистрация завершена
                                            @endif
                                        </td>
                                        <td>{{ $proposed_program->getClasses() }}</td>
                                        <td>{{ $proposed_program->subject }}</td>
                                        <td>{{ $proposed_program->modul }}</td>
                                        <td>{{ $proposed_program->hours }}</td>
                                        <td>{{ $proposed_program->form_of_education}}</td>
                                        <td>{{ $proposed_program->form_education_implementation}}</td>
                                        <td>{{ $proposed_program->educational_program }}</td>
                                        <td>{{ $proposed_program->educational_activity }}</td>
                                        <td>{{ $proposed_program->content }}</td>
                                        <td>{{ $proposed_program->getDataBegin() }}</td>
                                        <td>{{ $proposed_program->getDataEnd() }}</td>
                                        <td>
                                            <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                class="btn btn-outline-success">
                                                Программа
                                            </a>
                                        </td>
                                        <td><a href="/{{ $proposed_program->id }}/show-pupils-reg">Ученики</a></td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr><td colspan="15">Дополнительные</td></tr>
                            <tr>
                                <th scope="col">Название СПО</th>
                                <th scope="col">Регистрация</th>
                                <th scope="col">Класс</th>
                                <th scope="col">Предмет(курс)</th>
                                <th scope="col">Раздел(модуль)</th>
                                <th scope="col">Кол-во часов</th>
                                <th scope="col">Форма реализации обучения</th>
                                <th scope="col">Условия реализации обучения</th>
                                <th scope="col">Образовательная программа</th>
                                <th scope="col">Образовательная деятельность</th>
                                <th scope="col">Комментарий</th>
                                <th scope="col">Начало</th>
                                <th scope="col">Конец</th>
                                <th scope="col">Программа</th>
                                <th scope="col">Ученики</th>
                            </tr>
                            @foreach($proposed_programs as $proposed_program)
                                @if($proposed_program->user->org_type == 'prof' and $proposed_program->archive == NULL and $proposed_program->educational_program == 'дополнительная')
                                    <tr>
                                        <td>{{ $proposed_program->user->fullname }}</td>
                                        <td>
                                            @if($proposed_program->cancel_registration == 1)
                                                Регистрация завершена
                                            @endif
                                        </td>
                                        <td>{{ $proposed_program->getClasses() }}</td>
                                        <td>{{ $proposed_program->subject }}</td>
                                        <td>{{ $proposed_program->modul }}</td>
                                        <td>{{ $proposed_program->hours }}</td>
                                        <td>{{ $proposed_program->form_of_education}}</td>
                                        <td>{{ $proposed_program->form_education_implementation}}</td>
                                        <td>{{ $proposed_program->educational_program }}</td>
                                        <td>{{ $proposed_program->educational_activity }}</td>
                                        <td>{{ $proposed_program->content }}</td>
                                        <td>{{ $proposed_program->getDataBegin() }}</td>
                                        <td>{{ $proposed_program->getDataEnd() }}</td>
                                        <td>
                                            <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                                class="btn btn-outline-success">
                                                Программа
                                            </a>
                                        </td>
                                        <td><a href="/{{ $proposed_program->id }}/show-pupils-reg">Ученики</a></td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

        {{-- <p>Архив</p>
        <table class="table table-striped table-bordered">
            <tbody>
                @foreach($proposed_programs as $proposed_program)
                    @if($proposed_program->user->org_type == 'prof' and $proposed_program->archive != NULL)
                        <tr>
                            <td>{{ $proposed_program->user->fullname }}</td>
                            <td>{{ $proposed_program->subject }}</td>
                            <td>{{ $proposed_program->modul }}</td>
                            <td>{{ $proposed_program->getClasses() }}</td>
                            <td>{{ $proposed_program->getDataBegin() }}</td>
                            <td>{{ $proposed_program->getDataEnd() }}</td>
                            <td>{{ $proposed_program->archive }}</td>
                            <td>
                                <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                    class="btn btn-outline-success">
                                    Программа
                                </a>
                            </td>
                            <td><a href="/{{ $proposed_program->id }}/show-pupils-reg">Ученики</a></td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table> --}}
    </div>
@endif
@endsection