@extends('layouts.app')

@section('content')
@if(Auth::user()->status == 17)
    <div class="container-fluid">
        <div class="card-header">
            <h2>{{$user->fullname}}</h2>
        </div>
        <br>
        <h4>Образовательные программы ВУЗов</h4>
        <p>
            <a class="btn btn-outline-dark" href="/pupils/export_programs">Скачать (все)</a>
            <a class="btn btn-outline-dark" href="/pupils/export_programs_not_archive">Скачать (не архив)</a>
        </p>

        <table class="table table-striped table-bordered">
            <tbody>
                @foreach($proposed_programs as $proposed_program)
                    @if($proposed_program->user->status == 100 and $proposed_program->archive == NULL)
                        <tr>
                            <td>{{ $proposed_program->user->fullname }}</td>
                            <td>{{ $proposed_program->subject }}</td>
                            <td>{{ $proposed_program->modul }}</td>
                            <td>{{ $proposed_program->getClasses() }}</td>
                            <td>{{ $proposed_program->getDataBegin() }}</td>
                            <td>{{ $proposed_program->getDataEnd() }}</td>
                            <td>
                                <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                    class="btn btn-outline-success">
                                    Программа
                                </a>
                            </td>
                            <td><a href="/{{ $proposed_program->id }}/show-pupils-reg">Ученики</a></td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>

        <p>Архив</p>
        <table class="table table-striped table-bordered">
            <tbody>
                @foreach($proposed_programs as $proposed_program)
                    @if($proposed_program->user->status == 100 and $proposed_program->archive != NULL)
                        <tr>
                            <td>{{ $proposed_program->user->fullname }}</td>
                            <td>{{ $proposed_program->subject }}</td>
                            <td>{{ $proposed_program->modul }}</td>
                            <td>{{ $proposed_program->getClasses() }}</td>
                            <td>{{ $proposed_program->getDataBegin() }}</td>
                            <td>{{ $proposed_program->getDataEnd() }}</td>
                            <td>{{ $proposed_program->archive }}</td>
                            <td>
                                <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                    class="btn btn-outline-success">
                                    Программа
                                </a>
                            </td>
                            <td><a href="/{{ $proposed_program->id }}/show-pupils-reg">Ученики</a></td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endif
@endsection