<table>
    <thead>
        <tr>
            <td>Муниципалитет</td>
            <td>
                Наименование ОО
                <br>
                (место отправления)
            </td>
            <td>
                Количество
                <br>
                обучающихся
                <br>
                @if($orgtype == 'prof')
                    в 9 классе в ОО
                @elseif($orgtype == 'vuz')
                    в 10 и 11 классах в ОО
                @endif
            </td>
            <td>
                Количество
                <br>
                обучающихся
                <br>
                по профессии
            </td>
            <td>
                Код
                <br>
                Профессия
            </td>
            <td>Муниципалитет</td>
            <td>
                Наименование
                <br>
                организации СПО
                <br>
                (место локации)
            </td>
            <td>
                Адрес
                <br>
                организации СПО
            </td>
        </tr>
    </thead>
    <tbody>
        @foreach($profs as $prof)
            @if($prof->proposed_programs_main_year($year) !== null)
                @foreach($prof->proposed_programs_main_year($year) as $pp)
                    @if($pp->getSelectedSchools() !== null)
                        @foreach($pp->getSelectedSchools() as $school)
                            <tr>
                                <td>{{ $school->getDistrict->fullname }}</td>
                                <td>{{ $school->fullname }}</td>
                                <td>
                                    @if($orgtype == 'prof')
                                        {{ $school->getAmountOfStudentsInSchool9($year) }}
                                    @elseif($orgtype == 'vuz')
                                        {{ $school->getAmountOfStudentsInSchool10_11($year) }}
                                    @endif
                                </td>
                                <td>{{ $school->getAmountOfStudentsInSchoolPerProgram($pp->id, $year) }}</td>
                                <td>{{ $pp->subject }}</td>
                                <td>{{ $prof->getDistrict->fullname }}</td>
                                <td>{{ $prof->fullname }}</td>
                                <td>{{ $prof->address }}</td>
                            </tr>
                        @endforeach
                    @endif
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>