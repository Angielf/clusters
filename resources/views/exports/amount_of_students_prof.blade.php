<table>
    <thead>
        <tr>
            <td>Муниципалитет профессиональной образовательной организации</td>
            <td>Профессиональная образовательная организация</td>
            <td>Муниципалитет базовой организации</td>
            <td>Название базовой организации</td>
            <td>Количество обучающихся</td>
            <td>Перечень программ</td>
        </tr>
    </thead>
    <tbody>
        @foreach($profs as $prof)
            @if($prof->proposed_programs_main_year($year) !== null)
                @foreach($prof->proposed_programs_main_year($year) as $pp)
                    @if($pp->getSelectedSchools() !== null)
                        @foreach($pp->getSelectedSchools() as $school)
                            <tr>
                                <td>{{ $prof->getDistrict->fullname }}</td>
                                <td>
                                    {{ $prof->fullname }}
                                    <br>
                                    {{ $prof->getAmountOfStudentsMainProf($year) }}
                                </td>
                                <td>{{ $school->getDistrict->fullname }}</td>
                                <td>{{ $school->fullname }}</td>
                                <td>{{ $pp->getAmountOfStudentsMainSchool($school->id) }}</td>
                                <td>
                                    {{ $pp->subject }}
                                    <br>
                                    {{ $pp->getAmountOfStudentsMain() }} чел.
                                </td>
                            </tr>
                        @endforeach
                    @endif
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>