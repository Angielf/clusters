@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10 || Auth::user()->id == $user_org->id))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @if(Auth::user()->id == $user_org->id)
                <h5 align="center">Ваши образовательные программы периода {{ $year }}</h5>
            @elseif(Auth::user()->status == 1 or Auth::user()->status == 10)
                <h5 align="center">Образовательные программы периода {{ $year }} {{ $user_org->fullname }}</h5>
            @endif

            <table class="table table-striped" id="myTable2">
                <thead>
                    <tr>
                        <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>
                        <th scope="col">
                            Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                        </th>
                        <th scope="col">Даты</th>
                        <th scope="col">Программа</th>
                    </tr>
                </thead>

                <tbody id="own_proposed">
                    @if ($user_org->proposed_programs_archives($year))
                        @foreach( $user_org->proposed_programs_archives($year) as $proposed_program)
                        <tr>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $proposed_program->getClasses() }}</li>
                                    <li class="list-group-item">{{ $proposed_program->subject }} </li>
                                    <li class="list-group-item">{{ $proposed_program->modul }}</li>
                                    <li class="list-group-item">{{ $proposed_program->hours }}</li>
                                    <li class="list-group-item">
                                        <a href="/proposed_programs/{{ $proposed_program->id }}/update"
                                            class="btn btn-outline-info btn">
                                            <i class="far fa-eye"></i> Редактирование, архивирование, удаление
                                        </a>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $proposed_program->form_of_education }}</li>
                                    <li class="list-group-item">{{ $proposed_program->form_education_implementation }}</li>
                                    <li class="list-group-item">{{ $proposed_program->educational_program }}</li>
                                    <li class="list-group-item">{{ $proposed_program->educational_activity }}</li>
                                    <li class="list-group-item">{{ $proposed_program->content }}</li>
                                </ul>
                            </td>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $proposed_program->getDataBegin() }}</li>
                                    <li class="list-group-item">{{ $proposed_program->getDataEnd() }}</li>
                                </ul>
                            </td>
                            <td>
                                <a href="/files/proposed_programs/{{ $proposed_program->filename }}"
                                    class="btn btn-outline-success">
                                    Программа
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>

            <hr>
            <h5 align="center">Организации, которые взяли эти образовательные программы</h5>

                <table class="table table-stripe border-bottom">
                    <thead>
                        <tr>
                            <th scope="col">Базовая организация</th>
                            <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                            <th scope="col">
                                Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
                            </th>

                            <th scope="col">Даты</th>

                            <th scope="col">Программа</th>
                            <th scope="col">Расписание</th>
                            <th scope="col">Ученики</th>
                            <th scope="col">Договор</th>
                        </tr>
                    </thead>

                    <tbody id="own_proposed">
                        @if ($user_org->proposed_programs_archives($year))
                            @foreach( $user_org->proposed_programs_archives($year) as $proposed_program)
                            @if ($proposed_program->selected_programs())
                            @foreach($proposed_program->selected_programs() as $selected_program)
                            @if($selected_program->user->status != 7)
                            <tr>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $selected_program->user->fullname }}</li>
                                    <li class="list-group-item">
                                        <a class="btn btn-outline-dark" href="/users/{{ $selected_program->user->id }}/about-org">Сведения</a>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $selected_program->proposed_program->getClasses() }}</li>
                                    <li class="list-group-item">{{ $selected_program->proposed_program->subject }} </li>
                                    <li class="list-group-item">{{ $selected_program->proposed_program->modul }}</li>
                                    <li class="list-group-item">{{ $selected_program->proposed_program->hours }}</li>
                                </ul>
                            </td>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $selected_program->proposed_program->form_of_education }}</li>
                                    <li class="list-group-item">{{ $selected_program->proposed_program->form_education_implementation }}</li>
                                    <li class="list-group-item">{{ $selected_program->proposed_program->educational_program }}</li>
                                    <li class="list-group-item">{{ $selected_program->proposed_program->educational_activity }}</li>
                                    <li class="list-group-item">{{ $selected_program->proposed_program->content }}</li>
                                </ul>
                            </td>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $selected_program->proposed_program->getDataBegin() }}</li>
                                    <li class="list-group-item">{{ $selected_program->proposed_program->getDataEnd() }}</li>
                                </ul>
                            </td>
                            <td>
                                <a href="/files/proposed_programs/{{ $selected_program->proposed_program->filename }}"
                                    class="btn btn-outline-success">
                                    Программа
                                </a>
                            </td>
                            @if ($selected_program->selected_schedule)
                                <td>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            @if($selected_program->selected_schedule->status !== 2)
                                                <p>
                                                    {!! $selected_program->selected_schedule->getStatus() !!}
                                                </p>
                                                <a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                                                    class="btn btn-outline-success">
                                                        Расписание
                                                </a>
                                            @endif
                                        </li>
                                        @if($selected_program->selected_schedule->status === 1)
                                            <li class="list-group-item">
                                                @if($selected_program->selected_schedule->selected_months_hour)
                                                    <a href="/selected_months_hours/{{ $selected_program->selected_schedule->selected_months_hour->id }}/update"
                                                        class="btn btn-outline-info btn">
                                                        Кол-во часов по месяцам
                                                    </a>
                                                @endif
                                            </li>
                                        @endif
                                    </ul>
                                </td>
                                @if($selected_program->selected_schedule->status === 1)
                                    <td>
                                        @if ($selected_program->selected_schedule->selected_student)
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    {{ $selected_program->selected_schedule->selected_student->students_amount }}
                                                </li>
                                                <li class="list-group-item">
                                                    <a href="/files/students/{{ $selected_program->selected_schedule->selected_student->filename }}"
                                                        class="btn btn-outline-success">
                                                        Список учеников
                                                    </a>
                                                </li>
                                            </ul>

                                            <td>
                                                @if ($selected_program->selected_schedule->selected_student->selected_agreement)
                                                    <a href="/files/selected_agreements/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                                        class="btn btn-outline-success">
                                                        Договор
                                                    </a>
                                                    <br>
                                                    @if($selected_program->selected_schedule->selected_student->selected_agreement->sign == 1)
                                                    <a href="/files/selected_agreements/signed/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign }}"
                                                        class="btn btn-outline-success">
                                                        Подпись базовой организации
                                                    </a>
                                                    @endif
                                                    <br>
                                                    @if($selected_program->selected_schedule->selected_student->selected_agreement->sign2 == 1)
                                                    <a href="/files/selected_agreements/signed2/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign2 }}"
                                                        class="btn btn-outline-success">
                                                        Подпись организации-участника
                                                    </a>
                                                    @else
                                                    {{-- <a href='/agreement-sign-ordinary/2/selected_agreements/{{ $user_org->id }}/{{ $selected_program->selected_schedule->selected_student->selected_agreement->id }}'
                                                        type="button" id="signButton-{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                                        class="btn btn-outline-primary signButton">
                                                        Ссылка на подпись документа
                                                    </a> --}}
                                                    @endif

                                                @endif
                                            </td>
                                        @endif
                                    </td>
                                @endif
                            @endif
                            </tr>
                            @endif
                            @endforeach
                            @endif
                            @endforeach
                        @endif
                    </tbody>

                </table>

        </div>
    </div>
</div>
@endif
@endsection

