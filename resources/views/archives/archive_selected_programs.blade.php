@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10 || Auth::user()->id == $user_org->id))
<div class="container-fluid">

<h5 align="center">Взятые вами образовательные программы ({{ $year }})</h5>

<table class="table table-stripe">
    <thead>
        <tr>
            <th scope="col">Организация-участник</th>

            <th scope="col">Класс/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

            <th scope="col">
                Форма/условия реализации обучения/ Образовательная программа/деятельность / Комментарий
            </th>

            <th scope="col">Даты</th>

            <th scope="col">Программа</th>

            <th scope="col">Расписание</th>
            <th scope="col">Ученики</th>
            <th scope="col">Договор</th>
        </tr>
    </thead>

    <tbody id="taken_programs">
            @if ($user_org->selected_programs_archive($year))
                @foreach( $user_org->selected_programs_archive($year) as $selected_program)
                <tr>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">{{ $selected_program->proposed_program->user->fullname }}</li>
                        <li class="list-group-item">
                            <a class="btn btn-outline-dark" href="/users/{{ $selected_program->proposed_program->user->id }}/about-org">Сведения</a>
                        </li>
                        <li class="list-group-item">
                            <a href="/selected_programs/{{ $selected_program->id }}/show"
                                class="btn btn-outline-info btn">
                                <i class="far fa-eye"></i> Удаление
                            </a>
                        </li>
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">{{ $selected_program->proposed_program->getClasses() }}</li>
                        <li class="list-group-item">{{ $selected_program->proposed_program->subject }} </li>
                        <li class="list-group-item">{{ $selected_program->proposed_program->modul }}</li>
                        <li class="list-group-item">{{ $selected_program->proposed_program->hours }}</li>
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">{{ $selected_program->proposed_program->form_of_education }}</li>
                        <li class="list-group-item">{{ $selected_program->proposed_program->form_education_implementation }}</li>
                        <li class="list-group-item">{{ $selected_program->proposed_program->educational_program }}</li>
                        <li class="list-group-item">{{ $selected_program->proposed_program->educational_activity }}</li>
                        <li class="list-group-item">{{ $selected_program->proposed_program->content }}</li>
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">{{ $selected_program->proposed_program->getDataBegin() }}</li>
                        <li class="list-group-item">{{ $selected_program->proposed_program->getDataEnd() }}</li>
                    </ul>
                </td>
                <td>
                    <a href="/files/proposed_programs/{{ $selected_program->proposed_program->filename }}"
                        class="btn btn-outline-success">
                        Программа
                    </a>
                </td>
                @if ($selected_program->selected_schedule)
                    <td>
                        <p><a href="/files/selected_schedules/{{ $selected_program->selected_schedule->filename }}"
                            class="btn btn-outline-success">
                            Расписание
                        </a><br></p>

                        @if ($selected_program->selected_schedule->status !== 1)
                            <p><a href="selected_schedule/add/{{ $selected_program->selected_schedule->id }}"
                                class="btn btn-outline-info">
                                <i class="far fa-check-square"></i> Согласовать
                            </a></p>
                            <form action="{{ action('SelectedScheduleController@delete',$selected_program->selected_schedule->id) }}"
                                    method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit"
                                        class="btn btn-outline-danger">
                                        <i class="far fa-trash-alt"></i> Отклонить
                                </button>
                            </form>
                        @endif

                        @if($selected_program->selected_schedule->selected_months_hour)
                            <a href="/selected_months_hours/{{ $selected_program->selected_schedule->selected_months_hour->id }}/update-rez"
                                class="btn btn-outline-info btn">
                                    Кол-во часов по месяцам
                            </a>
                        @endif
                    </td>

                    @if($selected_program->selected_schedule->status === 1)
                        <td>
                            @if ($selected_program->selected_schedule->selected_student)
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <a href="/files/selected_students/{{ $selected_program->selected_schedule->selected_student->filename }}"
                                        class="btn btn-outline-success">
                                            Список учеников
                                        </a>
                                    </li>


                                    <li class="list-group-item">
                                        {{ $selected_program->selected_schedule->selected_student->students_amount }}
                                    </li>
                                </ul>


                                <td>
                                    @if ($selected_program->selected_schedule->selected_student->selected_agreement)
                                        <a href="/files/selected_agreements/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                            class="btn btn-outline-success">
                                            Договор
                                        </a>
                                        <br>
                                        @if($selected_program->selected_schedule->selected_student->selected_agreement->sign != 1)
                                        {{-- <a href='/agreement-sign-ordinary/selected_agreements/{{ $user_org->id }}/{{ $selected_program->selected_schedule->selected_student->selected_agreement->id }}'
                                            type="button" id="signButton-{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename }}"
                                            class="btn btn-outline-primary signButton">
                                            Ссылка на подпись документа
                                        </a> --}}
                                        @else
                                        <a href="/files/selected_agreements/signed/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign }}"
                                            class="btn btn-outline-success">
                                            Ваша подпись
                                        </a>
                                        @endif
                                        @if($selected_program->selected_schedule->selected_student->selected_agreement->sign2 == 1)
                                        <a href="/files/selected_agreements/signed2/{{ $selected_program->selected_schedule->selected_student->selected_agreement->filename_sign2 }}"
                                            class="btn btn-outline-success">
                                            Подпись другой строны
                                        </a>
                                        @endif

                                    @else
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                @if($user_org->date_license != NULL and $user_org->number_license != NULL and $user_org->who_license != NULL and
                                                    $user_org->director_post_rod != NULL and $user_org->director_rod != NULL and
                                                    $user_org->bik != NULL and $user_org->bank != NULL and $user_org->single_treasury_account != NULL and $user_org->treasury_account != NULL)

                                                    @if($selected_program->proposed_program->user->status != 100)
                                                        {{-- <a href="/agreement-document-selected/create-document/{{ $user->id }}/{{$selected_program->id}}"
                                                            class="btn btn-outline-primary">
                                                            Скачать образец договора
                                                        </a> --}}
                                                    @else
                                                        <a href="/agreement-document-selected/show-document/{{ $user_org->id }}/{{$selected_program->id}}"
                                                            class="btn btn-outline-primary">
                                                            Форма для договора
                                                        </a>
                                                    @endif

                                                @elseif($selected_program->proposed_program->user->date_license == NULL or $selected_program->proposed_program->user->number_license == NULL or $selected_program->proposed_program->user->who_license == NULL or
                                                        $selected_program->proposed_program->user->director_post_rod == NULL or $selected_program->proposed_program->user->director_rod == NULL or
                                                        $selected_program->proposed_program->user->bik == NULL or $selected_program->proposed_program->user->bank == NULL or $selected_program->proposed_program->user->single_treasury_account == NULL or $selected_program->proposed_program->user->treasury_account == NULL)
                                                    <div class="alert alert-danger" role="alert">
                                                        Другая организация должна заполнить сведения о себе
                                                    </div>
                                                @else
                                                    <div class="alert alert-danger" role="alert">
                                                        Заполните полностью <a href="/users/{{ $user->id }}/show-org">Сведениях о вашей организации</a>
                                                    </div>
                                                @endif
                                            </li>
                                            <li class="list-group-item"> 
                                                <a href="/selected_agreement/{{ $selected_program->selected_schedule->selected_student->id }}"
                                                    class="btn btn-outline-primary">
                                                    <i class="fas fa-file-upload"></i> Добавить договор
                                                </a>
                                            </li>
                                        </ul>
                                    @endif


                                </td>

                            @else
                                <a href="/selected_student/{{ $selected_program->selected_schedule->id }}"
                                    class="btn btn-outline-primary">
                                    <i class="fas fa-file-upload"></i> Добавить список учеников
                                </a>
                            @endif
                        </td>
                    @endif

                @endif

                </tr>
                @endforeach
            @endif
    </tbody>
</table>

</div>
@endif
@endsection