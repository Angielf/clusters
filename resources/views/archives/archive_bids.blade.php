@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10 || Auth::user()->id == $user_org->id))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @if(Auth::user()->id == $user_org->id)
                <h5 align="center">Ваши заявки на дефицит периода {{ $year }}</h5>
            @elseif(Auth::user()->status == 1 or Auth::user()->status == 10)
                <h5 align="center">Заявки на дефицит периода {{ $year }} {{ $user_org->fullname }}</h5>
            @endif

            <table class="table table-stripe">
                <thead>
                    <tr>
                        <th scope="col">Организация-участник</th>

                        <th scope="col">Класс/ Кол-во классов(групп)/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                        <th scope="col">
                            Форма/условия реализации обучения/ Образовательная программа/деятельность/ Комментарий
                        </th>

                        <th scope="col">
                            Даты
                        </th>

                        <th scope="col">Программа/ Расписание</th>
                        <th scope="col">Кол-во учеников/Список</th>
                        <th scope="col">Договор</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($user_org->bids_archives($year))
                        @foreach( $user_org->bids_archives($year) as $bid)
                            <tr>
                                <td>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            {{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->fullname}}
                                        </li>
                                        <li class="list-group-item">
                                            @if (Auth::user()->status == 1 || Auth::user()->status == 10)
                                            <p><a class="btn btn-outline-dark" href="/users/{{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->id }}/show-org">Информация</a></p>
                                            @elseif(Auth::user()->id == $user_org->id)
                                            <p><a class="btn btn-outline-dark" href="/users/{{ $bid->programs()->sortByDesc('status')->first()->sender()->first()->id }}/about-org">Сведения</a></p>
                                            @endif
                                        </li>
                                        <li class="list-group-item">
                                            <a href="/bids/{{ $bid->id }}/update"
                                                class="btn btn-outline-info btn">
                                                <i class="far fa-eye"></i> Редактировать заявку
                                            </a>
                                        </li>
                                        <li class="list-group-item">
                                            <form action="{{ action('BidController@from_archive', $bid->id) }}" method="POST">
                                                @method('PUT')
                                                @csrf
                                                <button type="submit"
                                                    class="btn btn-outline-info btn">
                                                    <i class="fas fa-box-open"></i> Убрать из архива
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </td>

                                <td>
                                    <ul class="list-group" id="ul1">
                                        <li class="list-group-item">{{ $bid->getClasses() }}</li>
                                        <li class="list-group-item">{{ $bid->amount_of_classes }}</li>
                                        <li class="list-group-item">{{ $bid->subject }}</li>
                                        <li class="list-group-item">{{ $bid->modul }}</li>
                                        <li class="list-group-item">{{ $bid->hours }}</li>
                                    </ul>
                                </td>

                                <td>
                                    <ul class="list-group" id="ul2">
                                        <li class="list-group-item">{{ $bid->form_of_education }} </li>
                                        <li class="list-group-item">{{ $bid->form_education_implementation }}</li>
                                        <li class="list-group-item">{{ $bid->getEducationalPrograms() }}</li>
                                        <li class="list-group-item">{{ $bid->getEducationalActivities() }}</li>
                                        <li class="list-group-item">{{ $bid->content }}</li>
                                    </ul>
                                </td>

                                <td>
                                    <ul class="list-group">
                                        <li class="list-group-item">{{ $bid->getDataBegin() }} </li>
                                        <li class="list-group-item">{{ $bid->getDataEnd() }}</li>
                                    </ul>
                                </td>


                                <td>
                                    <li class="list-group-item">
                                        <a href="/files/programs/{{ $bid->programs()->sortByDesc('status')->first()->filename }}"
                                            class="btn btn-outline-success">
                                            Программа
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        @if(($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1))
                                            <a href="/files/schedules/{{ $bid->programs()->sortByDesc('status')->first()->schedule->filename }}"
                                                class="btn btn-outline-success">
                                                Расписание
                                            </a>
                                        @endif
                                    </li>
                                    <li class="list-group-item">
                                        @if(($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1) and ($bid->programs()->sortByDesc('status')->first()->schedule->months_hour))
                                            <a href="/months_hours/{{ $bid->programs()->sortByDesc('status')->first()->schedule->months_hour->id }}/inf"
                                                class="btn btn-outline-success">
                                                    Кол-во часов по месяцам
                                            </a>
                                        @endif
                                    </li>

                                </td>

                                <td>
                                    @if (($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                    and ($bid->programs()->sortByDesc('status')->first()->schedule->student))
                                        <li class="list-group-item">
                                            {{$bid->programs()->sortByDesc('status')->first()->schedule->student->students_amount}}
                                        </li>
                                        <li class="list-group-item">
                                            <a href="/files/students/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->filename }}"
                                                class="btn btn-outline-success">
                                                Список учеников
                                            </a>
                                        </li>
                                    @endif
                                </td>

                                <td>
                                    @if (($bid->programs()->sortByDesc('status')->first()->schedule) and ($bid->programs()->sortByDesc('status')->first()->schedule->status === 1)
                                    and ($bid->programs()->sortByDesc('status')->first()->schedule->student) and ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement))
                                        <li class="list-group-item">
                                            <a href="/files/agreements/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename }}"
                                                class="btn btn-outline-success">
                                                    Договор
                                            </a>
                                            <br>
                                            @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign == 1)
                                            <a href="/files/agreements/signed/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign }}"
                                                class="btn btn-outline-success">
                                                Подпись базовой организации
                                            </a>
                                            @endif
                                            <br>
                                            @if ($bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->sign2 == 1)
                                                <a href="/files/agreements/signed2/{{ $bid->programs()->sortByDesc('status')->first()->schedule->student->agreement->filename_sign2 }}"
                                                    class="btn btn-outline-success">
                                                    Подпись организации-участника
                                                </a>
                                            @endif
                                        </li>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
@endsection
