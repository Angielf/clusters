@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10 || Auth::user()->id == $user_org->id))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @if(Auth::user()->status == 1)
            <p><a href="{{ route('org_list', ['orgtype' => 'all'])}}"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться к списку организаций Воронежской области
            </a></p>
            @elseif(Auth::user()->status == 10)
            <p><a href="/users/{{ Auth::user()->getDistrict->id }}/all/org-list-mun"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться к списку организаций муниципалитета
            </a></p>
            @elseif(Auth::user()->id == $user_org->id)
            <p><a href="{{ route('home')}}"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться на главную страницу
            </a></p>
            @endif

            <div class="row justify-content-center">
                <div style="width:400px;max-width:100%;">
                    <ul class="list-group">
                        @for($i = date('Y'); $i >= 2020; $i--)
                            <li class="list-group-item">
                                <a class="btn btn-outline-dark" href="/users/{{ $user_org->id }}/{{ $i }}-{{ $i+1 }}/archive-programs" target="_blank">
                                    {{ $i }}-{{ $i+1 }}
                                </a>
                            </li>
                        @endfor
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
@endif
@endsection
