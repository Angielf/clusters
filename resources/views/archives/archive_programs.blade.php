@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10 || Auth::user()->id == $user_org->id))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @if(Auth::user()->id == $user_org->id)
                <h5 align="center">Ваши утверждённые программы периода {{ $year }}</h5>
            @elseif(Auth::user()->status == 1 or Auth::user()->status == 10)
                <h5 align="center">Утверждённые программы периода {{ $year }} {{ $user_org->fullname }}</h5>
            @endif

            <table class="table table-striped" id="myTable2">
                <thead>
                    <tr>
                        <th scope="col">Организация-участник</th>

                        <th scope="col">Класс/ Кол-во классов(групп)/ Предмет(курс)/ Раздел(модуль)/ Кол-во часов</th>

                        <th scope="col">
                            Форма/условия реализации обучения/ Образовательная программа/деятельность/ Комментарий
                        </th>

                        <th scope="col">
                            Даты
                        </th>

                        <th scope="col">Программа/ Расписание</th>
                        <th scope="col">Кол-во учеников/Список</th>
                        <th scope="col">Договор</th>
                    </tr>
                </thead>

                <tbody id="table2">
                    @foreach($user_org->programs_archives($year) as $program)
                        <tr>
                            <td>
                                {{ $program->bid->user->fullname }}
                                @if (Auth::user()->status == 1 || Auth::user()->status == 10)
                                <p><a class="btn btn-outline-dark" href="/users/{{ $program->bid->user->id }}/show-org">Информация</a></p>
                                @elseif(Auth::user()->id == $user_org->id)
                                <p><a class="btn btn-outline-dark" href="/users/{{ $program->bid->user->id }}/about-org">Сведения</a></p>
                                @endif
                            </td>

                            <td>
                                <ul class="list-group" id="ul1">
                                    <li class="list-group-item">{{ $program->bid->getClasses() }}</li>
                                    <li class="list-group-item">{{ $program->bid->amount_of_classes }}</li>
                                    <li class="list-group-item">{{ $program->bid->subject }}</li>
                                    <li class="list-group-item">{{ $program->bid->modul }}</li>
                                    <li class="list-group-item">{{ $program->bid->hours }}</li>
                                </ul>
                            </td>

                            <td>
                                <ul class="list-group" id="ul2">
                                    <li class="list-group-item">{{ $program->bid->form_of_education }} </li>
                                    <li class="list-group-item">{{ $program->bid->form_education_implementation }}</li>
                                    <li class="list-group-item">{{ $program->bid->getEducationalPrograms() }}</li>
                                    <li class="list-group-item">{{ $program->bid->getEducationalActivities() }}</li>
                                    <li class="list-group-item">{{ $program->bid->content }}</li>
                                </ul>
                            </td>

                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $program->bid->getDataBegin() }} </li>
                                    <li class="list-group-item">{{ $program->bid->getDataEnd() }}</li>
                                </ul>
                            </td>


                            <td>
                                <li class="list-group-item">
                                    <a href="/files/programs/{{ $program->filename }}"
                                        class="btn btn-outline-success">
                                        Программа
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    @if(($program->schedule) and ($program->schedule->status === 1))
                                        <a href="/files/schedules/{{ $program->schedule->filename }}"
                                            class="btn btn-outline-success">
                                            Расписание
                                        </a>
                                    @endif
                                </li>
                                <li class="list-group-item">
                                    @if(($program->schedule) and ($program->schedule->status === 1) and ($program->schedule->months_hour))
                                        <a href="/months_hours/{{ $program->schedule->months_hour->id }}/inf"
                                            class="btn btn-outline-success">
                                                Кол-во часов по месяцам
                                        </a>
                                    @endif
                                </li>

                            </td>

                            <td>
                                @if (($program->schedule) and ($program->schedule->status === 1)
                                and ($program->schedule->student))
                                    <li class="list-group-item">
                                        {{$program->schedule->student->students_amount}}
                                    </li>
                                    <li class="list-group-item">
                                        <a href="/files/students/{{ $program->schedule->student->filename }}"
                                            class="btn btn-outline-success">
                                            Список учеников
                                        </a>
                                    </li>
                                @endif
                            </td>

                            <td>
                                @if (($program->schedule) and ($program->schedule->status === 1)
                                and ($program->schedule->student) and ($program->schedule->student->agreement))
                                    <li class="list-group-item">
                                        <a href="/files/agreements/{{ $program->schedule->student->agreement->filename }}"
                                            class="btn btn-outline-success">
                                                Договор
                                        </a>
                                        <br>
                                        @if($program->schedule->student->agreement->sign == 1)
                                        <a href="/files/agreements/signed/{{ $program->schedule->student->agreement->filename_sign }}"
                                            class="btn btn-outline-success">
                                            Подпись базовой организации
                                        </a>
                                        @endif
                                        <br>
                                        @if($program->schedule->student->agreement->sign2 == 1)
                                        <a href="/files/agreements/signed2/{{ $program->schedule->student->agreement->filename_sign2 }}"
                                            class="btn btn-outline-success">
                                            Подпись организации-участника
                                        </a>
                                        @endif
                                    </li>
                                @endif
                            </td>
                        <tr>
                    @endforeach
                </tbody>

        </div>
    </div>
</div>
@endif
@endsection
