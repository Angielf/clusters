@extends('layouts.app')

@section('content')



<div class="container-fluid">
    <div class="card-header">
        <h4>Образовательные программы ВУЗов</h4>
        <p>
            <a class="btn btn-outline-dark" href="/pupils/export_programs">Скачать (все)</a>
            <a class="btn btn-outline-dark" href="/pupils/export_programs_not_archive">Скачать (не архив)</a>
        </p>
        <h4>Образовательные программы СПО</h4>
        <p>
            <a class="btn btn-outline-dark" href="/pupils/export_programs_prof">Скачать (все)</a>
            <a class="btn btn-outline-dark" href="/pupils/export_programs_not_archive_prof">Скачать (не архив)</a>
        </p>
    </div>
</div>





{{-- <p>Загрузить файл в public/instructions</p>
<form method="post" action="/instruction/add" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="instruction">
            <label class="custom-file-label">Загрузить</label>
        </div>
    </div>

    <br>
    <button type="submit" class="btn btn-primary">Добавить</button>
</form>


<p>Загрузить файл в public</p>
<form method="post" action="/public/add2" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="public">
            <label class="custom-file-label">Загрузить</label>
        </div>
    </div>

    <br>
    <button type="submit" class="btn btn-primary">Добавить</button>
</form> --}}



@endsection
