@extends('layouts.app')

@section('content')
@if (Auth::user() && (Auth::user()->status == 1 || Auth::user()->status == 10))
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <p><a href="{{ route('home')}}"
                class="btn btn-outline-info btn">
                <i class="fas fa-arrow-left"></i> Вернуться на главную страницу
            </a></p>

            <div class="row justify-content-center">
                <div style="width:400px;max-width:100%;">
                    <ul class="list-group">
                        @if(Auth::user()->status == 1)
                            @for($i = 2020; $i <= date('Y')-1; $i++)
                                <li class="list-group-item">
                                    <a class="btn btn-outline-dark" href="/archive/{{ $i }}-{{ $i+1 }}" target="_blank">
                                        {{ $i }}-{{ $i+1 }}
                                    </a>
                                </li>
                            @endfor
                        @elseif(Auth::user()->status == 10)
                            @for($i = 2020; $i <= date('Y')-1; $i++)
                                <li class="list-group-item">
                                    <a class="btn btn-outline-dark" href="/mun/archive/{{ $i }}-{{ $i+1 }}" target="_blank">
                                        {{ $i }}-{{ $i+1 }}
                                    </a>
                                </li>
                            @endfor
                        @endif
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
@endif
@endsection
