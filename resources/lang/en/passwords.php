<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    // 'reset' => 'Your password has been reset!',
    'reset' => 'Ваш пароль изменён',
    // 'sent' => 'We have emailed your password reset link!',
    'sent' => 'Мы отправили вам по электронной почте ссылку для сброса пароля',
    // 'throttled' => 'Please wait before retrying.',
    'throttled' => 'Прежде чем повторить попытку, подождите',
    // 'token' => 'This password reset token is invalid.',
    'token' => 'Этот токен сброса пароля недействителен',
    // 'user' => "We can't find a user with that email address.",
    'user' => "Мы не можем найти пользователя с этим адресом электронной почты",

];
